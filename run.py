#!venv/bin/python

# Documentation:
# argparse  https://docs.python.org/fr/3.8/library/argparse.html
# POSIX error status codes  https://docs.python.org/2/library/os.html#process-management

# IMPORTS #####################################################################
from typing import List, Optional, Union
from argparse import ArgumentParser, Namespace, Action

import exrex
from sty import ef, rs
from os.path import join
from os import system
import sys

from src.types.analyse import AnalyseTypes
from src.util.filter import Filter
from src.core.engine import Engine
from src.util.logging import log

try:
	from os import EX_OK, EX_DATAERR, EX_NOINPUT
except ImportError:
	EX_OK = 0
	EX_DATAERR = 65
	EX_NOINPUT = 66


if sys.platform == "win32":
	system('color')

# CONSTANTS ###################################################################
DIR_ROOT: str = sys.path[0]  # racine du projet
DIR_APK: str = "apk"  # répertoire contenant les apk
DIR_REPORT: str = "report"  # répertoire de sortie
DIR_SRC: str = "src"  # package du code source
ANALYSE_TYPES: List[str] = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]


# FUNCTIONS ###################################################################
def get_apk_path(apk_file_name: str) -> str:
	u"""
	Chemin d'accès vers l'APK

	:param apk_file_name: le nom du fichier à récupérer
	:type apk_file_name: str

	:return: Le chemin relatif vers le fichier
	"""
	return join(DIR_APK, apk_file_name)


# CLASSES #####################################################################
class ShowFiltersAction(Action):
	def __init__(self, option_strings, dest, **kwargs):
		kwargs.__setitem__("nargs", 0)
		super(ShowFiltersAction, self).__init__(option_strings, dest, **kwargs)

	def __call__(self, parser, namespace, values, option_string=None):
		print("Les filtres de classe et/ou de méthodes permettent de ne pas évaluer certaines méthodes pour plusieurs raisons.")
		print("Par exemple, ce concentrer uniquement sur le code développé par une personne et pas avoir le code autogénéré par Android.")
		print("Ou pour ne pas évaluer des méthodes dont les instructions ne sont pas prises en charges par le programme.", end="\n\n")
		print(f"{ef.bold}CLASSE{rs.bold_dim}")
		print("nom:")
		for name in Filter.CLASSE_NAMES_TO_EXCLUDE:
			print(f"  - {name}")
		print(f"\n{ef.bold}MÉTHODE{rs.bold_dim}")
		print("nom:")
		#for name in methods_filter.names:
		#	print(f"  - {name}")
		print("tag:")
		for flag in Filter.METHOD_TAGS_TO_EXCLUDE:
			print(f"  - {flag}")
		print("instruction:")
		for regx_instr in Filter.METHOD_INSTRUCTIONS_TO_EXCLUDE:
			print("\n".join(list(map(lambda e: f"  - {e}", exrex.generate(regx_instr)))))
		setattr(namespace, self.dest, values)


# RUNNABLE ####################################################################
if __name__ == '__main__':
	# créer le parseur
	parser = ArgumentParser(description="Vérificateur de bytecode d'apk android.", allow_abbrev=False)
	parser.version = "1.0"
	m = parser.add_argument("-v", "--version", action="version", help="Affiche la version du programme.")
	parser.add_argument("--show-filters", action=ShowFiltersAction, help="Affiche les filtres.")
	subparser = parser.add_subparsers(title="groupes de commandes", dest="subparser")

	# >----------------------------------------------------------------------------------------------------------------<#
	# commandes préformatées
	subparser_samples = subparser.add_parser(
		name="samples",
		help="Ensemble d'exemple dont Le type d'analyse et l'apk sont présélectionnés par le programme.",
		description="Sous-commandes permettant de tester rapidement un cas d'analyse dans une configuration prédéfinis."
	)
	# définir les différentes configurations
	group_config = subparser_samples.add_argument_group(title="configurations")
	group_config_ex = group_config.add_mutually_exclusive_group(required=True)
	group_config_ex.add_argument("-01", action="store_true", help="Analyse des registres avec un apk ne contenant aucune erreur de registre.")
	group_config_ex.add_argument("-02", action="store_true", help="Analyse des registres avec un apk contenant des erreurs de registre.")
	group_config_ex.add_argument("-03", action="store_true", help="Analyse des objets avec un apk ne contenant aucune erreur d'instanciation.")
	group_config_ex.add_argument("-04", action="store_true", help="Analyse des objets avec un apk contenant des erreurs d'instanciation.")
	group_config_ex.add_argument("-05", action="store_true", help="Analyse des permissions.")
	group_config_ex.add_argument("-06", action="store_true", help="Analyse combinée entre les configurations 01 et 03.")
	#group_config_ex.add_argument("-07", action="store_true", help="Analyse combinée entre les configurations 01 et 04.")
	group_config_ex.add_argument("-08", action="store_true", help="Analyse combinée entre les configurations 01 et 05.")
	group_config_ex.add_argument("-09", action="store_true", help="Analyse combinée entre les configurations 02 et 03.")
	group_config_ex.add_argument("-10", action="store_true", help="Analyse combinée entre les configurations 02 et 04.")
	group_config_ex.add_argument("-11", action="store_true", help="Analyse combinée entre les configurations 02 et 05.")
	group_config_ex.add_argument("-12", action="store_true", help="Analyse combinée entre les configurations 03 et 05.")
	group_config_ex.add_argument("-13", action="store_true", help="Analyse combinée entre les configurations 04 et 05.")
	group_config_ex.add_argument("-14", action="store_true", help="Analyse combinée entre les configurations 01, 03 et 05.")
	#group_config_ex.add_argument("-15", action="store_true", help="Analyse combinée entre les configurations 01, 04 et 05.")
	group_config_ex.add_argument("-16", action="store_true", help="Analyse combinée entre les configurations 02, 03 et 05.")
	group_config_ex.add_argument("-17", action="store_true", help="Analyse combinée entre les configurations 02, 04 et 05.")

	# >----------------------------------------------------------------------------------------------------------------<#
	# commandes paramétrables
	subparser_analyse = subparser.add_parser(
		name="analyse",
		help="Il faut fournir un apk à analyser ainsi qu'une combinaison de méthode d'analyse.",
		description="Sous-commandes permettant d'analyser un apk en définissant la configuration d'exécution du programme."

	)
	# éléments obligatoires
	subparser_analyse.add_argument("apk", action="store", type=str, help="L'archive Android au format \".apk\" à analyser.",)
	subparser_analyse.add_argument("types", action="store", type=str, nargs="+", help="Le ou les types d'analyses à exécuter entre \"registre\", \"objet\" et \"permission\".")
	subparser_analyse.add_argument("--classes", action="store", type=str, nargs="+", default=None, help="Le nom des classes à analyser. Si non spécifié, analyse toutes les classes.")
	subparser_analyse.add_argument("--methods", action="store", type=str, nargs="+", default=None, help="Le nom des méthodes à analyses. Si non spécifié, analyse toutes les méthodes des classes.")
	# groupe de commande pour les rapports
	group_log = subparser_analyse.add_argument_group(title="rapports")
	group_log.add_argument("--out", action="store", default=DIR_REPORT, help=f"Spécifie le répertoire des rapports. Utilise par défaut le répertoire \"{DIR_REPORT}\".", dest="report_directory", metavar="PATH")
	# groupe de commande pour les filtres
	group_log = subparser_analyse.add_argument_group(title="filtres")
	group_log.add_argument("--filter", action="store_true", help=f"Filtre les classes en fonction du nom de la classe.", dest="filter_enable")

	# >----------------------------------------------------------------------------------------------------------------<#
	args: Namespace = parser.parse_args()


	# Exemples d'analyses préconfigurés
	if args.__getattribute__("subparser") == "samples":
		# Analyse des registres avec un apk ne contenant aucune erreur de registre
		apk_path: str = ""
		analyse_types: Union[str, List[str]] = []

		list_apk = ["typeSuccess.apk", "typeErreur.apk", "objectsAnalyseSuccess.apk", "objectsAnalyseErrors.apk","eva.apk"]
		if args.__getattribute__("01"):
			apk_path = get_apk_path(list_apk[0])
			analyse_types = AnalyseTypes.REGISTER
		elif args.__getattribute__("02"):
			apk_path = get_apk_path(list_apk[1])
			analyse_types = AnalyseTypes.REGISTER
		elif args.__getattribute__("03"):
			apk_path = get_apk_path(list_apk[2])
			analyse_types = AnalyseTypes.OBJECT
		elif args.__getattribute__("04"):
			apk_path = get_apk_path(list_apk[3])
			analyse_types = AnalyseTypes.OBJECT
		elif args.__getattribute__("05"):
			apk_path = get_apk_path(list_apk[4])
			analyse_types = AnalyseTypes.PERMISSION
		elif args.__getattribute__("06"):
			apk_path = get_apk_path(list_apk[0])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT]
		#elif args.__getattribute__("07"):
		#	apk_path = get_apk_path(list_apk[3])
		#	analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT]
		elif args.__getattribute__("08"):
			apk_path = get_apk_path(list_apk[4])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("09"):
			apk_path = get_apk_path(list_apk[1])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT]
		elif args.__getattribute__("10"):
			apk_path = get_apk_path(list_apk[3])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT]
		elif args.__getattribute__("11"):
			apk_path = get_apk_path(list_apk[1])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("12"):
			apk_path = get_apk_path(list_apk[2])
			analyse_types = [AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("13"):
			apk_path = get_apk_path(list_apk[3])
			analyse_types = [AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("14"):
			apk_path = get_apk_path(list_apk[4])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		#elif args.__getattribute__("15"):
		#	apk_path = get_apk_path("my-application.apk")
		#	analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("16"):
			apk_path = get_apk_path(list_apk[1])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		elif args.__getattribute__("17"):
			apk_path = get_apk_path(list_apk[3])
			analyse_types = [AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION]
		# créer le moteur d'analyse et faire l'analyse
		log.info("Création du moteur d'analyse.")
		engine: Engine = Engine(apk_path, report_directory=DIR_REPORT)
		if len(analyse_types) > 1:
			log.info(f"Démarrage des analyses.")
		else:
			log.info(f"Démarrage de l'analyse.")
		engine.analyse(analyse_types)
		if len(analyse_types) > 1:
			log.info(f"Fin des analyses.")
		else:
			log.info(f"Fin de l'analyse.")
		sys.exit(EX_OK)


	# Analyse paramétrable
	elif args.__getattribute__("subparser") == "analyse":
		apk: str = args.__getattribute__("apk")
		types_input: List[str] = args.__getattribute__("types")
		report_directory: str = args.__getattribute__("report_directory")
		class_names: Optional[List[str]] = args.__getattribute__("classes")
		method_names: Optional[List[str]] = args.__getattribute__("methods")
		filter_enable: bool = args.__getattribute__("filter_enable")

		# création du moteur d'analyse
		engine: Engine = Engine(
			apk_path=apk,
			class_names=class_names,
			method_names=method_names,
			filter_classes=filter_enable,
			report_directory=report_directory
		)
		engine.analyse(types_input)