# Vérificateur de Bytecode

**date:** novembre à janvier  
**principe:** scanner des apk android  
**sujet:** [PDF](sujet.pdf)  
___


## Table des matières
[[_TOC_]]

<br/><br>

## Réalisation
 [Architecture du projet](doc/architecture.md)  
 [Instructions implémentées](doc/instructions.md)

### Liste des fonctionnalitées à réaliser

- [x] Analyse des types des registres 
- [x] Analyse validité du numéro des registres
- [x] Analyse de la bonne initialisation des objets
- [x] Extraction des permissions demandées par l'application (statique et dynamique)
- [x] Extraction des permissions déclarées par l'application
- [ ] Extraction des composants de l’application pouvant répondre à une sollicitation interne ou externe
- [ ] Extraction des demandes de communication réalisée au travers d’intents
- [x] Génération des rapports en cas de succès
- [x] Génération des rapports en cas d'erreurs
- [ ] Extraction des chaînes de caractères
- [x] Héritages des types android (classe AndroidX et Java)
- [ ] Héritages des types android (classe external)



## Installation
### Téléchargement du projet
```sh
$ git clone https://gitlab.com/univ-orleans/m2/san/verificateur-de-bytecode.git
```


<br/><br>

## Environnement virtuel
### Création du venv
Le répertoire `venv` permet d'avoir un environnement python spécifique au projet. Ainsi, il contient toutes les librairies nécessaires au strictement nécessaires à l'application. Il n'est pas versionné et nécessite donc de maintenir une liste des librairies installées (voir [Librairies python](#librairies-python)).
#### Linux
```sh
$ cd verificateur-de-bytecode/
$ python3 -m venv venv
```
#### Windows
```sh
C:> cd verificateur-de-bytecode/
C:> py -m venv venv
```

### Activation / Désactivation
Pour utiliser l'environnement virtuel ou se placer dedans, il faut l'activer. La commande inverse permet de désactiver l'environnement virtuel ou d'en sortir.
#### Linux
```sh
$ source venv/bin/activate
$ # work...
$ deactivate
```
#### Windows
```sh
C:> . venv\Scripts\activate
C:> # work...
C:> . venv\Scripts\deactivate
```


<br/><br>

## Librairies python
### Installer les librairies du projet
À la récupération du projet ou suite à l'ajout d'une librairie par un tiers, il faut mettre à jour le venv en installant les dépendances nouvellement ajoutées.
```sh
$ pip install -r requirements.txt
```

### Sauvegarder la liste des librairies
Pour sauvegarder la liste des librairies installées dans le venv, il faut les lister et les inscrire dans le fichier [requirements.txt](requirements.txt) avec la commande suivante.
```sh
$ pip freeze > requirements.txt
```


<br/><br>

## Exécution du projet
Se placer dans le répertoire racine du projet : `/verificateur-de-bytecode`  

Voici les commandes pour afficher l'aide d'utilisation du programme en passent par les lignes de commandes :
```sh
$ ./run.py --help # doc globale 
$ ./run.py samples --help # doc utilisation des exemples fournis
$ ./run.py analyse --help # doc utilisation de l'analyse d'un apk
```

**Afficher les filtres :**
```sh
$ ./run.py --show-filters
```

**Analyser un exemple fourni :**

```sh
$ ./run.py samples -01 # faire l'analyse des registres sur un apk sans erreur.  
$ ./run.py samples -02 # faire l'analyse des registres sur un apk avec erreurs.  
$ ./run.py samples -03 # faire l'analyse des objets sur un apk sans erreur.  
$ ./run.py samples -04 # faire l'analyse des objets sur un apk avec erreurs.
$ ./run.py samples -05 # faire l'analyse des permissions statiques et dynamiques.  

# voir la commande aide pour voir les autres configurations 
```

Voir la doc ([description erreurs apk](doc/exemple_apk_errors.md)) pour plus d'informations sur les erreurs.

**Analyser un apk :**
```sh
# faire l'analyse des registres sur l'apk typeSuccess.apk 
$ ./run.py analyse apk/typeSuccess.apk registre

# faire l'analyse avcec les trois types d'analyse sur l'apk typeSuccess.apk 
$ ./run.py analyse apk/typeSuccess.apk registre objet permission

# voir la commande aide pour les autres commandes
```

**Analyser une classe / une méthode spécifique ou plusieurs :**
```sh
# spécifier les classes à analyser
$ ./run.py analyse apk/typeSuccess.apk registre --classes com.example.testeadd2.MainActivity

# spécifier les méthodes à analyser
$ ./run.py analyse apk/typeSuccess.apk registre --methods ageMoyen getPersonne
```

<br/><br>

## Prérequis logiciel
| Logiciel | Version | Documentation | Description |
| :-- | --: | :-- | :-- |
| Python | 3.8 | [python.org](https://www.python.org/downloads/release/python-380/) | Langage de programmation utilisé pour exécuter le projet. |
| Androguard | 3.3.5 | [readthedocs.io](https://androguard.readthedocs.io/en/latest/intro/index.html) | Librairie utilisé pour manipuler les apk. |


<br/><br>

## Contributeurs
- Arnaud ORLAY (@arnorlay) : Master 2 IMIS Informatique
- Marion JURÉ (@Marionjure) : Master 2 IMIS Informatique
- Nicolas ZHOU (@Zhou_Nicolas) : Master 2 IMIS Informatique
- My Nina HONG (@ninahg) : Master 2 IMIS Informatique
