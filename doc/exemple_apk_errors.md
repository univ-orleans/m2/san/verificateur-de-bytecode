# APK avec erreurs

## Apk : typeErreur.apk

**Class MainActivity**

*age_moyen*

Cette fonction a une erreur sur l'instruction `return vAA` à la ligne 21. Le type de retour et le registre `vAA` n’ont pas le même type.

*inst_if*

Cette fonction a 9 erreurs, soit sur les instructions `if-test vA, vB, +CCCC` ou les instructions `add-int/lit16 vA, vB, #+CCCC`.  Le type des registres `vA` et `vB` sont différents.

*instr_operation*

Cette fonction a 9 erreurs  sur les instructions `(add | mul | div | sub)-int vAA, vBB, vCC`.  Le type des registres `vAA` et `vBB` sont différents. Pour les instructions `add-int/2addr vA, vB`, il y a  15 erreurs. Les registres `vA` et `vB` sont  None car les instructions  `(add | mul | div | sub)-int vAA, vBB, vCC` sont fausses donc le type du registre `vAA` n’est pas modifié.

*onCreate*

Cette fonction a 2 erreurs. Sur l'instruction `invoke-direct` à la ligne 41, la classe d'appel ne correspond pas. Sur l'instruction  `invoke-virtual` à la ligne 45, les registres et les paramètres ne sont pas du même type. La classe homme et femme héritent de la classe Personne. Mais le type Homme2 n’hérite pas de la classe Personne.

**Class Static**

*<clinit>*

Cette fonction a 2 erreurs sur l'instruction `sput vAA, field@BBBB`. Le registre vAA ne correspond pas au type du `field` . La deuxième erreur est sur l'instruction `sput-boolean vAA, field@BBBB`.  L'instruction ne correspond pas au type du registre `vAA` et du `field`. Elle est boolean au lieu de float (donc sput).

*getPoidsStandard*

Cette fonction a une erreur sur l'instruction `sget-boolean vAA, field@BBBB`.  L'instruction ne correspond pas au type du  `field`. C’est un boolean au lieu de float (donc sget). 


**Class Tableau**

*nbMajeur*

Cette fonction a une erreur sur l'instruction `aget-char vAA, vBB, vCC`.  L'instruction ne correspond pas au type du tableau. Elle est char au lieu de boolean (donc aget-boolean). 

*setMyArrayMajeur*

Cette fonction a 2 erreurs sur l'instruction `iput vA, vB, field@CCCC`.  L'instruction ne correspond pas au type du champ. C’est un int ou float  au lieu object (donc aget-object). 

*remplirTableauMajeur*

Cette fonction a 3  erreurs sur l'instruction `aput vAA, vBB, vCC`.  L'instruction ne correspond pas au type du tableau. C’est un int ou float  au lieu de boolean (donc aget-object). Le registre `vBB` n'est pas un tableau mais un boolean.

*remplirTableauInitialNom*

Cette fonction a 3 erreurs sur l’instruction `aput-char vAA, vBB, vCC` . Le registre `vBB` n’est pas du bon type. C’est un `[Z` au lieu `[C`.  L’index `vCC` n’est pas un int.

**Class Personne**

*arrondirAge*

Cette fonction a une erreur sur l'instruction  `return vAA` .  Le type de retour est boolean au lien float.

*arrondirPoids*

Cette fonction a 2 erreurs sur les instructions `int-to-float vA, vB` et `return vAA`.  Le type est un int au lieu float.

## Apk : ObjectsAnalyseErrors.apk

**Classe MainActivity**

*getPutObject*  

Erreur d'instanciation, le registre `v0` comportant une instance de Personne n'est pas instancié car elle est mise à `null`.
Ainsi, les instructions `iput-object` et `iget-object` nous renvoient une erreur.  

*invoke*

Erreur d'instanciation, le registre `v0` comportant une instance de Personne n'est pas instancié car elle est mise à `null`.
Ainsi, l'instruction `invoke-virtual` nous renvoie une erreur.  

*newArray*  

Erreur d'instanciation, le registre `v0` comportant une instance d'un tableau d'entiers n'est pas instancié car elle est mise à `null`.
Ainsi, l'instruction `invoke-virtual` nous renvoie une erreur.  


*newObject*  

Erreur d'instanciation, le registre `v0` comportant une instance de Personne n'est pas instancié car elle est mise à `null`.
Ainsi, l'instruction `invoke-virtual` nous renvoie une erreur.  
