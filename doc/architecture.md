# Structure du projet

- `apk`: la liste des apks
- `doc`: la documentation du projet
- `report`: les rapports d'analyse des apks
- `res`: les ressources
- `src`: le code source
    - `analyse`: les classes permettant de faire l'analyse
    - `core`:
        - `data`: les objets d'Androguard + nos structures de données
        - `engine`: le moteur d'analyse
        - `instruction`: encapsulation des instructions Androguard
        - `method`: encapsulation des méthodes Androguard
        - `...`
    - `types`: les types d'analyse
    - `util`: les outils de supports pour le programme
