# Les instructions implémentées

Les types implémentés sont : **int**, **float**, **char**, **boolean** et **object**

- `nop`
- `move` - `move/from16` - `move/16 `
- `move-wide` - `move-wide/from16` - `move-wide/16` -` move-wide/16` 
- `move-object/from16` - `move-object/16` 
- `move-result` - `move-result-wide` - `move-result-object`
- `move-exception`
- `return-void` - `return` - `return-wide` - `return-object` 
- `const/4` - `const/16` - `const` - `const/high16` - `const-wide/32` - `const-string` - `const-class`
- `iput-object` - `iput` - `iput-boolean`
- `iget-object` - `iget` - `iget-boolean`
- `if-test`
- `if-testz`
- `binop`
- `binop/2addr`
- `binop/lit8`
- `fill-array-data`
- `instance-of`
- `check-cast`
- `new-instance`
- `invoke-kind`
- `new-array`
- `aput-object` - `aput` - `aput-boolean` - `aput-char`
- `aget-object` - `aget` - `aget-boolean` - `aget-char`
- `array-length`
- `sput-object` - `sput` - `sput-boolean` - `sput-char`
- `sget-object` - `sget` - `sget-boolean` - `sget-char`
- `int-to-float` - `float-to-int` - `int-to-char` - `neg-int` - `neg-float`
- `goto`
- `monitor-enter` - `monitor-exit`
- `throw`
- `fill-array-data-payload`
