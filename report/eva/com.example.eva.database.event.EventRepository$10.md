
com.example.eva.database.event.EventRepository$10
=================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [call](#call)
	* [call](#call)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/event/EventRepository; I)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
EventRepository$10(com.example.eva.database.event.EventRepository p1, int p2)
{
    this.this$0 = p1;
    this.val$daysNumber = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput")
	4("n°2<br/>iput") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/event/EventRepository$10;->this$0 Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository$10;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|iput|v2, v0, Lcom/example/eva/database/event/EventRepository$10;->val$daysNumber I|Lcom/example/eva/database/event/EventRepository$10;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|int|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/event/EventRepository$10;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/event/EventRepository$10;->this$0 Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput|v2, v0, Lcom/example/eva/database/event/EventRepository$10;->val$daysNumber I|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Object;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic Object call()
{
    return this.call();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/eva/database/event/EventRepository$10;->call()Ljava/util/List;|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventRepository$10;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/eva/database/event/EventRepository$10;->call()Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List call()
{
    java.util.GregorianCalendar v0_1 = new java.util.GregorianCalendar();
    v0_1.set(5, (v0_1.get(5) + this.val$daysNumber));
    return com.example.eva.database.event.EventRepository.access$000(this.this$0).getAllEventBeforeLimit(new java.util.Date().getTime(), v0_1.getTimeInMillis());
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>const/4")
	10("n°3<br/>const/4") --> 12("n°4<br/>invoke-virtual")
	12("n°4<br/>invoke-virtual") --> 18("n°5<br/>move-result")
	18("n°5<br/>move-result") --> 20("n°6<br/>iget")
	20("n°6<br/>iget") --> 24("n°7<br/>add-int/2addr")
	24("n°7<br/>add-int/2addr") --> 26("n°8<br/>invoke-virtual")
	26("n°8<br/>invoke-virtual") --> 32("n°9<br/>iget-object")
	32("n°9<br/>iget-object") --> 36("n°10<br/>invoke-static")
	36("n°10<br/>invoke-static") --> 42("n°11<br/>move-result-object")
	42("n°11<br/>move-result-object") --> 44("n°12<br/>new-instance")
	44("n°12<br/>new-instance") --> 48("n°13<br/>invoke-direct")
	48("n°13<br/>invoke-direct") --> 54("n°14<br/>invoke-virtual")
	54("n°14<br/>invoke-virtual") --> 60("n°15<br/>move-result-wide")
	60("n°15<br/>move-result-wide") --> 62("n°16<br/>invoke-virtual")
	62("n°16<br/>invoke-virtual") --> 68("n°17<br/>move-result-wide")
	68("n°17<br/>move-result-wide") --> 70("n°18<br/>invoke-interface")
	70("n°18<br/>invoke-interface") --> 76("n°19<br/>move-result-object")
	76("n°19<br/>move-result-object") --> 78("n°20<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/util/GregorianCalendar;|Ljava/util/GregorianCalendar;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/util/GregorianCalendar;-><init>()V|Ljava/util/GregorianCalendar;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**3**|10|const/4|v1, 5|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-virtual|v0, v1, Ljava/util/Calendar;->get(I)I|Ljava/util/GregorianCalendar;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**5**|18|move-result|v2|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**6**|20|iget|v3, v6, Lcom/example/eva/database/event/EventRepository$10;->val$daysNumber I|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventRepository$10;|<span style='color:#f14848'></span>|
|**7**|24|add-int/2addr|v2, v3|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>int</span>|int|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**8**|26|invoke-virtual|v0, v1, v2, Ljava/util/Calendar;->set(I I)V|Ljava/util/GregorianCalendar;|int|int|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**9**|32|iget-object|v1, v6, Lcom/example/eva/database/event/EventRepository$10;->this$0 Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventRepository$10;|<span style='color:#f14848'></span>|
|**10**|36|invoke-static|v1, Lcom/example/eva/database/event/EventRepository;->access$000(Lcom/example/eva/database/event/EventRepository;)Lcom/example/eva/database/MyDao;|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**11**|42|move-result-object|v1|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Lcom/example/eva/database/MyDao;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**12**|44|new-instance|v2, Ljava/util/Date;|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**13**|48|invoke-direct|v2, Ljava/util/Date;-><init>()V|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**14**|54|invoke-virtual|v2, Ljava/util/Date;->getTime()J|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**15**|60|move-result-wide|v2|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|long|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**16**|62|invoke-virtual|v0, Ljava/util/Calendar;->getTimeInMillis()J|Ljava/util/GregorianCalendar;|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**17**|68|move-result-wide|v4|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>int</span>|long|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**18**|70|invoke-interface|v1, v2, v3, v4, v5, Lcom/example/eva/database/MyDao;->getAllEventBeforeLimit(J J)Ljava/util/List;|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Lcom/example/eva/database/MyDao;|long|int|long|None|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**19**|76|move-result-object|v1|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Ljava/util/List;|<span style='color:grey'>long</span>|<span style='color:grey'>int</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
|**20**|78|return-object|v1|<span style='color:grey'>Ljava/util/GregorianCalendar;</span>|Ljava/util/List;|<span style='color:grey'>long</span>|<span style='color:grey'>int</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$10;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/util/GregorianCalendar;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/util/GregorianCalendar;-><init>()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const/4|v1, 5|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, v1, Ljava/util/Calendar;->get(I)I|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget|v3, v6, Lcom/example/eva/database/event/EventRepository$10;->val$daysNumber I|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**7**|add-int/2addr|v2, v3|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, v1, v2, Ljava/util/Calendar;->set(I I)V|True|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v1, v6, Lcom/example/eva/database/event/EventRepository$10;->this$0 Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**10**|invoke-static|v1, Lcom/example/eva/database/event/EventRepository;->access$000(Lcom/example/eva/database/event/EventRepository;)Lcom/example/eva/database/MyDao;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|new-instance|v2, Ljava/util/Date;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-direct|v2, Ljava/util/Date;-><init>()V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v2, Ljava/util/Date;->getTime()J|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-wide|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v0, Ljava/util/Calendar;->getTimeInMillis()J|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result-wide|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-interface|v1, v2, v3, v4, v5, Lcom/example/eva/database/MyDao;->getAllEventBeforeLimit(J J)Ljava/util/List;|<span style='color:grey'>True</span>|True|True|False|False|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
