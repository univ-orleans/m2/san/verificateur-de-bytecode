
com.example.eva.database.MyDao_Impl$6
=====================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [bind](#bind)
	* [bind](#bind)
	* [createQuery](#createquery)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**bind**](#bind)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**bind**](#bind)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createQuery**](#createquery)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
MyDao_Impl$6(com.example.eva.database.MyDao_Impl p1, androidx.room.RoomDatabase p2)
{
    this.this$0 = p1;
    super(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$6;->this$0 Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/MyDao_Impl$6;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, v2, Landroidx/room/EntityDeletionOrUpdateAdapter;-><init>(Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$6;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$6;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$6;->this$0 Lcom/example/eva/database/MyDao_Impl;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, v2, Landroidx/room/EntityDeletionOrUpdateAdapter;-><init>(Landroidx/room/RoomDatabase;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## bind
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void bind(androidx.sqlite.db.SupportSQLiteStatement p5, com.example.eva.database.association.Association p6)
{
    p5.bindLong(1, ((long) p6.getId()));
    if (p6.getIdGoogleCalendar() != null) {
        p5.bindString(2, p6.getIdGoogleCalendar());
    } else {
        p5.bindNull(2);
    }
    if (p6.getName() != null) {
        p5.bindString(3, p6.getName());
    } else {
        p5.bindNull(3);
    }
    if (p6.getLogo() != null) {
        p5.bindLong(4, ((long) p6.getLogo().intValue()));
    } else {
        p5.bindNull(4);
    }
    Integer v0_11;
    if (p6.getSubscribe() != null) {
        v0_11 = Integer.valueOf(p6.getSubscribe().booleanValue());
    } else {
        v0_11 = 0;
    }
    if (v0_11 != null) {
        p5.bindLong(5, ((long) v0_11.intValue()));
    } else {
        p5.bindNull(5);
    }
    p5.bindLong(6, ((long) p6.getId()));
    return;
}
```
## bind
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteStatement; Ljava/lang/Object;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void bind(androidx.sqlite.db.SupportSQLiteStatement p1, Object p2)
{
    this.bind(p1, ((com.example.eva.database.association.Association) p2));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v2, Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$6;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$6;->bind(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/MyDao_Impl$6;|Landroidx/sqlite/db/SupportSQLiteStatement;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$6;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v2, Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$6;->bind(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/association/Association;)V|True|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## createQuery
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String createQuery()
{
    return "UPDATE OR ABORT `association_table` SET `id` = ?,`idGoogleCalendar` = ?,`name` = ?,`logo` = ?,`subscribe` = ? WHERE `id` = ?";
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'UPDATE OR ABORT `association_table` SET `id` = ?,`idGoogleCalendar` = ?,`name` = ?,`logo` = ?,`subscribe` = ? WHERE `id` = ?'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$6;</span>|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$6;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'UPDATE OR ABORT `association_table` SET `id` = ?,`idGoogleCalendar` = ?,`name` = ?,`logo` = ?,`subscribe` = ? WHERE `id` = ?'|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
