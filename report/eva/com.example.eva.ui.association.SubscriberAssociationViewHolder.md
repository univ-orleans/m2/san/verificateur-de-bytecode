
com.example.eva.ui.association.SubscriberAssociationViewHolder
==============================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [toString](#tostring)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**toString**](#tostring)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Landroid/view/View; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public SubscriberAssociationViewHolder(android.view.View p3, com.example.eva.Environment p4)
{
    super(p3);
    super.subscribeView = ((android.widget.Switch) p3.findViewById(2131230802));
    super.logoView = ((android.widget.ImageView) p3.findViewById(2131230800));
    super.subscribeView.setOnClickListener(new com.example.eva.ui.association.SubscriberAssociationViewHolder$1(super, p4));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>const")
	6("n°2<br/>const") --> 12("n°3<br/>invoke-virtual")
	12("n°3<br/>invoke-virtual") --> 18("n°4<br/>move-result-object")
	18("n°4<br/>move-result-object") --> 20("n°5<br/>check-cast")
	20("n°5<br/>check-cast") --> 24("n°6<br/>iput-object")
	24("n°6<br/>iput-object") --> 28("n°7<br/>const")
	28("n°7<br/>const") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>move-result-object")
	40("n°9<br/>move-result-object") --> 42("n°10<br/>check-cast")
	42("n°10<br/>check-cast") --> 46("n°11<br/>iput-object")
	46("n°11<br/>iput-object") --> 50("n°12<br/>iget-object")
	50("n°12<br/>iget-object") --> 54("n°13<br/>new-instance")
	54("n°13<br/>new-instance") --> 58("n°14<br/>invoke-direct")
	58("n°14<br/>invoke-direct") --> 64("n°15<br/>invoke-virtual")
	64("n°15<br/>invoke-virtual") --> 70("n°16<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v2, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|6|const|v0, 2131230802 # [1.8077667058433606e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|12|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|18|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**5**|20|check-cast|v0, Landroid/widget/Switch;|Landroid/widget/Switch;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**6**|24|iput-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|Landroid/widget/Switch;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**7**|28|const|v0, 2131230800 # [1.8077663001951685e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**9**|40|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**10**|42|check-cast|v0, Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**11**|46|iput-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**12**|50|iget-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|Landroid/widget/Switch;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**13**|54|new-instance|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/widget/Switch;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**14**|58|invoke-direct|v1, v2, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;-><init>(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; Lcom/example/eva/Environment;)V|<span style='color:grey'>Landroid/widget/Switch;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**15**|64|invoke-virtual|v0, v1, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V|Landroid/widget/Switch;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**16**|70|return-void||<span style='color:grey'>Landroid/widget/Switch;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v2, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const|v0, 2131230802 # [1.8077667058433606e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|check-cast|v0, Landroid/widget/Switch;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iput-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const|v0, 2131230800 # [1.8077663001951685e+38]|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|check-cast|v0, Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|iput-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iget-object|v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|new-instance|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-direct|v1, v2, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;-><init>(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; Lcom/example/eva/Environment;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v0, v1, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## toString
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String toString()
{
    String v0_1 = new StringBuilder();
    v0_1.append(super.toString());
    v0_1.append(" SubscriberAssociationViewHolder{ nameView=");
    v0_1.append(this.subscribeView.getText());
    v0_1.append("}");
    return v0_1.toString();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>invoke-super")
	10("n°3<br/>invoke-super") --> 16("n°4<br/>move-result-object")
	16("n°4<br/>move-result-object") --> 18("n°5<br/>invoke-virtual")
	18("n°5<br/>invoke-virtual") --> 24("n°6<br/>const-string")
	24("n°6<br/>const-string") --> 28("n°7<br/>invoke-virtual")
	28("n°7<br/>invoke-virtual") --> 34("n°8<br/>iget-object")
	34("n°8<br/>iget-object") --> 38("n°9<br/>invoke-virtual")
	38("n°9<br/>invoke-virtual") --> 44("n°10<br/>move-result-object")
	44("n°10<br/>move-result-object") --> 46("n°11<br/>invoke-virtual")
	46("n°11<br/>invoke-virtual") --> 52("n°12<br/>const-string")
	52("n°12<br/>const-string") --> 56("n°13<br/>invoke-virtual")
	56("n°13<br/>invoke-virtual") --> 62("n°14<br/>invoke-virtual")
	62("n°14<br/>invoke-virtual") --> 68("n°15<br/>move-result-object")
	68("n°15<br/>move-result-object") --> 70("n°16<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-super|v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->toString()Ljava/lang/String;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:#f14848'></span>|
|**4**|16|move-result-object|v1|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**6**|24|const-string|v1, ' SubscriberAssociationViewHolder{ nameView='|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**7**|28|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**8**|34|iget-object|v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Landroid/widget/Switch;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:#f14848'></span>|
|**9**|38|invoke-virtual|v1, Landroid/widget/Switch;->getText()Ljava/lang/CharSequence;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Landroid/widget/Switch;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**10**|44|move-result-object|v1|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/CharSequence;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**11**|46|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/CharSequence;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**12**|52|const-string|v1, '}'|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**13**|56|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**14**|62|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|Ljava/lang/StringBuilder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**15**|68|move-result-object|v0|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**16**|70|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/lang/StringBuilder;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-super|v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->toString()Ljava/lang/String;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**4**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const-string|v1, ' SubscriberAssociationViewHolder{ nameView='|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|iget-object|v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v1, Landroid/widget/Switch;->getText()Ljava/lang/CharSequence;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|const-string|v1, '}'|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
