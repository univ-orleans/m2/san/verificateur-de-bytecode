
com.example.eva.ui.association.SubscriberAssociationFragment
============================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onCreate](#oncreate)
	* [onCreateView](#oncreateview)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateView**](#oncreateview)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public SubscriberAssociationFragment()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/fragment/app/Fragment;-><init>()V|Lcom/example/eva/ui/association/SubscriberAssociationFragment;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/fragment/app/Fragment;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onCreate(android.os.Bundle p2)
{
    super.onCreate(p2);
    this.activity = ((com.example.eva.Environment) this.requireParentFragment().getActivity());
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>invoke-virtual")
	6("n°2<br/>invoke-virtual") --> 12("n°3<br/>move-result-object")
	12("n°3<br/>move-result-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>check-cast")
	22("n°6<br/>check-cast") --> 26("n°7<br/>iput-object")
	26("n°7<br/>iput-object") --> 30("n°8<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v1, v2, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationFragment;|Landroid/os/Bundle;|<span style='color:#f14848'></span>|
|**2**|6|invoke-virtual|v1, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->requireParentFragment()Landroidx/fragment/app/Fragment;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationFragment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|12|move-result-object|v0|Landroidx/fragment/app/Fragment;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;|Landroidx/fragment/app/Fragment;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v0|Landroidx/fragment/app/FragmentActivity;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|22|check-cast|v0, Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|26|iput-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/SubscriberAssociationFragment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|30|return-void||<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v1, v2, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v1, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->requireParentFragment()Landroidx/fragment/app/Fragment;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|check-cast|v0, Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iput-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->activity Lcom/example/eva/Environment;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateView
  
**Signature :** `(Landroid/view/LayoutInflater; Landroid/view/ViewGroup; Landroid/os/Bundle;)Landroid/view/View;`  
**Nombre de registre :** 9  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public android.view.View onCreateView(android.view.LayoutInflater p6, android.view.ViewGroup p7, android.os.Bundle p8)
{
    android.view.View v0_1 = p6.inflate(2131427390, p7, 0);
    if ((v0_1 instanceof androidx.recyclerview.widget.RecyclerView)) {
        androidx.recyclerview.widget.RecyclerView v2_1 = ((androidx.recyclerview.widget.RecyclerView) v0_1);
        v2_1.setLayoutManager(new androidx.recyclerview.widget.LinearLayoutManager(v0_1.getContext()));
        v2_1.setAdapter(new com.example.eva.ui.association.SubscriberAssociationRecyclerViewAdapter(this.activity));
    }
    return v0_1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const") --> 6("n°2<br/>const/4")
	6("n°2<br/>const/4") --> 8("n°3<br/>invoke-virtual")
	8("n°3<br/>invoke-virtual") --> 14("n°4<br/>move-result-object")
	14("n°4<br/>move-result-object") --> 16("n°5<br/>instance-of")
	16("n°5<br/>instance-of") --> 20("n°6<br/>if-eqz")
	20("n°6<br/>if-eqz") --> 24("n°7<br/>invoke-virtual")
	20("n°6<br/>if-eqz") -.-> 74("n°18<br/>return-object")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>move-object")
	32("n°9<br/>move-object") --> 34("n°10<br/>check-cast")
	34("n°10<br/>check-cast") --> 38("n°11<br/>new-instance")
	38("n°11<br/>new-instance") --> 42("n°12<br/>invoke-direct")
	42("n°12<br/>invoke-direct") --> 48("n°13<br/>invoke-virtual")
	48("n°13<br/>invoke-virtual") --> 54("n°14<br/>new-instance")
	54("n°14<br/>new-instance") --> 58("n°15<br/>iget-object")
	58("n°15<br/>iget-object") --> 62("n°16<br/>invoke-direct")
	62("n°16<br/>invoke-direct") --> 68("n°17<br/>invoke-virtual")
	68("n°17<br/>invoke-virtual") --> 74("n°18<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const|v0, 2131427390 # [1.8476394892349873e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**2**|6|const/4|v1, 0|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-virtual|v6, v0, v7, v1, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|int|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|Landroid/view/LayoutInflater;|Landroid/view/ViewGroup;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|16|instance-of|v1, v0, Landroidx/recyclerview/widget/RecyclerView;|Landroid/view/View;|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|20|if-eqz|v1, +1b|<span style='color:grey'>Landroid/view/View;</span>|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, Landroid/view/View;->getContext()Landroid/content/Context;|Landroid/view/View;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v1|<span style='color:grey'>Landroid/view/View;</span>|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**9**|32|move-object|v2, v0|Landroid/view/View;|<span style='color:grey'>Landroid/content/Context;</span>|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**10**|34|check-cast|v2, Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**11**|38|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**12**|42|invoke-direct|v3, v1, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>Landroid/view/View;</span>|Landroid/content/Context;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**13**|48|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|Landroidx/recyclerview/widget/RecyclerView;|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**14**|54|new-instance|v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**15**|58|iget-object|v4, v5, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/SubscriberAssociationFragment;|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**16**|62|invoke-direct|v3, v4, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;-><init>(Lcom/example/eva/Environment;)V|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**17**|68|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Landroid/content/Context;</span>|Landroidx/recyclerview/widget/RecyclerView;|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**18**|74|return-object|v0|Landroid/view/View;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationFragment;</span>|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const|v0, 2131427390 # [1.8476394892349873e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const/4|v1, 0|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v6, v0, v7, v1, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|False|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|instance-of|v1, v0, Landroidx/recyclerview/widget/RecyclerView;|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-eqz|v1, +1b|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, Landroid/view/View;->getContext()Landroid/content/Context;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-object|v2, v0|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|check-cast|v2, Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-direct|v3, v1, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|new-instance|v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|iget-object|v4, v5, Lcom/example/eva/ui/association/SubscriberAssociationFragment;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-direct|v3, v4, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;-><init>(Lcom/example/eva/Environment;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
