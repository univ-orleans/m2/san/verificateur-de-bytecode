
com.example.eva.ui.event.EventViewHolder$1
==========================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onClick](#onclick)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onClick**](#onclick)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/event/EventViewHolder; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
EventViewHolder$1(com.example.eva.ui.event.EventViewHolder p1, com.example.eva.Environment p2)
{
    this.this$0 = p1;
    this.val$activity = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/event/EventViewHolder$1;->this$0 Lcom/example/eva/ui/event/EventViewHolder;|Lcom/example/eva/ui/event/EventViewHolder$1;|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/event/EventViewHolder$1;->this$0 Lcom/example/eva/ui/event/EventViewHolder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onClick
  
**Signature :** `(Landroid/view/View;)V`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onClick(android.view.View p5)
{
    android.content.Intent v0_1 = new android.content.Intent(this.val$activity, com.example.eva.EventActivity);
    v0_1.putExtra("idEvent", this.this$0.event.getId());
    this.val$activity.startActivityForResult(v0_1, 0);
    this.val$activity.overridePendingTransition(2130772011, 2130772012);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>iget-object")
	4("n°2<br/>iget-object") --> 8("n°3<br/>const-class")
	8("n°3<br/>const-class") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>iget-object")
	18("n°5<br/>iget-object") --> 22("n°6<br/>iget-object")
	22("n°6<br/>iget-object") --> 26("n°7<br/>invoke-virtual")
	26("n°7<br/>invoke-virtual") --> 32("n°8<br/>move-result")
	32("n°8<br/>move-result") --> 34("n°9<br/>const-string")
	34("n°9<br/>const-string") --> 38("n°10<br/>invoke-virtual")
	38("n°10<br/>invoke-virtual") --> 44("n°11<br/>iget-object")
	44("n°11<br/>iget-object") --> 48("n°12<br/>const/4")
	48("n°12<br/>const/4") --> 50("n°13<br/>invoke-virtual")
	50("n°13<br/>invoke-virtual") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>const")
	60("n°15<br/>const") --> 66("n°16<br/>const")
	66("n°16<br/>const") --> 72("n°17<br/>invoke-virtual")
	72("n°17<br/>invoke-virtual") --> 78("n°18<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Landroid/content/Intent;|Landroid/content/Intent;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**2**|4|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**3**|8|const-class|v2, Lcom/example/eva/EventActivity;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/EventActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, v1, v2, Landroid/content/Intent;-><init>(Landroid/content/Context; Ljava/lang/Class;)V|Landroid/content/Intent;|Lcom/example/eva/Environment;|Lcom/example/eva/EventActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**5**|18|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->this$0 Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**6**|22|iget-object|v1, v1, Lcom/example/eva/ui/event/EventViewHolder;->event Lcom/example/eva/database/event/Event;|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**7**|26|invoke-virtual|v1, Lcom/example/eva/database/event/Event;->getId()I|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**8**|32|move-result|v1|<span style='color:grey'>Landroid/content/Intent;</span>|int|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**9**|34|const-string|v2, 'idEvent'|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**10**|38|invoke-virtual|v0, v2, v1, Landroid/content/Intent;->putExtra(Ljava/lang/String; I)Landroid/content/Intent;|Landroid/content/Intent;|int|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**11**|44|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**12**|48|const/4|v2, 0|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**13**|50|invoke-virtual|v1, v0, v2, Lcom/example/eva/Environment;->startActivityForResult(Landroid/content/Intent; I)V|Landroid/content/Intent;|Lcom/example/eva/Environment;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/EventViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**15**|60|const|v2, 2130772011 # [1.714712835998671e+38]|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**16**|66|const|v3, 2130772012 # [1.714713038822767e+38]|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**17**|72|invoke-virtual|v1, v2, v3, Lcom/example/eva/Environment;->overridePendingTransition(I I)V|<span style='color:grey'>Landroid/content/Intent;</span>|Lcom/example/eva/Environment;|int|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**18**|78|return-void||<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Landroid/content/Intent;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const-class|v2, Lcom/example/eva/EventActivity;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, v1, v2, Landroid/content/Intent;-><init>(Landroid/content/Context; Ljava/lang/Class;)V|True|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->this$0 Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v1, Lcom/example/eva/ui/event/EventViewHolder;->event Lcom/example/eva/database/event/Event;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v1, Lcom/example/eva/database/event/Event;->getId()I|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const-string|v2, 'idEvent'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, v2, v1, Landroid/content/Intent;->putExtra(Ljava/lang/String; I)Landroid/content/Intent;|True|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v1, v0, v2, Lcom/example/eva/Environment;->startActivityForResult(Landroid/content/Intent; I)V|True|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v4, Lcom/example/eva/ui/event/EventViewHolder$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|const|v2, 2130772011 # [1.714712835998671e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|const|v3, 2130772012 # [1.714713038822767e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-virtual|v1, v2, v3, Lcom/example/eva/Environment;->overridePendingTransition(I I)V|<span style='color:grey'>True</span>|True|False|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
