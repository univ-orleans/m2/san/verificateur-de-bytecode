
com.example.eva.ui.event.EventRecyclerViewAdapter
=================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [getItemCount](#getitemcount)
	* [onBindViewHolder](#onbindviewholder)
	* [onBindViewHolder](#onbindviewholder)
	* [onCreateViewHolder](#oncreateviewholder)
	* [onCreateViewHolder](#oncreateviewholder)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getItemCount**](#getitemcount)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Ljava/util/List; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EventRecyclerViewAdapter(java.util.List p1, com.example.eva.Environment p2)
{
    this.events = p1;
    this.activity = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>iput-object")
	10("n°3<br/>iput-object") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v1, v0, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->events Ljava/util/List;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|10|iput-object|v2, v0, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v1, v0, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->events Ljava/util/List;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v2, v0, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getItemCount
  
**Signature :** `()I`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int getItemCount()
{
    return this.events.size();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>move-result")
	10("n°3<br/>move-result") --> 12("n°4<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->events Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v0, Ljava/util/List;->size()I|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result|v0|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**4**|12|return|v0|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->events Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v0, Ljava/util/List;->size()I|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Landroidx/recyclerview/widget/RecyclerView$ViewHolder; I)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder p1, int p2)
{
    this.onBindViewHolder(((com.example.eva.ui.event.EventViewHolder) p1), p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/event/EventViewHolder; I)V|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|Lcom/example/eva/ui/event/EventViewHolder;|int|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/event/EventViewHolder; I)V|True|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Lcom/example/eva/ui/event/EventViewHolder; I)V`  
**Nombre de registre :** 13  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onBindViewHolder(com.example.eva.ui.event.EventViewHolder p11, int p12)
{
    p11.event = ((com.example.eva.database.event.Event) this.events.get(p12));
    p11.summaryView.setText(p11.event.getSummary());
    java.util.GregorianCalendar v0_2 = new java.util.GregorianCalendar();
    v0_2.set(11, 0);
    v0_2.set(12, 0);
    v0_2.set(13, 0);
    v0_2.set(14, 0);
    java.util.Calendar v1_5 = ((java.util.Calendar) v0_2.clone());
    v1_5.set(5, (v0_2.get(5) + 1));
    if ((v0_2.getTimeInMillis() > p11.event.getDateStart().getTime()) || (p11.event.getDateStart().getTime() >= v1_5.getTimeInMillis())) {
        p11.dateStartView.setText(new java.text.SimpleDateFormat("dd MMM yyyy \u00e0 HH:mm").format(p11.event.getDateStart()));
    } else {
        android.widget.ImageView v3_15 = p11.dateStartView;
        int v4_11 = new StringBuilder();
        v4_11.append("Aujourd\'hui \u00e0 ");
        v4_11.append(new java.text.SimpleDateFormat("HH:mm").format(p11.event.getDateStart()));
        v3_15.setText(v4_11.toString());
    }
    if (!p11.event.isCanceled().booleanValue()) {
        com.example.eva.database.association.Association v2_6 = ((androidx.constraintlayout.widget.ConstraintLayout) p11.canceled_text.getParent());
        android.widget.ImageView v3_20 = new androidx.constraintlayout.widget.ConstraintSet();
        v3_20.clone(v2_6);
        v3_20.connect(2131230882, 6, 0, 7, 0);
        v3_20.clear(2131230882, 7);
        v3_20.applyTo(v2_6);
        p11.canceled_text.setVisibility(4);
        p11.canceled_background.setVisibility(4);
    } else {
        p11.canceled_text.setVisibility(0);
        p11.canceled_background.setVisibility(0);
    }
    com.example.eva.database.association.Association v2_2 = this.activity.getAssociationViewModel().get(p11.event.getIdAssociation().intValue());
    if (v2_2.getLogo() != null) {
        if (android.os.Build$VERSION.SDK_INT >= 23) {
            p11.logoView.setForeground(0);
        }
        p11.logoView.setImageResource(v2_2.getLogo().intValue());
    }
    return;
}
```
## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic androidx.recyclerview.widget.RecyclerView$ViewHolder onCreateViewHolder(android.view.ViewGroup p1, int p2)
{
    return this.onCreateViewHolder(p1, p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/event/EventViewHolder;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|Landroid/view/ViewGroup;|int|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v1|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/event/EventViewHolder;|True|True|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/event/EventViewHolder;`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.ui.event.EventViewHolder onCreateViewHolder(android.view.ViewGroup p4, int p5)
{
    return new com.example.eva.ui.event.EventViewHolder(android.view.LayoutInflater.from(p4.getContext()).inflate(2131427382, p4, 0), this.activity);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>invoke-static")
	8("n°3<br/>invoke-static") --> 14("n°4<br/>move-result-object")
	14("n°4<br/>move-result-object") --> 16("n°5<br/>const")
	16("n°5<br/>const") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>new-instance")
	32("n°9<br/>new-instance") --> 36("n°10<br/>iget-object")
	36("n°10<br/>iget-object") --> 40("n°11<br/>invoke-direct")
	40("n°11<br/>invoke-direct") --> 46("n°12<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result-object|v0|Landroid/view/LayoutInflater;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|16|const|v1, 2131427382 # [1.847637866642219e+38]|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v2, 0|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|Landroid/view/LayoutInflater;|int|int|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|32|new-instance|v1, Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|36|iget-object|v2, v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventViewHolder;</span>|Lcom/example/eva/Environment;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|40|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/event/EventViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|Landroid/view/View;|Lcom/example/eva/ui/event/EventViewHolder;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|46|return-object|v1|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const|v1, 2131427382 # [1.847637866642219e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|True|False|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|new-instance|v1, Lcom/example/eva/ui/event/EventViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v2, v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/event/EventViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|True|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
