
Makefile
========

Table des matières
==================

* [Permissions utilisées](#permissions-utilises)
* [Permissions déclarées](#permissions-dclares)

# Permissions utilisées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.FOREGROUND_SERVICE|normal, instant|run foreground service|Allows the app to make use of foreground services.|
|2|android.permission.INTERNET|normal, instant|have full network access|Allows the app to create network sockets and use custom network protocols. The browser and other applications provide means to send data to the internet, so this permission is not required to send data to the internet.|
|3|android.permission.RECEIVE_BOOT_COMPLETED|normal|run at startup|Allows the app to have itself started as soon as the system has finished booting. This can make it take longer to start the phone and allow the app to slow down the overall phone by always running.|
|4|android.permission.ACCESS_NETWORK_STATE|normal, instant|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
|5|android.permission.WAKE_LOCK|normal, instant|prevent phone from sleeping|Allows the app to prevent the phone from going to sleep.|
  

# Permissions déclarées


Aucune permission n'est déclarée.