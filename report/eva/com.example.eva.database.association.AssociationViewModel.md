
com.example.eva.database.association.AssociationViewModel
=========================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [get](#get)
	* [get](#get)
	* [getList](#getlist)
	* [getSubscriberList](#getsubscriberlist)
	* [insert](#insert)
	* [remove](#remove)
	* [update](#update)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**get**](#get)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**get**](#get)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getList**](#getlist)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getSubscriberList**](#getsubscriberlist)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**insert**](#insert)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remove**](#remove)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**update**](#update)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Landroid/app/Application;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public AssociationViewModel(android.app.Application p2)
{
    super(p2);
    super.repository = new com.example.eva.database.association.AssociationRepository(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>new-instance")
	6("n°2<br/>new-instance") --> 10("n°3<br/>invoke-direct")
	10("n°3<br/>invoke-direct") --> 16("n°4<br/>iput-object")
	16("n°4<br/>iput-object") --> 20("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v1, v2, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V|<span style='color:grey'>None</span>|Lcom/example/eva/database/association/AssociationViewModel;|Landroid/app/Application;|<span style='color:#f14848'></span>|
|**2**|6|new-instance|v0, Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-direct|v0, v2, Lcom/example/eva/database/association/AssociationRepository;-><init>(Landroid/app/Application;)V|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Landroid/app/Application;|<span style='color:#f14848'></span>|
|**4**|16|iput-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**5**|20|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v1, v2, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|new-instance|v0, Lcom/example/eva/database/association/AssociationRepository;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, v2, Lcom/example/eva/database/association/AssociationRepository;-><init>(Landroid/app/Application;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|iput-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## get
  
**Signature :** `(I)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association get(int p2)
{
    return this.repository.getAssociation(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->getAssociation(I)Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->getAssociation(I)Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## get
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association get(String p2)
{
    return this.repository.getAssociation(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->getAssociation(Ljava/lang/String;)Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->getAssociation(Ljava/lang/String;)Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getList
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getList()
{
    return this.repository.getAllAssociationList();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationRepository;->getAllAssociationList()Ljava/util/List;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationRepository;->getAllAssociationList()Ljava/util/List;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getSubscriberList
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getSubscriberList()
{
    return this.repository.getAllAssociationSubscribe();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationRepository;->getAllAssociationSubscribe()Ljava/util/List;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationRepository;->getAllAssociationSubscribe()Ljava/util/List;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## insert
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void insert(com.example.eva.database.association.Association p2)
{
    this.repository.insert(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->insert(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->insert(Lcom/example/eva/database/association/Association;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remove
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remove(com.example.eva.database.association.Association p2)
{
    this.repository.delete(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->delete(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->delete(Lcom/example/eva/database/association/Association;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## update
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void update(com.example.eva.database.association.Association p2)
{
    this.repository.update(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->update(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->repository Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/association/AssociationRepository;->update(Lcom/example/eva/database/association/Association;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
