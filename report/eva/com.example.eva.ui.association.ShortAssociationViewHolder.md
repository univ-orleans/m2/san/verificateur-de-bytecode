
com.example.eva.ui.association.ShortAssociationViewHolder
=========================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [toString](#tostring)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**toString**](#tostring)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Landroid/view/View; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public ShortAssociationViewHolder(android.view.View p3, com.example.eva.Environment p4)
{
    super(p3);
    super.view = p3;
    super.nameView = ((android.widget.TextView) p3.findViewById(2131230801));
    super.logoView = ((android.widget.ImageView) p3.findViewById(2131230800));
    super.counterView = ((android.widget.TextView) p3.findViewById(2131230799));
    super.couterBackgroundView = ((android.widget.ImageView) p3.findViewById(2131230798));
    super.view.setOnClickListener(new com.example.eva.ui.association.ShortAssociationViewHolder$1(super, p4));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>const")
	10("n°3<br/>const") --> 16("n°4<br/>invoke-virtual")
	16("n°4<br/>invoke-virtual") --> 22("n°5<br/>move-result-object")
	22("n°5<br/>move-result-object") --> 24("n°6<br/>check-cast")
	24("n°6<br/>check-cast") --> 28("n°7<br/>iput-object")
	28("n°7<br/>iput-object") --> 32("n°8<br/>const")
	32("n°8<br/>const") --> 38("n°9<br/>invoke-virtual")
	38("n°9<br/>invoke-virtual") --> 44("n°10<br/>move-result-object")
	44("n°10<br/>move-result-object") --> 46("n°11<br/>check-cast")
	46("n°11<br/>check-cast") --> 50("n°12<br/>iput-object")
	50("n°12<br/>iput-object") --> 54("n°13<br/>const")
	54("n°13<br/>const") --> 60("n°14<br/>invoke-virtual")
	60("n°14<br/>invoke-virtual") --> 66("n°15<br/>move-result-object")
	66("n°15<br/>move-result-object") --> 68("n°16<br/>check-cast")
	68("n°16<br/>check-cast") --> 72("n°17<br/>iput-object")
	72("n°17<br/>iput-object") --> 76("n°18<br/>const")
	76("n°18<br/>const") --> 82("n°19<br/>invoke-virtual")
	82("n°19<br/>invoke-virtual") --> 88("n°20<br/>move-result-object")
	88("n°20<br/>move-result-object") --> 90("n°21<br/>check-cast")
	90("n°21<br/>check-cast") --> 94("n°22<br/>iput-object")
	94("n°22<br/>iput-object") --> 98("n°23<br/>iget-object")
	98("n°23<br/>iget-object") --> 102("n°24<br/>new-instance")
	102("n°24<br/>new-instance") --> 106("n°25<br/>invoke-direct")
	106("n°25<br/>invoke-direct") --> 112("n°26<br/>invoke-virtual")
	112("n°26<br/>invoke-virtual") --> 118("n°27<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v2, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v3, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->view Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|10|const|v0, 2131230801 # [1.8077665030192645e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|16|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**5**|22|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**6**|24|check-cast|v0, Landroid/widget/TextView;|Landroid/widget/TextView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**7**|28|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|Landroid/widget/TextView;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**8**|32|const|v0, 2131230800 # [1.8077663001951685e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**9**|38|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**10**|44|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**11**|46|check-cast|v0, Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**12**|50|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**13**|54|const|v0, 2131230799 # [1.8077660973710725e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**14**|60|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**15**|66|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**16**|68|check-cast|v0, Landroid/widget/TextView;|Landroid/widget/TextView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**17**|72|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|Landroid/widget/TextView;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**18**|76|const|v0, 2131230798 # [1.8077658945469764e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**19**|82|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|Landroid/view/View;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**20**|88|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**21**|90|check-cast|v0, Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**22**|94|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->couterBackgroundView Landroid/widget/ImageView;|Landroid/widget/ImageView;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**23**|98|iget-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->view Landroid/view/View;|Landroid/view/View;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**24**|102|new-instance|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**25**|106|invoke-direct|v1, v2, v4, Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;-><init>(Lcom/example/eva/ui/association/ShortAssociationViewHolder; Lcom/example/eva/Environment;)V|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**26**|112|invoke-virtual|v0, v1, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V|Landroid/view/View;|Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**27**|118|return-void||<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v2, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v3, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->view Landroid/view/View;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v0, 2131230801 # [1.8077665030192645e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|check-cast|v0, Landroid/widget/TextView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|const|v0, 2131230800 # [1.8077663001951685e+38]|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|check-cast|v0, Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|const|v0, 2131230799 # [1.8077660973710725e+38]|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|check-cast|v0, Landroid/widget/TextView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|const|v0, 2131230798 # [1.8077658945469764e+38]|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v3, v0, Landroid/view/View;->findViewById(I)Landroid/view/View;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|check-cast|v0, Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->couterBackgroundView Landroid/widget/ImageView;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|iget-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->view Landroid/view/View;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|new-instance|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-direct|v1, v2, v4, Lcom/example/eva/ui/association/ShortAssociationViewHolder$1;-><init>(Lcom/example/eva/ui/association/ShortAssociationViewHolder; Lcom/example/eva/Environment;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**26**|invoke-virtual|v0, v1, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## toString
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String toString()
{
    String v0_1 = new StringBuilder();
    v0_1.append(super.toString());
    v0_1.append(" AssociationViewHolder{ nameView=");
    v0_1.append(this.nameView);
    v0_1.append("}");
    return v0_1.toString();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>invoke-super")
	10("n°3<br/>invoke-super") --> 16("n°4<br/>move-result-object")
	16("n°4<br/>move-result-object") --> 18("n°5<br/>invoke-virtual")
	18("n°5<br/>invoke-virtual") --> 24("n°6<br/>const-string")
	24("n°6<br/>const-string") --> 28("n°7<br/>invoke-virtual")
	28("n°7<br/>invoke-virtual") --> 34("n°8<br/>iget-object")
	34("n°8<br/>iget-object") --> 38("n°9<br/>invoke-virtual")
	38("n°9<br/>invoke-virtual") --> 44("n°10<br/>const-string")
	44("n°10<br/>const-string") --> 48("n°11<br/>invoke-virtual")
	48("n°11<br/>invoke-virtual") --> 54("n°12<br/>invoke-virtual")
	54("n°12<br/>invoke-virtual") --> 60("n°13<br/>move-result-object")
	60("n°13<br/>move-result-object") --> 62("n°14<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-super|v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->toString()Ljava/lang/String;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:#f14848'></span>|
|**4**|16|move-result-object|v1|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**6**|24|const-string|v1, ' AssociationViewHolder{ nameView='|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**7**|28|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**8**|34|iget-object|v1, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Landroid/widget/TextView;|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:#f14848'></span>|
|**9**|38|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Landroid/widget/TextView;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**10**|44|const-string|v1, '}'|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**11**|48|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**12**|54|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|Ljava/lang/StringBuilder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**13**|60|move-result-object|v0|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**14**|62|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/lang/StringBuilder;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-super|v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->toString()Ljava/lang/String;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**4**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const-string|v1, ' AssociationViewHolder{ nameView='|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|iget-object|v1, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|const-string|v1, '}'|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
