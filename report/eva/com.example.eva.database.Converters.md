
com.example.eva.database.Converters
===================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [dateToTimestamp](#datetotimestamp)
	* [fromTimestamp](#fromtimestamp)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**dateToTimestamp**](#datetotimestamp)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**fromTimestamp**](#fromtimestamp)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Converters()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/Converters;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/database/Converters;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## dateToTimestamp
  
**Signature :** `(Ljava/util/Date;)Ljava/lang/Long;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static Long dateToTimestamp(java.util.Date p2)
{
    Long v0_1;
    if (p2 != null) {
        v0_1 = Long.valueOf(p2.getTime());
    } else {
        v0_1 = 0;
    }
    return v0_1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>if-nez") --> 4("n°2<br/>const/4")
	0("n°1<br/>if-nez") -.-> 8("n°4<br/>invoke-virtual")
	4("n°2<br/>const/4") --> 6("n°3<br/>goto")
	6("n°3<br/>goto") --> 24("n°8<br/>return-object")
	8("n°4<br/>invoke-virtual") --> 14("n°5<br/>move-result-wide")
	14("n°5<br/>move-result-wide") --> 16("n°6<br/>invoke-static")
	16("n°6<br/>invoke-static") --> 22("n°7<br/>move-result-object")
	22("n°7<br/>move-result-object") --> 24("n°8<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|if-nez|v2, +4|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Ljava/util/Date;|<span style='color:#f14848'></span>|
|**2**|4|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
|**3**|6|goto|+9|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
|**4**|8|invoke-virtual|v2, Ljava/util/Date;->getTime()J|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Ljava/util/Date;|<span style='color:#f14848'></span>|
|**5**|14|move-result-wide|v0|long|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
|**6**|16|invoke-static|v0, v1, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;|long|None|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
|**7**|22|move-result-object|v0|Ljava/lang/Long;|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
|**8**|24|return-object|v0|int|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|if-nez|v2, +4|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|goto|+9|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v2, Ljava/util/Date;->getTime()J|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**5**|move-result-wide|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-static|v0, v1, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;|False|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return-object|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## fromTimestamp
  
**Signature :** `(Ljava/lang/Long;)Ljava/util/Date;`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static java.util.Date fromTimestamp(Long p3)
{
    java.util.Date v0_1;
    if (p3 != null) {
        v0_1 = new java.util.Date(p3.longValue());
    } else {
        v0_1 = 0;
    }
    return v0_1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>if-nez") --> 4("n°2<br/>const/4")
	0("n°1<br/>if-nez") -.-> 8("n°4<br/>new-instance")
	4("n°2<br/>const/4") --> 6("n°3<br/>goto")
	6("n°3<br/>goto") --> 26("n°8<br/>return-object")
	8("n°4<br/>new-instance") --> 12("n°5<br/>invoke-virtual")
	12("n°5<br/>invoke-virtual") --> 18("n°6<br/>move-result-wide")
	18("n°6<br/>move-result-wide") --> 20("n°7<br/>invoke-direct")
	20("n°7<br/>invoke-direct") --> 26("n°8<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|if-nez|v3, +4|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Ljava/lang/Long;|<span style='color:#f14848'></span>|
|**2**|4|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
|**3**|6|goto|+a|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
|**4**|8|new-instance|v0, Ljava/util/Date;|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
|**5**|12|invoke-virtual|v3, Ljava/lang/Long;->longValue()J|<span style='color:grey'>Ljava/util/Date;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Ljava/lang/Long;|<span style='color:#f14848'></span>|
|**6**|18|move-result-wide|v1|<span style='color:grey'>Ljava/util/Date;</span>|long|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
|**7**|20|invoke-direct|v0, v1, v2, Ljava/util/Date;-><init>(J)V|Ljava/util/Date;|long|None|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
|**8**|26|return-object|v0|int|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Ljava/lang/Long;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|if-nez|v3, +4|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|goto|+a|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|new-instance|v0, Ljava/util/Date;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v3, Ljava/lang/Long;->longValue()J|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**6**|move-result-wide|v1|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-direct|v0, v1, v2, Ljava/util/Date;-><init>(J)V|True|False|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return-object|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
