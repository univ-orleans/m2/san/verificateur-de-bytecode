
android.media.AudioManager
==========================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [adjustStreamVolume](#adjuststreamvolume)
	* [setStreamVolume](#setstreamvolume)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**adjustStreamVolume**](#adjuststreamvolume)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**setStreamVolume**](#setstreamvolume)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## adjustStreamVolume

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.BLUETOOTH|normal|pair with Bluetooth devices|Allows the app to view the configuration of the Bluetooth on the phone, and to make and accept connections with paired devices.|
  

## setStreamVolume

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.BLUETOOTH|normal|pair with Bluetooth devices|Allows the app to view the configuration of the Bluetooth on the phone, and to make and accept connections with paired devices.|
  
