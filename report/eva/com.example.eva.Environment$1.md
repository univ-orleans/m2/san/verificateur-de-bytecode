
com.example.eva.Environment$1
=============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [run](#run)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**run**](#run)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/Environment; Lcom/example/eva/Environment; I)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
Environment$1(com.example.eva.Environment p1, com.example.eva.Environment p2, int p3)
{
    this.this$0 = p1;
    this.val$activity = p2;
    this.val$ressourceId = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>iput")
	8("n°3<br/>iput") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/Environment$1;->this$0 Lcom/example/eva/Environment;|Lcom/example/eva/Environment$1;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/Environment$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment$1;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|iput|v3, v0, Lcom/example/eva/Environment$1;->val$ressourceId I|Lcom/example/eva/Environment$1;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/Environment$1;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|18|return-void||<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/Environment$1;->this$0 Lcom/example/eva/Environment;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/Environment$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput|v3, v0, Lcom/example/eva/Environment$1;->val$ressourceId I|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## run
  
**Signature :** `()V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void run()
{
    android.widget.Toast.makeText(this.val$activity, this.val$ressourceId, 1).show();
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>iget")
	4("n°2<br/>iget") --> 8("n°3<br/>const/4")
	8("n°3<br/>const/4") --> 10("n°4<br/>invoke-static")
	10("n°4<br/>invoke-static") --> 16("n°5<br/>move-result-object")
	16("n°5<br/>move-result-object") --> 18("n°6<br/>invoke-virtual")
	18("n°6<br/>invoke-virtual") --> 24("n°7<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/Environment$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/Environment$1;|<span style='color:#f14848'></span>|
|**2**|4|iget|v1, v3, Lcom/example/eva/Environment$1;->val$ressourceId I|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|<span style='color:grey'>None</span>|Lcom/example/eva/Environment$1;|<span style='color:#f14848'></span>|
|**3**|8|const/4|v2, 1|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-static|v0, v1, v2, Landroid/widget/Toast;->makeText(Landroid/content/Context; I I)Landroid/widget/Toast;|Lcom/example/eva/Environment;|int|int|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result-object|v0|Landroid/widget/Toast;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:#f14848'></span>|
|**6**|18|invoke-virtual|v0, Landroid/widget/Toast;->show()V|Landroid/widget/Toast;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:#f14848'></span>|
|**7**|24|return-void||<span style='color:grey'>Landroid/widget/Toast;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/Environment$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|iget|v1, v3, Lcom/example/eva/Environment$1;->val$ressourceId I|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|const/4|v2, 1|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-static|v0, v1, v2, Landroid/widget/Toast;->makeText(Landroid/content/Context; I I)Landroid/widget/Toast;|True|False|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, Landroid/widget/Toast;->show()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
