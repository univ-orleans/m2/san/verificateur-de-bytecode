
com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter
==================================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [access$000](#access000)
	* [getItemCount](#getitemcount)
	* [onBindViewHolder](#onbindviewholder)
	* [onBindViewHolder](#onbindviewholder)
	* [onCreateViewHolder](#oncreateviewholder)
	* [onCreateViewHolder](#oncreateviewholder)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$000**](#access000)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getItemCount**](#getitemcount)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Ljava/util/List; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public ShortAssociationRecyclerViewAdapter(java.util.List p1, com.example.eva.Environment p2)
{
    this.activity = p2;
    this.associations = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>iput-object")
	10("n°3<br/>iput-object") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v2, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**3**|10|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$000
  
**Signature :** `(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;)Lcom/example/eva/Environment;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic com.example.eva.Environment access$000(com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter p1)
{
    return p1.activity;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getItemCount
  
**Signature :** `()I`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int getItemCount()
{
    return this.associations.size();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>move-result")
	10("n°3<br/>move-result") --> 12("n°4<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v0, Ljava/util/List;->size()I|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result|v0|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**4**|12|return|v0|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v0, Ljava/util/List;->size()I|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Landroidx/recyclerview/widget/RecyclerView$ViewHolder; I)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder p1, int p2)
{
    this.onBindViewHolder(((com.example.eva.ui.association.ShortAssociationViewHolder) p1), p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/association/ShortAssociationViewHolder; I)V|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|int|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/association/ShortAssociationViewHolder; I)V|True|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Lcom/example/eva/ui/association/ShortAssociationViewHolder; I)V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onBindViewHolder(com.example.eva.ui.association.ShortAssociationViewHolder p5, int p6)
{
    com.example.eva.database.association.Association v0_2 = ((com.example.eva.database.association.Association) this.associations.get(p6));
    p5.association = v0_2;
    p5.nameView.setText(v0_2.getName());
    this.activity.getEventViewModel().getData().observe(this.activity, new com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter$1(this, v0_2, p5));
    if (v0_2.getLogo() != null) {
        if (android.os.Build$VERSION.SDK_INT >= 23) {
            p5.logoView.setForeground(0);
        }
        p5.logoView.setImageResource(v0_2.getLogo().intValue());
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>check-cast")
	12("n°4<br/>check-cast") --> 16("n°5<br/>iput-object")
	16("n°5<br/>iput-object") --> 20("n°6<br/>iget-object")
	20("n°6<br/>iget-object") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>invoke-virtual")
	32("n°9<br/>invoke-virtual") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>invoke-virtual")
	42("n°11<br/>invoke-virtual") --> 48("n°12<br/>move-result-object")
	48("n°12<br/>move-result-object") --> 50("n°13<br/>invoke-virtual")
	50("n°13<br/>invoke-virtual") --> 56("n°14<br/>move-result-object")
	56("n°14<br/>move-result-object") --> 58("n°15<br/>iget-object")
	58("n°15<br/>iget-object") --> 62("n°16<br/>new-instance")
	62("n°16<br/>new-instance") --> 66("n°17<br/>invoke-direct")
	66("n°17<br/>invoke-direct") --> 72("n°18<br/>invoke-virtual")
	72("n°18<br/>invoke-virtual") --> 78("n°19<br/>invoke-virtual")
	78("n°19<br/>invoke-virtual") --> 84("n°20<br/>move-result-object")
	84("n°20<br/>move-result-object") --> 86("n°21<br/>if-eqz")
	86("n°21<br/>if-eqz") --> 90("n°22<br/>sget")
	86("n°21<br/>if-eqz") -.-> 140("n°34<br/>return-void")
	90("n°22<br/>sget") --> 94("n°23<br/>const/16")
	94("n°23<br/>const/16") --> 98("n°24<br/>if-lt")
	98("n°24<br/>if-lt") --> 102("n°25<br/>iget-object")
	98("n°24<br/>if-lt") -.-> 114("n°28<br/>iget-object")
	102("n°25<br/>iget-object") --> 106("n°26<br/>const/4")
	106("n°26<br/>const/4") --> 108("n°27<br/>invoke-virtual")
	108("n°27<br/>invoke-virtual") --> 114("n°28<br/>iget-object")
	114("n°28<br/>iget-object") --> 118("n°29<br/>invoke-virtual")
	118("n°29<br/>invoke-virtual") --> 124("n°30<br/>move-result-object")
	124("n°30<br/>move-result-object") --> 126("n°31<br/>invoke-virtual")
	126("n°31<br/>invoke-virtual") --> 132("n°32<br/>move-result")
	132("n°32<br/>move-result") --> 134("n°33<br/>invoke-virtual")
	134("n°33<br/>invoke-virtual") --> 140("n°34<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v0, v6, Ljava/util/List;->get(I)Ljava/lang/Object;|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|check-cast|v0, Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|16|iput-object|v0, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|20|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/TextView;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/TextView;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/TextView;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|32|invoke-virtual|v1, v2, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/TextView;|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v1, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-virtual|v1, Lcom/example/eva/Environment;->getEventViewModel()Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|48|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**13**|50|invoke-virtual|v1, Lcom/example/eva/database/event/EventViewModel;->getData()Landroidx/lifecycle/LiveData;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**14**|56|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroidx/lifecycle/LiveData;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**15**|58|iget-object|v2, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**16**|62|new-instance|v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**17**|66|invoke-direct|v3, v4, v0, v5, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;-><init>(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter; Lcom/example/eva/database/association/Association; Lcom/example/eva/ui/association/ShortAssociationViewHolder;)V|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**18**|72|invoke-virtual|v1, v2, v3, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner; Landroidx/lifecycle/Observer;)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroidx/lifecycle/LiveData;|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**19**|78|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**20**|84|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**21**|86|if-eqz|v1, +1b|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**22**|90|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**23**|94|const/16|v2, 23|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**24**|98|if-lt|v1, v2, +8|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**25**|102|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**26**|106|const/4|v2, 0|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**27**|108|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**28**|114|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**29**|118|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/ImageView;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**30**|124|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**31**|126|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**32**|132|move-result|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**33**|134|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setImageResource(I)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**34**|140|return-void||<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v0, v6, Ljava/util/List;->get(I)Ljava/lang/Object;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|check-cast|v0, Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iput-object|v0, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->nameView Landroid/widget/TextView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v1, v2, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v1, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v1, Lcom/example/eva/Environment;->getEventViewModel()Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v1, Lcom/example/eva/database/event/EventViewModel;->getData()Landroidx/lifecycle/LiveData;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|iget-object|v2, v4, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|new-instance|v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-direct|v3, v4, v0, v5, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;-><init>(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter; Lcom/example/eva/database/association/Association; Lcom/example/eva/ui/association/ShortAssociationViewHolder;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v1, v2, v3, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner; Landroidx/lifecycle/Observer;)V|<span style='color:grey'>True</span>|True|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|if-eqz|v1, +1b|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|const/16|v2, 23|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|if-lt|v1, v2, +8|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|iget-object|v1, v5, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setImageResource(I)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic androidx.recyclerview.widget.RecyclerView$ViewHolder onCreateViewHolder(android.view.ViewGroup p1, int p2)
{
    return this.onCreateViewHolder(p1, p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/ShortAssociationViewHolder;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Landroid/view/ViewGroup;|int|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v1|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/ShortAssociationViewHolder;|True|True|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/ShortAssociationViewHolder;`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.ui.association.ShortAssociationViewHolder onCreateViewHolder(android.view.ViewGroup p4, int p5)
{
    return new com.example.eva.ui.association.ShortAssociationViewHolder(android.view.LayoutInflater.from(p4.getContext()).inflate(2131427387, p4, 0), this.activity);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>invoke-static")
	8("n°3<br/>invoke-static") --> 14("n°4<br/>move-result-object")
	14("n°4<br/>move-result-object") --> 16("n°5<br/>const")
	16("n°5<br/>const") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>new-instance")
	32("n°9<br/>new-instance") --> 36("n°10<br/>iget-object")
	36("n°10<br/>iget-object") --> 40("n°11<br/>invoke-direct")
	40("n°11<br/>invoke-direct") --> 46("n°12<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result-object|v0|Landroid/view/LayoutInflater;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|16|const|v1, 2131427387 # [1.8476388807626992e+38]|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v2, 0|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|Landroid/view/LayoutInflater;|int|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|32|new-instance|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|36|iget-object|v2, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|40|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|Landroid/view/View;|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|46|return-object|v1|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const|v1, 2131427387 # [1.8476388807626992e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|True|False|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|new-instance|v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v2, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/association/ShortAssociationViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|True|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
