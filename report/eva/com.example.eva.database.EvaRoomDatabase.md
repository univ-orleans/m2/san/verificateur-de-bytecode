
com.example.eva.database.EvaRoomDatabase
========================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<clinit>](#clinit)
	* [\<init>](#init)
	* [getDatabase](#getdatabase)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<clinit>**](#clinit)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getDatabase**](#getdatabase)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<clinit>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static EvaRoomDatabase()
{
    com.example.eva.database.EvaRoomDatabase.EXECUTOR = java.util.concurrent.Executors.newFixedThreadPool(4);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>invoke-static")
	2("n°2<br/>invoke-static") --> 8("n°3<br/>move-result-object")
	8("n°3<br/>move-result-object") --> 10("n°4<br/>sput-object")
	10("n°4<br/>sput-object") --> 14("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 4|int|<span style='color:#f14848'></span>|
|**2**|2|invoke-static|v0, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;|int|<span style='color:#f14848'></span>|
|**3**|8|move-result-object|v0|Ljava/util/concurrent/ExecutorService;|<span style='color:#f14848'></span>|
|**4**|10|sput-object|v0, Lcom/example/eva/database/EvaRoomDatabase;->EXECUTOR Ljava/util/concurrent/ExecutorService;|Ljava/util/concurrent/ExecutorService;|<span style='color:#f14848'></span>|
|**5**|14|return-void||<span style='color:grey'>Ljava/util/concurrent/ExecutorService;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 4|False|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;|False|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:#f14848'></span>|
|**4**|sput-object|v0, Lcom/example/eva/database/EvaRoomDatabase;->EXECUTOR Ljava/util/concurrent/ExecutorService;|True|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EvaRoomDatabase()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/room/RoomDatabase;-><init>()V|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/room/RoomDatabase;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getDatabase
  
**Signature :** `(Landroid/content/Context;)Lcom/example/eva/database/EvaRoomDatabase;`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static com.example.eva.database.EvaRoomDatabase getDatabase(android.content.Context p3)
{
    if (com.example.eva.database.EvaRoomDatabase.INSTANCE == null) {
        try {
            if (com.example.eva.database.EvaRoomDatabase.INSTANCE == null) {
                com.example.eva.database.EvaRoomDatabase.INSTANCE = ((com.example.eva.database.EvaRoomDatabase) androidx.room.Room.databaseBuilder(p3.getApplicationContext(), com.example.eva.database.EvaRoomDatabase, "eva_database").fallbackToDestructiveMigration().build());
            }
        } catch (com.example.eva.database.EvaRoomDatabase v1_4) {
            throw v1_4;
        }
    }
    return com.example.eva.database.EvaRoomDatabase.INSTANCE;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-class") --> 4("n°2<br/>sget-object")
	4("n°2<br/>sget-object") --> 8("n°3<br/>if-nez")
	8("n°3<br/>if-nez") --> 12("n°4<br/>monitor-enter")
	8("n°3<br/>if-nez") -.-> 76("n°23<br/>sget-object")
	12("n°4<br/>monitor-enter") --> 14("n°5<br/>sget-object")
	14("n°5<br/>sget-object") --> 18("n°6<br/>if-nez")
	18("n°6<br/>if-nez") --> 22("n°7<br/>invoke-virtual")
	18("n°6<br/>if-nez") -.-> 66("n°18<br/>monitor-exit")
	22("n°7<br/>invoke-virtual") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>const-string")
	30("n°9<br/>const-string") --> 34("n°10<br/>invoke-static")
	34("n°10<br/>invoke-static") --> 40("n°11<br/>move-result-object")
	40("n°11<br/>move-result-object") --> 42("n°12<br/>invoke-virtual")
	42("n°12<br/>invoke-virtual") --> 48("n°13<br/>move-result-object")
	48("n°13<br/>move-result-object") --> 50("n°14<br/>invoke-virtual")
	50("n°14<br/>invoke-virtual") --> 56("n°15<br/>move-result-object")
	56("n°15<br/>move-result-object") --> 58("n°16<br/>check-cast")
	58("n°16<br/>check-cast") --> 62("n°17<br/>sput-object")
	62("n°17<br/>sput-object") --> 66("n°18<br/>monitor-exit")
	66("n°18<br/>monitor-exit") --> 68("n°19<br/>goto")
	68("n°19<br/>goto") --> 76("n°23<br/>sget-object")
	70("n°20<br/>move-exception") --> 72("n°21<br/>monitor-exit")
	72("n°21<br/>monitor-exit") --> 74("n°22<br/>throw")
	76("n°23<br/>sget-object") --> 80("n°24<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-class|v0, Lcom/example/eva/database/EvaRoomDatabase;|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**2**|4|sget-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**3**|8|if-nez|v1, +22|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**4**|12|monitor-enter|v0|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**5**|14|sget-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**6**|18|if-nez|v1, +18|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-virtual|v3, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>None</span>|Landroid/content/Context;|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**9**|30|const-string|v2, 'eva_database'|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>Landroid/content/Context;</span>|Ljava/lang/String;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**10**|34|invoke-static|v1, v0, v2, Landroidx/room/Room;->databaseBuilder(Landroid/content/Context; Ljava/lang/Class; Ljava/lang/String;)Landroidx/room/RoomDatabase$Builder;|Lcom/example/eva/database/EvaRoomDatabase;|Landroid/content/Context;|Ljava/lang/String;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**11**|40|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**12**|42|invoke-virtual|v1, Landroidx/room/RoomDatabase$Builder;->fallbackToDestructiveMigration()Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**13**|48|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**14**|50|invoke-virtual|v1, Landroidx/room/RoomDatabase$Builder;->build()Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**15**|56|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**16**|58|check-cast|v1, Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**17**|62|sput-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**18**|66|monitor-exit|v0|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**19**|68|goto|+4|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**20**|70|move-exception|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**21**|72|monitor-exit|v0|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**22**|74|throw|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**23**|76|sget-object|v0, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
|**24**|80|return-object|v0|Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-class|v0, Lcom/example/eva/database/EvaRoomDatabase;|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|sget-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|if-nez|v1, +22|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|monitor-enter|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|sget-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-nez|v1, +18|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v3, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**8**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const-string|v2, 'eva_database'|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-static|v1, v0, v2, Landroidx/room/Room;->databaseBuilder(Landroid/content/Context; Ljava/lang/Class; Ljava/lang/String;)Landroidx/room/RoomDatabase$Builder;|False|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v1, Landroidx/room/RoomDatabase$Builder;->fallbackToDestructiveMigration()Landroidx/room/RoomDatabase$Builder;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v1, Landroidx/room/RoomDatabase$Builder;->build()Landroidx/room/RoomDatabase;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|check-cast|v1, Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|sput-object|v1, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|monitor-exit|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|goto|+4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-exception|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|monitor-exit|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|throw|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|sget-object|v0, Lcom/example/eva/database/EvaRoomDatabase;->INSTANCE Lcom/example/eva/database/EvaRoomDatabase;|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
