
com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter$1
====================================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onChanged](#onchanged)
	* [onChanged](#onchanged)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter; 
Lcom/example/eva/database/association/Association; Lcom/example/eva/ui/association/ShortAssociationViewHolder;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
ShortAssociationRecyclerViewAdapter$1(com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter p1, com.example.eva.database.association.Association p2, com.example.eva.ui.association.ShortAssociationViewHolder p3)
{
    this.this$0 = p1;
    this.val$association = p2;
    this.val$holder = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>iput-object")
	8("n°3<br/>iput-object") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$association Lcom/example/eva/database/association/Association;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**3**|8|iput-object|v3, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
|**5**|18|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationViewHolder;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v3, v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/lang/Object;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onChanged(Object p1)
{
    this.onChanged(((java.util.List) p1));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|Ljava/util/List;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->onChanged(Ljava/util/List;)V|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|Ljava/util/List;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Ljava/util/List;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->onChanged(Ljava/util/List;)V|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/util/List;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onChanged(java.util.List p4)
{
    int v0_3 = com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter.access$000(this.this$0).getEventViewModel().numberConfirmed(this.val$association);
    if (v0_3 <= 0) {
        this.val$holder.counterView.setVisibility(4);
        this.val$holder.couterBackgroundView.setVisibility(4);
    } else {
        this.val$holder.counterView.setText(String.valueOf(v0_3));
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>invoke-virtual")
	12("n°4<br/>invoke-virtual") --> 18("n°5<br/>move-result-object")
	18("n°5<br/>move-result-object") --> 20("n°6<br/>iget-object")
	20("n°6<br/>iget-object") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result")
	30("n°8<br/>move-result") --> 32("n°9<br/>if-lez")
	32("n°9<br/>if-lez") --> 36("n°10<br/>iget-object")
	32("n°9<br/>if-lez") -.-> 60("n°16<br/>iget-object")
	36("n°10<br/>iget-object") --> 40("n°11<br/>iget-object")
	40("n°11<br/>iget-object") --> 44("n°12<br/>invoke-static")
	44("n°12<br/>invoke-static") --> 50("n°13<br/>move-result-object")
	50("n°13<br/>move-result-object") --> 52("n°14<br/>invoke-virtual")
	52("n°14<br/>invoke-virtual") --> 58("n°15<br/>goto")
	58("n°15<br/>goto") --> 90("n°23<br/>return-void")
	60("n°16<br/>iget-object") --> 64("n°17<br/>iget-object")
	64("n°17<br/>iget-object") --> 68("n°18<br/>const/4")
	68("n°18<br/>const/4") --> 70("n°19<br/>invoke-virtual")
	70("n°19<br/>invoke-virtual") --> 76("n°20<br/>iget-object")
	76("n°20<br/>iget-object") --> 80("n°21<br/>iget-object")
	80("n°21<br/>iget-object") --> 84("n°22<br/>invoke-virtual")
	84("n°22<br/>invoke-virtual") --> 90("n°23<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->access$000(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;)Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-virtual|v0, Lcom/example/eva/Environment;->getEventViewModel()Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**5**|18|move-result-object|v0|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**6**|20|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, v1, Lcom/example/eva/database/event/EventViewModel;->numberConfirmed(Lcom/example/eva/database/association/Association;)I|Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result|v0|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**9**|32|if-lez|v0, +e|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**10**|36|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**11**|40|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|<span style='color:grey'>int</span>|Landroid/widget/TextView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**12**|44|invoke-static|v0, Ljava/lang/String;->valueOf(I)Ljava/lang/String;|int|<span style='color:grey'>Landroid/widget/TextView;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**13**|50|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/widget/TextView;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**14**|52|invoke-virtual|v1, v2, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>int</span>|Landroid/widget/TextView;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**15**|58|goto|+10|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/widget/TextView;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**16**|60|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**17**|64|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|<span style='color:grey'>int</span>|Landroid/widget/TextView;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**18**|68|const/4|v2, 4|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/widget/TextView;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**19**|70|invoke-virtual|v1, v2, Landroid/widget/TextView;->setVisibility(I)V|<span style='color:grey'>int</span>|Landroid/widget/TextView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**20**|76|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>int</span>|Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**21**|80|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->couterBackgroundView Landroid/widget/ImageView;|<span style='color:grey'>int</span>|Landroid/widget/ImageView;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**22**|84|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setVisibility(I)V|<span style='color:grey'>int</span>|Landroid/widget/ImageView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**23**|90|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/widget/TextView;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;->access$000(Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter;)Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Lcom/example/eva/Environment;->getEventViewModel()Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, Lcom/example/eva/database/event/EventViewModel;->numberConfirmed(Lcom/example/eva/database/association/Association;)I|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|if-lez|v0, +e|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-static|v0, Ljava/lang/String;->valueOf(I)Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v1, v2, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|goto|+10|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->counterView Landroid/widget/TextView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|const/4|v2, 4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v1, v2, Landroid/widget/TextView;->setVisibility(I)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|iget-object|v1, v3, Lcom/example/eva/ui/association/ShortAssociationRecyclerViewAdapter$1;->val$holder Lcom/example/eva/ui/association/ShortAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|iget-object|v1, v1, Lcom/example/eva/ui/association/ShortAssociationViewHolder;->couterBackgroundView Landroid/widget/ImageView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setVisibility(I)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
