
com.example.eva.Environment
===========================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [getInstance](#getinstance)
	* [getAssociationViewModel](#getassociationviewmodel)
	* [getEventViewModel](#geteventviewmodel)
	* [onCreate](#oncreate)
	* [showToast](#showtoast)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getInstance**](#getinstance)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAssociationViewModel**](#getassociationviewmodel)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getEventViewModel**](#geteventviewmodel)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**showToast**](#showtoast)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Environment()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/appcompat/app/AppCompatActivity;-><init>()V|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/appcompat/app/AppCompatActivity;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getInstance
  
**Signature :** `()Lcom/example/eva/Environment;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static com.example.eva.Environment getInstance()
{
    if (com.example.eva.Environment.instance == null) {
        try {
            if (com.example.eva.Environment.instance == null) {
                com.example.eva.Environment.instance = new com.example.eva.Environment();
            }
        } catch (com.example.eva.Environment v1_3) {
            throw v1_3;
        }
    }
    return com.example.eva.Environment.instance;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget-object") --> 4("n°2<br/>if-nez")
	4("n°2<br/>if-nez") --> 8("n°3<br/>const-class")
	4("n°2<br/>if-nez") -.-> 46("n°15<br/>sget-object")
	8("n°3<br/>const-class") --> 12("n°4<br/>monitor-enter")
	12("n°4<br/>monitor-enter") --> 14("n°5<br/>sget-object")
	14("n°5<br/>sget-object") --> 18("n°6<br/>if-nez")
	18("n°6<br/>if-nez") --> 22("n°7<br/>new-instance")
	18("n°6<br/>if-nez") -.-> 36("n°10<br/>monitor-exit")
	22("n°7<br/>new-instance") --> 26("n°8<br/>invoke-direct")
	26("n°8<br/>invoke-direct") --> 32("n°9<br/>sput-object")
	32("n°9<br/>sput-object") --> 36("n°10<br/>monitor-exit")
	36("n°10<br/>monitor-exit") --> 38("n°11<br/>goto")
	38("n°11<br/>goto") --> 46("n°15<br/>sget-object")
	40("n°12<br/>move-exception") --> 42("n°13<br/>monitor-exit")
	42("n°13<br/>monitor-exit") --> 44("n°14<br/>throw")
	46("n°15<br/>sget-object") --> 50("n°16<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget-object|v0, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**2**|4|if-nez|v0, +15|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**3**|8|const-class|v0, Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**4**|12|monitor-enter|v0|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**5**|14|sget-object|v1, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**6**|18|if-nez|v1, +9|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**7**|22|new-instance|v1, Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**8**|26|invoke-direct|v1, Lcom/example/eva/Environment;-><init>()V|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**9**|32|sput-object|v1, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**10**|36|monitor-exit|v0|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**11**|38|goto|+4|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**12**|40|move-exception|v1|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Ljava/lang/Exception;|<span style='color:#f14848'></span>|
|**13**|42|monitor-exit|v0|Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:#f14848'></span>|
|**14**|44|throw|v1|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Ljava/lang/Exception;|<span style='color:#f14848'></span>|
|**15**|46|sget-object|v0, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**16**|50|return-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|sget-object|v0, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**2**|if-nez|v0, +15|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**3**|const-class|v0, Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**4**|monitor-enter|v0|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**5**|sget-object|v1, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**6**|if-nez|v1, +9|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|new-instance|v1, Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**8**|invoke-direct|v1, Lcom/example/eva/Environment;-><init>()V|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**9**|sput-object|v1, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**10**|monitor-exit|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|goto|+4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-exception|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**13**|monitor-exit|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|throw|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**15**|sget-object|v0, Lcom/example/eva/Environment;->instance Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|return-object|v0|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
  

## getAssociationViewModel
  
**Signature :** `()Lcom/example/eva/database/association/AssociationViewModel;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.AssociationViewModel getAssociationViewModel()
{
    if (this.associationViewModel == null) {
        this.associationViewModel = ((com.example.eva.database.association.AssociationViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.association.AssociationViewModel));
    }
    return this.associationViewModel;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>if-nez")
	4("n°2<br/>if-nez") --> 8("n°3<br/>new-instance")
	4("n°2<br/>if-nez") -.-> 38("n°10<br/>iget-object")
	8("n°3<br/>new-instance") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>const-class")
	18("n°5<br/>const-class") --> 22("n°6<br/>invoke-virtual")
	22("n°6<br/>invoke-virtual") --> 28("n°7<br/>move-result-object")
	28("n°7<br/>move-result-object") --> 30("n°8<br/>check-cast")
	30("n°8<br/>check-cast") --> 34("n°9<br/>iput-object")
	34("n°9<br/>iput-object") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**2**|4|if-nez|v0, +11|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|8|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**5**|18|const-class|v1, Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Landroidx/lifecycle/ViewModelProvider;</span>|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**6**|22|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|Landroidx/lifecycle/ViewModelProvider;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**7**|28|move-result-object|v0|Landroidx/lifecycle/ViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**8**|30|check-cast|v0, Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**9**|34|iput-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**11**|42|return-object|v0|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|if-nez|v0, +11|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**5**|const-class|v1, Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|check-cast|v0, Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iput-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**10**|iget-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**11**|return-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getEventViewModel
  
**Signature :** `()Lcom/example/eva/database/event/EventViewModel;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.event.EventViewModel getEventViewModel()
{
    if (this.eventViewModel == null) {
        this.eventViewModel = ((com.example.eva.database.event.EventViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.event.EventViewModel));
    }
    return this.eventViewModel;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>if-nez")
	4("n°2<br/>if-nez") --> 8("n°3<br/>new-instance")
	4("n°2<br/>if-nez") -.-> 38("n°10<br/>iget-object")
	8("n°3<br/>new-instance") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>const-class")
	18("n°5<br/>const-class") --> 22("n°6<br/>invoke-virtual")
	22("n°6<br/>invoke-virtual") --> 28("n°7<br/>move-result-object")
	28("n°7<br/>move-result-object") --> 30("n°8<br/>check-cast")
	30("n°8<br/>check-cast") --> 34("n°9<br/>iput-object")
	34("n°9<br/>iput-object") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**2**|4|if-nez|v0, +11|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|8|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**5**|18|const-class|v1, Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Landroidx/lifecycle/ViewModelProvider;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**6**|22|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|Landroidx/lifecycle/ViewModelProvider;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**7**|28|move-result-object|v0|Landroidx/lifecycle/ViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**8**|30|check-cast|v0, Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**9**|34|iput-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**11**|42|return-object|v0|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|if-nez|v0, +11|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**5**|const-class|v1, Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|check-cast|v0, Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iput-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**10**|iget-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**11**|return-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void onCreate(android.os.Bundle p3)
{
    super.onCreate(p3);
    this.eventViewModel = ((com.example.eva.database.event.EventViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.event.EventViewModel));
    this.associationViewModel = ((com.example.eva.database.association.AssociationViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.association.AssociationViewModel));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>new-instance")
	6("n°2<br/>new-instance") --> 10("n°3<br/>invoke-direct")
	10("n°3<br/>invoke-direct") --> 16("n°4<br/>const-class")
	16("n°4<br/>const-class") --> 20("n°5<br/>invoke-virtual")
	20("n°5<br/>invoke-virtual") --> 26("n°6<br/>move-result-object")
	26("n°6<br/>move-result-object") --> 28("n°7<br/>check-cast")
	28("n°7<br/>check-cast") --> 32("n°8<br/>iput-object")
	32("n°8<br/>iput-object") --> 36("n°9<br/>new-instance")
	36("n°9<br/>new-instance") --> 40("n°10<br/>invoke-direct")
	40("n°10<br/>invoke-direct") --> 46("n°11<br/>const-class")
	46("n°11<br/>const-class") --> 50("n°12<br/>invoke-virtual")
	50("n°12<br/>invoke-virtual") --> 56("n°13<br/>move-result-object")
	56("n°13<br/>move-result-object") --> 58("n°14<br/>check-cast")
	58("n°14<br/>check-cast") --> 62("n°15<br/>iput-object")
	62("n°15<br/>iput-object") --> 66("n°16<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v2, v3, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|Landroid/os/Bundle;|<span style='color:#f14848'></span>|
|**2**|6|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|16|const-class|v1, Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Landroidx/lifecycle/ViewModelProvider;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|20|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|Landroidx/lifecycle/ViewModelProvider;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|26|move-result-object|v0|Landroidx/lifecycle/ViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|28|check-cast|v0, Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|32|iput-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**9**|36|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**10**|40|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|Landroidx/lifecycle/ViewModelProvider;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**11**|46|const-class|v1, Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Landroidx/lifecycle/ViewModelProvider;</span>|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**12**|50|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|Landroidx/lifecycle/ViewModelProvider;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**13**|56|move-result-object|v0|Landroidx/lifecycle/ViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**14**|58|check-cast|v0, Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**15**|62|iput-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**16**|66|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v2, v3, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const-class|v1, Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|check-cast|v0, Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|iput-object|v0, v2, Lcom/example/eva/Environment;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|new-instance|v0, Landroidx/lifecycle/ViewModelProvider;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-direct|v0, v2, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|const-class|v1, Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v0, v1, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|check-cast|v0, Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|iput-object|v0, v2, Lcom/example/eva/Environment;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## showToast
  
**Signature :** `(I)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void showToast(int p3)
{
    this.runOnUiThread(new com.example.eva.Environment$1(this, this, p3));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>move-object") --> 2("n°2<br/>new-instance")
	2("n°2<br/>new-instance") --> 6("n°3<br/>invoke-direct")
	6("n°3<br/>invoke-direct") --> 12("n°4<br/>invoke-virtual")
	12("n°4<br/>invoke-virtual") --> 18("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|move-object|v0, v2|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|2|new-instance|v1, Lcom/example/eva/Environment$1;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment$1;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|6|invoke-direct|v1, v2, v0, v3, Lcom/example/eva/Environment$1;-><init>(Lcom/example/eva/Environment; Lcom/example/eva/Environment; I)V|Lcom/example/eva/Environment;|Lcom/example/eva/Environment$1;|Lcom/example/eva/Environment;|int|<span style='color:#f14848'></span>|
|**4**|12|invoke-virtual|v2, v1, Lcom/example/eva/Environment;->runOnUiThread(Ljava/lang/Runnable;)V|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/Environment$1;|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|18|return-void||<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/Environment$1;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|move-object|v0, v2|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|new-instance|v1, Lcom/example/eva/Environment$1;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v1, v2, v0, v3, Lcom/example/eva/Environment$1;-><init>(Lcom/example/eva/Environment; Lcom/example/eva/Environment; I)V|True|True|True|True|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v2, v1, Lcom/example/eva/Environment;->runOnUiThread(Ljava/lang/Runnable;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
