
com.example.eva.ui.association.SubscriberAssociationViewHolder$1
================================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onClick](#onclick)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onClick**](#onclick)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
SubscriberAssociationViewHolder$1(com.example.eva.ui.association.SubscriberAssociationViewHolder p1, com.example.eva.Environment p2)
{
    this.this$0 = p1;
    this.val$activity = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onClick
  
**Signature :** `(Landroid/view/View;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onClick(android.view.View p4)
{
    this.val$activity.getAssociationViewModel().update(this.this$0.association.setSubscribe(Boolean.valueOf((this.this$0.association.getSubscribe().booleanValue() ^ 1))));
    if (this.this$0.association.getSubscribe().booleanValue()) {
        com.example.eva.service.api.google.calendar.DataSynchronisationWorker.sheduler(this.val$activity, this.this$0.association);
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>iget-object")
	12("n°4<br/>iget-object") --> 16("n°5<br/>iget-object")
	16("n°5<br/>iget-object") --> 20("n°6<br/>iget-object")
	20("n°6<br/>iget-object") --> 24("n°7<br/>iget-object")
	24("n°7<br/>iget-object") --> 28("n°8<br/>invoke-virtual")
	28("n°8<br/>invoke-virtual") --> 34("n°9<br/>move-result-object")
	34("n°9<br/>move-result-object") --> 36("n°10<br/>invoke-virtual")
	36("n°10<br/>invoke-virtual") --> 42("n°11<br/>move-result")
	42("n°11<br/>move-result") --> 44("n°12<br/>xor-int/lit8")
	44("n°12<br/>xor-int/lit8") --> 48("n°13<br/>invoke-static")
	48("n°13<br/>invoke-static") --> 54("n°14<br/>move-result-object")
	54("n°14<br/>move-result-object") --> 56("n°15<br/>invoke-virtual")
	56("n°15<br/>invoke-virtual") --> 62("n°16<br/>move-result-object")
	62("n°16<br/>move-result-object") --> 64("n°17<br/>invoke-virtual")
	64("n°17<br/>invoke-virtual") --> 70("n°18<br/>iget-object")
	70("n°18<br/>iget-object") --> 74("n°19<br/>iget-object")
	74("n°19<br/>iget-object") --> 78("n°20<br/>invoke-virtual")
	78("n°20<br/>invoke-virtual") --> 84("n°21<br/>move-result-object")
	84("n°21<br/>move-result-object") --> 86("n°22<br/>invoke-virtual")
	86("n°22<br/>invoke-virtual") --> 92("n°23<br/>move-result")
	92("n°23<br/>move-result") --> 94("n°24<br/>if-eqz")
	94("n°24<br/>if-eqz") --> 98("n°25<br/>iget-object")
	94("n°24<br/>if-eqz") -.-> 116("n°29<br/>return-void")
	98("n°25<br/>iget-object") --> 102("n°26<br/>iget-object")
	102("n°26<br/>iget-object") --> 106("n°27<br/>iget-object")
	106("n°27<br/>iget-object") --> 110("n°28<br/>invoke-static")
	110("n°28<br/>invoke-static") --> 116("n°29<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**4**|12|iget-object|v1, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**5**|16|iget-object|v1, v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**6**|20|iget-object|v2, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**7**|24|iget-object|v2, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**8**|28|invoke-virtual|v2, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**9**|34|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**10**|36|invoke-virtual|v2, Ljava/lang/Boolean;->booleanValue()Z|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**11**|42|move-result|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|boolean|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**12**|44|xor-int/lit8|v2, v2, 1|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|boolean|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**13**|48|invoke-static|v2, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|boolean|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**14**|54|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**15**|56|invoke-virtual|v1, v2, Lcom/example/eva/database/association/Association;->setSubscribe(Ljava/lang/Boolean;)Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**16**|62|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**17**|64|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->update(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**18**|70|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**19**|74|iget-object|v0, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**20**|78|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**21**|84|move-result-object|v0|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**22**|86|invoke-virtual|v0, Ljava/lang/Boolean;->booleanValue()Z|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**23**|92|move-result|v0|boolean|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**24**|94|if-eqz|v0, +b|boolean|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**25**|98|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**26**|102|iget-object|v1, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Ljava/lang/Boolean;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**27**|106|iget-object|v1, v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**28**|110|invoke-static|v0, v1, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->sheduler(Landroid/content/Context; Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/Environment;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**29**|116|return-void||<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iget-object|v1, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v1, v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v2, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iget-object|v2, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v2, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v2, Ljava/lang/Boolean;->booleanValue()Z|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|xor-int/lit8|v2, v2, 1|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-static|v2, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, v2, Lcom/example/eva/database/association/Association;->setSubscribe(Ljava/lang/Boolean;)Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->update(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|iget-object|v0, v0, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v0, Ljava/lang/Boolean;->booleanValue()Z|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|if-eqz|v0, +b|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|iget-object|v1, v3, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder$1;->this$0 Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|iget-object|v1, v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|invoke-static|v0, v1, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->sheduler(Landroid/content/Context; Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
