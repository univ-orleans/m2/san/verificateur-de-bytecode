
android.net.ConnectivityManager
===============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [getNetworkInfo](#getnetworkinfo)
	* [getRestrictBackgroundStatus](#getrestrictbackgroundstatus)
	* [isActiveNetworkMetered](#isactivenetworkmetered)
	* [getActiveNetworkInfo](#getactivenetworkinfo)
	* [getActiveNetwork](#getactivenetwork)
	* [getNetworkCapabilities](#getnetworkcapabilities)
	* [registerDefaultNetworkCallback](#registerdefaultnetworkcallback)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**getNetworkInfo**](#getnetworkinfo)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getRestrictBackgroundStatus**](#getrestrictbackgroundstatus)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**isActiveNetworkMetered**](#isactivenetworkmetered)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getActiveNetworkInfo**](#getactivenetworkinfo)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getActiveNetwork**](#getactivenetwork)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getNetworkCapabilities**](#getnetworkcapabilities)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**registerDefaultNetworkCallback**](#registerdefaultnetworkcallback)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## getNetworkInfo

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getRestrictBackgroundStatus

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## isActiveNetworkMetered

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getActiveNetworkInfo

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getActiveNetwork

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getNetworkCapabilities

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## registerDefaultNetworkCallback

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
|2|android.permission.ACCESS_WIFI_STATE|normal|view Wi-Fi connections|Allows the app to view information about Wi-Fi networking, such as whether Wi-Fi is enabled and name of connected Wi-Fi devices.|
|3|android.permission.CHANGE_NETWORK_STATE|normal|change network connectivity|Allows the app to change the state of network connectivity.|
  
