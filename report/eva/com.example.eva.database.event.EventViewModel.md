
com.example.eva.database.event.EventViewModel
=============================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [deleteOldEvents](#deleteoldevents)
	* [get](#get)
	* [get](#get)
	* [getData](#getdata)
	* [getList](#getlist)
	* [getListBeforeLimit](#getlistbeforelimit)
	* [getListByAssociation](#getlistbyassociation)
	* [insertUpdate](#insertupdate)
	* [numberConfirmed](#numberconfirmed)
	* [remove](#remove)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**deleteOldEvents**](#deleteoldevents)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**get**](#get)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**get**](#get)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getData**](#getdata)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getList**](#getlist)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getListBeforeLimit**](#getlistbeforelimit)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getListByAssociation**](#getlistbyassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**insertUpdate**](#insertupdate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**numberConfirmed**](#numberconfirmed)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remove**](#remove)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Landroid/app/Application;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EventViewModel(android.app.Application p2)
{
    super(p2);
    androidx.lifecycle.LiveData v0_1 = new com.example.eva.database.event.EventRepository(p2);
    super.repository = v0_1;
    super.data = v0_1.getData();
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>new-instance")
	6("n°2<br/>new-instance") --> 10("n°3<br/>invoke-direct")
	10("n°3<br/>invoke-direct") --> 16("n°4<br/>iput-object")
	16("n°4<br/>iput-object") --> 20("n°5<br/>invoke-virtual")
	20("n°5<br/>invoke-virtual") --> 26("n°6<br/>move-result-object")
	26("n°6<br/>move-result-object") --> 28("n°7<br/>iput-object")
	28("n°7<br/>iput-object") --> 32("n°8<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v1, v2, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventViewModel;|Landroid/app/Application;|<span style='color:#f14848'></span>|
|**2**|6|new-instance|v0, Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-direct|v0, v2, Lcom/example/eva/database/event/EventRepository;-><init>(Landroid/app/Application;)V|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Landroid/app/Application;|<span style='color:#f14848'></span>|
|**4**|16|iput-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**5**|20|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->getData()Landroidx/lifecycle/LiveData;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**6**|26|move-result-object|v0|Landroidx/lifecycle/LiveData;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**7**|28|iput-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->data Landroidx/lifecycle/LiveData;|Landroidx/lifecycle/LiveData;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
|**8**|32|return-void||<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Landroid/app/Application;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v1, v2, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|new-instance|v0, Lcom/example/eva/database/event/EventRepository;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, v2, Lcom/example/eva/database/event/EventRepository;-><init>(Landroid/app/Application;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|iput-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->getData()Landroidx/lifecycle/LiveData;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iput-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->data Landroidx/lifecycle/LiveData;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## deleteOldEvents
  
**Signature :** `()V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void deleteOldEvents()
{
    this.repository.deleteOldEvents();
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->deleteOldEvents()V|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->deleteOldEvents()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## get
  
**Signature :** `(I)Lcom/example/eva/database/event/Event;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.event.Event get(int p2)
{
    return this.repository.getEvent(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getEvent(I)Lcom/example/eva/database/event/Event;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getEvent(I)Lcom/example/eva/database/event/Event;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## get
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/event/Event;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.event.Event get(String p2)
{
    return this.repository.getEvent(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getEvent(Ljava/lang/String;)Lcom/example/eva/database/event/Event;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getEvent(Ljava/lang/String;)Lcom/example/eva/database/event/Event;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getData
  
**Signature :** `()Landroidx/lifecycle/LiveData;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public androidx.lifecycle.LiveData getData()
{
    return this.data;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->data Landroidx/lifecycle/LiveData;|Landroidx/lifecycle/LiveData;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Landroidx/lifecycle/LiveData;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->data Landroidx/lifecycle/LiveData;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getList
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getList()
{
    return this.repository.getAllEventList();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->getAllEventList()Ljava/util/List;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Lcom/example/eva/database/event/EventRepository;->getAllEventList()Ljava/util/List;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getListBeforeLimit
  
**Signature :** `(I)Ljava/util/List;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getListBeforeLimit(int p2)
{
    return this.repository.getAllEventBeforeLimit(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getAllEventBeforeLimit(I)Ljava/util/List;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getAllEventBeforeLimit(I)Ljava/util/List;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getListByAssociation
  
**Signature :** `(Lcom/example/eva/database/association/Association;)Ljava/util/List;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getListByAssociation(com.example.eva.database.association.Association p2)
{
    return this.repository.getAllEventListByAssociation(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getAllEventListByAssociation(Lcom/example/eva/database/association/Association;)Ljava/util/List;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->getAllEventListByAssociation(Lcom/example/eva/database/association/Association;)Ljava/util/List;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## insertUpdate
  
**Signature :** `(Ljava/util/List;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void insertUpdate(java.util.List p2)
{
    this.repository.insertUpdate(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->insertUpdate(Ljava/util/List;)V|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Ljava/util/List;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->insertUpdate(Ljava/util/List;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## numberConfirmed
  
**Signature :** `(Lcom/example/eva/database/association/Association;)I`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int numberConfirmed(com.example.eva.database.association.Association p2)
{
    return this.repository.numberEventConfirmedByAssociation(p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result")
	10("n°3<br/>move-result") --> 12("n°4<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->numberEventConfirmedByAssociation(Lcom/example/eva/database/association/Association;)I|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|10|move-result|v0|int|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|12|return|v0|int|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->numberEventConfirmedByAssociation(Lcom/example/eva/database/association/Association;)I|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remove
  
**Signature :** `(Lcom/example/eva/database/event/Event;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remove(com.example.eva.database.event.Event p2)
{
    this.repository.delete(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->delete(Lcom/example/eva/database/event/Event;)V|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/event/EventViewModel;->repository Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v2, Lcom/example/eva/database/event/EventRepository;->delete(Lcom/example/eva/database/event/Event;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
