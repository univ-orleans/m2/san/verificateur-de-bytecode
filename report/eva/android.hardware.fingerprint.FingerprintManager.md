
android.hardware.fingerprint.FingerprintManager
===============================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [authenticate](#authenticate)
	* [hasEnrolledFingerprints](#hasenrolledfingerprints)
	* [isHardwareDetected](#ishardwaredetected)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**authenticate**](#authenticate)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**hasEnrolledFingerprints**](#hasenrolledfingerprints)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**isHardwareDetected**](#ishardwaredetected)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## authenticate

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.USE_FINGERPRINT|normal|use fingerprint hardware|Allows the app to use fingerprint hardware for authentication|
  

## hasEnrolledFingerprints

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.USE_FINGERPRINT|normal|use fingerprint hardware|Allows the app to use fingerprint hardware for authentication|
  

## isHardwareDetected

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.USE_FINGERPRINT|normal|use fingerprint hardware|Allows the app to use fingerprint hardware for authentication|
  
