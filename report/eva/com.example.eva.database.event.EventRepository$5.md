
com.example.eva.database.event.EventRepository$5
================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [call](#call)
	* [call](#call)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/event/EventRepository;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
EventRepository$5(com.example.eva.database.event.EventRepository p1)
{
    this.this$0 = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/event/EventRepository$5;->this$0 Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository$5;|Lcom/example/eva/database/event/EventRepository;|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/event/EventRepository$5;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/event/EventRepository$5;->this$0 Lcom/example/eva/database/event/EventRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Object;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic Object call()
{
    return this.call();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/eva/database/event/EventRepository$5;->call()Ljava/lang/Void;|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventRepository$5;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/lang/Void;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v0|Ljava/lang/Void;|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/eva/database/event/EventRepository$5;->call()Ljava/lang/Void;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Void;`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Void call()
{
    com.example.eva.database.event.EventRepository.access$000(this.this$0).deleteOldEvents(new java.util.Date().getTime());
    return 0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>new-instance")
	12("n°4<br/>new-instance") --> 16("n°5<br/>invoke-direct")
	16("n°5<br/>invoke-direct") --> 22("n°6<br/>invoke-virtual")
	22("n°6<br/>invoke-virtual") --> 28("n°7<br/>move-result-wide")
	28("n°7<br/>move-result-wide") --> 30("n°8<br/>invoke-interface")
	30("n°8<br/>invoke-interface") --> 36("n°9<br/>const/4")
	36("n°9<br/>const/4") --> 38("n°10<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/database/event/EventRepository$5;->this$0 Lcom/example/eva/database/event/EventRepository;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/event/EventRepository$5;|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/database/event/EventRepository;->access$000(Lcom/example/eva/database/event/EventRepository;)Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/event/EventRepository;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/MyDao;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**4**|12|new-instance|v1, Ljava/util/Date;|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**5**|16|invoke-direct|v1, Ljava/util/Date;-><init>()V|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**6**|22|invoke-virtual|v1, Ljava/util/Date;->getTime()J|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**7**|28|move-result-wide|v1|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|long|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**8**|30|invoke-interface|v0, v1, v2, Lcom/example/eva/database/MyDao;->deleteOldEvents(J)V|Lcom/example/eva/database/MyDao;|long|None|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**9**|36|const/4|v0, 0|int|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
|**10**|38|return-object|v0|int|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventRepository$5;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/database/event/EventRepository$5;->this$0 Lcom/example/eva/database/event/EventRepository;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/database/event/EventRepository;->access$000(Lcom/example/eva/database/event/EventRepository;)Lcom/example/eva/database/MyDao;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|new-instance|v1, Ljava/util/Date;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-direct|v1, Ljava/util/Date;-><init>()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v1, Ljava/util/Date;->getTime()J|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-wide|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-interface|v0, v1, v2, Lcom/example/eva/database/MyDao;->deleteOldEvents(J)V|True|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const/4|v0, 0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|return-object|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
