
com.example.eva.ui.event.WeekFragment$1
=======================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onChanged](#onchanged)
	* [onChanged](#onchanged)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/event/WeekFragment; Landroidx/constraintlayout/widget/ConstraintLayout; 
Landroidx/constraintlayout/widget/ConstraintLayout; Landroidx/recyclerview/widget/RecyclerView;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
WeekFragment$1(com.example.eva.ui.event.WeekFragment p1, androidx.constraintlayout.widget.ConstraintLayout p2, androidx.constraintlayout.widget.ConstraintLayout p3, androidx.recyclerview.widget.RecyclerView p4)
{
    this.this$0 = p1;
    this.val$information_no_subscribe = p2;
    this.val$information_no_event = p3;
    this.val$recyclerView = p4;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>iput-object")
	8("n°3<br/>iput-object") --> 12("n°4<br/>iput-object")
	12("n°4<br/>iput-object") --> 16("n°5<br/>invoke-direct")
	16("n°5<br/>invoke-direct") --> 22("n°6<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|Lcom/example/eva/ui/event/WeekFragment$1;|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment;</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:#f14848'></span>|
|**3**|8|iput-object|v3, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:#f14848'></span>|
|**4**|12|iput-object|v4, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:#f14848'></span>|
|**5**|16|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:#f14848'></span>|
|**6**|22|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v3, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iput-object|v4, v0, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/lang/Object;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onChanged(Object p1)
{
    this.onChanged(((java.util.List) p1));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|Ljava/util/List;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, Lcom/example/eva/ui/event/WeekFragment$1;->onChanged(Ljava/util/List;)V|Lcom/example/eva/ui/event/WeekFragment$1;|Ljava/util/List;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Ljava/util/List;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, Lcom/example/eva/ui/event/WeekFragment$1;->onChanged(Ljava/util/List;)V|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/util/List;)V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onChanged(java.util.List p6)
{
    java.util.List v0_2 = this.this$0.modelEvent.getListBeforeLimit(7);
    if (com.example.eva.ui.event.WeekFragment.access$000(this.this$0).getAssociationViewModel().getSubscriberList().size() != 0) {
        if (v0_2.size() != 0) {
            this.val$information_no_subscribe.setVisibility(4);
            this.val$information_no_event.setVisibility(4);
            this.val$recyclerView.setVisibility(0);
            this.val$recyclerView.setLayoutManager(new androidx.recyclerview.widget.LinearLayoutManager(this.val$recyclerView.getContext()));
            this.val$recyclerView.setAdapter(new com.example.eva.ui.event.EventRecyclerViewAdapter(v0_2, com.example.eva.ui.event.WeekFragment.access$000(this.this$0)));
        } else {
            this.val$information_no_subscribe.setVisibility(4);
            this.val$information_no_event.setVisibility(0);
            this.val$recyclerView.setVisibility(4);
        }
    } else {
        this.val$information_no_subscribe.setVisibility(0);
        this.val$information_no_event.setVisibility(4);
        this.val$recyclerView.setVisibility(4);
        this.val$information_no_subscribe.findViewById(2131230939).setOnClickListener(new com.example.eva.ui.event.WeekFragment$1$1(this));
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>iget-object")
	4("n°2<br/>iget-object") --> 8("n°3<br/>const/4")
	8("n°3<br/>const/4") --> 10("n°4<br/>invoke-virtual")
	10("n°4<br/>invoke-virtual") --> 16("n°5<br/>move-result-object")
	16("n°5<br/>move-result-object") --> 18("n°6<br/>iget-object")
	18("n°6<br/>iget-object") --> 22("n°7<br/>invoke-static")
	22("n°7<br/>invoke-static") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>invoke-virtual")
	30("n°9<br/>invoke-virtual") --> 36("n°10<br/>move-result-object")
	36("n°10<br/>move-result-object") --> 38("n°11<br/>invoke-virtual")
	38("n°11<br/>invoke-virtual") --> 44("n°12<br/>move-result-object")
	44("n°12<br/>move-result-object") --> 46("n°13<br/>invoke-interface")
	46("n°13<br/>invoke-interface") --> 52("n°14<br/>move-result")
	52("n°14<br/>move-result") --> 54("n°15<br/>const/4")
	54("n°15<br/>const/4") --> 56("n°16<br/>const/4")
	56("n°16<br/>const/4") --> 58("n°17<br/>if-nez")
	58("n°17<br/>if-nez") --> 62("n°18<br/>iget-object")
	58("n°17<br/>if-nez") -.-> 128("n°32<br/>invoke-interface")
	62("n°18<br/>iget-object") --> 66("n°19<br/>invoke-virtual")
	66("n°19<br/>invoke-virtual") --> 72("n°20<br/>iget-object")
	72("n°20<br/>iget-object") --> 76("n°21<br/>invoke-virtual")
	76("n°21<br/>invoke-virtual") --> 82("n°22<br/>iget-object")
	82("n°22<br/>iget-object") --> 86("n°23<br/>invoke-virtual")
	86("n°23<br/>invoke-virtual") --> 92("n°24<br/>iget-object")
	92("n°24<br/>iget-object") --> 96("n°25<br/>const")
	96("n°25<br/>const") --> 102("n°26<br/>invoke-virtual")
	102("n°26<br/>invoke-virtual") --> 108("n°27<br/>move-result-object")
	108("n°27<br/>move-result-object") --> 110("n°28<br/>new-instance")
	110("n°28<br/>new-instance") --> 114("n°29<br/>invoke-direct")
	114("n°29<br/>invoke-direct") --> 120("n°30<br/>invoke-virtual")
	120("n°30<br/>invoke-virtual") --> 126("n°31<br/>goto")
	126("n°31<br/>goto") --> 266("n°62<br/>return-void")
	128("n°32<br/>invoke-interface") --> 134("n°33<br/>move-result")
	134("n°33<br/>move-result") --> 136("n°34<br/>if-nez")
	136("n°34<br/>if-nez") --> 140("n°35<br/>iget-object")
	136("n°34<br/>if-nez") -.-> 172("n°42<br/>iget-object")
	140("n°35<br/>iget-object") --> 144("n°36<br/>invoke-virtual")
	144("n°36<br/>invoke-virtual") --> 150("n°37<br/>iget-object")
	150("n°37<br/>iget-object") --> 154("n°38<br/>invoke-virtual")
	154("n°38<br/>invoke-virtual") --> 160("n°39<br/>iget-object")
	160("n°39<br/>iget-object") --> 164("n°40<br/>invoke-virtual")
	164("n°40<br/>invoke-virtual") --> 170("n°41<br/>goto")
	170("n°41<br/>goto") --> 266("n°62<br/>return-void")
	172("n°42<br/>iget-object") --> 176("n°43<br/>invoke-virtual")
	176("n°43<br/>invoke-virtual") --> 182("n°44<br/>iget-object")
	182("n°44<br/>iget-object") --> 186("n°45<br/>invoke-virtual")
	186("n°45<br/>invoke-virtual") --> 192("n°46<br/>iget-object")
	192("n°46<br/>iget-object") --> 196("n°47<br/>invoke-virtual")
	196("n°47<br/>invoke-virtual") --> 202("n°48<br/>iget-object")
	202("n°48<br/>iget-object") --> 206("n°49<br/>new-instance")
	206("n°49<br/>new-instance") --> 210("n°50<br/>iget-object")
	210("n°50<br/>iget-object") --> 214("n°51<br/>invoke-virtual")
	214("n°51<br/>invoke-virtual") --> 220("n°52<br/>move-result-object")
	220("n°52<br/>move-result-object") --> 222("n°53<br/>invoke-direct")
	222("n°53<br/>invoke-direct") --> 228("n°54<br/>invoke-virtual")
	228("n°54<br/>invoke-virtual") --> 234("n°55<br/>iget-object")
	234("n°55<br/>iget-object") --> 238("n°56<br/>new-instance")
	238("n°56<br/>new-instance") --> 242("n°57<br/>iget-object")
	242("n°57<br/>iget-object") --> 246("n°58<br/>invoke-static")
	246("n°58<br/>invoke-static") --> 252("n°59<br/>move-result-object")
	252("n°59<br/>move-result-object") --> 254("n°60<br/>invoke-direct")
	254("n°60<br/>invoke-direct") --> 260("n°61<br/>invoke-virtual")
	260("n°61<br/>invoke-virtual") --> 266("n°62<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**2**|4|iget-object|v0, v0, Lcom/example/eva/ui/event/WeekFragment;->modelEvent Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**3**|8|const/4|v1, 7|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-virtual|v0, v1, Lcom/example/eva/database/event/EventViewModel;->getListBeforeLimit(I)Ljava/util/List;|Lcom/example/eva/database/event/EventViewModel;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**6**|18|iget-object|v1, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-static|v1, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v1|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**9**|30|invoke-virtual|v1, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**10**|36|move-result-object|v1|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**11**|38|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationViewModel;->getSubscriberList()Ljava/util/List;|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**12**|44|move-result-object|v1|<span style='color:grey'>Ljava/util/List;</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**13**|46|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>Ljava/util/List;</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**14**|52|move-result|v1|<span style='color:grey'>Ljava/util/List;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**15**|54|const/4|v2, 0|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**16**|56|const/4|v3, 4|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**17**|58|if-nez|v1, +23|<span style='color:grey'>Ljava/util/List;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**18**|62|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**19**|66|invoke-virtual|v4, v2, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**20**|72|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**21**|76|invoke-virtual|v2, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|int|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**22**|82|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**23**|86|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|int|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**24**|92|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**25**|96|const|v3, 2131230939 # [1.8077944927445176e+38]|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|int|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**26**|102|invoke-virtual|v2, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->findViewById(I)Landroid/view/View;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|int|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**27**|108|move-result-object|v2|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**28**|110|new-instance|v3, Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**29**|114|invoke-direct|v3, v5, Lcom/example/eva/ui/event/WeekFragment$1$1;-><init>(Lcom/example/eva/ui/event/WeekFragment$1;)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**30**|120|invoke-virtual|v2, v3, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroid/view/View;|Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**31**|126|goto|+46|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**32**|128|invoke-interface|v0, Ljava/util/List;->size()I|Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**33**|134|move-result|v4|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**34**|136|if-nez|v4, +12|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**35**|140|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**36**|144|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**37**|150|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**38**|154|invoke-virtual|v4, v2, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**39**|160|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**40**|164|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|int|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**41**|170|goto|+30|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**42**|172|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**43**|176|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**44**|182|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**45**|186|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**46**|192|iget-object|v3, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**47**|196|invoke-virtual|v3, v2, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**48**|202|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**49**|206|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**50**|210|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Landroidx/recyclerview/widget/RecyclerView;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**51**|214|invoke-virtual|v4, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**52**|220|move-result-object|v4|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Landroid/content/Context;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**53**|222|invoke-direct|v3, v4, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|Landroid/content/Context;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**54**|228|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**55**|234|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|<span style='color:grey'>Landroid/content/Context;</span>|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**56**|238|new-instance|v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**57**|242|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/ui/event/WeekFragment;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**58**|246|invoke-static|v4, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**59**|252|move-result-object|v4|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**60**|254|invoke-direct|v3, v0, v4, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;-><init>(Ljava/util/List; Lcom/example/eva/Environment;)V|Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**61**|260|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|Landroidx/recyclerview/widget/RecyclerView;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**62**|266|return-void||<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v0, v0, Lcom/example/eva/ui/event/WeekFragment;->modelEvent Lcom/example/eva/database/event/EventViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const/4|v1, 7|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, v1, Lcom/example/eva/database/event/EventViewModel;->getListBeforeLimit(I)Ljava/util/List;|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-static|v1, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v1, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationViewModel;->getSubscriberList()Ljava/util/List;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|const/4|v3, 4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|if-nez|v1, +23|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v4, v2, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-virtual|v2, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|const|v3, 2131230939 # [1.8077944927445176e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|invoke-virtual|v2, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->findViewById(I)Landroid/view/View;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|new-instance|v3, Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|invoke-direct|v3, v5, Lcom/example/eva/ui/event/WeekFragment$1$1;-><init>(Lcom/example/eva/ui/event/WeekFragment$1;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-virtual|v2, v3, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|goto|+46|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|invoke-interface|v0, Ljava/util/List;->size()I|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|move-result|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|if-nez|v4, +12|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**35**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**37**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**38**|invoke-virtual|v4, v2, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**39**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**40**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**41**|goto|+30|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**42**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_subscribe Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**43**|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**44**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$information_no_event Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**45**|invoke-virtual|v4, v3, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**46**|iget-object|v3, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**47**|invoke-virtual|v3, v2, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**48**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**49**|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**50**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**51**|invoke-virtual|v4, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**52**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**53**|invoke-direct|v3, v4, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**54**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**55**|iget-object|v2, v5, Lcom/example/eva/ui/event/WeekFragment$1;->val$recyclerView Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**56**|new-instance|v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**57**|iget-object|v4, v5, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**58**|invoke-static|v4, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**59**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**60**|invoke-direct|v3, v0, v4, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;-><init>(Ljava/util/List; Lcom/example/eva/Environment;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**61**|invoke-virtual|v2, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**62**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
