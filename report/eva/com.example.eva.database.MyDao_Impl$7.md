
com.example.eva.database.MyDao_Impl$7
=====================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [createQuery](#createquery)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createQuery**](#createquery)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
MyDao_Impl$7(com.example.eva.database.MyDao_Impl p1, androidx.room.RoomDatabase p2)
{
    this.this$0 = p1;
    super(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$7;->this$0 Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/MyDao_Impl$7;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, v2, Landroidx/room/SharedSQLiteStatement;-><init>(Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$7;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$7;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$7;->this$0 Lcom/example/eva/database/MyDao_Impl;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, v2, Landroidx/room/SharedSQLiteStatement;-><init>(Landroidx/room/RoomDatabase;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## createQuery
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String createQuery()
{
    return "DELETE FROM event_table WHERE dateEnd < ? AND dateUpdated < ?";
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>const-string")
	4("n°2<br/>const-string") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'DELETE FROM event_table WHERE dateEnd < ? AND dateUpdated < ?'|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$7;</span>|<span style='color:#f14848'></span>|
|**2**|4|const-string|v1, 'DELETE FROM event_table WHERE dateEnd < ? AND dateUpdated < ?'|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$7;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v1|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$7;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'DELETE FROM event_table WHERE dateEnd < ? AND dateUpdated < ?'|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const-string|v1, 'DELETE FROM event_table WHERE dateEnd < ? AND dateUpdated < ?'|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
