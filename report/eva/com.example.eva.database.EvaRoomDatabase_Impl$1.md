
com.example.eva.database.EvaRoomDatabase_Impl$1
===============================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [createAllTables](#createalltables)
	* [dropAllTables](#dropalltables)
	* [onCreate](#oncreate)
	* [onOpen](#onopen)
	* [onPostMigrate](#onpostmigrate)
	* [onPreMigrate](#onpremigrate)
	* [onValidateSchema](#onvalidateschema)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createAllTables**](#createalltables)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**dropAllTables**](#dropalltables)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onOpen**](#onopen)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onPostMigrate**](#onpostmigrate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onPreMigrate**](#onpremigrate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onValidateSchema**](#onvalidateschema)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl; I)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
EvaRoomDatabase_Impl$1(com.example.eva.database.EvaRoomDatabase_Impl p1, int p2)
{
    this.this$0 = p1;
    super(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, v2, Landroidx/room/RoomOpenHelper$Delegate;-><init>(I)V|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, v2, Landroidx/room/RoomOpenHelper$Delegate;-><init>(I)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## createAllTables
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void createAllTables(androidx.sqlite.db.SupportSQLiteDatabase p2)
{
    p2.execSQL("CREATE TABLE IF NOT EXISTS `association_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idGoogleCalendar` TEXT NOT NULL, `name` TEXT NOT NULL, `logo` INTEGER, `subscribe` INTEGER NOT NULL)");
    p2.execSQL("CREATE TABLE IF NOT EXISTS `event_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idAssociation` INTEGER NOT NULL, `idGoogleEvent` TEXT NOT NULL, `summary` TEXT NOT NULL, `canceled` INTEGER NOT NULL, `dateStart` INTEGER NOT NULL, `dateEnd` INTEGER NOT NULL, `dateUpdated` INTEGER NOT NULL, `location` TEXT, `description` TEXT, FOREIGN KEY(`idAssociation`) REFERENCES `association_table`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");
    p2.execSQL("CREATE INDEX IF NOT EXISTS `association_index` ON `event_table` (`idAssociation`)");
    p2.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    p2.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \'deff8f62b11b3c219184a63f7488c87e\')");
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>const-string")
	10("n°3<br/>const-string") --> 14("n°4<br/>invoke-interface")
	14("n°4<br/>invoke-interface") --> 20("n°5<br/>const-string")
	20("n°5<br/>const-string") --> 24("n°6<br/>invoke-interface")
	24("n°6<br/>invoke-interface") --> 30("n°7<br/>const-string")
	30("n°7<br/>const-string") --> 34("n°8<br/>invoke-interface")
	34("n°8<br/>invoke-interface") --> 40("n°9<br/>const-string")
	40("n°9<br/>const-string") --> 44("n°10<br/>invoke-interface")
	44("n°10<br/>invoke-interface") --> 50("n°11<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'CREATE TABLE IF NOT EXISTS `association_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idGoogleCalendar` TEXT NOT NULL, `name` TEXT NOT NULL, `logo` INTEGER, `subscribe` INTEGER NOT NULL)'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**3**|10|const-string|v0, 'CREATE TABLE IF NOT EXISTS `event_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idAssociation` INTEGER NOT NULL, `idGoogleEvent` TEXT NOT NULL, `summary` TEXT NOT NULL, `canceled` INTEGER NOT NULL, `dateStart` INTEGER NOT NULL, `dateEnd` INTEGER NOT NULL, `dateUpdated` INTEGER NOT NULL, `location` TEXT, `description` TEXT, FOREIGN KEY(`idAssociation`) REFERENCES `association_table`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**5**|20|const-string|v0, 'CREATE INDEX IF NOT EXISTS `association_index` ON `event_table` (`idAssociation`)'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**7**|30|const-string|v0, 'CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**9**|40|const-string|v0, "INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'deff8f62b11b3c219184a63f7488c87e')"|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**11**|50|return-void||<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'CREATE TABLE IF NOT EXISTS `association_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idGoogleCalendar` TEXT NOT NULL, `name` TEXT NOT NULL, `logo` INTEGER, `subscribe` INTEGER NOT NULL)'|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|const-string|v0, 'CREATE TABLE IF NOT EXISTS `event_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `idAssociation` INTEGER NOT NULL, `idGoogleEvent` TEXT NOT NULL, `summary` TEXT NOT NULL, `canceled` INTEGER NOT NULL, `dateStart` INTEGER NOT NULL, `dateEnd` INTEGER NOT NULL, `dateUpdated` INTEGER NOT NULL, `location` TEXT, `description` TEXT, FOREIGN KEY(`idAssociation`) REFERENCES `association_table`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )'|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|const-string|v0, 'CREATE INDEX IF NOT EXISTS `association_index` ON `event_table` (`idAssociation`)'|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|const-string|v0, 'CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)'|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**9**|const-string|v0, "INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'deff8f62b11b3c219184a63f7488c87e')"|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-interface|v2, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**11**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## dropAllTables
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void dropAllTables(androidx.sqlite.db.SupportSQLiteDatabase p4)
{
    p4.execSQL("DROP TABLE IF EXISTS `association_table`");
    p4.execSQL("DROP TABLE IF EXISTS `event_table`");
    if (com.example.eva.database.EvaRoomDatabase_Impl.access$000(this.this$0) != null) {
        int v0_2 = 0;
        int v1_1 = com.example.eva.database.EvaRoomDatabase_Impl.access$100(this.this$0).size();
        while (v0_2 < v1_1) {
            ((androidx.room.RoomDatabase$Callback) com.example.eva.database.EvaRoomDatabase_Impl.access$200(this.this$0).get(v0_2)).onDestructiveMigration(p4);
            v0_2++;
        }
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>const-string")
	10("n°3<br/>const-string") --> 14("n°4<br/>invoke-interface")
	14("n°4<br/>invoke-interface") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-static")
	24("n°6<br/>invoke-static") --> 30("n°7<br/>move-result-object")
	30("n°7<br/>move-result-object") --> 32("n°8<br/>if-eqz")
	32("n°8<br/>if-eqz") --> 36("n°9<br/>const/4")
	32("n°8<br/>if-eqz") -.-> 98("n°25<br/>return-void")
	36("n°9<br/>const/4") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>invoke-static")
	42("n°11<br/>invoke-static") --> 48("n°12<br/>move-result-object")
	48("n°12<br/>move-result-object") --> 50("n°13<br/>invoke-interface")
	50("n°13<br/>invoke-interface") --> 56("n°14<br/>move-result")
	56("n°14<br/>move-result") --> 58("n°15<br/>if-ge")
	58("n°15<br/>if-ge") --> 62("n°16<br/>iget-object")
	58("n°15<br/>if-ge") -.-> 98("n°25<br/>return-void")
	62("n°16<br/>iget-object") --> 66("n°17<br/>invoke-static")
	66("n°17<br/>invoke-static") --> 72("n°18<br/>move-result-object")
	72("n°18<br/>move-result-object") --> 74("n°19<br/>invoke-interface")
	74("n°19<br/>invoke-interface") --> 80("n°20<br/>move-result-object")
	80("n°20<br/>move-result-object") --> 82("n°21<br/>check-cast")
	82("n°21<br/>check-cast") --> 86("n°22<br/>invoke-virtual")
	86("n°22<br/>invoke-virtual") --> 92("n°23<br/>add-int/lit8")
	92("n°23<br/>add-int/lit8") --> 96("n°24<br/>goto")
	96("n°24<br/>goto") --> 58("n°15<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'DROP TABLE IF EXISTS `association_table`'|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**3**|10|const-string|v0, 'DROP TABLE IF EXISTS `event_table`'|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$000(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**7**|30|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**8**|32|if-eqz|v0, +21|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**9**|36|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$100(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**12**|48|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**13**|50|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**14**|56|move-result|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**15**|58|if-ge|v0, v1, +14|int|int|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**16**|62|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**17**|66|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$200(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**18**|72|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**19**|74|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|int|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**20**|80|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/lang/Object;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**21**|82|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**22**|86|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onDestructiveMigration(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**23**|92|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**24**|96|goto|-13|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**25**|98|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'DROP TABLE IF EXISTS `association_table`'|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|const-string|v0, 'DROP TABLE IF EXISTS `event_table`'|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$000(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|if-eqz|v0, +21|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$100(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|if-ge|v0, v1, +14|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$200(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onDestructiveMigration(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**23**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|goto|-13|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void onCreate(androidx.sqlite.db.SupportSQLiteDatabase p4)
{
    if (com.example.eva.database.EvaRoomDatabase_Impl.access$300(this.this$0) != null) {
        int v0_1 = 0;
        int v1_2 = com.example.eva.database.EvaRoomDatabase_Impl.access$400(this.this$0).size();
        while (v0_1 < v1_2) {
            ((androidx.room.RoomDatabase$Callback) com.example.eva.database.EvaRoomDatabase_Impl.access$500(this.this$0).get(v0_1)).onCreate(p4);
            v0_1++;
        }
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>if-eqz")
	12("n°4<br/>if-eqz") --> 16("n°5<br/>const/4")
	12("n°4<br/>if-eqz") -.-> 78("n°21<br/>return-void")
	16("n°5<br/>const/4") --> 18("n°6<br/>iget-object")
	18("n°6<br/>iget-object") --> 22("n°7<br/>invoke-static")
	22("n°7<br/>invoke-static") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>invoke-interface")
	30("n°9<br/>invoke-interface") --> 36("n°10<br/>move-result")
	36("n°10<br/>move-result") --> 38("n°11<br/>if-ge")
	38("n°11<br/>if-ge") --> 42("n°12<br/>iget-object")
	38("n°11<br/>if-ge") -.-> 78("n°21<br/>return-void")
	42("n°12<br/>iget-object") --> 46("n°13<br/>invoke-static")
	46("n°13<br/>invoke-static") --> 52("n°14<br/>move-result-object")
	52("n°14<br/>move-result-object") --> 54("n°15<br/>invoke-interface")
	54("n°15<br/>invoke-interface") --> 60("n°16<br/>move-result-object")
	60("n°16<br/>move-result-object") --> 62("n°17<br/>check-cast")
	62("n°17<br/>check-cast") --> 66("n°18<br/>invoke-virtual")
	66("n°18<br/>invoke-virtual") --> 72("n°19<br/>add-int/lit8")
	72("n°19<br/>add-int/lit8") --> 76("n°20<br/>goto")
	76("n°20<br/>goto") --> 38("n°11<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$300(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**4**|12|if-eqz|v0, +21|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**5**|16|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**6**|18|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$400(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**9**|30|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**10**|36|move-result|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**11**|38|if-ge|v0, v1, +14|int|int|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**12**|42|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**13**|46|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$500(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**14**|52|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**15**|54|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|int|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**16**|60|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/lang/Object;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**17**|62|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**18**|66|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onCreate(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**19**|72|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**20**|76|goto|-13|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**21**|78|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$300(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-eqz|v0, +21|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$400(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|if-ge|v0, v1, +14|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$500(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onCreate(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**19**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|goto|-13|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onOpen
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onOpen(androidx.sqlite.db.SupportSQLiteDatabase p4)
{
    com.example.eva.database.EvaRoomDatabase_Impl.access$602(this.this$0, p4);
    p4.execSQL("PRAGMA foreign_keys = ON");
    com.example.eva.database.EvaRoomDatabase_Impl.access$700(this.this$0, p4);
    if (com.example.eva.database.EvaRoomDatabase_Impl.access$800(this.this$0) != null) {
        int v0_1 = 0;
        int v1_2 = com.example.eva.database.EvaRoomDatabase_Impl.access$900(this.this$0).size();
        while (v0_1 < v1_2) {
            ((androidx.room.RoomDatabase$Callback) com.example.eva.database.EvaRoomDatabase_Impl.access$1000(this.this$0).get(v0_1)).onOpen(p4);
            v0_1++;
        }
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>const-string")
	10("n°3<br/>const-string") --> 14("n°4<br/>invoke-interface")
	14("n°4<br/>invoke-interface") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-static")
	24("n°6<br/>invoke-static") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-static")
	34("n°8<br/>invoke-static") --> 40("n°9<br/>move-result-object")
	40("n°9<br/>move-result-object") --> 42("n°10<br/>if-eqz")
	42("n°10<br/>if-eqz") --> 46("n°11<br/>const/4")
	42("n°10<br/>if-eqz") -.-> 108("n°27<br/>return-void")
	46("n°11<br/>const/4") --> 48("n°12<br/>iget-object")
	48("n°12<br/>iget-object") --> 52("n°13<br/>invoke-static")
	52("n°13<br/>invoke-static") --> 58("n°14<br/>move-result-object")
	58("n°14<br/>move-result-object") --> 60("n°15<br/>invoke-interface")
	60("n°15<br/>invoke-interface") --> 66("n°16<br/>move-result")
	66("n°16<br/>move-result") --> 68("n°17<br/>if-ge")
	68("n°17<br/>if-ge") --> 72("n°18<br/>iget-object")
	68("n°17<br/>if-ge") -.-> 108("n°27<br/>return-void")
	72("n°18<br/>iget-object") --> 76("n°19<br/>invoke-static")
	76("n°19<br/>invoke-static") --> 82("n°20<br/>move-result-object")
	82("n°20<br/>move-result-object") --> 84("n°21<br/>invoke-interface")
	84("n°21<br/>invoke-interface") --> 90("n°22<br/>move-result-object")
	90("n°22<br/>move-result-object") --> 92("n°23<br/>check-cast")
	92("n°23<br/>check-cast") --> 96("n°24<br/>invoke-virtual")
	96("n°24<br/>invoke-virtual") --> 102("n°25<br/>add-int/lit8")
	102("n°25<br/>add-int/lit8") --> 106("n°26<br/>goto")
	106("n°26<br/>goto") --> 68("n°17<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, v4, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$602(Lcom/example/eva/database/EvaRoomDatabase_Impl; Landroidx/sqlite/db/SupportSQLiteDatabase;)Landroidx/sqlite/db/SupportSQLiteDatabase;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**3**|10|const-string|v0, 'PRAGMA foreign_keys = ON'|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-static|v0, v4, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$700(Lcom/example/eva/database/EvaRoomDatabase_Impl; Landroidx/sqlite/db/SupportSQLiteDatabase;)V|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$800(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**9**|40|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**10**|42|if-eqz|v0, +21|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**11**|46|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**12**|48|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**13**|52|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$900(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**14**|58|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**16**|66|move-result|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**17**|68|if-ge|v0, v1, +14|int|int|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**18**|72|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**19**|76|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$1000(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**20**|82|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**21**|84|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|int|<span style='color:grey'>int</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**22**|90|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Ljava/lang/Object;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**23**|92|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**24**|96|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onOpen(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**25**|102|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**26**|106|goto|-13|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase$Callback;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
|**27**|108|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, v4, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$602(Lcom/example/eva/database/EvaRoomDatabase_Impl; Landroidx/sqlite/db/SupportSQLiteDatabase;)Landroidx/sqlite/db/SupportSQLiteDatabase;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|const-string|v0, 'PRAGMA foreign_keys = ON'|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v4, v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-static|v0, v4, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$700(Lcom/example/eva/database/EvaRoomDatabase_Impl; Landroidx/sqlite/db/SupportSQLiteDatabase;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-static|v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$800(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|if-eqz|v0, +21|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iget-object|v1, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-static|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$900(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-interface|v1, Ljava/util/List;->size()I|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|move-result|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|if-ge|v0, v1, +14|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|iget-object|v2, v3, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;->this$0 Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-static|v2, Lcom/example/eva/database/EvaRoomDatabase_Impl;->access$1000(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-interface|v2, v0, Ljava/util/List;->get(I)Ljava/lang/Object;|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|check-cast|v2, Landroidx/room/RoomDatabase$Callback;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v2, v4, Landroidx/room/RoomDatabase$Callback;->onOpen(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**25**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|goto|-13|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onPostMigrate
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onPostMigrate(androidx.sqlite.db.SupportSQLiteDatabase p1)
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;


```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onPreMigrate
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onPreMigrate(androidx.sqlite.db.SupportSQLiteDatabase p1)
{
    androidx.room.util.DBUtil.dropFtsSyncTriggers(p1);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-static") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-static|v1, Landroidx/room/util/DBUtil;->dropFtsSyncTriggers(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-static|v1, Landroidx/room/util/DBUtil;->dropFtsSyncTriggers(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onValidateSchema
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteDatabase;)Landroidx/room/RoomOpenHelper$ValidationResult;`  
**Nombre de registre :** 27  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected androidx.room.RoomOpenHelper$ValidationResult onValidateSchema(androidx.sqlite.db.SupportSQLiteDatabase p26)
{
    java.util.HashMap v1_1 = new java.util.HashMap(5);
    androidx.room.RoomOpenHelper$ValidationResult v9_4 = new androidx.room.util.TableInfo$Column;
    v9_4("id", "INTEGER", 1, 1, 0, 1);
    v1_1.put("id", v9_4);
    java.util.HashSet v3_0 = new androidx.room.util.TableInfo$Column;
    v3_0("idGoogleCalendar", "TEXT", 1, 0, 0, 1);
    v1_1.put("idGoogleCalendar", v3_0);
    java.util.HashSet v3_1 = new androidx.room.util.TableInfo$Column;
    v3_1("name", "TEXT", 1, 0, 0, 1);
    v1_1.put("name", v3_1);
    java.util.HashSet v3_2 = new androidx.room.util.TableInfo$Column;
    v3_2("logo", "INTEGER", 0, 0, 0, 1);
    v1_1.put("logo", v3_2);
    java.util.HashSet v3_3 = new androidx.room.util.TableInfo$Column;
    v3_3("subscribe", "INTEGER", 1, 0, 0, 1);
    v1_1.put("subscribe", v3_3);
    androidx.room.util.TableInfo v6_4 = new androidx.room.util.TableInfo("association_table", v1_1, new java.util.HashSet(0), new java.util.HashSet(0));
    androidx.room.util.TableInfo v7_4 = androidx.room.util.TableInfo.read(p26, "association_table");
    if (v6_4.equals(v7_4)) {
        java.util.HashMap v8_8 = new java.util.HashMap(10);
        String v15_3 = new androidx.room.util.TableInfo$Column;
        java.util.HashSet v4_15 = v15_3;
        v15_3("id", "INTEGER", 1, 1, 0, 1);
        v8_8.put("id", v4_15);
        java.util.HashSet v4_16 = new androidx.room.util.TableInfo$Column;
        v4_16("idAssociation", "INTEGER", 1, 0, 0, 1);
        v8_8.put("idAssociation", v4_16);
        java.util.HashSet v4_17 = new androidx.room.util.TableInfo$Column;
        v4_17("idGoogleEvent", "TEXT", 1, 0, 0, 1);
        v8_8.put("idGoogleEvent", v4_17);
        java.util.HashSet v4_0 = new androidx.room.util.TableInfo$Column;
        v4_0("summary", "TEXT", 1, 0, 0, 1);
        v8_8.put("summary", v4_0);
        java.util.HashSet v4_1 = new androidx.room.util.TableInfo$Column;
        v4_1("canceled", "INTEGER", 1, 0, 0, 1);
        v8_8.put("canceled", v4_1);
        java.util.HashSet v4_2 = new androidx.room.util.TableInfo$Column;
        v4_2("dateStart", "INTEGER", 1, 0, 0, 1);
        v8_8.put("dateStart", v4_2);
        java.util.HashSet v4_3 = new androidx.room.util.TableInfo$Column;
        v4_3("dateEnd", "INTEGER", 1, 0, 0, 1);
        v8_8.put("dateEnd", v4_3);
        java.util.HashSet v4_4 = new androidx.room.util.TableInfo$Column;
        v4_4("dateUpdated", "INTEGER", 1, 0, 0, 1);
        v8_8.put("dateUpdated", v4_4);
        java.util.HashSet v4_5 = new androidx.room.util.TableInfo$Column;
        v4_5("location", "TEXT", 0, 0, 0, 1);
        v8_8.put("location", v4_5);
        java.util.HashSet v4_6 = new androidx.room.util.TableInfo$Column;
        v4_6("description", "TEXT", 0, 0, 0, 1);
        v8_8.put("description", v4_6);
        java.util.HashSet v4_8 = new java.util.HashSet(1);
        String v15_0 = new androidx.room.util.TableInfo$ForeignKey;
        androidx.room.util.TableInfo v11_10 = v15_0;
        v15_0("association_table", "CASCADE", "NO ACTION", java.util.Arrays.asList(new String[] {"idAssociation"})), java.util.Arrays.asList(new String[] {"id"})));
        v4_8.add(v11_10);
        java.util.HashSet v2_5 = new java.util.HashSet(1);
        v2_5.add(new androidx.room.util.TableInfo$Index("association_index", 0, java.util.Arrays.asList(new String[] {"idAssociation"}))));
        androidx.room.util.TableInfo v10_3 = new androidx.room.util.TableInfo("event_table", v8_8, v4_8, v2_5);
        androidx.room.util.TableInfo v11_15 = androidx.room.util.TableInfo.read(p26, "event_table");
        if (v10_3.equals(v11_15)) {
            return new androidx.room.RoomOpenHelper$ValidationResult(1, 0);
        } else {
            int v13_5 = new StringBuilder();
            v13_5.append("event_table(com.example.eva.database.event.Event).\n Expected:\n");
            v13_5.append(v10_3);
            v13_5.append("\n Found:\n");
            v13_5.append(v11_15);
            return new androidx.room.RoomOpenHelper$ValidationResult(0, v13_5.toString());
        }
    } else {
        java.util.HashMap v8_2 = new StringBuilder();
        v8_2.append("association_table(com.example.eva.database.association.Association).\n Expected:\n");
        v8_2.append(v6_4);
        v8_2.append("\n Found:\n");
        v8_2.append(v7_4);
        return new androidx.room.RoomOpenHelper$ValidationResult(0, v8_2.toString());
    }
}
```