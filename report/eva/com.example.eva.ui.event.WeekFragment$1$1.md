
com.example.eva.ui.event.WeekFragment$1$1
=========================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onClick](#onclick)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onClick**](#onclick)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/event/WeekFragment$1;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
WeekFragment$1$1(com.example.eva.ui.event.WeekFragment$1 p1)
{
    this.this$1 = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/event/WeekFragment$1$1;->this$1 Lcom/example/eva/ui/event/WeekFragment$1;|Lcom/example/eva/ui/event/WeekFragment$1$1;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/event/WeekFragment$1$1;->this$1 Lcom/example/eva/ui/event/WeekFragment$1;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onClick
  
**Signature :** `(Landroid/view/View;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onClick(android.view.View p4)
{
    ((androidx.navigation.fragment.NavHostFragment) java.util.Objects.requireNonNull(((androidx.navigation.fragment.NavHostFragment) com.example.eva.ui.event.WeekFragment.access$000(this.this$1.this$0).getSupportFragmentManager().findFragmentById(2131230993)))).getNavController().navigate(2131230996);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>iget-object")
	4("n°2<br/>iget-object") --> 8("n°3<br/>invoke-static")
	8("n°3<br/>invoke-static") --> 14("n°4<br/>move-result-object")
	14("n°4<br/>move-result-object") --> 16("n°5<br/>invoke-virtual")
	16("n°5<br/>invoke-virtual") --> 22("n°6<br/>move-result-object")
	22("n°6<br/>move-result-object") --> 24("n°7<br/>const")
	24("n°7<br/>const") --> 30("n°8<br/>invoke-virtual")
	30("n°8<br/>invoke-virtual") --> 36("n°9<br/>move-result-object")
	36("n°9<br/>move-result-object") --> 38("n°10<br/>check-cast")
	38("n°10<br/>check-cast") --> 42("n°11<br/>invoke-static")
	42("n°11<br/>invoke-static") --> 48("n°12<br/>move-result-object")
	48("n°12<br/>move-result-object") --> 50("n°13<br/>check-cast")
	50("n°13<br/>check-cast") --> 54("n°14<br/>invoke-virtual")
	54("n°14<br/>invoke-virtual") --> 60("n°15<br/>move-result-object")
	60("n°15<br/>move-result-object") --> 62("n°16<br/>const")
	62("n°16<br/>const") --> 68("n°17<br/>invoke-virtual")
	68("n°17<br/>invoke-virtual") --> 74("n°18<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/ui/event/WeekFragment$1$1;->this$1 Lcom/example/eva/ui/event/WeekFragment$1;|Lcom/example/eva/ui/event/WeekFragment$1;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/event/WeekFragment$1$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**2**|4|iget-object|v0, v0, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-static|v0, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|Lcom/example/eva/ui/event/WeekFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**5**|16|invoke-virtual|v0, Lcom/example/eva/Environment;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**6**|22|move-result-object|v0|Landroidx/fragment/app/FragmentManager;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**7**|24|const|v1, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>Landroidx/fragment/app/FragmentManager;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**8**|30|invoke-virtual|v0, v1, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|Landroidx/fragment/app/FragmentManager;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**9**|36|move-result-object|v0|Landroidx/fragment/app/Fragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**10**|38|check-cast|v0, Landroidx/navigation/fragment/NavHostFragment;|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**12**|48|move-result-object|v1|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**13**|50|check-cast|v1, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**14**|54|invoke-virtual|v1, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**15**|60|move-result-object|v1|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**16**|62|const|v2, 2131230996 # [1.8078060537179917e+38]|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**17**|68|invoke-virtual|v1, v2, Landroidx/navigation/NavController;->navigate(I)V|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|int|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**18**|74|return-void||<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/event/WeekFragment$1$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/ui/event/WeekFragment$1$1;->this$1 Lcom/example/eva/ui/event/WeekFragment$1;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v0, v0, Lcom/example/eva/ui/event/WeekFragment$1;->this$0 Lcom/example/eva/ui/event/WeekFragment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-static|v0, Lcom/example/eva/ui/event/WeekFragment;->access$000(Lcom/example/eva/ui/event/WeekFragment;)Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, Lcom/example/eva/Environment;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const|v1, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, v1, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|check-cast|v0, Landroidx/navigation/fragment/NavHostFragment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|check-cast|v1, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v1, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|const|v2, 2131230996 # [1.8078060537179917e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-virtual|v1, v2, Landroidx/navigation/NavController;->navigate(I)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
