
com.example.eva.ui.association.ShortAssociationFragment
=======================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [access$000](#access000)
	* [onCreate](#oncreate)
	* [onCreateView](#oncreateview)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$000**](#access000)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateView**](#oncreateview)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public ShortAssociationFragment()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Landroidx/fragment/app/Fragment;-><init>()V|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Landroidx/fragment/app/Fragment;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$000
  
**Signature :** `(Lcom/example/eva/ui/association/ShortAssociationFragment;)Lcom/example/eva/Environment;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic com.example.eva.Environment access$000(com.example.eva.ui.association.ShortAssociationFragment p1)
{
    return p1.activity;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationFragment;->activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/ui/association/ShortAssociationFragment;->activity Lcom/example/eva/Environment;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onCreate(android.os.Bundle p3)
{
    super.onCreate(p3);
    com.example.eva.Environment v0_2 = ((com.example.eva.Environment) this.requireParentFragment().getActivity());
    this.columnCount = 2;
    this.activity = v0_2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>invoke-virtual")
	6("n°2<br/>invoke-virtual") --> 12("n°3<br/>move-result-object")
	12("n°3<br/>move-result-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>check-cast")
	22("n°6<br/>check-cast") --> 26("n°7<br/>const/4")
	26("n°7<br/>const/4") --> 28("n°8<br/>iput")
	28("n°8<br/>iput") --> 32("n°9<br/>iput-object")
	32("n°9<br/>iput-object") --> 36("n°10<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v2, v3, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationFragment;|Landroid/os/Bundle;|<span style='color:#f14848'></span>|
|**2**|6|invoke-virtual|v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->requireParentFragment()Landroidx/fragment/app/Fragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|12|move-result-object|v0|Landroidx/fragment/app/Fragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;|Landroidx/fragment/app/Fragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v0|Landroidx/fragment/app/FragmentActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|22|check-cast|v0, Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|26|const/4|v1, 2|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|28|iput|v1, v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->columnCount I|<span style='color:grey'>Lcom/example/eva/Environment;</span>|int|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**9**|32|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->activity Lcom/example/eva/Environment;|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**10**|36|return-void||<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v2, v3, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->requireParentFragment()Landroidx/fragment/app/Fragment;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|check-cast|v0, Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const/4|v1, 2|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|iput|v1, v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->columnCount I|<span style='color:grey'>True</span>|False|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iput-object|v0, v2, Lcom/example/eva/ui/association/ShortAssociationFragment;->activity Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateView
  
**Signature :** `(Landroid/view/LayoutInflater; Landroid/view/ViewGroup; Landroid/os/Bundle;)Landroid/view/View;`  
**Nombre de registre :** 13  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public android.view.View onCreateView(android.view.LayoutInflater p10, android.view.ViewGroup p11, android.os.Bundle p12)
{
    android.view.View v0_1 = p10.inflate(2131427388, p11, 0);
    androidx.recyclerview.widget.RecyclerView v2_2 = ((androidx.recyclerview.widget.RecyclerView) v0_1.findViewById(2131230952));
    androidx.constraintlayout.widget.ConstraintLayout v3_1 = ((androidx.constraintlayout.widget.ConstraintLayout) v0_1.findViewById(2131230938));
    v3_1.findViewById(2131230939).setOnClickListener(new com.example.eva.ui.association.ShortAssociationFragment$1(this));
    if (android.os.Build$VERSION.SDK_INT < 30) {
        java.util.List v4_4 = new android.graphics.Point();
        this.activity.getWindowManager().getDefaultDisplay().getSize(v4_4);
        this.columnCount = ((int) Math.ceil((((double) v4_4.x) / 360.0)));
    } else {
        this.columnCount = ((int) Math.ceil((((double) this.activity.getWindowManager().getCurrentWindowMetrics().getBounds().width()) / 360.0)));
    }
    java.util.List v4_12 = this.activity.getAssociationViewModel().getSubscriberList();
    if (v4_12.size() != 0) {
        v3_1.setVisibility(4);
        v2_2.setVisibility(0);
        v2_2.setLayoutManager(new androidx.recyclerview.widget.GridLayoutManager(v2_2.getContext(), this.columnCount));
        v2_2.setAdapter(new com.example.eva.ui.association.ShortAssociationRecyclerViewAdapter(v4_12, this.activity));
    } else {
        v3_1.setVisibility(0);
        v2_2.setVisibility(4);
    }
    return v0_1;
}
```