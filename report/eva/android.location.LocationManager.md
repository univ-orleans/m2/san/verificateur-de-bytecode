
android.location.LocationManager
================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [getLastKnownLocation](#getlastknownlocation)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**getLastKnownLocation**](#getlastknownlocation)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## getLastKnownLocation

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_COARSE_LOCATION|dangerous|access approximate location (network-based)|Allows the app to get your approximate location. This location is derived by location services using network location sources such as cell towers and Wi-Fi. These location services must be turned on and available to your device for the app to use them. Apps may use this to determine approximately where you are.|
|2|android.permission.ACCESS_FINE_LOCATION|dangerous|access precise location (GPS and network-based)|Allows the app to get your precise location using the Global Positioning System (GPS) or network location sources such as cell towers and Wi-Fi. These location services must be turned on and available to your device for the app to use them. Apps may use this to determine where you are, and may consume additional battery power.|
  
