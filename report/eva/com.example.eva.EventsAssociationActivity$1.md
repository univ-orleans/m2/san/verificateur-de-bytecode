
com.example.eva.EventsAssociationActivity$1
===========================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onChanged](#onchanged)
	* [onChanged](#onchanged)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onChanged**](#onchanged)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/EventsAssociationActivity; Lcom/example/eva/database/association/Association; 
Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
EventsAssociationActivity$1(com.example.eva.EventsAssociationActivity p1, com.example.eva.database.association.Association p2, com.example.eva.Environment p3)
{
    this.this$0 = p1;
    this.val$association = p2;
    this.val$activity = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>iput-object")
	8("n°3<br/>iput-object") --> 12("n°4<br/>invoke-direct")
	12("n°4<br/>invoke-direct") --> 18("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|Lcom/example/eva/EventsAssociationActivity$1;|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/EventsAssociationActivity$1;->val$association Lcom/example/eva/database/association/Association;|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**3**|8|iput-object|v3, v0, Lcom/example/eva/EventsAssociationActivity$1;->val$activity Lcom/example/eva/Environment;|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**4**|12|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**5**|18|return-void||<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/EventsAssociationActivity$1;->val$association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v3, v0, Lcom/example/eva/EventsAssociationActivity$1;->val$activity Lcom/example/eva/Environment;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/lang/Object;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onChanged(Object p1)
{
    this.onChanged(((java.util.List) p1));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|Ljava/util/List;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, Lcom/example/eva/EventsAssociationActivity$1;->onChanged(Ljava/util/List;)V|Lcom/example/eva/EventsAssociationActivity$1;|Ljava/util/List;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Ljava/util/List;|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, Lcom/example/eva/EventsAssociationActivity$1;->onChanged(Ljava/util/List;)V|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onChanged
  
**Signature :** `(Ljava/util/List;)V`  
**Nombre de registre :** 8  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onChanged(java.util.List p7)
{
    androidx.recyclerview.widget.RecyclerView v0_2 = ((androidx.recyclerview.widget.RecyclerView) this.this$0.findViewById(2131230952));
    androidx.constraintlayout.widget.ConstraintLayout v1_3 = ((androidx.constraintlayout.widget.ConstraintLayout) this.this$0.findViewById(2131230937));
    java.util.List v2_2 = this.this$0.eventViewModel.getListByAssociation(this.val$association);
    if (v2_2.size() != 0) {
        v1_3.setVisibility(4);
        v0_2.setVisibility(0);
        v0_2.setLayoutManager(new androidx.recyclerview.widget.LinearLayoutManager(this.this$0.getApplicationContext()));
        v0_2.setAdapter(new com.example.eva.ui.event.EventRecyclerViewAdapter(v2_2, this.val$activity));
    } else {
        v1_3.setVisibility(0);
        v0_2.setVisibility(4);
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>const")
	4("n°2<br/>const") --> 10("n°3<br/>invoke-virtual")
	10("n°3<br/>invoke-virtual") --> 16("n°4<br/>move-result-object")
	16("n°4<br/>move-result-object") --> 18("n°5<br/>check-cast")
	18("n°5<br/>check-cast") --> 22("n°6<br/>iget-object")
	22("n°6<br/>iget-object") --> 26("n°7<br/>const")
	26("n°7<br/>const") --> 32("n°8<br/>invoke-virtual")
	32("n°8<br/>invoke-virtual") --> 38("n°9<br/>move-result-object")
	38("n°9<br/>move-result-object") --> 40("n°10<br/>check-cast")
	40("n°10<br/>check-cast") --> 44("n°11<br/>iget-object")
	44("n°11<br/>iget-object") --> 48("n°12<br/>iget-object")
	48("n°12<br/>iget-object") --> 52("n°13<br/>iget-object")
	52("n°13<br/>iget-object") --> 56("n°14<br/>invoke-virtual")
	56("n°14<br/>invoke-virtual") --> 62("n°15<br/>move-result-object")
	62("n°15<br/>move-result-object") --> 64("n°16<br/>invoke-interface")
	64("n°16<br/>invoke-interface") --> 70("n°17<br/>move-result")
	70("n°17<br/>move-result") --> 72("n°18<br/>const/4")
	72("n°18<br/>const/4") --> 74("n°19<br/>const/4")
	74("n°19<br/>const/4") --> 76("n°20<br/>if-nez")
	76("n°20<br/>if-nez") --> 80("n°21<br/>invoke-virtual")
	76("n°20<br/>if-nez") -.-> 94("n°24<br/>invoke-virtual")
	80("n°21<br/>invoke-virtual") --> 86("n°22<br/>invoke-virtual")
	86("n°22<br/>invoke-virtual") --> 92("n°23<br/>goto")
	92("n°23<br/>goto") --> 154("n°36<br/>return-void")
	94("n°24<br/>invoke-virtual") --> 100("n°25<br/>invoke-virtual")
	100("n°25<br/>invoke-virtual") --> 106("n°26<br/>new-instance")
	106("n°26<br/>new-instance") --> 110("n°27<br/>iget-object")
	110("n°27<br/>iget-object") --> 114("n°28<br/>invoke-virtual")
	114("n°28<br/>invoke-virtual") --> 120("n°29<br/>move-result-object")
	120("n°29<br/>move-result-object") --> 122("n°30<br/>invoke-direct")
	122("n°30<br/>invoke-direct") --> 128("n°31<br/>invoke-virtual")
	128("n°31<br/>invoke-virtual") --> 134("n°32<br/>new-instance")
	134("n°32<br/>new-instance") --> 138("n°33<br/>iget-object")
	138("n°33<br/>iget-object") --> 142("n°34<br/>invoke-direct")
	142("n°34<br/>invoke-direct") --> 148("n°35<br/>invoke-virtual")
	148("n°35<br/>invoke-virtual") --> 154("n°36<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**2**|4|const|v1, 2131230952 # [1.807797129457766e+38]|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-virtual|v0, v1, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|Lcom/example/eva/EventsAssociationActivity;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**4**|16|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**5**|18|check-cast|v0, Landroidx/recyclerview/widget/RecyclerView;|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**6**|22|iget-object|v1, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**7**|26|const|v2, 2131230937 # [1.8077940870963255e+38]|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**8**|32|invoke-virtual|v1, v2, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Lcom/example/eva/EventsAssociationActivity;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**9**|38|move-result-object|v1|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**10**|40|check-cast|v1, Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**11**|44|iget-object|v2, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**12**|48|iget-object|v2, v2, Lcom/example/eva/EventsAssociationActivity;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**13**|52|iget-object|v3, v6, Lcom/example/eva/EventsAssociationActivity$1;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Lcom/example/eva/database/event/EventViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**14**|56|invoke-virtual|v2, v3, Lcom/example/eva/database/event/EventViewModel;->getListByAssociation(Lcom/example/eva/database/association/Association;)Ljava/util/List;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Lcom/example/eva/database/event/EventViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**15**|62|move-result-object|v2|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**16**|64|invoke-interface|v2, Ljava/util/List;->size()I|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**17**|70|move-result|v3|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**18**|72|const/4|v4, 0|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**19**|74|const/4|v5, 4|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**20**|76|if-nez|v3, +9|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**21**|80|invoke-virtual|v1, v4, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**22**|86|invoke-virtual|v0, v5, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**23**|92|goto|+1f|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**24**|94|invoke-virtual|v1, v5, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**25**|100|invoke-virtual|v0, v4, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**26**|106|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**27**|110|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>int</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**28**|114|invoke-virtual|v4, Lcom/example/eva/EventsAssociationActivity;->getApplicationContext()Landroid/content/Context;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**29**|120|move-result-object|v4|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Landroidx/recyclerview/widget/LinearLayoutManager;</span>|Landroid/content/Context;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**30**|122|invoke-direct|v3, v4, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|Landroid/content/Context;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**31**|128|invoke-virtual|v0, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**32**|134|new-instance|v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Landroid/content/Context;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**33**|138|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/ui/event/EventRecyclerViewAdapter;</span>|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**34**|142|invoke-direct|v3, v2, v4, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;-><init>(Ljava/util/List; Lcom/example/eva/Environment;)V|<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|Ljava/util/List;|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|Lcom/example/eva/Environment;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**35**|148|invoke-virtual|v0, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|Landroidx/recyclerview/widget/RecyclerView;|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
|**36**|154|return-void||<span style='color:grey'>Landroidx/recyclerview/widget/RecyclerView;</span>|<span style='color:grey'>Landroidx/constraintlayout/widget/ConstraintLayout;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Ljava/util/List;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const|v1, 2131230952 # [1.807797129457766e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v0, v1, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|check-cast|v0, Landroidx/recyclerview/widget/RecyclerView;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const|v2, 2131230937 # [1.8077940870963255e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v1, v2, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|check-cast|v1, Landroidx/constraintlayout/widget/ConstraintLayout;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|iget-object|v2, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iget-object|v2, v2, Lcom/example/eva/EventsAssociationActivity;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|iget-object|v3, v6, Lcom/example/eva/EventsAssociationActivity$1;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v2, v3, Lcom/example/eva/database/event/EventViewModel;->getListByAssociation(Lcom/example/eva/database/association/Association;)Ljava/util/List;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-interface|v2, Ljava/util/List;->size()I|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|const/4|v4, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|const/4|v5, 4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|if-nez|v3, +9|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-virtual|v1, v4, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v0, v5, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|goto|+1f|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v1, v5, Landroidx/constraintlayout/widget/ConstraintLayout;->setVisibility(I)V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-virtual|v0, v4, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|new-instance|v3, Landroidx/recyclerview/widget/LinearLayoutManager;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity$1;->this$0 Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|invoke-virtual|v4, Lcom/example/eva/EventsAssociationActivity;->getApplicationContext()Landroid/content/Context;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-direct|v3, v4, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|invoke-virtual|v0, v3, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|new-instance|v3, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity$1;->val$activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|invoke-direct|v3, v2, v4, Lcom/example/eva/ui/event/EventRecyclerViewAdapter;-><init>(Ljava/util/List; Lcom/example/eva/Environment;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**35**|invoke-virtual|v0, v3, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
