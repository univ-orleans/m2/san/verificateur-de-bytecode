
com.example.eva.database.association.AssociationRepository$3
============================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [call](#call)
	* [call](#call)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/association/AssociationRepository; 
Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
AssociationRepository$3(com.example.eva.database.association.AssociationRepository p1, com.example.eva.database.association.Association p2)
{
    this.this$0 = p1;
    this.val$association = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/AssociationRepository$3;->this$0 Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository$3;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/database/association/AssociationRepository$3;->val$association Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/AssociationRepository$3;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/association/AssociationRepository$3;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/AssociationRepository$3;->this$0 Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/database/association/AssociationRepository$3;->val$association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Object;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic Object call()
{
    return this.call();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationRepository$3;->call()Ljava/lang/Void;|<span style='color:grey'>None</span>|Lcom/example/eva/database/association/AssociationRepository$3;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/lang/Void;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v0|Ljava/lang/Void;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationRepository$3;->call()Ljava/lang/Void;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Void;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Void call()
{
    com.example.eva.database.association.AssociationRepository.access$000(this.this$0).deleteAssociation(this.val$association);
    return 0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>iget-object")
	12("n°4<br/>iget-object") --> 16("n°5<br/>invoke-interface")
	16("n°5<br/>invoke-interface") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/association/AssociationRepository$3;->this$0 Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>None</span>|Lcom/example/eva/database/association/AssociationRepository$3;|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/database/association/AssociationRepository;->access$000(Lcom/example/eva/database/association/AssociationRepository;)Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/MyDao;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
|**4**|12|iget-object|v1, v2, Lcom/example/eva/database/association/AssociationRepository$3;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/AssociationRepository$3;|<span style='color:#f14848'></span>|
|**5**|16|invoke-interface|v0, v1, Lcom/example/eva/database/MyDao;->deleteAssociation(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v0, 0|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
|**7**|24|return-object|v0|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$3;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/association/AssociationRepository$3;->this$0 Lcom/example/eva/database/association/AssociationRepository;|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/database/association/AssociationRepository;->access$000(Lcom/example/eva/database/association/AssociationRepository;)Lcom/example/eva/database/MyDao;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iget-object|v1, v2, Lcom/example/eva/database/association/AssociationRepository$3;->val$association Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**5**|invoke-interface|v0, v1, Lcom/example/eva/database/MyDao;->deleteAssociation(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v0, 0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|return-object|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
