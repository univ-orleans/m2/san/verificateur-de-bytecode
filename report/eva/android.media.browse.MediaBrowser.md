
android.media.browse.MediaBrowser
=================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [disconnect](#disconnect)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**disconnect**](#disconnect)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## disconnect

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.BROADCAST_STICKY|normal|send sticky broadcast|Allows the app to send sticky broadcasts, which remain after the broadcast ends. Excessive use may make the phone slow or unstable by causing it to use too much memory.|
  
