
com.example.eva.database.MyDao_Impl$1
=====================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [bind](#bind)
	* [bind](#bind)
	* [createQuery](#createquery)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**bind**](#bind)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**bind**](#bind)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createQuery**](#createquery)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
MyDao_Impl$1(com.example.eva.database.MyDao_Impl p1, androidx.room.RoomDatabase p2)
{
    this.this$0 = p1;
    super(p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$1;->this$0 Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/MyDao_Impl$1;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, v2, Landroidx/room/EntityInsertionAdapter;-><init>(Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$1;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$1;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$1;->this$0 Lcom/example/eva/database/MyDao_Impl;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, v2, Landroidx/room/EntityInsertionAdapter;-><init>(Landroidx/room/RoomDatabase;)V|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## bind
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/event/Event;)V`  
**Nombre de registre :** 10  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void bind(androidx.sqlite.db.SupportSQLiteStatement p8, com.example.eva.database.event.Event p9)
{
    p8.bindLong(1, ((long) p9.getId()));
    if (p9.getIdAssociation() != null) {
        p8.bindLong(2, ((long) p9.getIdAssociation().intValue()));
    } else {
        p8.bindNull(2);
    }
    if (p9.getIdGoogleEvent() != null) {
        p8.bindString(3, p9.getIdGoogleEvent());
    } else {
        p8.bindNull(3);
    }
    if (p9.getSummary() != null) {
        p8.bindString(4, p9.getSummary());
    } else {
        p8.bindNull(4);
    }
    Integer v0_11;
    if (p9.isCanceled() != null) {
        v0_11 = Integer.valueOf(p9.isCanceled().booleanValue());
    } else {
        v0_11 = 0;
    }
    if (v0_11 != null) {
        p8.bindLong(5, ((long) v0_11.intValue()));
    } else {
        p8.bindNull(5);
    }
    Long v1_4 = com.example.eva.database.Converters.dateToTimestamp(p9.getDateStart());
    if (v1_4 != null) {
        p8.bindLong(6, v1_4.longValue());
    } else {
        p8.bindNull(6);
    }
    Long v2_6 = com.example.eva.database.Converters.dateToTimestamp(p9.getDateEnd());
    if (v2_6 != null) {
        p8.bindLong(7, v2_6.longValue());
    } else {
        p8.bindNull(7);
    }
    Long v3_3 = com.example.eva.database.Converters.dateToTimestamp(p9.getDateUpdated());
    if (v3_3 != null) {
        p8.bindLong(8, v3_3.longValue());
    } else {
        p8.bindNull(8);
    }
    if (p9.getLocation() != null) {
        p8.bindString(9, p9.getLocation());
    } else {
        p8.bindNull(9);
    }
    if (p9.getDescription() != null) {
        p8.bindString(10, p9.getDescription());
    } else {
        p8.bindNull(10);
    }
    return;
}
```
## bind
  
**Signature :** `(Landroidx/sqlite/db/SupportSQLiteStatement; Ljava/lang/Object;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void bind(androidx.sqlite.db.SupportSQLiteStatement p1, Object p2)
{
    this.bind(p1, ((com.example.eva.database.event.Event) p2));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v2, Lcom/example/eva/database/event/Event;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$1;->bind(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/event/Event;)V|Lcom/example/eva/database/MyDao_Impl$1;|Landroidx/sqlite/db/SupportSQLiteStatement;|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$1;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v2, Lcom/example/eva/database/event/Event;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$1;->bind(Landroidx/sqlite/db/SupportSQLiteStatement; Lcom/example/eva/database/event/Event;)V|True|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## createQuery
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String createQuery()
{
    return "INSERT OR ABORT INTO `event_table` (`id`,`idAssociation`,`idGoogleEvent`,`summary`,`canceled`,`dateStart`,`dateEnd`,`dateUpdated`,`location`,`description`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?)";
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'INSERT OR ABORT INTO `event_table` (`id`,`idAssociation`,`idGoogleEvent`,`summary`,`canceled`,`dateStart`,`dateEnd`,`dateUpdated`,`location`,`description`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?)'|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$1;</span>|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$1;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'INSERT OR ABORT INTO `event_table` (`id`,`idAssociation`,`idGoogleEvent`,`summary`,`canceled`,`dateStart`,`dateEnd`,`dateUpdated`,`location`,`description`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?)'|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
