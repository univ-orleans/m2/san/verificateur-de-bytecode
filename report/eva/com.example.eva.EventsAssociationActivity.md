
com.example.eva.EventsAssociationActivity
=========================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [finish](#finish)
	* [onCreate](#oncreate)
	* [onOptionsItemSelected](#onoptionsitemselected)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**finish**](#finish)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onOptionsItemSelected**](#onoptionsitemselected)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EventsAssociationActivity()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|Lcom/example/eva/EventsAssociationActivity;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## finish
  
**Signature :** `()V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void finish()
{
    super.finish();
    this.overridePendingTransition(2130772010, 2130772013);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>const")
	6("n°2<br/>const") --> 12("n°3<br/>const")
	12("n°3<br/>const") --> 18("n°4<br/>invoke-virtual")
	18("n°4<br/>invoke-virtual") --> 24("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v2, Lcom/example/eva/Environment;->finish()V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:#f14848'></span>|
|**2**|6|const|v0, 2130772010 # [1.714712633174575e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:#f14848'></span>|
|**3**|12|const|v1, 2130772013 # [1.714713241646863e+38]|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:#f14848'></span>|
|**4**|18|invoke-virtual|v2, v0, v1, Lcom/example/eva/EventsAssociationActivity;->overridePendingTransition(I I)V|int|int|Lcom/example/eva/EventsAssociationActivity;|<span style='color:#f14848'></span>|
|**5**|24|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v2, Lcom/example/eva/Environment;->finish()V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|const|v0, 2130772010 # [1.714712633174575e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v1, 2130772013 # [1.714713241646863e+38]|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v2, v0, v1, Lcom/example/eva/EventsAssociationActivity;->overridePendingTransition(I I)V|False|False|True|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 8  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void onCreate(android.os.Bundle p7)
{
    super.onCreate(p7);
    this.setContentView(2131427357);
    com.example.eva.database.association.Association v3_0 = this.associationViewModel.get(this.getIntent().getIntExtra("idAsso", -1));
    this.setContentView(2131427357);
    this.setSupportActionBar(((androidx.appcompat.widget.Toolbar) this.findViewById(2131231128)));
    ((androidx.appcompat.app.ActionBar) java.util.Objects.requireNonNull(this.getSupportActionBar())).setDisplayHomeAsUpEnabled(1);
    this.getSupportActionBar().setTitle(v3_0.getName());
    this.eventViewModel.getData().observe(this, new com.example.eva.EventsAssociationActivity$1(this, v3_0, this));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>const")
	6("n°2<br/>const") --> 12("n°3<br/>invoke-virtual")
	12("n°3<br/>invoke-virtual") --> 18("n°4<br/>invoke-virtual")
	18("n°4<br/>invoke-virtual") --> 24("n°5<br/>move-result-object")
	24("n°5<br/>move-result-object") --> 26("n°6<br/>const-string")
	26("n°6<br/>const-string") --> 30("n°7<br/>const/4")
	30("n°7<br/>const/4") --> 32("n°8<br/>invoke-virtual")
	32("n°8<br/>invoke-virtual") --> 38("n°9<br/>move-result")
	38("n°9<br/>move-result") --> 40("n°10<br/>iget-object")
	40("n°10<br/>iget-object") --> 44("n°11<br/>invoke-virtual")
	44("n°11<br/>invoke-virtual") --> 50("n°12<br/>move-result-object")
	50("n°12<br/>move-result-object") --> 52("n°13<br/>invoke-virtual")
	52("n°13<br/>invoke-virtual") --> 58("n°14<br/>const")
	58("n°14<br/>const") --> 64("n°15<br/>invoke-virtual")
	64("n°15<br/>invoke-virtual") --> 70("n°16<br/>move-result-object")
	70("n°16<br/>move-result-object") --> 72("n°17<br/>check-cast")
	72("n°17<br/>check-cast") --> 76("n°18<br/>invoke-virtual")
	76("n°18<br/>invoke-virtual") --> 82("n°19<br/>invoke-virtual")
	82("n°19<br/>invoke-virtual") --> 88("n°20<br/>move-result-object")
	88("n°20<br/>move-result-object") --> 90("n°21<br/>invoke-static")
	90("n°21<br/>invoke-static") --> 96("n°22<br/>move-result-object")
	96("n°22<br/>move-result-object") --> 98("n°23<br/>check-cast")
	98("n°23<br/>check-cast") --> 102("n°24<br/>const/4")
	102("n°24<br/>const/4") --> 104("n°25<br/>invoke-virtual")
	104("n°25<br/>invoke-virtual") --> 110("n°26<br/>invoke-virtual")
	110("n°26<br/>invoke-virtual") --> 116("n°27<br/>move-result-object")
	116("n°27<br/>move-result-object") --> 118("n°28<br/>invoke-virtual")
	118("n°28<br/>invoke-virtual") --> 124("n°29<br/>move-result-object")
	124("n°29<br/>move-result-object") --> 126("n°30<br/>invoke-virtual")
	126("n°30<br/>invoke-virtual") --> 132("n°31<br/>move-object")
	132("n°31<br/>move-object") --> 134("n°32<br/>iget-object")
	134("n°32<br/>iget-object") --> 138("n°33<br/>invoke-virtual")
	138("n°33<br/>invoke-virtual") --> 144("n°34<br/>move-result-object")
	144("n°34<br/>move-result-object") --> 146("n°35<br/>new-instance")
	146("n°35<br/>new-instance") --> 150("n°36<br/>invoke-direct")
	150("n°36<br/>invoke-direct") --> 156("n°37<br/>invoke-virtual")
	156("n°37<br/>invoke-virtual") --> 162("n°38<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v6, v7, Lcom/example/eva/Environment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|Landroid/os/Bundle;|<span style='color:#f14848'></span>|
|**2**|6|const|v0, 2131427357 # [1.847632796039818e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|12|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setContentView(I)V|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|18|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getIntent()Landroid/content/Intent;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|24|move-result-object|v1|<span style='color:grey'>int</span>|Landroid/content/Intent;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|26|const-string|v2, 'idAsso'|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|30|const/4|v3, -1|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>Ljava/lang/String;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|32|invoke-virtual|v1, v2, v3, Landroid/content/Intent;->getIntExtra(Ljava/lang/String; I)I|<span style='color:grey'>int</span>|Landroid/content/Intent;|Ljava/lang/String;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**9**|38|move-result|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**10**|40|iget-object|v3, v6, Lcom/example/eva/EventsAssociationActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**11**|44|invoke-virtual|v3, v2, Lcom/example/eva/database/association/AssociationViewModel;->get(I)Lcom/example/eva/database/association/Association;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|int|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**12**|50|move-result-object|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**13**|52|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setContentView(I)V|int|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**14**|58|const|v0, 2131231128 # [1.8078328264986685e+38]|int|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**15**|64|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**16**|70|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**17**|72|check-cast|v0, Landroidx/appcompat/widget/Toolbar;|Landroidx/appcompat/widget/Toolbar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**18**|76|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V|Landroidx/appcompat/widget/Toolbar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**19**|82|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**20**|88|move-result-object|v0|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**21**|90|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**22**|96|move-result-object|v0|Ljava/lang/Object;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**23**|98|check-cast|v0, Landroidx/appcompat/app/ActionBar;|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**24**|102|const/4|v4, 1|<span style='color:grey'>Landroidx/appcompat/app/ActionBar;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**25**|104|invoke-virtual|v0, v4, Landroidx/appcompat/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**26**|110|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroidx/appcompat/app/ActionBar;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**27**|116|move-result-object|v0|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**28**|118|invoke-virtual|v3, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|<span style='color:grey'>Landroidx/appcompat/app/ActionBar;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**29**|124|move-result-object|v4|<span style='color:grey'>Landroidx/appcompat/app/ActionBar;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**30**|126|invoke-virtual|v0, v4, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V|Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**31**|132|move-object|v0, v6|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**32**|134|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**33**|138|invoke-virtual|v4, Lcom/example/eva/database/event/EventViewModel;->getData()Landroidx/lifecycle/LiveData;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**34**|144|move-result-object|v4|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroidx/lifecycle/LiveData;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**35**|146|new-instance|v5, Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**36**|150|invoke-direct|v5, v6, v3, v0, Lcom/example/eva/EventsAssociationActivity$1;-><init>(Lcom/example/eva/EventsAssociationActivity; Lcom/example/eva/database/association/Association; Lcom/example/eva/Environment;)V|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|Lcom/example/eva/EventsAssociationActivity$1;|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**37**|156|invoke-virtual|v4, v6, v5, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner; Landroidx/lifecycle/Observer;)V|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroidx/lifecycle/LiveData;|Lcom/example/eva/EventsAssociationActivity$1;|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**38**|162|return-void||<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/content/Intent;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroidx/lifecycle/LiveData;</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity$1;</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v6, v7, Lcom/example/eva/Environment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**2**|const|v0, 2131427357 # [1.847632796039818e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setContentView(I)V|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getIntent()Landroid/content/Intent;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const-string|v2, 'idAsso'|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const/4|v3, -1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v1, v2, v3, Landroid/content/Intent;->getIntExtra(Ljava/lang/String; I)I|<span style='color:grey'>False</span>|True|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v3, v6, Lcom/example/eva/EventsAssociationActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v3, v2, Lcom/example/eva/database/association/AssociationViewModel;->get(I)Lcom/example/eva/database/association/Association;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setContentView(I)V|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|const|v0, 2131231128 # [1.8078328264986685e+38]|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->findViewById(I)Landroid/view/View;|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|check-cast|v0, Landroidx/appcompat/widget/Toolbar;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v6, v0, Lcom/example/eva/EventsAssociationActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|check-cast|v0, Landroidx/appcompat/app/ActionBar;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|const/4|v4, 1|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-virtual|v0, v4, Landroidx/appcompat/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|invoke-virtual|v6, Lcom/example/eva/EventsAssociationActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|invoke-virtual|v3, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-virtual|v0, v4, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|move-object|v0, v6|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|iget-object|v4, v6, Lcom/example/eva/EventsAssociationActivity;->eventViewModel Lcom/example/eva/database/event/EventViewModel;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|invoke-virtual|v4, Lcom/example/eva/database/event/EventViewModel;->getData()Landroidx/lifecycle/LiveData;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**35**|new-instance|v5, Lcom/example/eva/EventsAssociationActivity$1;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|invoke-direct|v5, v6, v3, v0, Lcom/example/eva/EventsAssociationActivity$1;-><init>(Lcom/example/eva/EventsAssociationActivity; Lcom/example/eva/database/association/Association; Lcom/example/eva/Environment;)V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**37**|invoke-virtual|v4, v6, v5, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner; Landroidx/lifecycle/Observer;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**38**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onOptionsItemSelected
  
**Signature :** `(Landroid/view/MenuItem;)Z`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean onOptionsItemSelected(android.view.MenuItem p3)
{
    if (p3.getItemId() != 16908332) {
        return super.onOptionsItemSelected(p3);
    } else {
        this.finish();
        return 1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-interface") --> 6("n°2<br/>move-result")
	6("n°2<br/>move-result") --> 8("n°3<br/>const")
	8("n°3<br/>const") --> 14("n°4<br/>if-ne")
	14("n°4<br/>if-ne") --> 18("n°5<br/>invoke-virtual")
	14("n°4<br/>if-ne") -.-> 28("n°8<br/>invoke-super")
	18("n°5<br/>invoke-virtual") --> 24("n°6<br/>const/4")
	24("n°6<br/>const/4") --> 26("n°7<br/>return")
	28("n°8<br/>invoke-super") --> 34("n°9<br/>move-result")
	34("n°9<br/>move-result") --> 36("n°10<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|Landroid/view/MenuItem;|<span style='color:#f14848'></span>|
|**2**|6|move-result|v0|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**3**|8|const|v1, 16908332 # [2.3877352315342576e-38]|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**4**|14|if-ne|v0, v1, +7|int|int|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-virtual|v2, Lcom/example/eva/EventsAssociationActivity;->finish()V|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/EventsAssociationActivity;|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**6**|24|const/4|v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**7**|26|return|v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**8**|28|invoke-super|v2, v3, Lcom/example/eva/Environment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/EventsAssociationActivity;|Landroid/view/MenuItem;|<span style='color:#f14848'></span>|
|**9**|34|move-result|v0|boolean|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**10**|36|return|v0|boolean|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventsAssociationActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v1, 16908332 # [2.3877352315342576e-38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-ne|v0, v1, +7|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v2, Lcom/example/eva/EventsAssociationActivity;->finish()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v0, 1|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-super|v2, v3, Lcom/example/eva/Environment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**9**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
