
com.example.eva.ui.association.SubscriberAssociationRecyclerViewAdapter
=======================================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [getItemCount](#getitemcount)
	* [onBindViewHolder](#onbindviewholder)
	* [onBindViewHolder](#onbindviewholder)
	* [onCreateViewHolder](#oncreateviewholder)
	* [onCreateViewHolder](#oncreateviewholder)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getItemCount**](#getitemcount)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onBindViewHolder**](#onbindviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateViewHolder**](#oncreateviewholder)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/Environment;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public SubscriberAssociationRecyclerViewAdapter(com.example.eva.Environment p2)
{
    this.activity = p2;
    this.associations = p2.getAssociationViewModel().getList();
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>invoke-virtual")
	10("n°3<br/>invoke-virtual") --> 16("n°4<br/>move-result-object")
	16("n°4<br/>move-result-object") --> 18("n°5<br/>invoke-virtual")
	18("n°5<br/>invoke-virtual") --> 24("n°6<br/>move-result-object")
	24("n°6<br/>move-result-object") --> 26("n°7<br/>iput-object")
	26("n°7<br/>iput-object") --> 30("n°8<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v2, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**3**|10|invoke-virtual|v2, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/Environment;|<span style='color:#f14848'></span>|
|**4**|16|move-result-object|v0|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationViewModel;->getList()Ljava/util/List;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**6**|24|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**7**|26|iput-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
|**8**|30|return-void||<span style='color:grey'>Ljava/util/List;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v2, Lcom/example/eva/Environment;->getAssociationViewModel()Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v0, Lcom/example/eva/database/association/AssociationViewModel;->getList()Ljava/util/List;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iput-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getItemCount
  
**Signature :** `()I`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int getItemCount()
{
    return this.associations.size();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>move-result")
	10("n°3<br/>move-result") --> 12("n°4<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v0, Ljava/util/List;->size()I|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result|v0|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
|**4**|12|return|v0|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v0, Ljava/util/List;->size()I|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Landroidx/recyclerview/widget/RecyclerView$ViewHolder; I)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic void onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder p1, int p2)
{
    this.onBindViewHolder(((com.example.eva.ui.association.SubscriberAssociationViewHolder) p1), p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>check-cast") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|check-cast|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; I)V|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|int|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|check-cast|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->onBindViewHolder(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; I)V|True|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onBindViewHolder
  
**Signature :** `(Lcom/example/eva/ui/association/SubscriberAssociationViewHolder; I)V`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onBindViewHolder(com.example.eva.ui.association.SubscriberAssociationViewHolder p4, int p5)
{
    com.example.eva.database.association.Association v0_2 = ((com.example.eva.database.association.Association) this.associations.get(p5));
    p4.association = v0_2;
    p4.subscribeView.setText(v0_2.getName());
    p4.subscribeView.setChecked(v0_2.getSubscribe().booleanValue());
    if (v0_2.getLogo() != null) {
        if (android.os.Build$VERSION.SDK_INT >= 23) {
            p4.logoView.setForeground(0);
        }
        p4.logoView.setImageResource(v0_2.getLogo().intValue());
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-interface")
	4("n°2<br/>invoke-interface") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>check-cast")
	12("n°4<br/>check-cast") --> 16("n°5<br/>iput-object")
	16("n°5<br/>iput-object") --> 20("n°6<br/>iget-object")
	20("n°6<br/>iget-object") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>invoke-virtual")
	32("n°9<br/>invoke-virtual") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>invoke-virtual")
	42("n°11<br/>invoke-virtual") --> 48("n°12<br/>move-result-object")
	48("n°12<br/>move-result-object") --> 50("n°13<br/>invoke-virtual")
	50("n°13<br/>invoke-virtual") --> 56("n°14<br/>move-result")
	56("n°14<br/>move-result") --> 58("n°15<br/>invoke-virtual")
	58("n°15<br/>invoke-virtual") --> 64("n°16<br/>invoke-virtual")
	64("n°16<br/>invoke-virtual") --> 70("n°17<br/>move-result-object")
	70("n°17<br/>move-result-object") --> 72("n°18<br/>if-eqz")
	72("n°18<br/>if-eqz") --> 76("n°19<br/>sget")
	72("n°18<br/>if-eqz") -.-> 126("n°31<br/>return-void")
	76("n°19<br/>sget") --> 80("n°20<br/>const/16")
	80("n°20<br/>const/16") --> 84("n°21<br/>if-lt")
	84("n°21<br/>if-lt") --> 88("n°22<br/>iget-object")
	84("n°21<br/>if-lt") -.-> 100("n°25<br/>iget-object")
	88("n°22<br/>iget-object") --> 92("n°23<br/>const/4")
	92("n°23<br/>const/4") --> 94("n°24<br/>invoke-virtual")
	94("n°24<br/>invoke-virtual") --> 100("n°25<br/>iget-object")
	100("n°25<br/>iget-object") --> 104("n°26<br/>invoke-virtual")
	104("n°26<br/>invoke-virtual") --> 110("n°27<br/>move-result-object")
	110("n°27<br/>move-result-object") --> 112("n°28<br/>invoke-virtual")
	112("n°28<br/>invoke-virtual") --> 118("n°29<br/>move-result")
	118("n°29<br/>move-result") --> 120("n°30<br/>invoke-virtual")
	120("n°30<br/>invoke-virtual") --> 126("n°31<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-interface|v0, v5, Ljava/util/List;->get(I)Ljava/lang/Object;|Ljava/util/List;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|int|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|check-cast|v0, Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|16|iput-object|v0, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|20|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/Switch;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/Switch;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/Switch;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|32|invoke-virtual|v1, v2, Landroid/widget/Switch;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/Switch;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/Switch;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/Switch;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|48|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/Switch;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**13**|50|invoke-virtual|v2, Ljava/lang/Boolean;->booleanValue()Z|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/Switch;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**14**|56|move-result|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/Switch;</span>|boolean|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**15**|58|invoke-virtual|v1, v2, Landroid/widget/Switch;->setChecked(Z)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/Switch;|boolean|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**16**|64|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/Switch;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**17**|70|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**18**|72|if-eqz|v1, +1b|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**19**|76|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**20**|80|const/16|v2, 23|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**21**|84|if-lt|v1, v2, +8|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**22**|88|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**23**|92|const/4|v2, 0|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**24**|94|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**25**|100|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**26**|104|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Landroid/widget/ImageView;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**27**|110|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**28**|112|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**29**|118|move-result|v2|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**30**|120|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setImageResource(I)V|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Landroid/widget/ImageView;|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**31**|126|return-void||<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Landroid/widget/ImageView;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->associations Ljava/util/List;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-interface|v0, v5, Ljava/util/List;->get(I)Ljava/lang/Object;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|check-cast|v0, Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iput-object|v0, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->association Lcom/example/eva/database/association/Association;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getName()Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v1, v2, Landroid/widget/Switch;->setText(Ljava/lang/CharSequence;)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->subscribeView Landroid/widget/Switch;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getSubscribe()Ljava/lang/Boolean;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v2, Ljava/lang/Boolean;->booleanValue()Z|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, v2, Landroid/widget/Switch;->setChecked(Z)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|if-eqz|v1, +1b|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|const/16|v2, 23|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|if-lt|v1, v2, +8|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|iget-object|v1, v4, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;->logoView Landroid/widget/ImageView;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|invoke-virtual|v0, Lcom/example/eva/database/association/Association;->getLogo()Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-virtual|v1, v2, Landroid/widget/ImageView;->setImageResource(I)V|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic androidx.recyclerview.widget.RecyclerView$ViewHolder onCreateViewHolder(android.view.ViewGroup p1, int p2)
{
    return this.onCreateViewHolder(p1, p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|Landroid/view/ViewGroup;|int|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v1|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v1|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v0, v1, v2, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|True|True|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateViewHolder
  
**Signature :** `(Landroid/view/ViewGroup; I)Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.ui.association.SubscriberAssociationViewHolder onCreateViewHolder(android.view.ViewGroup p4, int p5)
{
    return new com.example.eva.ui.association.SubscriberAssociationViewHolder(android.view.LayoutInflater.from(p4.getContext()).inflate(2131427389, p4, 0), this.activity);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>invoke-static")
	8("n°3<br/>invoke-static") --> 14("n°4<br/>move-result-object")
	14("n°4<br/>move-result-object") --> 16("n°5<br/>const")
	16("n°5<br/>const") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>invoke-virtual")
	24("n°7<br/>invoke-virtual") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>new-instance")
	32("n°9<br/>new-instance") --> 36("n°10<br/>iget-object")
	36("n°10<br/>iget-object") --> 40("n°11<br/>invoke-direct")
	40("n°11<br/>invoke-direct") --> 46("n°12<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|Landroid/content/Context;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result-object|v0|Landroid/view/LayoutInflater;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**5**|16|const|v1, 2131427389 # [1.8476392864108913e+38]|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v2, 0|<span style='color:grey'>Landroid/view/LayoutInflater;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|Landroid/view/LayoutInflater;|int|int|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|Landroid/view/ViewGroup;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|32|new-instance|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|36|iget-object|v2, v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;</span>|Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|40|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|Landroid/view/View;|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|Lcom/example/eva/Environment;|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|46|return-object|v1|<span style='color:grey'>Landroid/view/View;</span>|Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>Lcom/example/eva/Environment;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;</span>|<span style='color:grey'>Landroid/view/ViewGroup;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v4, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-static|v0, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const|v1, 2131427389 # [1.8476392864108913e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v2, 0|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, v4, v2, Landroid/view/LayoutInflater;->inflate(I Landroid/view/ViewGroup; Z)Landroid/view/View;|True|False|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|new-instance|v1, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v2, v3, Lcom/example/eva/ui/association/SubscriberAssociationRecyclerViewAdapter;->activity Lcom/example/eva/Environment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-direct|v1, v0, v2, Lcom/example/eva/ui/association/SubscriberAssociationViewHolder;-><init>(Landroid/view/View; Lcom/example/eva/Environment;)V|True|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
