
com.example.eva.database.association.AssociationRepository$7
============================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [call](#call)
	* [call](#call)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/association/AssociationRepository;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
AssociationRepository$7(com.example.eva.database.association.AssociationRepository p1)
{
    this.this$0 = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/AssociationRepository$7;->this$0 Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository$7;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/association/AssociationRepository$7;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/AssociationRepository$7;->this$0 Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Object;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic Object call()
{
    return this.call();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationRepository$7;->call()Ljava/util/List;|<span style='color:grey'>None</span>|Lcom/example/eva/database/association/AssociationRepository$7;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/eva/database/association/AssociationRepository$7;->call()Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List call()
{
    return com.example.eva.database.association.AssociationRepository.access$000(this.this$0).getAllAssociationSubscribe();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>invoke-interface")
	12("n°4<br/>invoke-interface") --> 18("n°5<br/>move-result-object")
	18("n°5<br/>move-result-object") --> 20("n°6<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationRepository$7;->this$0 Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository;|Lcom/example/eva/database/association/AssociationRepository$7;|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/database/association/AssociationRepository;->access$000(Lcom/example/eva/database/association/AssociationRepository;)Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/association/AssociationRepository;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-interface|v0, Lcom/example/eva/database/MyDao;->getAllAssociationSubscribe()Ljava/util/List;|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
|**5**|18|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
|**6**|20|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationRepository$7;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/AssociationRepository$7;->this$0 Lcom/example/eva/database/association/AssociationRepository;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/database/association/AssociationRepository;->access$000(Lcom/example/eva/database/association/AssociationRepository;)Lcom/example/eva/database/MyDao;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v0, Lcom/example/eva/database/MyDao;->getAllAssociationSubscribe()Ljava/util/List;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
