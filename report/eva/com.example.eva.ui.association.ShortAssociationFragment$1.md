
com.example.eva.ui.association.ShortAssociationFragment$1
=========================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [onClick](#onclick)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onClick**](#onclick)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/ui/association/ShortAssociationFragment;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
ShortAssociationFragment$1(com.example.eva.ui.association.ShortAssociationFragment p1)
{
    this.this$0 = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationFragment$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationFragment;|Lcom/example/eva/ui/association/ShortAssociationFragment$1;|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/ui/association/ShortAssociationFragment$1;|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/ui/association/ShortAssociationFragment$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationFragment;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onClick
  
**Signature :** `(Landroid/view/View;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void onClick(android.view.View p4)
{
    ((androidx.navigation.fragment.NavHostFragment) java.util.Objects.requireNonNull(((androidx.navigation.fragment.NavHostFragment) com.example.eva.ui.association.ShortAssociationFragment.access$000(this.this$0).getSupportFragmentManager().findFragmentById(2131230993)))).getNavController().navigate(2131230996);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-static")
	4("n°2<br/>invoke-static") --> 10("n°3<br/>move-result-object")
	10("n°3<br/>move-result-object") --> 12("n°4<br/>invoke-virtual")
	12("n°4<br/>invoke-virtual") --> 18("n°5<br/>move-result-object")
	18("n°5<br/>move-result-object") --> 20("n°6<br/>const")
	20("n°6<br/>const") --> 26("n°7<br/>invoke-virtual")
	26("n°7<br/>invoke-virtual") --> 32("n°8<br/>move-result-object")
	32("n°8<br/>move-result-object") --> 34("n°9<br/>check-cast")
	34("n°9<br/>check-cast") --> 38("n°10<br/>invoke-static")
	38("n°10<br/>invoke-static") --> 44("n°11<br/>move-result-object")
	44("n°11<br/>move-result-object") --> 46("n°12<br/>check-cast")
	46("n°12<br/>check-cast") --> 50("n°13<br/>invoke-virtual")
	50("n°13<br/>invoke-virtual") --> 56("n°14<br/>move-result-object")
	56("n°14<br/>move-result-object") --> 58("n°15<br/>const")
	58("n°15<br/>const") --> 64("n°16<br/>invoke-virtual")
	64("n°16<br/>invoke-virtual") --> 70("n°17<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v3, Lcom/example/eva/ui/association/ShortAssociationFragment$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationFragment;|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/ui/association/ShortAssociationFragment$1;|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-static|v0, Lcom/example/eva/ui/association/ShortAssociationFragment;->access$000(Lcom/example/eva/ui/association/ShortAssociationFragment;)Lcom/example/eva/Environment;|Lcom/example/eva/ui/association/ShortAssociationFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result-object|v0|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-virtual|v0, Lcom/example/eva/Environment;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|Lcom/example/eva/Environment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**5**|18|move-result-object|v0|Landroidx/fragment/app/FragmentManager;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**6**|20|const|v1, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>Landroidx/fragment/app/FragmentManager;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**7**|26|invoke-virtual|v0, v1, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|Landroidx/fragment/app/FragmentManager;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**8**|32|move-result-object|v0|Landroidx/fragment/app/Fragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**9**|34|check-cast|v0, Landroidx/navigation/fragment/NavHostFragment;|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**10**|38|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**11**|44|move-result-object|v1|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**12**|46|check-cast|v1, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**13**|50|invoke-virtual|v1, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**14**|56|move-result-object|v1|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**15**|58|const|v2, 2131230996 # [1.8078060537179917e+38]|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**16**|64|invoke-virtual|v1, v2, Landroidx/navigation/NavController;->navigate(I)V|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|int|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
|**17**|70|return-void||<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/ui/association/ShortAssociationFragment$1;</span>|<span style='color:grey'>Landroid/view/View;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v3, Lcom/example/eva/ui/association/ShortAssociationFragment$1;->this$0 Lcom/example/eva/ui/association/ShortAssociationFragment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-static|v0, Lcom/example/eva/ui/association/ShortAssociationFragment;->access$000(Lcom/example/eva/ui/association/ShortAssociationFragment;)Lcom/example/eva/Environment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Lcom/example/eva/Environment;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const|v1, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-virtual|v0, v1, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|check-cast|v0, Landroidx/navigation/fragment/NavHostFragment;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-static|v0, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|check-cast|v1, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-virtual|v1, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|const|v2, 2131230996 # [1.8078060537179917e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v1, v2, Landroidx/navigation/NavController;->navigate(I)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
