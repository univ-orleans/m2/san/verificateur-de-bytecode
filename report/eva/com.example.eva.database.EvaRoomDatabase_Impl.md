
com.example.eva.database.EvaRoomDatabase_Impl
=============================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [access$000](#access000)
	* [access$100](#access100)
	* [access$1000](#access1000)
	* [access$200](#access200)
	* [access$300](#access300)
	* [access$400](#access400)
	* [access$500](#access500)
	* [access$602](#access602)
	* [access$700](#access700)
	* [access$800](#access800)
	* [access$900](#access900)
	* [clearAllTables](#clearalltables)
	* [createInvalidationTracker](#createinvalidationtracker)
	* [createOpenHelper](#createopenhelper)
	* [myDao](#mydao)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$000**](#access000)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$100**](#access100)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$1000**](#access1000)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$200**](#access200)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$300**](#access300)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$400**](#access400)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$500**](#access500)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$602**](#access602)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$700**](#access700)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$800**](#access800)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$900**](#access900)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**clearAllTables**](#clearalltables)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createInvalidationTracker**](#createinvalidationtracker)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**createOpenHelper**](#createopenhelper)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**myDao**](#mydao)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EvaRoomDatabase_Impl()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Lcom/example/eva/database/EvaRoomDatabase;-><init>()V|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Lcom/example/eva/database/EvaRoomDatabase;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$000
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$000(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$100
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$100(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$1000
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$1000(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$200
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$200(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$300
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$300(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$400
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$400(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$500
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$500(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$602
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl; 
Landroidx/sqlite/db/SupportSQLiteDatabase;)Landroidx/sqlite/db/SupportSQLiteDatabase;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic androidx.sqlite.db.SupportSQLiteDatabase access$602(com.example.eva.database.EvaRoomDatabase_Impl p0, androidx.sqlite.db.SupportSQLiteDatabase p1)
{
    p0.mDatabase = p1;
    return p1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mDatabase Landroidx/sqlite/db/SupportSQLiteDatabase;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v1|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mDatabase Landroidx/sqlite/db/SupportSQLiteDatabase;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
  

## access$700
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl; Landroidx/sqlite/db/SupportSQLiteDatabase;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic void access$700(com.example.eva.database.EvaRoomDatabase_Impl p0, androidx.sqlite.db.SupportSQLiteDatabase p1)
{
    p0.internalInitInvalidationTracker(p1);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->internalInitInvalidationTracker(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|Lcom/example/eva/database/EvaRoomDatabase_Impl;|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->internalInitInvalidationTracker(Landroidx/sqlite/db/SupportSQLiteDatabase;)V|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$800
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$800(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$900
  
**Signature :** `(Lcom/example/eva/database/EvaRoomDatabase_Impl;)Ljava/util/List;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic java.util.List access$900(com.example.eva.database.EvaRoomDatabase_Impl p1)
{
    return p1.mCallbacks;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|Ljava/util/List;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->mCallbacks Ljava/util/List;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## clearAllTables
  
**Signature :** `()V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void clearAllTables()
{
    int v1_1;
    super.assertNotMainThread();
    androidx.sqlite.db.SupportSQLiteDatabase v0_1 = super.getOpenHelper().getWritableDatabase();
    if (android.os.Build$VERSION.SDK_INT < 21) {
        v1_1 = 0;
    } else {
        v1_1 = 1;
    }
    try {
        if (v1_1 == 0) {
            v0_1.execSQL("PRAGMA foreign_keys = FALSE");
        }
    } catch (String v5_2) {
        super.endTransaction();
        if (v1_1 == 0) {
            v0_1.execSQL("PRAGMA foreign_keys = TRUE");
        }
        v0_1.query("PRAGMA wal_checkpoint(FULL)").close();
        if (!v0_1.inTransaction()) {
            v0_1.execSQL("VACUUM");
        }
        throw v5_2;
    }
    super.beginTransaction();
    if (v1_1 != 0) {
        v0_1.execSQL("PRAGMA defer_foreign_keys = TRUE");
    }
    v0_1.execSQL("DELETE FROM `association_table`");
    v0_1.execSQL("DELETE FROM `event_table`");
    super.setTransactionSuccessful();
    super.endTransaction();
    if (v1_1 == 0) {
        v0_1.execSQL("PRAGMA foreign_keys = TRUE");
    }
    v0_1.query("PRAGMA wal_checkpoint(FULL)").close();
    if (!v0_1.inTransaction()) {
        v0_1.execSQL("VACUUM");
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>invoke-super")
	6("n°2<br/>invoke-super") --> 12("n°3<br/>move-result-object")
	12("n°3<br/>move-result-object") --> 14("n°4<br/>invoke-interface")
	14("n°4<br/>invoke-interface") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>sget")
	22("n°6<br/>sget") --> 26("n°7<br/>const/16")
	26("n°7<br/>const/16") --> 30("n°8<br/>if-lt")
	30("n°8<br/>if-lt") --> 34("n°9<br/>const/4")
	30("n°8<br/>if-lt") -.-> 38("n°11<br/>const/4")
	34("n°9<br/>const/4") --> 36("n°10<br/>goto")
	36("n°10<br/>goto") --> 40("n°12<br/>const-string")
	38("n°11<br/>const/4") --> 40("n°12<br/>const-string")
	40("n°12<br/>const-string") --> 44("n°13<br/>const-string")
	44("n°13<br/>const-string") --> 48("n°14<br/>const-string")
	48("n°14<br/>const-string") --> 52("n°15<br/>if-nez")
	52("n°15<br/>if-nez") --> 56("n°16<br/>const-string")
	52("n°15<br/>if-nez") -.-> 66("n°18<br/>invoke-super")
	56("n°16<br/>const-string") --> 60("n°17<br/>invoke-interface")
	60("n°17<br/>invoke-interface") --> 66("n°18<br/>invoke-super")
	66("n°18<br/>invoke-super") --> 72("n°19<br/>if-eqz")
	72("n°19<br/>if-eqz") --> 76("n°20<br/>const-string")
	72("n°19<br/>if-eqz") -.-> 86("n°22<br/>const-string")
	76("n°20<br/>const-string") --> 80("n°21<br/>invoke-interface")
	80("n°21<br/>invoke-interface") --> 86("n°22<br/>const-string")
	86("n°22<br/>const-string") --> 90("n°23<br/>invoke-interface")
	90("n°23<br/>invoke-interface") --> 96("n°24<br/>const-string")
	96("n°24<br/>const-string") --> 100("n°25<br/>invoke-interface")
	100("n°25<br/>invoke-interface") --> 106("n°26<br/>invoke-super")
	106("n°26<br/>invoke-super") --> 112("n°27<br/>invoke-super")
	112("n°27<br/>invoke-super") --> 118("n°28<br/>if-nez")
	118("n°28<br/>if-nez") --> 122("n°29<br/>invoke-interface")
	118("n°28<br/>if-nez") -.-> 128("n°30<br/>invoke-interface")
	122("n°29<br/>invoke-interface") --> 128("n°30<br/>invoke-interface")
	128("n°30<br/>invoke-interface") --> 134("n°31<br/>move-result-object")
	134("n°31<br/>move-result-object") --> 136("n°32<br/>invoke-interface")
	136("n°32<br/>invoke-interface") --> 142("n°33<br/>invoke-interface")
	142("n°33<br/>invoke-interface") --> 148("n°34<br/>move-result")
	148("n°34<br/>move-result") --> 150("n°35<br/>if-nez")
	150("n°35<br/>if-nez") --> 154("n°36<br/>invoke-interface")
	150("n°35<br/>if-nez") -.-> 160("n°37<br/>return-void")
	154("n°36<br/>invoke-interface") --> 160("n°37<br/>return-void")
	162("n°38<br/>move-exception") --> 164("n°39<br/>invoke-super")
	164("n°39<br/>invoke-super") --> 170("n°40<br/>if-nez")
	170("n°40<br/>if-nez") --> 174("n°41<br/>invoke-interface")
	170("n°40<br/>if-nez") -.-> 180("n°42<br/>invoke-interface")
	174("n°41<br/>invoke-interface") --> 180("n°42<br/>invoke-interface")
	180("n°42<br/>invoke-interface") --> 186("n°43<br/>move-result-object")
	186("n°43<br/>move-result-object") --> 188("n°44<br/>invoke-interface")
	188("n°44<br/>invoke-interface") --> 194("n°45<br/>invoke-interface")
	194("n°45<br/>invoke-interface") --> 200("n°46<br/>move-result")
	200("n°46<br/>move-result") --> 202("n°47<br/>if-nez")
	202("n°47<br/>if-nez") --> 206("n°48<br/>invoke-interface")
	202("n°47<br/>if-nez") -.-> 212("n°49<br/>throw")
	206("n°48<br/>invoke-interface") --> 212("n°49<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->assertNotMainThread()V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|6|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->getOpenHelper()Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**3**|12|move-result-object|v0|Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteOpenHelper;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;|Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v0|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**6**|22|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**7**|26|const/16|v2, 21|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**8**|30|if-lt|v1, v2, +4|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**9**|34|const/4|v1, 1|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**10**|36|goto|+2|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**11**|38|const/4|v1, 0|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**12**|40|const-string|v2, 'VACUUM'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**13**|44|const-string|v3, 'PRAGMA foreign_keys = TRUE'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**14**|48|const-string|v4, 'PRAGMA wal_checkpoint(FULL)'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**15**|52|if-nez|v1, +7|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**16**|56|const-string|v5, 'PRAGMA foreign_keys = FALSE'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**17**|60|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**18**|66|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->beginTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**19**|72|if-eqz|v1, +7|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**20**|76|const-string|v5, 'PRAGMA defer_foreign_keys = TRUE'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**21**|80|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**22**|86|const-string|v5, 'DELETE FROM `association_table`'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**23**|90|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**24**|96|const-string|v5, 'DELETE FROM `event_table`'|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**25**|100|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**26**|106|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->setTransactionSuccessful()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**27**|112|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->endTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**28**|118|if-nez|v1, +5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**29**|122|invoke-interface|v0, v3, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**30**|128|invoke-interface|v0, v4, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Ljava/lang/String;)Landroid/database/Cursor;|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**31**|134|move-result-object|v3|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/database/Cursor;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**32**|136|invoke-interface|v3, Landroid/database/Cursor;->close()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/database/Cursor;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**33**|142|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->inTransaction()Z|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/database/Cursor;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**34**|148|move-result|v3|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**35**|150|if-nez|v3, +5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**36**|154|invoke-interface|v0, v2, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|Ljava/lang/String;|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**37**|160|return-void||<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**38**|162|move-exception|v5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/Exception;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**39**|164|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->endTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**40**|170|if-nez|v1, +5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**41**|174|invoke-interface|v0, v3, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>le registre v1 est de type "Ljava/lang/String;" au lieux de "boolean".</span>|
|**42**|180|invoke-interface|v0, v4, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Ljava/lang/String;)Landroid/database/Cursor;|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**43**|186|move-result-object|v3|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/database/Cursor;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**44**|188|invoke-interface|v3, Landroid/database/Cursor;->close()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/database/Cursor;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**45**|194|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->inTransaction()Z|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/database/Cursor;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**46**|200|move-result|v3|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**47**|202|if-nez|v3, +5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**48**|206|invoke-interface|v0, v2, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|Landroidx/sqlite/db/SupportSQLiteDatabase;|<span style='color:grey'>int</span>|Ljava/lang/String;|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**49**|212|throw|v5|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteDatabase;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/Exception;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->assertNotMainThread()V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->getOpenHelper()Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteOpenHelper;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|sget|v1, Landroid/os/Build$VERSION;->SDK_INT I|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const/16|v2, 21|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|if-lt|v1, v2, +4|<span style='color:grey'>True</span>|False|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const/4|v1, 1|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|goto|+2|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|const/4|v1, 0|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|const-string|v2, 'VACUUM'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|const-string|v3, 'PRAGMA foreign_keys = TRUE'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|const-string|v4, 'PRAGMA wal_checkpoint(FULL)'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|if-nez|v1, +7|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|const-string|v5, 'PRAGMA foreign_keys = FALSE'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->beginTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**19**|if-eqz|v1, +7|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|const-string|v5, 'PRAGMA defer_foreign_keys = TRUE'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|const-string|v5, 'DELETE FROM `association_table`'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|const-string|v5, 'DELETE FROM `event_table`'|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-interface|v0, v5, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->setTransactionSuccessful()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**27**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**28**|if-nez|v1, +5|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|invoke-interface|v0, v3, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-interface|v0, v4, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Ljava/lang/String;)Landroid/database/Cursor;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|invoke-interface|v3, Landroid/database/Cursor;->close()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->inTransaction()Z|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|move-result|v3|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**35**|if-nez|v3, +5|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|invoke-interface|v0, v2, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**37**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**38**|move-exception|v5|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**39**|invoke-super|v6, Lcom/example/eva/database/EvaRoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**40**|if-nez|v1, +5|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**41**|invoke-interface|v0, v3, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**42**|invoke-interface|v0, v4, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Ljava/lang/String;)Landroid/database/Cursor;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**43**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**44**|invoke-interface|v3, Landroid/database/Cursor;->close()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**45**|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteDatabase;->inTransaction()Z|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**46**|move-result|v3|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**47**|if-nez|v3, +5|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**48**|invoke-interface|v0, v2, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**49**|throw|v5|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## createInvalidationTracker
  
**Signature :** `()Landroidx/room/InvalidationTracker;`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected androidx.room.InvalidationTracker createInvalidationTracker()
{
    return new androidx.room.InvalidationTracker(this, new java.util.HashMap(0), new java.util.HashMap(0), new String[] {"association_table", "event_table"}));
}
```
## createOpenHelper
  
**Signature :** `(Landroidx/room/DatabaseConfiguration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected androidx.sqlite.db.SupportSQLiteOpenHelper createOpenHelper(androidx.room.DatabaseConfiguration p5)
{
    return p5.sqliteOpenHelperFactory.create(androidx.sqlite.db.SupportSQLiteOpenHelper$Configuration.builder(p5.context).name(p5.name).callback(new androidx.room.RoomOpenHelper(p5, new com.example.eva.database.EvaRoomDatabase_Impl$1(this, 3), "deff8f62b11b3c219184a63f7488c87e", "f03439b0da2eda12fd4900062631e52e")).build());
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>new-instance")
	4("n°2<br/>new-instance") --> 8("n°3<br/>const/4")
	8("n°3<br/>const/4") --> 10("n°4<br/>invoke-direct")
	10("n°4<br/>invoke-direct") --> 16("n°5<br/>const-string")
	16("n°5<br/>const-string") --> 20("n°6<br/>const-string")
	20("n°6<br/>const-string") --> 24("n°7<br/>invoke-direct")
	24("n°7<br/>invoke-direct") --> 30("n°8<br/>iget-object")
	30("n°8<br/>iget-object") --> 34("n°9<br/>invoke-static")
	34("n°9<br/>invoke-static") --> 40("n°10<br/>move-result-object")
	40("n°10<br/>move-result-object") --> 42("n°11<br/>iget-object")
	42("n°11<br/>iget-object") --> 46("n°12<br/>invoke-virtual")
	46("n°12<br/>invoke-virtual") --> 52("n°13<br/>move-result-object")
	52("n°13<br/>move-result-object") --> 54("n°14<br/>invoke-virtual")
	54("n°14<br/>invoke-virtual") --> 60("n°15<br/>move-result-object")
	60("n°15<br/>move-result-object") --> 62("n°16<br/>invoke-virtual")
	62("n°16<br/>invoke-virtual") --> 68("n°17<br/>move-result-object")
	68("n°17<br/>move-result-object") --> 70("n°18<br/>iget-object")
	70("n°18<br/>iget-object") --> 74("n°19<br/>invoke-interface")
	74("n°19<br/>invoke-interface") --> 80("n°20<br/>move-result-object")
	80("n°20<br/>move-result-object") --> 82("n°21<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Landroidx/room/RoomOpenHelper;|Landroidx/room/RoomOpenHelper;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**2**|4|new-instance|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**3**|8|const/4|v2, 3|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-direct|v1, v4, v2, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;-><init>(Lcom/example/eva/database/EvaRoomDatabase_Impl; I)V|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|int|<span style='color:grey'>None</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**5**|16|const-string|v2, 'deff8f62b11b3c219184a63f7488c87e'|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**6**|20|const-string|v3, 'f03439b0da2eda12fd4900062631e52e'|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl$1;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-direct|v0, v5, v1, v2, v3, Landroidx/room/RoomOpenHelper;-><init>(Landroidx/room/DatabaseConfiguration; Landroidx/room/RoomOpenHelper$Delegate; Ljava/lang/String; Ljava/lang/String;)V|Landroidx/room/RoomOpenHelper;|Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|Landroidx/room/DatabaseConfiguration;|<span style='color:#f14848'></span>|
|**8**|30|iget-object|v1, v5, Landroidx/room/DatabaseConfiguration;->context Landroid/content/Context;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroid/content/Context;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|Landroidx/room/DatabaseConfiguration;|<span style='color:#f14848'></span>|
|**9**|34|invoke-static|v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;->builder(Landroid/content/Context;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroid/content/Context;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**10**|40|move-result-object|v1|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**11**|42|iget-object|v2, v5, Landroidx/room/DatabaseConfiguration;->name Ljava/lang/String;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|Landroidx/room/DatabaseConfiguration;|<span style='color:#f14848'></span>|
|**12**|46|invoke-virtual|v1, v2, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->name(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**13**|52|move-result-object|v1|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**14**|54|invoke-virtual|v1, v0, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->callback(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|Landroidx/room/RoomOpenHelper;|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**15**|60|move-result-object|v1|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**16**|62|invoke-virtual|v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->build()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**17**|68|move-result-object|v1|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**18**|70|iget-object|v2, v5, Landroidx/room/DatabaseConfiguration;->sqliteOpenHelperFactory Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|Landroidx/room/DatabaseConfiguration;|<span style='color:#f14848'></span>|
|**19**|74|invoke-interface|v2, v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;->create(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;|Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**20**|80|move-result-object|v2|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
|**21**|82|return-object|v2|<span style='color:grey'>Landroidx/room/RoomOpenHelper;</span>|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;</span>|Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:grey'>Landroidx/room/DatabaseConfiguration;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Landroidx/room/RoomOpenHelper;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|new-instance|v1, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const/4|v2, 3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v1, v4, v2, Lcom/example/eva/database/EvaRoomDatabase_Impl$1;-><init>(Lcom/example/eva/database/EvaRoomDatabase_Impl; I)V|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const-string|v2, 'deff8f62b11b3c219184a63f7488c87e'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const-string|v3, 'f03439b0da2eda12fd4900062631e52e'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-direct|v0, v5, v1, v2, v3, Landroidx/room/RoomOpenHelper;-><init>(Landroidx/room/DatabaseConfiguration; Landroidx/room/RoomOpenHelper$Delegate; Ljava/lang/String; Ljava/lang/String;)V|True|True|True|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**8**|iget-object|v1, v5, Landroidx/room/DatabaseConfiguration;->context Landroid/content/Context;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**9**|invoke-static|v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;->builder(Landroid/content/Context;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|iget-object|v2, v5, Landroidx/room/DatabaseConfiguration;->name Ljava/lang/String;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v1, v2, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->name(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v1, v0, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->callback(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->build()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|iget-object|v2, v5, Landroidx/room/DatabaseConfiguration;->sqliteOpenHelperFactory Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**19**|invoke-interface|v2, v1, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;->create(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|return-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## myDao
  
**Signature :** `()Lcom/example/eva/database/MyDao;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.MyDao myDao()
{
    if (this._myDao == null) {
        try {
            if (this._myDao == null) {
                this._myDao = new com.example.eva.database.MyDao_Impl(this);
            }
        } catch (com.example.eva.database.MyDao v0_6) {
            throw v0_6;
        }
        return this._myDao;
    } else {
        return this._myDao;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>if-eqz")
	4("n°2<br/>if-eqz") --> 8("n°3<br/>iget-object")
	4("n°2<br/>if-eqz") -.-> 14("n°5<br/>monitor-enter")
	8("n°3<br/>iget-object") --> 12("n°4<br/>return-object")
	14("n°5<br/>monitor-enter") --> 16("n°6<br/>iget-object")
	16("n°6<br/>iget-object") --> 20("n°7<br/>if-nez")
	20("n°7<br/>if-nez") --> 24("n°8<br/>new-instance")
	20("n°7<br/>if-nez") -.-> 38("n°11<br/>iget-object")
	24("n°8<br/>new-instance") --> 28("n°9<br/>invoke-direct")
	28("n°9<br/>invoke-direct") --> 34("n°10<br/>iput-object")
	34("n°10<br/>iput-object") --> 38("n°11<br/>iget-object")
	38("n°11<br/>iget-object") --> 42("n°12<br/>monitor-exit")
	42("n°12<br/>monitor-exit") --> 44("n°13<br/>return-object")
	46("n°14<br/>move-exception") --> 48("n°15<br/>monitor-exit")
	48("n°15<br/>monitor-exit") --> 50("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**2**|4|if-eqz|v0, +5|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**3**|8|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**4**|12|return-object|v0|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**5**|14|monitor-enter|v1|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**6**|16|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**7**|20|if-nez|v0, +9|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**8**|24|new-instance|v0, Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**9**|28|invoke-direct|v0, v1, Lcom/example/eva/database/MyDao_Impl;-><init>(Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**10**|34|iput-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**11**|38|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**12**|42|monitor-exit|v1|<span style='color:grey'>Lcom/example/eva/database/MyDao;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**13**|44|return-object|v0|Lcom/example/eva/database/MyDao;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**14**|46|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
|**15**|48|monitor-exit|v1|<span style='color:grey'>Ljava/lang/Exception;</span>|Lcom/example/eva/database/EvaRoomDatabase_Impl;|<span style='color:#f14848'></span>|
|**16**|50|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Lcom/example/eva/database/EvaRoomDatabase_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|True|True|<span style='color:#f14848'></span>|
|**2**|if-eqz|v0, +5|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|True|True|<span style='color:#f14848'></span>|
|**4**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|monitor-enter|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**6**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|True|True|<span style='color:#f14848'></span>|
|**7**|if-nez|v0, +9|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-direct|v0, v1, Lcom/example/eva/database/MyDao_Impl;-><init>(Landroidx/room/RoomDatabase;)V|True|True|<span style='color:#f14848'></span>|
|**10**|iput-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|True|True|<span style='color:#f14848'></span>|
|**11**|iget-object|v0, v1, Lcom/example/eva/database/EvaRoomDatabase_Impl;->_myDao Lcom/example/eva/database/MyDao;|True|True|<span style='color:#f14848'></span>|
|**12**|monitor-exit|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**13**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|move-exception|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|monitor-exit|v1|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
