
com.example.eva.EventActivity
=============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [isSameDay](#issameday)
	* [finish](#finish)
	* [onCreate](#oncreate)
	* [onOptionsItemSelected](#onoptionsitemselected)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**isSameDay**](#issameday)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**finish**](#finish)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onOptionsItemSelected**](#onoptionsitemselected)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public EventActivity()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|Lcom/example/eva/EventActivity;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## isSameDay
  
**Signature :** `(Ljava/util/Calendar; Ljava/util/Calendar;)Z`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
private boolean isSameDay(java.util.Calendar p4, java.util.Calendar p5)
{
    java.text.SimpleDateFormat v0_1 = new java.text.SimpleDateFormat("yyyyMMdd");
    return v0_1.format(p4.getTime()).equals(v0_1.format(p5.getTime()));
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>const-string")
	4("n°2<br/>const-string") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>invoke-virtual")
	22("n°6<br/>invoke-virtual") --> 28("n°7<br/>move-result-object")
	28("n°7<br/>move-result-object") --> 30("n°8<br/>invoke-virtual")
	30("n°8<br/>invoke-virtual") --> 36("n°9<br/>move-result-object")
	36("n°9<br/>move-result-object") --> 38("n°10<br/>invoke-virtual")
	38("n°10<br/>invoke-virtual") --> 44("n°11<br/>move-result-object")
	44("n°11<br/>move-result-object") --> 46("n°12<br/>invoke-virtual")
	46("n°12<br/>invoke-virtual") --> 52("n°13<br/>move-result")
	52("n°13<br/>move-result") --> 54("n°14<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/text/SimpleDateFormat;|Ljava/text/SimpleDateFormat;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**2**|4|const-string|v1, 'yyyyMMdd'|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, v1, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V|Ljava/text/SimpleDateFormat;|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v4, Ljava/util/Calendar;->getTime()Ljava/util/Date;|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|Ljava/util/Calendar;|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v1|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**6**|22|invoke-virtual|v0, v1, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;|Ljava/text/SimpleDateFormat;|Ljava/util/Date;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**7**|28|move-result-object|v1|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**8**|30|invoke-virtual|v5, Ljava/util/Calendar;->getTime()Ljava/util/Date;|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|Ljava/util/Calendar;|<span style='color:#f14848'></span>|
|**9**|36|move-result-object|v2|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/util/Date;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**10**|38|invoke-virtual|v0, v2, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;|Ljava/text/SimpleDateFormat;|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/util/Date;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**11**|44|move-result-object|v2|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**12**|46|invoke-virtual|v1, v2, Ljava/lang/String;->equals(Ljava/lang/Object;)Z|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**13**|52|move-result|v1|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
|**14**|54|return|v1|<span style='color:grey'>Ljava/text/SimpleDateFormat;</span>|boolean|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:grey'>Ljava/util/Calendar;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/text/SimpleDateFormat;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const-string|v1, 'yyyyMMdd'|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, v1, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v4, Ljava/util/Calendar;->getTime()Ljava/util/Date;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v1, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v5, Ljava/util/Calendar;->getTime()Ljava/util/Date;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**9**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, v2, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v1, v2, Ljava/lang/String;->equals(Ljava/lang/Object;)Z|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|return|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## finish
  
**Signature :** `()V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void finish()
{
    super.finish();
    this.overridePendingTransition(2130772010, 2130772013);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>const")
	6("n°2<br/>const") --> 12("n°3<br/>const")
	12("n°3<br/>const") --> 18("n°4<br/>invoke-virtual")
	18("n°4<br/>invoke-virtual") --> 24("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v2, Lcom/example/eva/Environment;->finish()V|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/EventActivity;|<span style='color:#f14848'></span>|
|**2**|6|const|v0, 2130772010 # [1.714712633174575e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:#f14848'></span>|
|**3**|12|const|v1, 2130772013 # [1.714713241646863e+38]|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:#f14848'></span>|
|**4**|18|invoke-virtual|v2, v0, v1, Lcom/example/eva/EventActivity;->overridePendingTransition(I I)V|int|int|Lcom/example/eva/EventActivity;|<span style='color:#f14848'></span>|
|**5**|24|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v2, Lcom/example/eva/Environment;->finish()V|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|const|v0, 2130772010 # [1.714712633174575e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v1, 2130772013 # [1.714713241646863e+38]|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v2, v0, v1, Lcom/example/eva/EventActivity;->overridePendingTransition(I I)V|False|False|True|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 21  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void onCreate(android.os.Bundle p20)
{
    void v19_1 = super.onCreate(p20);
    this.setContentView(2131427356);
    android.view.View v3_19 = v19_1.getIntent().getIntExtra("idEvent", -1);
    this.eventViewModel = ((com.example.eva.database.event.EventViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.event.EventViewModel));
    this.associationViewModel = ((com.example.eva.database.association.AssociationViewModel) new androidx.lifecycle.ViewModelProvider(this).get(com.example.eva.database.association.AssociationViewModel));
    this.setContentView(2131427356);
    this.setSupportActionBar(((androidx.appcompat.widget.Toolbar) this.findViewById(2131231128)));
    ((androidx.appcompat.app.ActionBar) java.util.Objects.requireNonNull(v19_1.getSupportActionBar())).setDisplayHomeAsUpEnabled(1);
    com.example.eva.database.event.Event v1_8 = this.eventViewModel.get(v3_19);
    android.widget.LinearLayout v5_4 = ((android.widget.LinearLayout) this.findViewById(2131230895));
    android.text.Spanned v6_7 = ((android.widget.TextView) this.findViewById(2131230899));
    v6_7.setText(v1_8.getSummary());
    java.util.GregorianCalendar v7_2 = new java.util.GregorianCalendar();
    java.util.GregorianCalendar v8_1 = new java.util.GregorianCalendar();
    java.util.GregorianCalendar v9_1 = new java.util.GregorianCalendar();
    v8_1.setTime(v1_8.getDateEnd());
    v9_1.setTime(v1_8.getDateStart());
    StringBuilder v10_3 = new StringBuilder();
    StringBuilder v11_1 = new StringBuilder();
    java.text.SimpleDateFormat v12_1 = new java.text.SimpleDateFormat("HH:mm");
    java.text.SimpleDateFormat v13_2 = new java.text.SimpleDateFormat("EEEE dd MMM yyyy");
    java.text.SimpleDateFormat v14_2 = new java.text.SimpleDateFormat("EEEE dd MMM");
    if (!this.isSameDay(v9_1, v8_1)) {
        int v17 = v3_19;
        if (v9_1.get(1) != v7_2.get(1)) {
            v10_3.append(v13_2.format(v9_1.getTime()));
            v10_3.append(" \u25cf ");
            v10_3.append(v12_1.format(v9_1.getTime()));
            v10_3.append(" \u2500 ");
            v11_1.append(v13_2.format(v8_1.getTime()));
            v11_1.append(" \u25cf ");
            v11_1.append(v12_1.format(v8_1.getTime()));
        } else {
            v10_3.append(v14_2.format(v9_1.getTime()));
            v10_3.append(" \u25cf ");
            v10_3.append(v12_1.format(v9_1.getTime()));
            v10_3.append(" \u2500 ");
            v11_1.append(v14_2.format(v8_1.getTime()));
            v11_1.append(" \u25cf ");
            v11_1.append(v12_1.format(v8_1.getTime()));
        }
        ((android.widget.LinearLayout) this.findViewById(2131230886)).setOrientation(1);
    } else {
        if (!this.isSameDay(v9_1, v7_2)) {
            v17 = v3_19;
            if (v9_1.get(1) != v7_2.get(1)) {
                v10_3.append(v13_2.format(v9_1.getTime()));
                v10_3.append(" \u25cf ");
                v10_3.append(v12_1.format(v9_1.getTime()));
                v10_3.append(" \u2500 ");
                v11_1.append(v12_1.format(v8_1.getTime()));
            } else {
                v10_3.append(v14_2.format(v9_1.getTime()));
                v10_3.append(" \u25cf ");
                v10_3.append(v12_1.format(v9_1.getTime()));
                v10_3.append(" \u2500 ");
                v11_1.append(v12_1.format(v8_1.getTime()));
            }
        } else {
            v10_3.append("Aujourd\'hui");
            v10_3.append(" \u25cf ");
            v10_3.append(v12_1.format(v9_1.getTime()));
            v10_3.append(" \u2500 ");
            v11_1.append(v12_1.format(v8_1.getTime()));
            int v18 = v6_7;
        }
    }
    ((android.widget.TextView) this.findViewById(2131230887)).setText(v10_3);
    ((android.widget.TextView) this.findViewById(2131230885)).setText(v11_1);
    if (!v1_8.isCanceled().booleanValue()) {
        v5_4.removeView(this.findViewById(2131230896));
    }
    ((android.widget.TextView) this.findViewById(2131230884)).setText(this.associationViewModel.get(v1_8.getIdAssociation().intValue()).getName());
    if (v1_8.getLocation() == null) {
        v5_4.removeView(this.findViewById(2131230894));
    } else {
        ((android.widget.TextView) this.findViewById(2131230893)).setText(v1_8.getLocation());
    }
    if (v1_8.getDescription() == null) {
        v5_4.removeView(this.findViewById(2131230889));
    } else {
        ((android.widget.TextView) this.findViewById(2131230888)).setText(android.text.Html.fromHtml(v1_8.getDescription()));
    }
    return;
}
```
## onOptionsItemSelected
  
**Signature :** `(Landroid/view/MenuItem;)Z`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean onOptionsItemSelected(android.view.MenuItem p3)
{
    if (p3.getItemId() != 16908332) {
        return super.onOptionsItemSelected(p3);
    } else {
        this.finish();
        return 1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-interface") --> 6("n°2<br/>move-result")
	6("n°2<br/>move-result") --> 8("n°3<br/>const")
	8("n°3<br/>const") --> 14("n°4<br/>if-ne")
	14("n°4<br/>if-ne") --> 18("n°5<br/>invoke-virtual")
	14("n°4<br/>if-ne") -.-> 28("n°8<br/>invoke-super")
	18("n°5<br/>invoke-virtual") --> 24("n°6<br/>const/4")
	24("n°6<br/>const/4") --> 26("n°7<br/>return")
	28("n°8<br/>invoke-super") --> 34("n°9<br/>move-result")
	34("n°9<br/>move-result") --> 36("n°10<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|Landroid/view/MenuItem;|<span style='color:#f14848'></span>|
|**2**|6|move-result|v0|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**3**|8|const|v1, 16908332 # [2.3877352315342576e-38]|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**4**|14|if-ne|v0, v1, +7|int|int|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-virtual|v2, Lcom/example/eva/EventActivity;->finish()V|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/EventActivity;|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**6**|24|const/4|v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**7**|26|return|v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**8**|28|invoke-super|v2, v3, Lcom/example/eva/Environment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/eva/EventActivity;|Landroid/view/MenuItem;|<span style='color:#f14848'></span>|
|**9**|34|move-result|v0|boolean|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**10**|36|return|v0|boolean|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/EventActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v1, 16908332 # [2.3877352315342576e-38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-ne|v0, v1, +7|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v2, Lcom/example/eva/EventActivity;->finish()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v0, 1|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-super|v2, v3, Lcom/example/eva/Environment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**9**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
