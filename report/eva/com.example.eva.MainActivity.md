
com.example.eva.MainActivity
============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [loadData](#loaddata)
	* [onCreate](#oncreate)
	* [onCreateOptionsMenu](#oncreateoptionsmenu)
	* [onOptionsItemSelected](#onoptionsitemselected)
	* [onSupportNavigateUp](#onsupportnavigateup)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**loadData**](#loaddata)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreate**](#oncreate)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onCreateOptionsMenu**](#oncreateoptionsmenu)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onOptionsItemSelected**](#onoptionsitemselected)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**onSupportNavigateUp**](#onsupportnavigateup)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public MainActivity()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Lcom/example/eva/Environment;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## loadData
  
**Signature :** `()V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
private void loadData()
{
    this.associationViewModel.insert(new com.example.eva.database.association.Association("FIFO", "rss10msnvjieg58tcna32dki4c@group.calendar.google.com", Integer.valueOf(2131558404), Boolean.valueOf(1)));
    com.example.eva.database.association.AssociationViewModel v0_1 = this.associationViewModel;
    Integer v2_1 = Integer.valueOf(2131558405);
    Boolean v3_1 = Boolean.valueOf(0);
    v0_1.insert(new com.example.eva.database.association.Association("INFASSO", "edjm86nf7acjfq3gbvrmhur6hg@group.calendar.google.com", v2_1, v3_1));
    this.associationViewModel.insert(new com.example.eva.database.association.Association("Tribu Terre", "gu6jlhjui9s34ede58s3kub6ec@group.calendar.google.com", Integer.valueOf(2131558406), v3_1));
    this.associationViewModel.insert(new com.example.eva.database.association.Association("AMIGO", "au47go9j2h1ugt7rl98j9qdh7s@group.calendar.google.com", Integer.valueOf(2131558403), v3_1));
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>new-instance")
	4("n°2<br/>new-instance") --> 8("n°3<br/>const")
	8("n°3<br/>const") --> 14("n°4<br/>invoke-static")
	14("n°4<br/>invoke-static") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>invoke-static")
	24("n°7<br/>invoke-static") --> 30("n°8<br/>move-result-object")
	30("n°8<br/>move-result-object") --> 32("n°9<br/>const-string")
	32("n°9<br/>const-string") --> 36("n°10<br/>const-string")
	36("n°10<br/>const-string") --> 40("n°11<br/>invoke-direct")
	40("n°11<br/>invoke-direct") --> 46("n°12<br/>invoke-virtual")
	46("n°12<br/>invoke-virtual") --> 52("n°13<br/>iget-object")
	52("n°13<br/>iget-object") --> 56("n°14<br/>new-instance")
	56("n°14<br/>new-instance") --> 60("n°15<br/>const")
	60("n°15<br/>const") --> 66("n°16<br/>invoke-static")
	66("n°16<br/>invoke-static") --> 72("n°17<br/>move-result-object")
	72("n°17<br/>move-result-object") --> 74("n°18<br/>const/4")
	74("n°18<br/>const/4") --> 76("n°19<br/>invoke-static")
	76("n°19<br/>invoke-static") --> 82("n°20<br/>move-result-object")
	82("n°20<br/>move-result-object") --> 84("n°21<br/>const-string")
	84("n°21<br/>const-string") --> 88("n°22<br/>const-string")
	88("n°22<br/>const-string") --> 92("n°23<br/>invoke-direct")
	92("n°23<br/>invoke-direct") --> 98("n°24<br/>invoke-virtual")
	98("n°24<br/>invoke-virtual") --> 104("n°25<br/>iget-object")
	104("n°25<br/>iget-object") --> 108("n°26<br/>new-instance")
	108("n°26<br/>new-instance") --> 112("n°27<br/>const")
	112("n°27<br/>const") --> 118("n°28<br/>invoke-static")
	118("n°28<br/>invoke-static") --> 124("n°29<br/>move-result-object")
	124("n°29<br/>move-result-object") --> 126("n°30<br/>const-string")
	126("n°30<br/>const-string") --> 130("n°31<br/>const-string")
	130("n°31<br/>const-string") --> 134("n°32<br/>invoke-direct")
	134("n°32<br/>invoke-direct") --> 140("n°33<br/>invoke-virtual")
	140("n°33<br/>invoke-virtual") --> 146("n°34<br/>iget-object")
	146("n°34<br/>iget-object") --> 150("n°35<br/>new-instance")
	150("n°35<br/>new-instance") --> 154("n°36<br/>const")
	154("n°36<br/>const") --> 160("n°37<br/>invoke-static")
	160("n°37<br/>invoke-static") --> 166("n°38<br/>move-result-object")
	166("n°38<br/>move-result-object") --> 168("n°39<br/>const-string")
	168("n°39<br/>const-string") --> 172("n°40<br/>const-string")
	172("n°40<br/>const-string") --> 176("n°41<br/>invoke-direct")
	176("n°41<br/>invoke-direct") --> 182("n°42<br/>invoke-virtual")
	182("n°42<br/>invoke-virtual") --> 188("n°43<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**2**|4|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**3**|8|const|v2, 2131558404 # [1.8742122853531155e+38]|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v3, 1|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-static|v3, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**8**|30|move-result-object|v3|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|Ljava/lang/Boolean;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**9**|32|const-string|v4, 'FIFO'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**10**|36|const-string|v5, 'rss10msnvjieg58tcna32dki4c@group.calendar.google.com'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**11**|40|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|Ljava/lang/Integer;|Ljava/lang/Boolean;|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**12**|46|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**13**|52|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**14**|56|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**15**|60|const|v2, 2131558405 # [1.8742124881772116e+38]|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**16**|66|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**17**|72|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**18**|74|const/4|v3, 0|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**19**|76|invoke-static|v3, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|int|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**20**|82|move-result-object|v3|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|Ljava/lang/Boolean;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**21**|84|const-string|v4, 'INFASSO'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**22**|88|const-string|v5, 'edjm86nf7acjfq3gbvrmhur6hg@group.calendar.google.com'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**23**|92|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|Ljava/lang/Integer;|Ljava/lang/Boolean;|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**24**|98|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**25**|104|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**26**|108|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**27**|112|const|v2, 2131558406 # [1.8742126910013076e+38]|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**28**|118|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**29**|124|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**30**|126|const-string|v4, 'Tribu Terre'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**31**|130|const-string|v5, 'gu6jlhjui9s34ede58s3kub6ec@group.calendar.google.com'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**32**|134|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|Ljava/lang/Integer;|Ljava/lang/Boolean;|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**33**|140|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**34**|146|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/AssociationViewModel;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**35**|150|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**36**|154|const|v2, 2131558403 # [1.8742120825290195e+38]|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**37**|160|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|int|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**38**|166|move-result-object|v2|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**39**|168|const-string|v4, 'AMIGO'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**40**|172|const-string|v5, 'au47go9j2h1ugt7rl98j9qdh7s@group.calendar.google.com'|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**41**|176|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|Lcom/example/eva/database/association/Association;|Ljava/lang/Integer;|Ljava/lang/Boolean;|Ljava/lang/String;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**42**|182|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|Lcom/example/eva/database/association/AssociationViewModel;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**43**|188|return-void||<span style='color:grey'>Lcom/example/eva/database/association/AssociationViewModel;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v2, 2131558404 # [1.8742122853531155e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v3, 1|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-static|v3, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const-string|v4, 'FIFO'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|const-string|v5, 'rss10msnvjieg58tcna32dki4c@group.calendar.google.com'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>True</span>|True|True|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**14**|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|const|v2, 2131558405 # [1.8742124881772116e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|const/4|v3, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-static|v3, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|const-string|v4, 'INFASSO'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|const-string|v5, 'edjm86nf7acjfq3gbvrmhur6hg@group.calendar.google.com'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>True</span>|True|True|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**26**|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|const|v2, 2131558406 # [1.8742126910013076e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|const-string|v4, 'Tribu Terre'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|const-string|v5, 'gu6jlhjui9s34ede58s3kub6ec@group.calendar.google.com'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>True</span>|True|True|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|iget-object|v0, v6, Lcom/example/eva/MainActivity;->associationViewModel Lcom/example/eva/database/association/AssociationViewModel;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**35**|new-instance|v1, Lcom/example/eva/database/association/Association;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|const|v2, 2131558403 # [1.8742120825290195e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**37**|invoke-static|v2, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**38**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**39**|const-string|v4, 'AMIGO'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**40**|const-string|v5, 'au47go9j2h1ugt7rl98j9qdh7s@group.calendar.google.com'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**41**|invoke-direct|v1, v4, v5, v2, v3, Lcom/example/eva/database/association/Association;-><init>(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V|<span style='color:grey'>True</span>|True|True|True|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**42**|invoke-virtual|v0, v1, Lcom/example/eva/database/association/AssociationViewModel;->insert(Lcom/example/eva/database/association/Association;)V|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**43**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreate
  
**Signature :** `(Landroid/os/Bundle;)V`  
**Nombre de registre :** 11  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void onCreate(android.os.Bundle p10)
{
    super.onCreate(p10);
    this.setContentView(2131427358);
    this.setSupportActionBar(((androidx.appcompat.widget.Toolbar) this.findViewById(2131231128)));
    com.google.android.material.navigation.NavigationView v2_2 = ((com.google.android.material.navigation.NavigationView) this.findViewById(2131230997));
    androidx.navigation.NavController v4_1 = new int[4];
    v4_1 = {2131230992, 2131230991, 2131230995, 2131230996};
    this.mAppBarConfiguration = new androidx.navigation.ui.AppBarConfiguration$Builder(v4_1).setOpenableLayout(((androidx.drawerlayout.widget.DrawerLayout) this.findViewById(2131230871))).build();
    androidx.navigation.NavController v4_5 = ((androidx.navigation.fragment.NavHostFragment) java.util.Objects.requireNonNull(((androidx.navigation.fragment.NavHostFragment) this.getSupportFragmentManager().findFragmentById(2131230993)))).getNavController();
    androidx.navigation.ui.NavigationUI.setupActionBarWithNavController(this, v4_5, this.mAppBarConfiguration);
    androidx.navigation.ui.NavigationUI.setupWithNavController(v2_2, v4_5);
    android.content.SharedPreferences v5_1 = androidx.preference.PreferenceManager.getDefaultSharedPreferences(this);
    if (v5_1.getBoolean("FIRST_TIME_AFTER_INSTALLATION", 1)) {
        this.loadData();
        android.content.SharedPreferences$Editor v7_2 = v5_1.edit();
        v7_2.putBoolean("FIRST_TIME_AFTER_INSTALLATION", 0);
        v7_2.apply();
    }
    com.example.eva.service.api.google.calendar.DataSynchronisationWorker.shedulerPeriodic(this);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-super") --> 6("n°2<br/>const")
	6("n°2<br/>const") --> 12("n°3<br/>invoke-virtual")
	12("n°3<br/>invoke-virtual") --> 18("n°4<br/>const")
	18("n°4<br/>const") --> 24("n°5<br/>invoke-virtual")
	24("n°5<br/>invoke-virtual") --> 30("n°6<br/>move-result-object")
	30("n°6<br/>move-result-object") --> 32("n°7<br/>check-cast")
	32("n°7<br/>check-cast") --> 36("n°8<br/>invoke-virtual")
	36("n°8<br/>invoke-virtual") --> 42("n°9<br/>const")
	42("n°9<br/>const") --> 48("n°10<br/>invoke-virtual")
	48("n°10<br/>invoke-virtual") --> 54("n°11<br/>move-result-object")
	54("n°11<br/>move-result-object") --> 56("n°12<br/>check-cast")
	56("n°12<br/>check-cast") --> 60("n°13<br/>const")
	60("n°13<br/>const") --> 66("n°14<br/>invoke-virtual")
	66("n°14<br/>invoke-virtual") --> 72("n°15<br/>move-result-object")
	72("n°15<br/>move-result-object") --> 74("n°16<br/>check-cast")
	74("n°16<br/>check-cast") --> 78("n°17<br/>new-instance")
	78("n°17<br/>new-instance") --> 82("n°18<br/>const/4")
	82("n°18<br/>const/4") --> 84("n°19<br/>new-array")
	84("n°19<br/>new-array") --> 88("n°20<br/>fill-array-data")
	88("n°20<br/>fill-array-data") --> 94("n°21<br/>invoke-direct")
	88("n°20<br/>fill-array-data") -.-> 244("n°56<br/>fill-array-data-payload")
	94("n°21<br/>invoke-direct") --> 100("n°22<br/>invoke-virtual")
	100("n°22<br/>invoke-virtual") --> 106("n°23<br/>move-result-object")
	106("n°23<br/>move-result-object") --> 108("n°24<br/>invoke-virtual")
	108("n°24<br/>invoke-virtual") --> 114("n°25<br/>move-result-object")
	114("n°25<br/>move-result-object") --> 116("n°26<br/>iput-object")
	116("n°26<br/>iput-object") --> 120("n°27<br/>invoke-virtual")
	120("n°27<br/>invoke-virtual") --> 126("n°28<br/>move-result-object")
	126("n°28<br/>move-result-object") --> 128("n°29<br/>const")
	128("n°29<br/>const") --> 134("n°30<br/>invoke-virtual")
	134("n°30<br/>invoke-virtual") --> 140("n°31<br/>move-result-object")
	140("n°31<br/>move-result-object") --> 142("n°32<br/>check-cast")
	142("n°32<br/>check-cast") --> 146("n°33<br/>invoke-static")
	146("n°33<br/>invoke-static") --> 152("n°34<br/>move-result-object")
	152("n°34<br/>move-result-object") --> 154("n°35<br/>check-cast")
	154("n°35<br/>check-cast") --> 158("n°36<br/>invoke-virtual")
	158("n°36<br/>invoke-virtual") --> 164("n°37<br/>move-result-object")
	164("n°37<br/>move-result-object") --> 166("n°38<br/>iget-object")
	166("n°38<br/>iget-object") --> 170("n°39<br/>invoke-static")
	170("n°39<br/>invoke-static") --> 176("n°40<br/>invoke-static")
	176("n°40<br/>invoke-static") --> 182("n°41<br/>invoke-static")
	182("n°41<br/>invoke-static") --> 188("n°42<br/>move-result-object")
	188("n°42<br/>move-result-object") --> 190("n°43<br/>const-string")
	190("n°43<br/>const-string") --> 194("n°44<br/>const/4")
	194("n°44<br/>const/4") --> 196("n°45<br/>invoke-interface")
	196("n°45<br/>invoke-interface") --> 202("n°46<br/>move-result")
	202("n°46<br/>move-result") --> 204("n°47<br/>if-eqz")
	204("n°47<br/>if-eqz") --> 208("n°48<br/>invoke-direct")
	204("n°47<br/>if-eqz") -.-> 236("n°54<br/>invoke-static")
	208("n°48<br/>invoke-direct") --> 214("n°49<br/>invoke-interface")
	214("n°49<br/>invoke-interface") --> 220("n°50<br/>move-result-object")
	220("n°50<br/>move-result-object") --> 222("n°51<br/>const/4")
	222("n°51<br/>const/4") --> 224("n°52<br/>invoke-interface")
	224("n°52<br/>invoke-interface") --> 230("n°53<br/>invoke-interface")
	230("n°53<br/>invoke-interface") --> 236("n°54<br/>invoke-static")
	236("n°54<br/>invoke-static") --> 242("n°55<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|v9|v10|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-super|v9, v10, Lcom/example/eva/Environment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>None</span>|None|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**2**|6|const|v0, 2131427358 # [1.847632998863914e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**3**|12|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->setContentView(I)V|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**4**|18|const|v0, 2131231128 # [1.8078328264986685e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**5**|24|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**6**|30|move-result-object|v0|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**7**|32|check-cast|v0, Landroidx/appcompat/widget/Toolbar;|Landroidx/appcompat/widget/Toolbar;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**8**|36|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V|Landroidx/appcompat/widget/Toolbar;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**9**|42|const|v1, 2131230871 # [1.807780700705987e+38]|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**10**|48|invoke-virtual|v9, v1, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**11**|54|move-result-object|v1|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**12**|56|check-cast|v1, Landroidx/drawerlayout/widget/DrawerLayout;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|Landroidx/drawerlayout/widget/DrawerLayout;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**13**|60|const|v2, 2131230997 # [1.8078062565420877e+38]|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**14**|66|invoke-virtual|v9, v2, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**15**|72|move-result-object|v2|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|Landroid/view/View;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**16**|74|check-cast|v2, Lcom/google/android/material/navigation/NavigationView;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|Lcom/google/android/material/navigation/NavigationView;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**17**|78|new-instance|v3, Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**18**|82|const/4|v4, 4|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration$Builder;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**19**|84|new-array|v4, v4, [I|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration$Builder;</span>|[I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**20**|88|fill-array-data|v4, +4e (0x9b)|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration$Builder;</span>|[I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**21**|94|invoke-direct|v3, v4, Landroidx/navigation/ui/AppBarConfiguration$Builder;-><init>([I)V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration$Builder;|[I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**22**|100|invoke-virtual|v3, v1, Landroidx/navigation/ui/AppBarConfiguration$Builder;->setOpenableLayout(Landroidx/customview/widget/Openable;)Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|Landroidx/drawerlayout/widget/DrawerLayout;|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**23**|106|move-result-object|v3|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**24**|108|invoke-virtual|v3, Landroidx/navigation/ui/AppBarConfiguration$Builder;->build()Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**25**|114|move-result-object|v3|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**26**|116|iput-object|v3, v9, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**27**|120|invoke-virtual|v9, Lcom/example/eva/MainActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**28**|126|move-result-object|v3|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/fragment/app/FragmentManager;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**29**|128|const|v4, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/fragment/app/FragmentManager;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**30**|134|invoke-virtual|v3, v4, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/fragment/app/FragmentManager;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**31**|140|move-result-object|v3|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/fragment/app/Fragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**32**|142|check-cast|v3, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**33**|146|invoke-static|v3, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**34**|152|move-result-object|v4|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**35**|154|check-cast|v4, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**36**|158|invoke-virtual|v4, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**37**|164|move-result-object|v4|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**38**|166|iget-object|v5, v9, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**39**|170|invoke-static|v9, v4, v5, Landroidx/navigation/ui/NavigationUI;->setupActionBarWithNavController(Landroidx/appcompat/app/AppCompatActivity; Landroidx/navigation/NavController; Landroidx/navigation/ui/AppBarConfiguration;)V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**40**|176|invoke-static|v2, v4, Landroidx/navigation/ui/NavigationUI;->setupWithNavController(Lcom/google/android/material/navigation/NavigationView; Landroidx/navigation/NavController;)V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|Lcom/google/android/material/navigation/NavigationView;|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|Landroidx/navigation/NavController;|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**41**|182|invoke-static|v9, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**42**|188|move-result-object|v5|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|Landroid/content/SharedPreferences;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**43**|190|const-string|v6, 'FIRST_TIME_AFTER_INSTALLATION'|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**44**|194|const/4|v7, 1|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**45**|196|invoke-interface|v5, v6, v7, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String; Z)Z|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|Landroid/content/SharedPreferences;|Ljava/lang/String;|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**46**|202|move-result|v7|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**47**|204|if-eqz|v7, +10|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**48**|208|invoke-direct|v9, Lcom/example/eva/MainActivity;->loadData()V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**49**|214|invoke-interface|v5, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|Landroid/content/SharedPreferences;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**50**|220|move-result-object|v7|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**51**|222|const/4|v8, 0|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/SharedPreferences$Editor;</span>|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**52**|224|invoke-interface|v7, v6, v8, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String; Z)Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|Ljava/lang/String;|Landroid/content/SharedPreferences$Editor;|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**53**|230|invoke-interface|v7, Landroid/content/SharedPreferences$Editor;->apply()V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**54**|236|invoke-static|v9, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->shedulerPeriodic(Landroid/content/Context;)V|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Landroid/content/SharedPreferences$Editor;</span>|<span style='color:grey'>int</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**55**|242|return-void||<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/fragment/NavHostFragment;</span>|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>Landroid/content/SharedPreferences;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
|**56**|244|fill-array-data-payload|bytearray(b'\x10\x01\x08\x7f\x0f\x01\x08\x7f\x13\x01\x08\x7f\x14\x01\x08\x7f') \| \x10\x01\x08\x7f\x0f\x01\x08\x7f\x13\x01\x08\x7f\x14\x01\x08\x7f|<span style='color:grey'>Landroidx/appcompat/widget/Toolbar;</span>|<span style='color:grey'>Landroidx/drawerlayout/widget/DrawerLayout;</span>|<span style='color:grey'>Lcom/google/android/material/navigation/NavigationView;</span>|<span style='color:grey'>Landroidx/navigation/ui/AppBarConfiguration$Builder;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/os/Bundle;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|v9|v10|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-super|v9, v10, Lcom/example/eva/Environment;->onCreate(Landroid/os/Bundle;)V|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const|v0, 2131427358 # [1.847632998863914e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->setContentView(I)V|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const|v0, 2131231128 # [1.8078328264986685e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|check-cast|v0, Landroidx/appcompat/widget/Toolbar;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v9, v0, Lcom/example/eva/MainActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|const|v1, 2131230871 # [1.807780700705987e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v9, v1, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|check-cast|v1, Landroidx/drawerlayout/widget/DrawerLayout;|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|const|v2, 2131230997 # [1.8078062565420877e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v9, v2, Lcom/example/eva/MainActivity;->findViewById(I)Landroid/view/View;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|check-cast|v2, Lcom/google/android/material/navigation/NavigationView;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|new-instance|v3, Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|const/4|v4, 4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|new-array|v4, v4, [I|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|fill-array-data|v4, +4e (0x9b)|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|invoke-direct|v3, v4, Landroidx/navigation/ui/AppBarConfiguration$Builder;-><init>([I)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v3, v1, Landroidx/navigation/ui/AppBarConfiguration$Builder;->setOpenableLayout(Landroidx/customview/widget/Openable;)Landroidx/navigation/ui/AppBarConfiguration$Builder;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v3, Landroidx/navigation/ui/AppBarConfiguration$Builder;->build()Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|iput-object|v3, v9, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|invoke-virtual|v9, Lcom/example/eva/MainActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**28**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**29**|const|v4, 2131230993 # [1.8078054452457036e+38]|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**30**|invoke-virtual|v3, v4, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**31**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**32**|check-cast|v3, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**33**|invoke-static|v3, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**34**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**35**|check-cast|v4, Landroidx/navigation/fragment/NavHostFragment;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**36**|invoke-virtual|v4, Landroidx/navigation/fragment/NavHostFragment;->getNavController()Landroidx/navigation/NavController;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**37**|move-result-object|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**38**|iget-object|v5, v9, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**39**|invoke-static|v9, v4, v5, Landroidx/navigation/ui/NavigationUI;->setupActionBarWithNavController(Landroidx/appcompat/app/AppCompatActivity; Landroidx/navigation/NavController; Landroidx/navigation/ui/AppBarConfiguration;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**40**|invoke-static|v2, v4, Landroidx/navigation/ui/NavigationUI;->setupWithNavController(Lcom/google/android/material/navigation/NavigationView; Landroidx/navigation/NavController;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**41**|invoke-static|v9, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**42**|move-result-object|v5|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**43**|const-string|v6, 'FIRST_TIME_AFTER_INSTALLATION'|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**44**|const/4|v7, 1|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**45**|invoke-interface|v5, v6, v7, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String; Z)Z|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**46**|move-result|v7|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**47**|if-eqz|v7, +10|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**48**|invoke-direct|v9, Lcom/example/eva/MainActivity;->loadData()V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**49**|invoke-interface|v5, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**50**|move-result-object|v7|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**51**|const/4|v8, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**52**|invoke-interface|v7, v6, v8, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String; Z)Landroid/content/SharedPreferences$Editor;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**53**|invoke-interface|v7, Landroid/content/SharedPreferences$Editor;->apply()V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**54**|invoke-static|v9, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->shedulerPeriodic(Landroid/content/Context;)V|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**55**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**56**|fill-array-data-payload|bytearray(b'\x10\x01\x08\x7f\x0f\x01\x08\x7f\x13\x01\x08\x7f\x14\x01\x08\x7f') \| \x10\x01\x08\x7f\x0f\x01\x08\x7f\x13\x01\x08\x7f\x14\x01\x08\x7f|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onCreateOptionsMenu
  
**Signature :** `(Landroid/view/Menu;)Z`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean onCreateOptionsMenu(android.view.Menu p3)
{
    this.getMenuInflater().inflate(2131492864, p3);
    return 1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>const/high16")
	8("n°3<br/>const/high16") --> 12("n°4<br/>invoke-virtual")
	12("n°4<br/>invoke-virtual") --> 18("n°5<br/>const/4")
	18("n°5<br/>const/4") --> 20("n°6<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v2, Lcom/example/eva/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/view/Menu;</span>|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Landroid/view/MenuInflater;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/Menu;</span>|<span style='color:#f14848'></span>|
|**3**|8|const/high16|v1, 32524 # [1.8609191940988822e+38]|<span style='color:grey'>Landroid/view/MenuInflater;</span>|float|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/Menu;</span>|<span style='color:#f14848'></span>|
|**4**|12|invoke-virtual|v0, v1, v3, Landroid/view/MenuInflater;->inflate(I Landroid/view/Menu;)V|Landroid/view/MenuInflater;|float|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|Landroid/view/Menu;|<span style='color:#f14848'>**RegisterTypeError :**<br/>le registre v1 est de type "None" au lieux de "float".</span>|
|**5**|18|const/4|v0, 1|int|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/Menu;</span>|<span style='color:#f14848'></span>|
|**6**|20|return|v0|int|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/Menu;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v2, Lcom/example/eva/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const/high16|v1, 32524 # [1.8609191940988822e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, v1, v3, Landroid/view/MenuInflater;->inflate(I Landroid/view/Menu;)V|True|False|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|const/4|v0, 1|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onOptionsItemSelected
  
**Signature :** `(Landroid/view/MenuItem;)Z`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean onOptionsItemSelected(android.view.MenuItem p3)
{
    if (p3.getItemId() == 2131230775) {
        com.example.eva.service.api.google.calendar.DataSynchronisationWorker.sheduler(this, 0);
    }
    return 0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-interface") --> 6("n°2<br/>move-result")
	6("n°2<br/>move-result") --> 8("n°3<br/>const")
	8("n°3<br/>const") --> 14("n°4<br/>if-ne")
	14("n°4<br/>if-ne") --> 18("n°5<br/>const/4")
	14("n°4<br/>if-ne") -.-> 26("n°7<br/>const/4")
	18("n°5<br/>const/4") --> 20("n°6<br/>invoke-static")
	20("n°6<br/>invoke-static") --> 26("n°7<br/>const/4")
	26("n°7<br/>const/4") --> 28("n°8<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|Landroid/view/MenuItem;|<span style='color:#f14848'></span>|
|**2**|6|move-result|v0|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**3**|8|const|v1, 2131230775 # [1.8077612295927676e+38]|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**4**|14|if-ne|v0, v1, +6|int|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**5**|18|const/4|v0, 0|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**6**|20|invoke-static|v2, v0, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->sheduler(Landroid/content/Context; Lcom/example/eva/database/association/Association;)V|int|<span style='color:grey'>int</span>|Lcom/example/eva/MainActivity;|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**7**|26|const/4|v0, 0|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
|**8**|28|return|v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:grey'>Landroid/view/MenuItem;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-interface|v3, Landroid/view/MenuItem;->getItemId()I|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const|v1, 2131230775 # [1.8077612295927676e+38]|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-ne|v0, v1, +6|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-static|v2, v0, Lcom/example/eva/service/api/google/calendar/DataSynchronisationWorker;->sheduler(Landroid/content/Context; Lcom/example/eva/database/association/Association;)V|False|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## onSupportNavigateUp
  
**Signature :** `()Z`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean onSupportNavigateUp()
{
    if ((!androidx.navigation.ui.NavigationUI.navigateUp(androidx.navigation.Navigation.findNavController(this, 2131230993), this.mAppBarConfiguration)) && (!super.onSupportNavigateUp())) {
        int v1_0 = 0;
    } else {
        v1_0 = 1;
    }
    return v1_0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const") --> 6("n°2<br/>invoke-static")
	6("n°2<br/>invoke-static") --> 12("n°3<br/>move-result-object")
	12("n°3<br/>move-result-object") --> 14("n°4<br/>iget-object")
	14("n°4<br/>iget-object") --> 18("n°5<br/>invoke-static")
	18("n°5<br/>invoke-static") --> 24("n°6<br/>move-result")
	24("n°6<br/>move-result") --> 26("n°7<br/>if-nez")
	26("n°7<br/>if-nez") --> 30("n°8<br/>invoke-super")
	26("n°7<br/>if-nez") -.-> 48("n°14<br/>const/4")
	30("n°8<br/>invoke-super") --> 36("n°9<br/>move-result")
	36("n°9<br/>move-result") --> 38("n°10<br/>if-eqz")
	38("n°10<br/>if-eqz") --> 42("n°11<br/>goto")
	38("n°10<br/>if-eqz") -.-> 44("n°12<br/>const/4")
	42("n°11<br/>goto") --> 48("n°14<br/>const/4")
	44("n°12<br/>const/4") --> 46("n°13<br/>goto")
	46("n°13<br/>goto") --> 50("n°15<br/>return")
	48("n°14<br/>const/4") --> 50("n°15<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const|v0, 2131230993 # [1.8078054452457036e+38]|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**2**|6|invoke-static|v2, v0, Landroidx/navigation/Navigation;->findNavController(Landroid/app/Activity; I)Landroidx/navigation/NavController;|int|<span style='color:grey'>None</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**3**|12|move-result-object|v0|Landroidx/navigation/NavController;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**4**|14|iget-object|v1, v2, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>Landroidx/navigation/NavController;</span>|Landroidx/navigation/ui/AppBarConfiguration;|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**5**|18|invoke-static|v0, v1, Landroidx/navigation/ui/NavigationUI;->navigateUp(Landroidx/navigation/NavController; Landroidx/navigation/ui/AppBarConfiguration;)Z|Landroidx/navigation/NavController;|Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**6**|24|move-result|v1|<span style='color:grey'>Landroidx/navigation/NavController;</span>|boolean|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**7**|26|if-nez|v1, +b|<span style='color:grey'>Landroidx/navigation/NavController;</span>|boolean|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**8**|30|invoke-super|v2, Lcom/example/eva/Environment;->onSupportNavigateUp()Z|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>boolean</span>|Lcom/example/eva/MainActivity;|<span style='color:#f14848'></span>|
|**9**|36|move-result|v1|<span style='color:grey'>Landroidx/navigation/NavController;</span>|boolean|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**10**|38|if-eqz|v1, +3|<span style='color:grey'>Landroidx/navigation/NavController;</span>|boolean|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**11**|42|goto|+3|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**12**|44|const/4|v1, 0|<span style='color:grey'>Landroidx/navigation/NavController;</span>|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**13**|46|goto|+2|<span style='color:grey'>Landroidx/navigation/NavController;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**14**|48|const/4|v1, 1|<span style='color:grey'>Landroidx/navigation/NavController;</span>|boolean|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
|**15**|50|return|v1|<span style='color:grey'>Landroidx/navigation/NavController;</span>|int|<span style='color:grey'>Lcom/example/eva/MainActivity;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const|v0, 2131230993 # [1.8078054452457036e+38]|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-static|v2, v0, Landroidx/navigation/Navigation;->findNavController(Landroid/app/Activity; I)Landroidx/navigation/NavController;|False|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iget-object|v1, v2, Lcom/example/eva/MainActivity;->mAppBarConfiguration Landroidx/navigation/ui/AppBarConfiguration;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**5**|invoke-static|v0, v1, Landroidx/navigation/ui/NavigationUI;->navigateUp(Landroidx/navigation/NavController; Landroidx/navigation/ui/AppBarConfiguration;)Z|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|if-nez|v1, +b|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-super|v2, Lcom/example/eva/Environment;->onSupportNavigateUp()Z|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**9**|move-result|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|if-eqz|v1, +3|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|goto|+3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|const/4|v1, 0|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|goto|+2|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|const/4|v1, 1|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|return|v1|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
