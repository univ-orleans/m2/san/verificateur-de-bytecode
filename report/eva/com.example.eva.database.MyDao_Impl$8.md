
com.example.eva.database.MyDao_Impl$8
=====================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [call](#call)
	* [call](#call)
	* [finalize](#finalize)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**call**](#call)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**finalize**](#finalize)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomSQLiteQuery;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
MyDao_Impl$8(com.example.eva.database.MyDao_Impl p1, androidx.room.RoomSQLiteQuery p2)
{
    this.this$0 = p1;
    this.val$_statement = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>iput-object")
	4("n°2<br/>iput-object") --> 8("n°3<br/>invoke-direct")
	8("n°3<br/>invoke-direct") --> 14("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$8;->this$0 Lcom/example/eva/database/MyDao_Impl;|Lcom/example/eva/database/MyDao_Impl$8;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomSQLiteQuery;</span>|<span style='color:#f14848'></span>|
|**2**|4|iput-object|v2, v0, Lcom/example/eva/database/MyDao_Impl$8;->val$_statement Landroidx/room/RoomSQLiteQuery;|Lcom/example/eva/database/MyDao_Impl$8;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Landroidx/room/RoomSQLiteQuery;|<span style='color:#f14848'></span>|
|**3**|8|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/MyDao_Impl$8;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomSQLiteQuery;</span>|<span style='color:#f14848'></span>|
|**4**|14|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$8;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomSQLiteQuery;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/MyDao_Impl$8;->this$0 Lcom/example/eva/database/MyDao_Impl;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v0, Lcom/example/eva/database/MyDao_Impl$8;->val$_statement Landroidx/room/RoomSQLiteQuery;|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/lang/Object;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public bridge synthetic Object call()
{
    return this.call();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/eva/database/MyDao_Impl$8;->call()Ljava/util/List;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl$8;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$8;</span>|<span style='color:#f14848'></span>|
|**3**|8|return-object|v0|Ljava/util/List;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$8;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/eva/database/MyDao_Impl$8;->call()Ljava/util/List;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## call
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 32  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List call()
{
    int v4_1 = 0;
    android.database.Cursor v2_1 = androidx.room.util.DBUtil.query(com.example.eva.database.MyDao_Impl.access$000(this.this$0), this.val$_statement, 0, 0);
    try {
        Throwable v0_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "id");
        int v5_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "idAssociation");
        int v6_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "idGoogleEvent");
        int v7_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "summary");
        int v8_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "canceled");
        int v9_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "dateStart");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "dateEnd");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "dateUpdated");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "location");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v2_1, "description");
        java.util.ArrayList v14_1 = new java.util.ArrayList(v2_1.getCount());
    } catch (Throwable v0_2) {
        v2_1.close();
        throw v0_2;
    }
    while (v2_1.moveToNext()) {
        Integer v15_4;
        if (!v2_1.isNull(v5_1)) {
            v15_4 = Integer.valueOf(v2_1.getInt(v5_1));
        } else {
            v15_4 = 0;
        }
        Integer v26;
        String v18 = v2_1.getString(v6_1);
        String v19 = v2_1.getString(v7_1);
        if (!v2_1.isNull(v8_1)) {
            v26 = Integer.valueOf(v2_1.getInt(v8_1));
        } else {
            v26 = 0;
        }
        Boolean v20;
        if (v26 != null) {
            com.example.eva.database.event.Event v16_12;
            if (v26.intValue() == 0) {
                v16_12 = 0;
            } else {
                v16_12 = 1;
            }
            v20 = Boolean.valueOf(v16_12);
        } else {
            v20 = v4_1;
        }
        Long v27;
        if (!v2_1.isNull(v9_1)) {
            v27 = Long.valueOf(v2_1.getLong(v9_1));
        } else {
            v27 = 0;
        }
        Long v28;
        java.util.Date v21 = com.example.eva.database.Converters.fromTimestamp(v27);
        if (!v2_1.isNull(v10_1)) {
            v28 = Long.valueOf(v2_1.getLong(v10_1));
        } else {
            v28 = 0;
        }
        Long v29;
        java.util.Date v22 = com.example.eva.database.Converters.fromTimestamp(v28);
        if (!v2_1.isNull(v11_1)) {
            v29 = Long.valueOf(v2_1.getLong(v11_1));
        } else {
            v29 = 0;
        }
        int v30_0 = new com.example.eva.database.event.Event;
        v30_0(v15_4, v18, v19, v20, v21, v22, com.example.eva.database.Converters.fromTimestamp(v29), v2_1.getString(v12_1), v2_1.getString(v13_1));
        int v3_0 = v30_0;
        v3_0.setId(v2_1.getInt(v0_1));
        v14_1.add(v3_0);
        v4_1 = 0;
    }
    v2_1.close();
    return v14_1;
}
```
## finalize
  
**Signature :** `()V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
protected void finalize()
{
    this.val$_statement.release();
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/MyDao_Impl$8;->val$_statement Landroidx/room/RoomSQLiteQuery;|Landroidx/room/RoomSQLiteQuery;|Lcom/example/eva/database/MyDao_Impl$8;|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomSQLiteQuery;->release()V|Landroidx/room/RoomSQLiteQuery;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$8;</span>|<span style='color:#f14848'></span>|
|**3**|10|return-void||<span style='color:grey'>Landroidx/room/RoomSQLiteQuery;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$8;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/MyDao_Impl$8;->val$_statement Landroidx/room/RoomSQLiteQuery;|True|True|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomSQLiteQuery;->release()V|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
