
com.example.eva.database.association.Association
================================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [getId](#getid)
	* [getIdGoogleCalendar](#getidgooglecalendar)
	* [getLogo](#getlogo)
	* [getName](#getname)
	* [getSubscribe](#getsubscribe)
	* [setId](#setid)
	* [setIdGoogleCalendar](#setidgooglecalendar)
	* [setLogo](#setlogo)
	* [setName](#setname)
	* [setSubscribe](#setsubscribe)
	* [toString](#tostring)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getId**](#getid)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getIdGoogleCalendar**](#getidgooglecalendar)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getLogo**](#getlogo)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getName**](#getname)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getSubscribe**](#getsubscribe)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setId**](#setid)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setIdGoogleCalendar**](#setidgooglecalendar)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setLogo**](#setlogo)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setName**](#setname)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setSubscribe**](#setsubscribe)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**toString**](#tostring)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Ljava/lang/String; Ljava/lang/String; Ljava/lang/Integer; Ljava/lang/Boolean;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Association(String p1, String p2, Integer p3, Boolean p4)
{
    this.name = p1;
    this.idGoogleCalendar = p2;
    this.logo = p3;
    this.subscribe = p4;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>iput-object")
	10("n°3<br/>iput-object") --> 14("n°4<br/>iput-object")
	14("n°4<br/>iput-object") --> 18("n°5<br/>iput-object")
	18("n°5<br/>iput-object") --> 22("n°6<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|Lcom/example/eva/database/association/Association;|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
|**3**|10|iput-object|v2, v0, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
|**4**|14|iput-object|v3, v0, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
|**5**|18|iput-object|v4, v0, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|Ljava/lang/Boolean;|<span style='color:#f14848'></span>|
|**6**|22|return-void||<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v2, v0, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iput-object|v3, v0, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iput-object|v4, v0, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**6**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getId
  
**Signature :** `()I`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int getId()
{
    return this.id;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget|v0, v1, Lcom/example/eva/database/association/Association;->id I|int|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|return|v0|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget|v0, v1, Lcom/example/eva/database/association/Association;->id I|False|True|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getIdGoogleCalendar
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String getIdGoogleCalendar()
{
    return this.idGoogleCalendar;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getLogo
  
**Signature :** `()Ljava/lang/Integer;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Integer getLogo()
{
    return this.logo;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getName
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String getName()
{
    return this.name;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getSubscribe
  
**Signature :** `()Ljava/lang/Boolean;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Boolean getSubscribe()
{
    return this.subscribe;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setId
  
**Signature :** `(I)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setId(int p1)
{
    this.id = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput|v1, v0, Lcom/example/eva/database/association/Association;->id I|Lcom/example/eva/database/association/Association;|int|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput|v1, v0, Lcom/example/eva/database/association/Association;->id I|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setIdGoogleCalendar
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association setIdGoogleCalendar(String p1)
{
    this.idGoogleCalendar = p1;
    return this;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|Lcom/example/eva/database/association/Association;|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setLogo
  
**Signature :** `(Ljava/lang/Integer;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association setLogo(Integer p1)
{
    this.logo = p1;
    return this;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|Ljava/lang/Integer;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setName
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association setName(String p1)
{
    this.name = p1;
    return this;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|Lcom/example/eva/database/association/Association;|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setSubscribe
  
**Signature :** `(Ljava/lang/Boolean;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association setSubscribe(Boolean p1)
{
    this.subscribe = p1;
    return this;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|Ljava/lang/Boolean;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Lcom/example/eva/database/association/Association;|<span style='color:grey'>Ljava/lang/Boolean;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## toString
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String toString()
{
    String v0_1 = new StringBuilder();
    v0_1.append("Association { id=");
    v0_1.append(this.id);
    v0_1.append(", name=\'");
    v0_1.append(this.name);
    v0_1.append("\', idGoogleCalendar=");
    v0_1.append(this.idGoogleCalendar);
    v0_1.append(", logo=\'");
    v0_1.append(this.logo);
    v0_1.append("\', subscribe=");
    v0_1.append(this.subscribe);
    v0_1.append(125);
    return v0_1.toString();
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>const-string")
	10("n°3<br/>const-string") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget")
	20("n°5<br/>iget") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>const-string")
	30("n°7<br/>const-string") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>const-string")
	50("n°11<br/>const-string") --> 54("n°12<br/>invoke-virtual")
	54("n°12<br/>invoke-virtual") --> 60("n°13<br/>iget-object")
	60("n°13<br/>iget-object") --> 64("n°14<br/>invoke-virtual")
	64("n°14<br/>invoke-virtual") --> 70("n°15<br/>const-string")
	70("n°15<br/>const-string") --> 74("n°16<br/>invoke-virtual")
	74("n°16<br/>invoke-virtual") --> 80("n°17<br/>iget-object")
	80("n°17<br/>iget-object") --> 84("n°18<br/>invoke-virtual")
	84("n°18<br/>invoke-virtual") --> 90("n°19<br/>const-string")
	90("n°19<br/>const-string") --> 94("n°20<br/>invoke-virtual")
	94("n°20<br/>invoke-virtual") --> 100("n°21<br/>iget-object")
	100("n°21<br/>iget-object") --> 104("n°22<br/>invoke-virtual")
	104("n°22<br/>invoke-virtual") --> 110("n°23<br/>const/16")
	110("n°23<br/>const/16") --> 114("n°24<br/>invoke-virtual")
	114("n°24<br/>invoke-virtual") --> 120("n°25<br/>invoke-virtual")
	120("n°25<br/>invoke-virtual") --> 126("n°26<br/>move-result-object")
	126("n°26<br/>move-result-object") --> 128("n°27<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|Ljava/lang/StringBuilder;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**3**|10|const-string|v1, 'Association { id='|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget|v1, v2, Lcom/example/eva/database/association/Association;->id I|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|int|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**7**|30|const-string|v1, ", name='"|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**11**|50|const-string|v1, "', idGoogleCalendar="|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**12**|54|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**13**|60|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**14**|64|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**15**|70|const-string|v1, ", logo='"|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**16**|74|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**17**|80|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/Integer;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**18**|84|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**19**|90|const-string|v1, "', subscribe="|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**20**|94|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/String;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**21**|100|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|Ljava/lang/Boolean;|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**22**|104|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|Ljava/lang/Boolean;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**23**|110|const/16|v1, 125|<span style='color:grey'>Ljava/lang/StringBuilder;</span>|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**24**|114|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;|Ljava/lang/StringBuilder;|int|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**25**|120|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|Ljava/lang/StringBuilder;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**26**|126|move-result-object|v0|Ljava/lang/String;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**27**|128|return-object|v0|Ljava/lang/String;|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/lang/StringBuilder;|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/lang/StringBuilder;-><init>()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const-string|v1, 'Association { id='|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget|v1, v2, Lcom/example/eva/database/association/Association;->id I|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const-string|v1, ", name='"|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->name Ljava/lang/String;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|const-string|v1, "', idGoogleCalendar="|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->idGoogleCalendar Ljava/lang/String;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|const-string|v1, ", logo='"|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->logo Ljava/lang/Integer;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|const-string|v1, "', subscribe="|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|iget-object|v1, v2, Lcom/example/eva/database/association/Association;->subscribe Ljava/lang/Boolean;|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**22**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|const/16|v1, 125|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|invoke-virtual|v0, v1, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-virtual|v0, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**27**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
