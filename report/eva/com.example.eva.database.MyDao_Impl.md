
com.example.eva.database.MyDao_Impl
===================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [access$000](#access000)
	* [deleteAssociation](#deleteassociation)
	* [deleteEvent](#deleteevent)
	* [deleteOldEvents](#deleteoldevents)
	* [getAllAssociationList](#getallassociationlist)
	* [getAllAssociationSubscribe](#getallassociationsubscribe)
	* [getAllEventBeforeLimit](#getalleventbeforelimit)
	* [getAllEventByAssociation](#getalleventbyassociation)
	* [getAllEventList](#getalleventlist)
	* [getAssociation](#getassociation)
	* [getAssociation](#getassociation)
	* [getEvent](#getevent)
	* [getEvent](#getevent)
	* [getEventData](#geteventdata)
	* [insertAssociation](#insertassociation)
	* [insertEvent](#insertevent)
	* [numberEventConfirmedByAssociation](#numbereventconfirmedbyassociation)
	* [updateAssociation](#updateassociation)
	* [updateEvent](#updateevent)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**access$000**](#access000)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**deleteAssociation**](#deleteassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**deleteEvent**](#deleteevent)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**deleteOldEvents**](#deleteoldevents)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAllAssociationList**](#getallassociationlist)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAllAssociationSubscribe**](#getallassociationsubscribe)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAllEventBeforeLimit**](#getalleventbeforelimit)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAllEventByAssociation**](#getalleventbyassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAllEventList**](#getalleventlist)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAssociation**](#getassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAssociation**](#getassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getEvent**](#getevent)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getEvent**](#getevent)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getEventData**](#geteventdata)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**insertAssociation**](#insertassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**insertEvent**](#insertevent)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**numberEventConfirmedByAssociation**](#numbereventconfirmedbyassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**updateAssociation**](#updateassociation)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**updateEvent**](#updateevent)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Landroidx/room/RoomDatabase;)V`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public MyDao_Impl(androidx.room.RoomDatabase p2)
{
    this.__db = p2;
    this.__insertionAdapterOfEvent = new com.example.eva.database.MyDao_Impl$1(this, p2);
    this.__insertionAdapterOfAssociation = new com.example.eva.database.MyDao_Impl$2(this, p2);
    this.__deletionAdapterOfEvent = new com.example.eva.database.MyDao_Impl$3(this, p2);
    this.__deletionAdapterOfAssociation = new com.example.eva.database.MyDao_Impl$4(this, p2);
    this.__updateAdapterOfEvent = new com.example.eva.database.MyDao_Impl$5(this, p2);
    this.__updateAdapterOfAssociation = new com.example.eva.database.MyDao_Impl$6(this, p2);
    this.__preparedStmtOfDeleteOldEvents = new com.example.eva.database.MyDao_Impl$7(this, p2);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>new-instance")
	10("n°3<br/>new-instance") --> 14("n°4<br/>invoke-direct")
	14("n°4<br/>invoke-direct") --> 20("n°5<br/>iput-object")
	20("n°5<br/>iput-object") --> 24("n°6<br/>new-instance")
	24("n°6<br/>new-instance") --> 28("n°7<br/>invoke-direct")
	28("n°7<br/>invoke-direct") --> 34("n°8<br/>iput-object")
	34("n°8<br/>iput-object") --> 38("n°9<br/>new-instance")
	38("n°9<br/>new-instance") --> 42("n°10<br/>invoke-direct")
	42("n°10<br/>invoke-direct") --> 48("n°11<br/>iput-object")
	48("n°11<br/>iput-object") --> 52("n°12<br/>new-instance")
	52("n°12<br/>new-instance") --> 56("n°13<br/>invoke-direct")
	56("n°13<br/>invoke-direct") --> 62("n°14<br/>iput-object")
	62("n°14<br/>iput-object") --> 66("n°15<br/>new-instance")
	66("n°15<br/>new-instance") --> 70("n°16<br/>invoke-direct")
	70("n°16<br/>invoke-direct") --> 76("n°17<br/>iput-object")
	76("n°17<br/>iput-object") --> 80("n°18<br/>new-instance")
	80("n°18<br/>new-instance") --> 84("n°19<br/>invoke-direct")
	84("n°19<br/>invoke-direct") --> 90("n°20<br/>iput-object")
	90("n°20<br/>iput-object") --> 94("n°21<br/>new-instance")
	94("n°21<br/>new-instance") --> 98("n°22<br/>invoke-direct")
	98("n°22<br/>invoke-direct") --> 104("n°23<br/>iput-object")
	104("n°23<br/>iput-object") --> 108("n°24<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v1, Ljava/lang/Object;-><init>()V|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v2, v1, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**3**|10|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$1;|Lcom/example/eva/database/MyDao_Impl$1;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$1;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$1;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**5**|20|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfEvent Landroidx/room/EntityInsertionAdapter;|Lcom/example/eva/database/MyDao_Impl$1;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**6**|24|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$2;|Lcom/example/eva/database/MyDao_Impl$2;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**7**|28|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$2;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$2;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**8**|34|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfAssociation Landroidx/room/EntityInsertionAdapter;|Lcom/example/eva/database/MyDao_Impl$2;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**9**|38|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$3;|Lcom/example/eva/database/MyDao_Impl$3;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**10**|42|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$3;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$3;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**11**|48|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|Lcom/example/eva/database/MyDao_Impl$3;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**12**|52|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$4;|Lcom/example/eva/database/MyDao_Impl$4;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**13**|56|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$4;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$4;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**14**|62|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|Lcom/example/eva/database/MyDao_Impl$4;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**15**|66|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$5;|Lcom/example/eva/database/MyDao_Impl$5;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**16**|70|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$5;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$5;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**17**|76|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|Lcom/example/eva/database/MyDao_Impl$5;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**18**|80|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$6;|Lcom/example/eva/database/MyDao_Impl$6;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**19**|84|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$6;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$6;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**20**|90|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|Lcom/example/eva/database/MyDao_Impl$6;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**21**|94|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$7;|Lcom/example/eva/database/MyDao_Impl$7;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**22**|98|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$7;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|Lcom/example/eva/database/MyDao_Impl$7;|Lcom/example/eva/database/MyDao_Impl;|Landroidx/room/RoomDatabase;|<span style='color:#f14848'></span>|
|**23**|104|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|Lcom/example/eva/database/MyDao_Impl$7;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
|**24**|108|return-void||<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl$7;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v1, Ljava/lang/Object;-><init>()V|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v2, v1, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>False</span>|True|True|<span style='color:#f14848'></span>|
|**3**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$1;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$1;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**5**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfEvent Landroidx/room/EntityInsertionAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$2;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$2;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**8**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfAssociation Landroidx/room/EntityInsertionAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$3;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$3;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**11**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$4;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$4;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**14**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$5;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$5;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**17**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$6;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$6;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**20**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|new-instance|v0, Lcom/example/eva/database/MyDao_Impl$7;|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|invoke-direct|v0, v1, v2, Lcom/example/eva/database/MyDao_Impl$7;-><init>(Lcom/example/eva/database/MyDao_Impl; Landroidx/room/RoomDatabase;)V|True|True|True|<span style='color:#f14848'></span>|
|**23**|iput-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## access$000
  
**Signature :** `(Lcom/example/eva/database/MyDao_Impl;)Landroidx/room/RoomDatabase;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static synthetic androidx.room.RoomDatabase access$000(com.example.eva.database.MyDao_Impl p1)
{
    return p1.__db;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## deleteAssociation
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void deleteAssociation(com.example.eva.database.association.Association p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__deletionAdapterOfAssociation.handle(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## deleteEvent
  
**Signature :** `(Lcom/example/eva/database/event/Event;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void deleteEvent(com.example.eva.database.event.Event p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__deletionAdapterOfEvent.handle(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__deletionAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## deleteOldEvents
  
**Signature :** `(J)V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void deleteOldEvents(long p5)
{
    this.__db.assertNotSuspendingTransaction();
    androidx.sqlite.db.SupportSQLiteStatement v0_2 = this.__preparedStmtOfDeleteOldEvents.acquire();
    v0_2.bindLong(1, p5);
    v0_2.bindLong(2, p5);
    this.__db.beginTransaction();
    try {
        v0_2.executeUpdateDelete();
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        this.__preparedStmtOfDeleteOldEvents.release(v0_2);
        return;
    } catch (Throwable v2_3) {
        this.__db.endTransaction();
        this.__preparedStmtOfDeleteOldEvents.release(v0_2);
        throw v2_3;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>move-result-object")
	20("n°5<br/>move-result-object") --> 22("n°6<br/>const/4")
	22("n°6<br/>const/4") --> 24("n°7<br/>invoke-interface")
	24("n°7<br/>invoke-interface") --> 30("n°8<br/>const/4")
	30("n°8<br/>const/4") --> 32("n°9<br/>invoke-interface")
	32("n°9<br/>invoke-interface") --> 38("n°10<br/>iget-object")
	38("n°10<br/>iget-object") --> 42("n°11<br/>invoke-virtual")
	42("n°11<br/>invoke-virtual") --> 48("n°12<br/>invoke-interface")
	48("n°12<br/>invoke-interface") --> 54("n°13<br/>iget-object")
	54("n°13<br/>iget-object") --> 58("n°14<br/>invoke-virtual")
	58("n°14<br/>invoke-virtual") --> 64("n°15<br/>iget-object")
	64("n°15<br/>iget-object") --> 68("n°16<br/>invoke-virtual")
	68("n°16<br/>invoke-virtual") --> 74("n°17<br/>iget-object")
	74("n°17<br/>iget-object") --> 78("n°18<br/>invoke-virtual")
	78("n°18<br/>invoke-virtual") --> 84("n°19<br/>nop")
	84("n°19<br/>nop") --> 86("n°20<br/>return-void")
	88("n°21<br/>move-exception") --> 90("n°22<br/>iget-object")
	90("n°22<br/>iget-object") --> 94("n°23<br/>invoke-virtual")
	94("n°23<br/>invoke-virtual") --> 100("n°24<br/>iget-object")
	100("n°24<br/>iget-object") --> 104("n°25<br/>invoke-virtual")
	104("n°25<br/>invoke-virtual") --> 110("n°26<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/SharedSQLiteStatement;->acquire()Landroidx/sqlite/db/SupportSQLiteStatement;|Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**5**|20|move-result-object|v0|Landroidx/sqlite/db/SupportSQLiteStatement;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**6**|22|const/4|v1, 1|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**7**|24|invoke-interface|v0, v1, v5, v6, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(I J)V|Landroidx/sqlite/db/SupportSQLiteStatement;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|long|None|<span style='color:#f14848'></span>|
|**8**|30|const/4|v1, 2|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**9**|32|invoke-interface|v0, v1, v5, v6, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(I J)V|Landroidx/sqlite/db/SupportSQLiteStatement;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|long|None|<span style='color:#f14848'></span>|
|**10**|38|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-virtual|v2, Landroidx/room/RoomDatabase;->beginTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**12**|48|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteStatement;->executeUpdateDelete()I|Landroidx/sqlite/db/SupportSQLiteStatement;|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**13**|54|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**14**|58|invoke-virtual|v2, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**15**|64|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**16**|68|invoke-virtual|v2, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**17**|74|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**18**|78|invoke-virtual|v2, v0, Landroidx/room/SharedSQLiteStatement;->release(Landroidx/sqlite/db/SupportSQLiteStatement;)V|Landroidx/sqlite/db/SupportSQLiteStatement;|<span style='color:grey'>int</span>|Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**19**|84|nop||<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/SharedSQLiteStatement;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**20**|86|return-void||<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Landroidx/room/SharedSQLiteStatement;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**21**|88|move-exception|v2|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**22**|90|iget-object|v3, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**23**|94|invoke-virtual|v3, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**24**|100|iget-object|v3, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/SharedSQLiteStatement;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**25**|104|invoke-virtual|v3, v0, Landroidx/room/SharedSQLiteStatement;->release(Landroidx/sqlite/db/SupportSQLiteStatement;)V|Landroidx/sqlite/db/SupportSQLiteStatement;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**26**|110|throw|v2|<span style='color:grey'>Landroidx/sqlite/db/SupportSQLiteStatement;</span>|<span style='color:grey'>int</span>|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/SharedSQLiteStatement;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>long</span>|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/SharedSQLiteStatement;->acquire()Landroidx/sqlite/db/SupportSQLiteStatement;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**5**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**6**|const/4|v1, 1|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**7**|invoke-interface|v0, v1, v5, v6, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(I J)V|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|False|<span style='color:#f14848'></span>|
|**8**|const/4|v1, 2|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**9**|invoke-interface|v0, v1, v5, v6, Landroidx/sqlite/db/SupportSQLiteStatement;->bindLong(I J)V|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|False|<span style='color:#f14848'></span>|
|**10**|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v2, Landroidx/room/RoomDatabase;->beginTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**12**|invoke-interface|v0, Landroidx/sqlite/db/SupportSQLiteStatement;->executeUpdateDelete()I|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**13**|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v2, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**15**|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v2, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**17**|iget-object|v2, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v2, v0, Landroidx/room/SharedSQLiteStatement;->release(Landroidx/sqlite/db/SupportSQLiteStatement;)V|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**19**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**20**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**21**|move-exception|v2|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**22**|iget-object|v3, v4, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**23**|invoke-virtual|v3, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**24**|iget-object|v3, v4, Lcom/example/eva/database/MyDao_Impl;->__preparedStmtOfDeleteOldEvents Landroidx/room/SharedSQLiteStatement;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**25**|invoke-virtual|v3, v0, Landroidx/room/SharedSQLiteStatement;->release(Landroidx/sqlite/db/SupportSQLiteStatement;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**26**|throw|v2|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
  

## getAllAssociationList
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 20  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getAllAssociationList()
{
    androidx.room.RoomSQLiteQuery v4 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM association_table  ORDER BY name ASC", 0);
    this.__db.assertNotSuspendingTransaction();
    int v5_1 = 0;
    android.database.Cursor v6 = androidx.room.util.DBUtil.query(this.__db, v4, 0, 0);
    try {
        Throwable v0_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "id");
        int v7_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "idGoogleCalendar");
        int v8_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "name");
        int v9_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "logo");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "subscribe");
        java.util.ArrayList v11_1 = new java.util.ArrayList(v6.getCount());
    } catch (Throwable v0_4) {
        v6.close();
        v4.release();
        throw v0_4;
    }
    while (v6.moveToNext()) {
        Integer v14_2;
        String v12_2 = v6.getString(v7_1);
        String v13 = v6.getString(v8_1);
        if (!v6.isNull(v9_1)) {
            v14_2 = Integer.valueOf(v6.getInt(v9_1));
        } else {
            v14_2 = 0;
        }
        Integer v15_2;
        if (!v6.isNull(v10_1)) {
            v15_2 = Integer.valueOf(v6.getInt(v10_1));
        } else {
            v15_2 = 0;
        }
        Boolean v16_2;
        if (v15_2 != null) {
            Boolean v16_1;
            if (v15_2.intValue() == 0) {
                v16_1 = 0;
            } else {
                v16_1 = 1;
            }
            v16_2 = Boolean.valueOf(v16_1);
        } else {
            v16_2 = v5_1;
        }
        int v3_2 = new com.example.eva.database.association.Association(v13, v12_2, v14_2, v16_2);
        Throwable v17_2 = v0_1;
        v3_2.setId(v6.getInt(v0_1));
        v11_1.add(v3_2);
        v0_1 = v17_2;
        v5_1 = 0;
    }
    v6.close();
    v4.release();
    return v11_1;
}
```
## getAllAssociationSubscribe
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 20  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getAllAssociationSubscribe()
{
    androidx.room.RoomSQLiteQuery v4 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM association_table where subscribe = 1  ORDER BY name ASC", 0);
    this.__db.assertNotSuspendingTransaction();
    int v5_1 = 0;
    android.database.Cursor v6 = androidx.room.util.DBUtil.query(this.__db, v4, 0, 0);
    try {
        Throwable v0_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "id");
        int v7_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "idGoogleCalendar");
        int v8_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "name");
        int v9_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "logo");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "subscribe");
        java.util.ArrayList v11_1 = new java.util.ArrayList(v6.getCount());
    } catch (Throwable v0_4) {
        v6.close();
        v4.release();
        throw v0_4;
    }
    while (v6.moveToNext()) {
        Integer v14_2;
        String v12_2 = v6.getString(v7_1);
        String v13 = v6.getString(v8_1);
        if (!v6.isNull(v9_1)) {
            v14_2 = Integer.valueOf(v6.getInt(v9_1));
        } else {
            v14_2 = 0;
        }
        Integer v15_2;
        if (!v6.isNull(v10_1)) {
            v15_2 = Integer.valueOf(v6.getInt(v10_1));
        } else {
            v15_2 = 0;
        }
        Boolean v16_2;
        if (v15_2 != null) {
            Boolean v16_1;
            if (v15_2.intValue() == 0) {
                v16_1 = 0;
            } else {
                v16_1 = 1;
            }
            v16_2 = Boolean.valueOf(v16_1);
        } else {
            v16_2 = v5_1;
        }
        int v3_2 = new com.example.eva.database.association.Association(v13, v12_2, v14_2, v16_2);
        Throwable v17_2 = v0_1;
        v3_2.setId(v6.getInt(v0_1));
        v11_1.add(v3_2);
        v0_1 = v17_2;
        v5_1 = 0;
    }
    v6.close();
    v4.release();
    return v11_1;
}
```
## getAllEventBeforeLimit
  
**Signature :** `(J J)Ljava/util/List;`  
**Nombre de registre :** 38  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getAllEventBeforeLimit(long p34, long p36)
{
    androidx.room.RoomSQLiteQuery v3_1 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table WHERE dateEnd >= ? AND dateStart < ? AND dateStart <= dateEnd AND idAssociation IN (SELECT id FROM association_table WHERE subscribe=1) ORDER BY dateStart ASC", 2);
    v3_1.bindLong(1, p34);
    v3_1.bindLong(2, p36);
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v11 = androidx.room.util.DBUtil.query(this.__db, v3_1, 0, 0);
    try {
        Throwable v0_4 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "id");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "idAssociation");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "idGoogleEvent");
        int v14_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "summary");
        int v15_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "canceled");
        int v9_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "dateStart");
        int v10_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "dateEnd");
        int v1_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "dateUpdated");
        try {
            int v2_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "location");
            int v4_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v11, "description");
            try {
                java.util.ArrayList v5_1 = new java.util.ArrayList(v11.getCount());
            } catch (Throwable v0_1) {
                v11.close();
                v3_1.release();
                throw v0_1;
            }
            while (v11.moveToNext()) {
                Integer v6_4;
                if (!v11.isNull(v12_1)) {
                    v6_4 = Integer.valueOf(v11.getInt(v12_1));
                } else {
                    v6_4 = 0;
                }
                Integer v28;
                String v20 = v11.getString(v13_1);
                String v21 = v11.getString(v14_1);
                if (!v11.isNull(v15_1)) {
                    v28 = Integer.valueOf(v11.getInt(v15_1));
                } else {
                    v28 = 0;
                }
                Boolean v22;
                if (v28 != null) {
                    int v18_20;
                    if (v28.intValue() == 0) {
                        v18_20 = 0;
                    } else {
                        v18_20 = 1;
                    }
                    v22 = Boolean.valueOf(v18_20);
                } else {
                    v22 = 0;
                }
                Long v29;
                if (!v11.isNull(v9_2)) {
                    v29 = Long.valueOf(v11.getLong(v9_2));
                } else {
                    v29 = 0;
                }
                Long v30;
                java.util.Date v23 = com.example.eva.database.Converters.fromTimestamp(v29);
                if (!v11.isNull(v10_2)) {
                    v30 = Long.valueOf(v11.getLong(v10_2));
                } else {
                    v30 = 0;
                }
                Long v31;
                java.util.Date v24 = com.example.eva.database.Converters.fromTimestamp(v30);
                if (!v11.isNull(v1_2)) {
                    v31 = Long.valueOf(v11.getLong(v1_2));
                } else {
                    v31 = 0;
                }
                int v32_0 = new com.example.eva.database.event.Event;
                v32_0(v6_4, v20, v21, v22, v23, v24, com.example.eva.database.Converters.fromTimestamp(v31), v11.getString(v2_2), v11.getString(v4_1));
                Throwable v19_2 = v0_4;
                Throwable v0_3 = v32_0;
                int v18_14 = v1_2;
                v0_3.setId(v11.getInt(v0_4));
                v5_1.add(v0_3);
                v1_2 = v18_14;
                v0_4 = v19_2;
            }
            v11.close();
            v3_1.release();
            return v5_1;
        } catch (Throwable v0_1) {
        }
    } catch (Throwable v0_1) {
        int v17 = 2;
    }
}
```
## getAllEventByAssociation
  
**Signature :** `(J I)Ljava/util/List;`  
**Nombre de registre :** 36  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getAllEventByAssociation(long p33, int p35)
{
    androidx.room.RoomSQLiteQuery v3_1 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table WHERE dateEnd >= ? AND idAssociation == ? ORDER BY dateStart ASC", 2);
    v3_1.bindLong(1, p33);
    v3_1.bindLong(2, ((long) p35));
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v10 = androidx.room.util.DBUtil.query(this.__db, v3_1, 0, 0);
    try {
        Throwable v0_4 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "id");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "idAssociation");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "idGoogleEvent");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "summary");
        int v14_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "canceled");
        int v15_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "dateStart");
        int v8_3 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "dateEnd");
        int v9_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "dateUpdated");
        int v1_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "location");
        try {
            int v2_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v10, "description");
            java.util.ArrayList v4_1 = new java.util.ArrayList(v10.getCount());
        } catch (Throwable v0_1) {
            v10.close();
            v3_1.release();
            throw v0_1;
        }
        while (v10.moveToNext()) {
            Integer v5_4;
            if (!v10.isNull(v11_1)) {
                v5_4 = Integer.valueOf(v10.getInt(v11_1));
            } else {
                v5_4 = 0;
            }
            Integer v27;
            String v19 = v10.getString(v12_1);
            String v20 = v10.getString(v13_1);
            if (!v10.isNull(v14_1)) {
                v27 = Integer.valueOf(v10.getInt(v14_1));
            } else {
                v27 = 0;
            }
            Boolean v21;
            if (v27 != null) {
                int v17_19;
                if (v27.intValue() == 0) {
                    v17_19 = 0;
                } else {
                    v17_19 = 1;
                }
                v21 = Boolean.valueOf(v17_19);
            } else {
                v21 = 0;
            }
            Long v28;
            if (!v10.isNull(v15_1)) {
                v28 = Long.valueOf(v10.getLong(v15_1));
            } else {
                v28 = 0;
            }
            Long v29;
            java.util.Date v22 = com.example.eva.database.Converters.fromTimestamp(v28);
            if (!v10.isNull(v8_3)) {
                v29 = Long.valueOf(v10.getLong(v8_3));
            } else {
                v29 = 0;
            }
            Long v30;
            java.util.Date v23 = com.example.eva.database.Converters.fromTimestamp(v29);
            if (!v10.isNull(v9_2)) {
                v30 = Long.valueOf(v10.getLong(v9_2));
            } else {
                v30 = 0;
            }
            int v31_0 = new com.example.eva.database.event.Event;
            v31_0(v5_4, v19, v20, v21, v22, v23, com.example.eva.database.Converters.fromTimestamp(v30), v10.getString(v1_2), v10.getString(v2_2));
            Throwable v18_2 = v0_4;
            Throwable v0_2 = v31_0;
            int v17_13 = v1_2;
            v0_2.setId(v10.getInt(v0_4));
            v4_1.add(v0_2);
            v1_2 = v17_13;
            v0_4 = v18_2;
        }
        v10.close();
        v3_1.release();
        return v4_1;
    } catch (Throwable v0_1) {
    }
}
```
## getAllEventList
  
**Signature :** `()Ljava/util/List;`  
**Nombre de registre :** 32  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.List getAllEventList()
{
    androidx.room.RoomSQLiteQuery v4 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table ORDER BY dateStart ASC", 0);
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v6 = androidx.room.util.DBUtil.query(this.__db, v4, 0, 0);
    try {
        Throwable v0_3 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "id");
        int v7_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "idAssociation");
        int v8_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "idGoogleEvent");
        int v9_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "summary");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "canceled");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "dateStart");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "dateEnd");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "dateUpdated");
        int v14_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "location");
        int v15_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v6, "description");
        java.util.ArrayList v3_2 = new java.util.ArrayList(v6.getCount());
    } catch (Throwable v0_0) {
        v6.close();
        v4.release();
        throw v0_0;
    }
    while (v6.moveToNext()) {
        Integer v5_4;
        if (!v6.isNull(v7_1)) {
            v5_4 = Integer.valueOf(v6.getInt(v7_1));
        } else {
            v5_4 = 0;
        }
        Integer v26;
        String v18 = v6.getString(v8_1);
        String v19 = v6.getString(v9_1);
        if (!v6.isNull(v10_1)) {
            v26 = Integer.valueOf(v6.getInt(v10_1));
        } else {
            v26 = 0;
        }
        Boolean v20;
        if (v26 != null) {
            com.example.eva.database.event.Event v16_14;
            if (v26.intValue() == 0) {
                v16_14 = 0;
            } else {
                v16_14 = 1;
            }
            v20 = Boolean.valueOf(v16_14);
        } else {
            v20 = 0;
        }
        Long v27;
        if (!v6.isNull(v11_1)) {
            v27 = Long.valueOf(v6.getLong(v11_1));
        } else {
            v27 = 0;
        }
        Long v28;
        java.util.Date v21 = com.example.eva.database.Converters.fromTimestamp(v27);
        if (!v6.isNull(v12_1)) {
            v28 = Long.valueOf(v6.getLong(v12_1));
        } else {
            v28 = 0;
        }
        Long v29;
        java.util.Date v22 = com.example.eva.database.Converters.fromTimestamp(v28);
        if (!v6.isNull(v13_1)) {
            v29 = Long.valueOf(v6.getLong(v13_1));
        } else {
            v29 = 0;
        }
        int v30_0 = new com.example.eva.database.event.Event;
        v30_0(v5_4, v18, v19, v20, v21, v22, com.example.eva.database.Converters.fromTimestamp(v29), v6.getString(v14_1), v6.getString(v15_1));
        Throwable v17_2 = v0_3;
        Throwable v0_2 = v30_0;
        v0_2.setId(v6.getInt(v0_3));
        v3_2.add(v0_2);
        v0_3 = v17_2;
    }
    v6.close();
    v4.release();
    return v3_2;
}
```
## getAssociation
  
**Signature :** `(I)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 21  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association getAssociation(int p20)
{
    androidx.room.RoomSQLiteQuery v4 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM association_table WHERE id == ?", 1);
    v4.bindLong(1, ((long) p20));
    this.__db.assertNotSuspendingTransaction();
    Boolean v8_0 = 0;
    android.database.Cursor v9 = androidx.room.util.DBUtil.query(this.__db, v4, 0, 0);
    try {
        Boolean v8_1;
        int v0_3 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "id");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idGoogleCalendar");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "name");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "logo");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "subscribe");
    } catch (int v0_5) {
        v9.close();
        v4.release();
        throw v0_5;
    }
    if (!v9.moveToFirst()) {
        v8_1 = 0;
    } else {
        Integer v3_1;
        String v14_1 = v9.getString(v10_1);
        String v15 = v9.getString(v11_1);
        if (!v9.isNull(v12_1)) {
            v3_1 = Integer.valueOf(v9.getInt(v12_1));
        } else {
            v3_1 = 0;
        }
        Integer v16_6;
        if (!v9.isNull(v13_1)) {
            v16_6 = Integer.valueOf(v9.getInt(v13_1));
        } else {
            v16_6 = 0;
        }
        if (v16_6 != null) {
            int v17_0;
            if (v16_6.intValue() == 0) {
                v17_0 = 0;
            } else {
                v17_0 = 1;
            }
            v8_0 = Boolean.valueOf(v17_0);
        }
        v8_1 = new com.example.eva.database.association.Association(v15, v14_1, v3_1, v8_0);
        v8_1.setId(v9.getInt(v0_3));
    }
    v9.close();
    v4.release();
    return v8_1;
}
```
## getAssociation
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/association/Association;`  
**Nombre de registre :** 21  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.association.Association getAssociation(String p20)
{
    androidx.room.RoomSQLiteQuery v5 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM association_table WHERE idGoogleCalendar == ?", 1);
    if (p20 != null) {
        v5.bindString(1, p20);
    } else {
        v5.bindNull(1);
    }
    this.__db.assertNotSuspendingTransaction();
    Boolean v8_0 = 0;
    android.database.Cursor v9 = androidx.room.util.DBUtil.query(this.__db, v5, 0, 0);
    try {
        Boolean v8_1;
        int v0_3 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "id");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idGoogleCalendar");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "name");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "logo");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "subscribe");
    } catch (int v0_5) {
        v9.close();
        v5.release();
        throw v0_5;
    }
    if (!v9.moveToFirst()) {
        v8_1 = 0;
    } else {
        Integer v4_1;
        String v14_1 = v9.getString(v10_1);
        String v15 = v9.getString(v11_1);
        if (!v9.isNull(v12_1)) {
            v4_1 = Integer.valueOf(v9.getInt(v12_1));
        } else {
            v4_1 = 0;
        }
        Integer v16_6;
        if (!v9.isNull(v13_1)) {
            v16_6 = Integer.valueOf(v9.getInt(v13_1));
        } else {
            v16_6 = 0;
        }
        if (v16_6 != null) {
            int v17_0;
            if (v16_6.intValue() == 0) {
                v17_0 = 0;
            } else {
                v17_0 = 1;
            }
            v8_0 = Boolean.valueOf(v17_0);
        }
        v8_1 = new com.example.eva.database.association.Association(v15, v14_1, v4_1, v8_0);
        v8_1.setId(v9.getInt(v0_3));
    }
    v9.close();
    v5.release();
    return v8_1;
}
```
## getEvent
  
**Signature :** `(I)Lcom/example/eva/database/event/Event;`  
**Nombre de registre :** 34  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.event.Event getEvent(int p33)
{
    androidx.room.RoomSQLiteQuery v4 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table WHERE id == ? ", 1);
    v4.bindLong(1, ((long) p33));
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v9 = androidx.room.util.DBUtil.query(this.__db, v4, 0, 0);
    try {
        com.example.eva.database.event.Event v20_3;
        com.example.eva.database.event.Event v0_5 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "id");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idAssociation");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idGoogleEvent");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "summary");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "canceled");
        int v14_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateStart");
        int v15_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateEnd");
        int v3_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateUpdated");
        int v7_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "location");
        int v8_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "description");
    } catch (com.example.eva.database.event.Event v0_1) {
        v9.close();
        v4.release();
        throw v0_1;
    }
    if (!v9.moveToFirst()) {
        v20_3 = 0;
    } else {
        Integer v19_3;
        if (!v9.isNull(v10_1)) {
            v19_3 = Integer.valueOf(v9.getInt(v10_1));
        } else {
            v19_3 = 0;
        }
        Integer v30;
        String v22 = v9.getString(v11_1);
        String v23 = v9.getString(v12_1);
        if (!v9.isNull(v13_1)) {
            v30 = Integer.valueOf(v9.getInt(v13_1));
        } else {
            v30 = 0;
        }
        Boolean v24;
        if (v30 != null) {
            Boolean v16_0;
            if (v30.intValue() == 0) {
                v16_0 = 0;
            } else {
                v16_0 = 1;
            }
            v24 = Boolean.valueOf(v16_0);
        } else {
            v24 = 0;
        }
        Boolean v16_4;
        if (!v9.isNull(v14_1)) {
            v16_4 = Long.valueOf(v9.getLong(v14_1));
        } else {
            v16_4 = 0;
        }
        Long v17_0;
        java.util.Date v25 = com.example.eva.database.Converters.fromTimestamp(v16_4);
        if (!v9.isNull(v15_1)) {
            v17_0 = Long.valueOf(v9.getLong(v15_1));
        } else {
            v17_0 = 0;
        }
        Long v18_1;
        java.util.Date v26 = com.example.eva.database.Converters.fromTimestamp(v17_0);
        if (!v9.isNull(v3_2)) {
            v18_1 = Long.valueOf(v9.getLong(v3_2));
        } else {
            v18_1 = 0;
        }
        int v31_0 = new com.example.eva.database.event.Event;
        v31_0(v19_3, v22, v23, v24, v25, v26, com.example.eva.database.Converters.fromTimestamp(v18_1), v9.getString(v7_2), v9.getString(v8_2));
        com.example.eva.database.event.Event v0_3 = v31_0;
        v0_3.setId(v9.getInt(v0_5));
        v20_3 = v0_3;
    }
    v9.close();
    v4.release();
    return v20_3;
}
```
## getEvent
  
**Signature :** `(Ljava/lang/String;)Lcom/example/eva/database/event/Event;`  
**Nombre de registre :** 34  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.eva.database.event.Event getEvent(String p33)
{
    androidx.room.RoomSQLiteQuery v5 = androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table WHERE idGoogleEvent == ?", 1);
    if (p33 != null) {
        v5.bindString(1, p33);
    } else {
        v5.bindNull(1);
    }
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v9 = androidx.room.util.DBUtil.query(this.__db, v5, 0, 0);
    try {
        com.example.eva.database.event.Event v20_3;
        com.example.eva.database.event.Event v0_5 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "id");
        int v10_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idAssociation");
        int v11_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "idGoogleEvent");
        int v12_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "summary");
        int v13_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "canceled");
        int v14_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateStart");
        int v15_1 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateEnd");
        int v4_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "dateUpdated");
        int v7_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "location");
        int v8_2 = androidx.room.util.CursorUtil.getColumnIndexOrThrow(v9, "description");
    } catch (com.example.eva.database.event.Event v0_1) {
        v9.close();
        v5.release();
        throw v0_1;
    }
    if (!v9.moveToFirst()) {
        v20_3 = 0;
    } else {
        Integer v19_3;
        if (!v9.isNull(v10_1)) {
            v19_3 = Integer.valueOf(v9.getInt(v10_1));
        } else {
            v19_3 = 0;
        }
        Integer v30;
        String v22 = v9.getString(v11_1);
        String v23 = v9.getString(v12_1);
        if (!v9.isNull(v13_1)) {
            v30 = Integer.valueOf(v9.getInt(v13_1));
        } else {
            v30 = 0;
        }
        Boolean v24;
        if (v30 != null) {
            Boolean v16_0;
            if (v30.intValue() == 0) {
                v16_0 = 0;
            } else {
                v16_0 = 1;
            }
            v24 = Boolean.valueOf(v16_0);
        } else {
            v24 = 0;
        }
        Boolean v16_4;
        if (!v9.isNull(v14_1)) {
            v16_4 = Long.valueOf(v9.getLong(v14_1));
        } else {
            v16_4 = 0;
        }
        Long v17_0;
        java.util.Date v25 = com.example.eva.database.Converters.fromTimestamp(v16_4);
        if (!v9.isNull(v15_1)) {
            v17_0 = Long.valueOf(v9.getLong(v15_1));
        } else {
            v17_0 = 0;
        }
        Long v18_1;
        java.util.Date v26 = com.example.eva.database.Converters.fromTimestamp(v17_0);
        if (!v9.isNull(v4_2)) {
            v18_1 = Long.valueOf(v9.getLong(v4_2));
        } else {
            v18_1 = 0;
        }
        int v31_0 = new com.example.eva.database.event.Event;
        v31_0(v19_3, v22, v23, v24, v25, v26, com.example.eva.database.Converters.fromTimestamp(v18_1), v9.getString(v7_2), v9.getString(v8_2));
        com.example.eva.database.event.Event v0_3 = v31_0;
        v0_3.setId(v9.getInt(v0_5));
        v20_3 = v0_3;
    }
    v9.close();
    v5.release();
    return v20_3;
}
```
## getEventData
  
**Signature :** `()Landroidx/lifecycle/LiveData;`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public androidx.lifecycle.LiveData getEventData()
{
    return this.__db.getInvalidationTracker().createLiveData(new String[] {"event_table"}), 0, new com.example.eva.database.MyDao_Impl$8(this, androidx.room.RoomSQLiteQuery.acquire("SELECT * FROM event_table ORDER BY dateStart ASC", 0)));
}
```
## insertAssociation
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void insertAssociation(com.example.eva.database.association.Association p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__insertionAdapterOfAssociation.insert(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfAssociation Landroidx/room/EntityInsertionAdapter;|Landroidx/room/EntityInsertionAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityInsertionAdapter;->insert(Ljava/lang/Object;)V|Landroidx/room/EntityInsertionAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfAssociation Landroidx/room/EntityInsertionAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityInsertionAdapter;->insert(Ljava/lang/Object;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## insertEvent
  
**Signature :** `(Lcom/example/eva/database/event/Event;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void insertEvent(com.example.eva.database.event.Event p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__insertionAdapterOfEvent.insert(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfEvent Landroidx/room/EntityInsertionAdapter;|Landroidx/room/EntityInsertionAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityInsertionAdapter;->insert(Ljava/lang/Object;)V|Landroidx/room/EntityInsertionAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__insertionAdapterOfEvent Landroidx/room/EntityInsertionAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityInsertionAdapter;->insert(Ljava/lang/Object;)V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## numberEventConfirmedByAssociation
  
**Signature :** `(J I)I`  
**Nombre de registre :** 10  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int numberEventConfirmedByAssociation(long p7, int p9)
{
    androidx.room.RoomSQLiteQuery v1_1 = androidx.room.RoomSQLiteQuery.acquire("SELECT count(*) FROM event_table WHERE dateEnd >= ? AND idAssociation == ? AND canceled = 0 ORDER BY dateStart ASC", 2);
    v1_1.bindLong(1, p7);
    v1_1.bindLong(2, ((long) p9));
    this.__db.assertNotSuspendingTransaction();
    android.database.Cursor v3_2 = androidx.room.util.DBUtil.query(this.__db, v1_1, 0, 0);
    try {
        int v4_2;
        if (!v3_2.moveToFirst()) {
            v4_2 = 0;
        } else {
            v4_2 = v3_2.getInt(0);
        }
    } catch (int v4_1) {
        v3_2.close();
        v1_1.release();
        throw v4_1;
    }
    v3_2.close();
    v1_1.release();
    return v4_2;
}
```
## updateAssociation
  
**Signature :** `(Lcom/example/eva/database/association/Association;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void updateAssociation(com.example.eva.database.association.Association p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__updateAdapterOfAssociation.handle(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/association/Association;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/association/Association;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfAssociation Landroidx/room/EntityDeletionOrUpdateAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## updateEvent
  
**Signature :** `(Lcom/example/eva/database/event/Event;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void updateEvent(com.example.eva.database.event.Event p3)
{
    this.__db.assertNotSuspendingTransaction();
    this.__db.beginTransaction();
    try {
        this.__updateAdapterOfEvent.handle(p3);
        this.__db.setTransactionSuccessful();
        this.__db.endTransaction();
        return;
    } catch (Throwable v0_1) {
        this.__db.endTransaction();
        throw v0_1;
    }
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>iget-object")
	10("n°3<br/>iget-object") --> 14("n°4<br/>invoke-virtual")
	14("n°4<br/>invoke-virtual") --> 20("n°5<br/>iget-object")
	20("n°5<br/>iget-object") --> 24("n°6<br/>invoke-virtual")
	24("n°6<br/>invoke-virtual") --> 30("n°7<br/>iget-object")
	30("n°7<br/>iget-object") --> 34("n°8<br/>invoke-virtual")
	34("n°8<br/>invoke-virtual") --> 40("n°9<br/>iget-object")
	40("n°9<br/>iget-object") --> 44("n°10<br/>invoke-virtual")
	44("n°10<br/>invoke-virtual") --> 50("n°11<br/>nop")
	50("n°11<br/>nop") --> 52("n°12<br/>return-void")
	54("n°13<br/>move-exception") --> 56("n°14<br/>iget-object")
	56("n°14<br/>iget-object") --> 60("n°15<br/>invoke-virtual")
	60("n°15<br/>invoke-virtual") --> 66("n°16<br/>throw")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**3**|10|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**4**|14|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**5**|20|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**6**|24|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|Landroidx/room/EntityDeletionOrUpdateAdapter;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|Lcom/example/eva/database/event/Event;|<span style='color:#f14848'></span>|
|**7**|30|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**8**|34|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**9**|40|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**10**|44|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|Landroidx/room/RoomDatabase;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**11**|50|nop||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**12**|52|return-void||<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**13**|54|move-exception|v0|Ljava/lang/Exception;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**14**|56|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|Lcom/example/eva/database/MyDao_Impl;|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**15**|60|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>Ljava/lang/Exception;</span>|Landroidx/room/RoomDatabase;|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
|**16**|66|throw|v0|Ljava/lang/Exception;|<span style='color:grey'>Landroidx/room/RoomDatabase;</span>|<span style='color:grey'>Lcom/example/eva/database/MyDao_Impl;</span>|<span style='color:grey'>Lcom/example/eva/database/event/Event;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->assertNotSuspendingTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->beginTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__updateAdapterOfEvent Landroidx/room/EntityDeletionOrUpdateAdapter;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|invoke-virtual|v0, v3, Landroidx/room/EntityDeletionOrUpdateAdapter;->handle(Ljava/lang/Object;)I|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->setTransactionSuccessful()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|iget-object|v0, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v0, Landroidx/room/RoomDatabase;->endTransaction()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-exception|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|iget-object|v1, v2, Lcom/example/eva/database/MyDao_Impl;->__db Landroidx/room/RoomDatabase;|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|invoke-virtual|v1, Landroidx/room/RoomDatabase;->endTransaction()V|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|throw|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
