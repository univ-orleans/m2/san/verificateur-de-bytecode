
com.example.testeadd2.Homme2
============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Ljava/lang/Integer; Ljava/lang/String; Ljava/lang/Float;)V`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Homme2(Integer p2, String p3, Float p4)
{
    this.age = p2.intValue();
    this.nom = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>invoke-virtual")
	6("n°2<br/>invoke-virtual") --> 12("n°3<br/>move-result")
	12("n°3<br/>move-result") --> 14("n°4<br/>iput")
	14("n°4<br/>iput") --> 18("n°5<br/>iput-object")
	18("n°5<br/>iput-object") --> 22("n°6<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v1, Ljava/lang/Object;-><init>()V|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Homme2;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
|**2**|6|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Homme2;</span>|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
|**3**|12|move-result|v0|int|<span style='color:grey'>Lcom/example/testeadd2/Homme2;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
|**4**|14|iput|v0, v1, Lcom/example/testeadd2/Homme2;->age I|int|Lcom/example/testeadd2/Homme2;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
|**5**|18|iput-object|v3, v1, Lcom/example/testeadd2/Homme2;->nom Ljava/lang/String;|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Homme2;|<span style='color:grey'>Ljava/lang/Integer;</span>|Ljava/lang/String;|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
|**6**|22|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Homme2;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>Ljava/lang/Float;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v1, Ljava/lang/Object;-><init>()V|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v2, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iput|v0, v1, Lcom/example/testeadd2/Homme2;->age I|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iput-object|v3, v1, Lcom/example/testeadd2/Homme2;->nom Ljava/lang/String;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
