
android.net.ConnectivityManager
===============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [getNetworkInfo](#getnetworkinfo)
	* [getRestrictBackgroundStatus](#getrestrictbackgroundstatus)
	* [isActiveNetworkMetered](#isactivenetworkmetered)
	* [getActiveNetworkInfo](#getactivenetworkinfo)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**getNetworkInfo**](#getnetworkinfo)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getRestrictBackgroundStatus**](#getrestrictbackgroundstatus)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**isActiveNetworkMetered**](#isactivenetworkmetered)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**getActiveNetworkInfo**](#getactivenetworkinfo)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## getNetworkInfo

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getRestrictBackgroundStatus

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## isActiveNetworkMetered

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  

## getActiveNetworkInfo

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.ACCESS_NETWORK_STATE|normal|view network connections|Allows the app to view information about network connections such as which networks exist and are connected.|
  
