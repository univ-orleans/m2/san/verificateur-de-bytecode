
com.example.testeadd2.Tableau
=============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [\<init>](#init)
	* [addPersonne](#addpersonne)
	* [addPersonne](#addpersonne)
	* [getMyArrayHomme](#getmyarrayhomme)
	* [moyenneTableauAge](#moyennetableauage)
	* [moyenneTableauPoids](#moyennetableaupoids)
	* [nbMajeur](#nbmajeur)
	* [nbNomCommancePar](#nbnomcommancepar)
	* [printHomme](#printhomme)
	* [remplirTableauAge](#remplirtableauage)
	* [remplirTableauInitialNom](#remplirtableauinitialnom)
	* [remplirTableauMajeur](#remplirtableaumajeur)
	* [remplirTableauPoids](#remplirtableaupoids)
	* [setMyArrayAge](#setmyarrayage)
	* [setMyArrayHomme](#setmyarrayhomme)
	* [setMyArrayInitialNom](#setmyarrayinitialnom)
	* [setMyArrayMajeur](#setmyarraymajeur)
	* [setMyArrayPoids](#setmyarraypoids)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**addPersonne**](#addpersonne)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**addPersonne**](#addpersonne)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getMyArrayHomme**](#getmyarrayhomme)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**moyenneTableauAge**](#moyennetableauage)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**moyenneTableauPoids**](#moyennetableaupoids)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nbMajeur**](#nbmajeur)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nbNomCommancePar**](#nbnomcommancepar)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**printHomme**](#printhomme)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remplirTableauAge**](#remplirtableauage)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remplirTableauInitialNom**](#remplirtableauinitialnom)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remplirTableauMajeur**](#remplirtableaumajeur)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**remplirTableauPoids**](#remplirtableaupoids)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setMyArrayAge**](#setmyarrayage)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setMyArrayHomme**](#setmyarrayhomme)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setMyArrayInitialNom**](#setmyarrayinitialnom)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setMyArrayMajeur**](#setmyarraymajeur)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setMyArrayPoids**](#setmyarraypoids)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Tableau()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## \<init>
  
**Signature :** `([Lcom/example/testeadd2/Personne; [I [F [C [Z)V`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Tableau(com.example.testeadd2.Personne[] p1, int[] p2, float[] p3, char[] p4, boolean[] p5)
{
    this.myArrayHomme = p1;
    this.myArrayAge = p2;
    this.myArrayPoids = p3;
    this.myArrayInitialNom = p4;
    this.myArrayMajeur = p5;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>iput-object")
	10("n°3<br/>iput-object") --> 14("n°4<br/>iput-object")
	14("n°4<br/>iput-object") --> 18("n°5<br/>iput-object")
	18("n°5<br/>iput-object") --> 22("n°6<br/>iput-object")
	22("n°6<br/>iput-object") --> 26("n°7<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>[C</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|Lcom/example/testeadd2/Tableau;|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>[I</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>[C</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
|**3**|10|iput-object|v2, v0, Lcom/example/testeadd2/Tableau;->myArrayAge [I|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|[I|<span style='color:grey'>[F</span>|<span style='color:grey'>[C</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
|**4**|14|iput-object|v3, v0, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|[F|<span style='color:grey'>[C</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
|**5**|18|iput-object|v4, v0, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>[F</span>|[C|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
|**6**|22|iput-object|v5, v0, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>[C</span>|[Z|<span style='color:#f14848'></span>|
|**7**|26|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>[C</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v2, v0, Lcom/example/testeadd2/Tableau;->myArrayAge [I|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iput-object|v3, v0, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|iput-object|v4, v0, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iput-object|v5, v0, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**7**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## addPersonne
  
**Signature :** `(Lcom/example/testeadd2/Personne;)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void addPersonne(com.example.testeadd2.Personne p3)
{
    com.example.testeadd2.Personne[] v0 = this.myArrayHomme;
    v0[(v0.length - 1)] = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>array-length")
	4("n°2<br/>array-length") --> 6("n°3<br/>add-int/lit8")
	6("n°3<br/>add-int/lit8") --> 10("n°4<br/>aput-object")
	10("n°4<br/>aput-object") --> 14("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v2, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**2**|4|array-length|v1, v0|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**3**|6|add-int/lit8|v1, v1, -1|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**4**|10|aput-object|v3, v0, v1|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**5**|14|return-void||<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v2, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|array-length|v1, v0|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|add-int/lit8|v1, v1, -1|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|aput-object|v3, v0, v1|True|False|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## addPersonne
  
**Signature :** `(Lcom/example/testeadd2/Personne; I)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void addPersonne(com.example.testeadd2.Personne p2, int p3)
{
    this.myArrayHomme[p3] = p2;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>aput-object")
	4("n°2<br/>aput-object") --> 8("n°3<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|[Lcom/example/testeadd2/Personne;|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|aput-object|v2, v0, v3|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|Lcom/example/testeadd2/Personne;|int|<span style='color:#f14848'></span>|
|**3**|8|return-void||<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|aput-object|v2, v0, v3|True|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**3**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getMyArrayHomme
  
**Signature :** `()[Lcom/example/testeadd2/Personne;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public com.example.testeadd2.Personne[] getMyArrayHomme()
{
    return this.myArrayHomme;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|[Lcom/example/testeadd2/Personne;|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## moyenneTableauAge
  
**Signature :** `()I`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int moyenneTableauAge()
{
    int v0 = 0;
    int v1_0 = this.myArrayAge;
    int v3 = 0;
    while (v3 < v1_0.length) {
        v0 += v1_0[v3];
        v3++;
    }
    return (v0 / this.myArrayAge.length);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>const/4")
	8("n°4<br/>const/4") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget")
	10("n°5<br/>if-ge") -.-> 26("n°10<br/>iget-object")
	14("n°6<br/>aget") --> 18("n°7<br/>add-int/2addr")
	18("n°7<br/>add-int/2addr") --> 20("n°8<br/>add-int/lit8")
	20("n°8<br/>add-int/lit8") --> 24("n°9<br/>goto")
	24("n°9<br/>goto") --> 10("n°5<br/>if-ge")
	26("n°10<br/>iget-object") --> 30("n°11<br/>array-length")
	30("n°11<br/>array-length") --> 32("n°12<br/>div-int")
	32("n°12<br/>div-int") --> 36("n°13<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v5, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>int</span>|[I|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v2, v1|<span style='color:grey'>int</span>|[I|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|const/4|v3, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v3, v2, +8|<span style='color:grey'>int</span>|<span style='color:grey'>[I</span>|int|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|aget|v4, v1, v3|<span style='color:grey'>int</span>|[I|<span style='color:grey'>int</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|18|add-int/2addr|v0, v4|int|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**8**|20|add-int/lit8|v3, v3, 1|<span style='color:grey'>int</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|24|goto|-7|<span style='color:grey'>int</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|26|iget-object|v1, v5, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>int</span>|[I|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**11**|30|array-length|v1, v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**12**|32|div-int|v1, v0, v1|int|int|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**13**|36|return|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v5, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v2, v1|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const/4|v3, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v3, v2, +8|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget|v4, v1, v3|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|False|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|add-int/2addr|v0, v4|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|add-int/lit8|v3, v3, 1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|goto|-7|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|iget-object|v1, v5, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**11**|array-length|v1, v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|div-int|v1, v0, v1|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|return|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## moyenneTableauPoids
  
**Signature :** `()I`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int moyenneTableauPoids()
{
    int v0 = 0;
    int v1_0 = 0;
    while(true) {
        float v2_0 = this.myArrayPoids;
        if (v1_0 >= v2_0.length) {
            break;
        }
        v0 = ((int) (((float) v0) + v2_0[v1_0]));
        v1_0++;
    }
    return (v0 / v2_0.length);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>const/4")
	2("n°2<br/>const/4") --> 4("n°3<br/>iget-object")
	4("n°3<br/>iget-object") --> 8("n°4<br/>array-length")
	8("n°4<br/>array-length") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>int-to-float")
	10("n°5<br/>if-ge") -.-> 30("n°12<br/>array-length")
	14("n°6<br/>int-to-float") --> 16("n°7<br/>aget")
	16("n°7<br/>aget") --> 20("n°8<br/>add-float/2addr")
	20("n°8<br/>add-float/2addr") --> 22("n°9<br/>float-to-int")
	22("n°9<br/>float-to-int") --> 24("n°10<br/>add-int/lit8")
	24("n°10<br/>add-int/lit8") --> 28("n°11<br/>goto")
	28("n°11<br/>goto") --> 4("n°3<br/>iget-object")
	30("n°12<br/>array-length") --> 32("n°13<br/>div-int")
	32("n°13<br/>div-int") --> 36("n°14<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|const/4|v1, 0|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**3**|4|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|float|<span style='color:grey'>float</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**4**|8|array-length|v3, v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|[F|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v1, v3, +a|<span style='color:grey'>int</span>|int|<span style='color:grey'>[F</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|int-to-float|v3, v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>[F</span>|float|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|16|aget|v2, v2, v1|<span style='color:grey'>int</span>|int|float|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**8**|20|add-float/2addr|v3, v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|float|float|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|22|float-to-int|v0, v3|int|<span style='color:grey'>int</span>|<span style='color:grey'>float</span>|float|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|24|add-int/lit8|v1, v1, 1|<span style='color:grey'>int</span>|int|<span style='color:grey'>float</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|28|goto|-c|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>float</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**12**|30|array-length|v1, v2|<span style='color:grey'>int</span>|int|[F|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**13**|32|div-int|v1, v0, v1|int|int|<span style='color:grey'>[F</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**14**|36|return|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>[F</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const/4|v1, 0|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**4**|array-length|v3, v2|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v1, v3, +a|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|int-to-float|v3, v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|aget|v2, v2, v1|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|add-float/2addr|v3, v2|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|float-to-int|v0, v3|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|add-int/lit8|v1, v1, 1|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|goto|-c|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|array-length|v1, v2|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|div-int|v1, v0, v1|False|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|return|v1|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## nbMajeur
  
**Signature :** `()I`  
**Nombre de registre :** 5  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int nbMajeur()
{
    int v0 = 0;
    int v1 = 0;
    while(true) {
        boolean v2_0 = this.myArrayMajeur;
        if (v1 >= v2_0.length) {
            break;
        }
        if (v2_0[v1]) {
            v0++;
        }
        v1++;
    }
    return v0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>const/4")
	2("n°2<br/>const/4") --> 4("n°3<br/>iget-object")
	4("n°3<br/>iget-object") --> 8("n°4<br/>array-length")
	8("n°4<br/>array-length") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget-char")
	10("n°5<br/>if-ge") -.-> 32("n°11<br/>return")
	14("n°6<br/>aget-char") --> 18("n°7<br/>if-eqz")
	18("n°7<br/>if-eqz") --> 22("n°8<br/>add-int/lit8")
	18("n°7<br/>if-eqz") -.-> 26("n°9<br/>add-int/lit8")
	22("n°8<br/>add-int/lit8") --> 26("n°9<br/>add-int/lit8")
	26("n°9<br/>add-int/lit8") --> 30("n°10<br/>goto")
	30("n°10<br/>goto") --> 4("n°3<br/>iget-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|const/4|v1, 0|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**3**|4|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|[Z|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**4**|8|array-length|v3, v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|[Z|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v1, v3, +b|<span style='color:grey'>int</span>|int|<span style='color:grey'>[Z</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|aget-char|v2, v2, v1|<span style='color:grey'>int</span>|int|[Z|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|18|if-eqz|v2, +4|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|[Z|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**8**|22|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|26|add-int/lit8|v1, v1, 1|<span style='color:grey'>int</span>|int|<span style='color:grey'>[Z</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|30|goto|-d|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|32|return|v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const/4|v1, 0|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**4**|array-length|v3, v2|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v1, v3, +b|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget-char|v2, v2, v1|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|if-eqz|v2, +4|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|add-int/lit8|v1, v1, 1|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|goto|-d|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|return|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## nbNomCommancePar
  
**Signature :** `(C)I`  
**Nombre de registre :** 6  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int nbNomCommancePar(char p5)
{
    int v0 = 0;
    int v1 = 0;
    while(true) {
        char v2_0 = this.myArrayInitialNom;
        if (v1 >= v2_0.length) {
            break;
        }
        if (p5 == v2_0[v1]) {
            v0++;
        }
        v1++;
    }
    return v0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>const/4")
	2("n°2<br/>const/4") --> 4("n°3<br/>iget-object")
	4("n°3<br/>iget-object") --> 8("n°4<br/>array-length")
	8("n°4<br/>array-length") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget-char")
	10("n°5<br/>if-ge") -.-> 32("n°11<br/>return")
	14("n°6<br/>aget-char") --> 18("n°7<br/>if-ne")
	18("n°7<br/>if-ne") --> 22("n°8<br/>add-int/lit8")
	18("n°7<br/>if-ne") -.-> 26("n°9<br/>add-int/lit8")
	22("n°8<br/>add-int/lit8") --> 26("n°9<br/>add-int/lit8")
	26("n°9<br/>add-int/lit8") --> 30("n°10<br/>goto")
	30("n°10<br/>goto") --> 4("n°3<br/>iget-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**2**|2|const/4|v1, 0|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**3**|4|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|char|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Tableau;|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**4**|8|array-length|v3, v2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|[C|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v1, v3, +b|<span style='color:grey'>int</span>|int|<span style='color:grey'>[C</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**6**|14|aget-char|v2, v2, v1|<span style='color:grey'>int</span>|int|char|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**7**|18|if-ne|v5, v2, +4|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|char|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|char|<span style='color:#f14848'></span>|
|**8**|22|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>int</span>|<span style='color:grey'>char</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**9**|26|add-int/lit8|v1, v1, 1|<span style='color:grey'>int</span>|int|<span style='color:grey'>char</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**10**|30|goto|-d|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>char</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
|**11**|32|return|v0|int|<span style='color:grey'>int</span>|<span style='color:grey'>[C</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|const/4|v1, 0|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iget-object|v2, v4, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|array-length|v3, v2|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v1, v3, +b|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget-char|v2, v2, v1|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|if-ne|v5, v2, +4|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**8**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|add-int/lit8|v1, v1, 1|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|goto|-d|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|return|v0|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## printHomme
  
**Signature :** `()V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void printHomme()
{
    int v0 = 0;
    while (v0 < this.myArrayHomme.length) {
        System.out.println(this.myArrayHomme[v0]);
        v0++;
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>if-ge")
	8("n°4<br/>if-ge") --> 12("n°5<br/>sget-object")
	8("n°4<br/>if-ge") -.-> 36("n°11<br/>return-void")
	12("n°5<br/>sget-object") --> 16("n°6<br/>iget-object")
	16("n°6<br/>iget-object") --> 20("n°7<br/>aget-object")
	20("n°7<br/>aget-object") --> 24("n°8<br/>invoke-virtual")
	24("n°8<br/>invoke-virtual") --> 30("n°9<br/>add-int/lit8")
	30("n°9<br/>add-int/lit8") --> 34("n°10<br/>goto")
	34("n°10<br/>goto") --> 2("n°2<br/>iget-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v3, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|Ljava/io/PrintStream;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v1, v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|if-ge|v0, v1, +e|int|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|12|sget-object|v1, Ljava/lang/System;->out Ljava/io/PrintStream;|<span style='color:grey'>int</span>|Ljava/io/PrintStream;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|16|iget-object|v2, v3, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/io/PrintStream;</span>|[Lcom/example/testeadd2/Personne;|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**7**|20|aget-object|v2, v2, v0|int|<span style='color:grey'>Ljava/io/PrintStream;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**8**|24|invoke-virtual|v1, v2, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V|<span style='color:grey'>int</span>|Ljava/io/PrintStream;|Lcom/example/testeadd2/Personne;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|30|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>Ljava/io/PrintStream;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|34|goto|-10|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/io/PrintStream;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|36|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v3, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v1, v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-ge|v0, v1, +e|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|sget-object|v1, Ljava/lang/System;->out Ljava/io/PrintStream;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|iget-object|v2, v3, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**7**|aget-object|v2, v2, v0|False|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v1, v2, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V|<span style='color:grey'>False</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|goto|-10|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remplirTableauAge
  
**Signature :** `()V`  
**Nombre de registre :** 8  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remplirTableauAge()
{
    int v0 = 0;
    com.example.testeadd2.Personne[] v1 = this.myArrayHomme;
    int v2 = v1.length;
    int v3 = 0;
    while (v3 < v2) {
        this.myArrayAge[v0] = v1[v3].getAge().intValue();
        v0++;
        v3++;
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>const/4")
	8("n°4<br/>const/4") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget-object")
	10("n°5<br/>if-ge") -.-> 54("n°17<br/>return-void")
	14("n°6<br/>aget-object") --> 18("n°7<br/>iget-object")
	18("n°7<br/>iget-object") --> 22("n°8<br/>invoke-virtual")
	22("n°8<br/>invoke-virtual") --> 28("n°9<br/>move-result-object")
	28("n°9<br/>move-result-object") --> 30("n°10<br/>invoke-virtual")
	30("n°10<br/>invoke-virtual") --> 36("n°11<br/>move-result")
	36("n°11<br/>move-result") --> 38("n°12<br/>aput")
	38("n°12<br/>aput") --> 42("n°13<br/>nop")
	42("n°13<br/>nop") --> 44("n°14<br/>add-int/lit8")
	44("n°14<br/>add-int/lit8") --> 48("n°15<br/>add-int/lit8")
	48("n°15<br/>add-int/lit8") --> 52("n°16<br/>goto")
	52("n°16<br/>goto") --> 10("n°5<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v2, v1|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|const/4|v3, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v3, v2, +16|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|aget-object|v4, v1, v3|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|int|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|18|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|[I|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**8**|22|invoke-virtual|v4, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>[I</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|28|move-result-object|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|30|invoke-virtual|v6, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|36|move-result|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**12**|38|aput|v6, v5, v0|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|[I|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**13**|42|nop||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**14**|44|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**15**|48|add-int/lit8|v3, v3, 1|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**16**|52|goto|-15|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[I</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**17**|54|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v2, v1|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const/4|v3, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v3, v2, +16|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget-object|v4, v1, v3|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayAge [I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v4, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v6, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|aput|v6, v5, v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|nop||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|add-int/lit8|v3, v3, 1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|goto|-15|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remplirTableauInitialNom
  
**Signature :** `()V`  
**Nombre de registre :** 9  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remplirTableauInitialNom()
{
    int v0 = 0;
    com.example.testeadd2.Personne[] v1 = this.myArrayHomme;
    int v2 = v1.length;
    int v4 = 0;
    while (v4 < v2) {
        v1[v4].getNom().charAt(0)[char v6_0] = v1[v4].getNom().charAt(0);
        v0++;
        v4++;
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>const/4")
	8("n°4<br/>const/4") --> 10("n°5<br/>const/4")
	10("n°5<br/>const/4") --> 12("n°6<br/>if-ge")
	12("n°6<br/>if-ge") --> 16("n°7<br/>aget-object")
	12("n°6<br/>if-ge") -.-> 56("n°18<br/>return-void")
	16("n°7<br/>aget-object") --> 20("n°8<br/>invoke-virtual")
	20("n°8<br/>invoke-virtual") --> 26("n°9<br/>move-result-object")
	26("n°9<br/>move-result-object") --> 28("n°10<br/>invoke-virtual")
	28("n°10<br/>invoke-virtual") --> 34("n°11<br/>move-result")
	34("n°11<br/>move-result") --> 36("n°12<br/>iget-object")
	36("n°12<br/>iget-object") --> 40("n°13<br/>aput-char")
	40("n°13<br/>aput-char") --> 44("n°14<br/>nop")
	44("n°14<br/>nop") --> 46("n°15<br/>add-int/lit8")
	46("n°15<br/>add-int/lit8") --> 50("n°16<br/>add-int/lit8")
	50("n°16<br/>add-int/lit8") --> 54("n°17<br/>goto")
	54("n°17<br/>goto") --> 12("n°6<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v8, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v2, v1|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|const/4|v3, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|const/4|v4, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|12|if-ge|v4, v2, +16|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|16|aget-object|v5, v1, v4|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**8**|20|invoke-virtual|v5, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|26|move-result-object|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|28|invoke-virtual|v6, v3, Ljava/lang/String;->charAt(I)C|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|34|move-result|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|char|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**12**|36|iget-object|v7, v8, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [Z|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|[Z|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**13**|40|aput-char|v6, v7, v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|char|[Z|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le tableau n'est pas du bon type, le registre v7 n'est pas de type "char"<br/>Le registre v6 est de type "char" au lieu de "int".<br/>le registre v6 est de type "None" au lieux de "char".</span>|
|**14**|44|nop||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**15**|46|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**16**|50|add-int/lit8|v4, v4, 1|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**17**|54|goto|-15|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**18**|56|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|v8|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v8, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v2, v1|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const/4|v3, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|const/4|v4, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-ge|v4, v2, +16|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|aget-object|v5, v1, v4|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v5, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v6, v3, Ljava/lang/String;->charAt(I)C|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|iget-object|v7, v8, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [Z|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:#f14848'></span>|
|**13**|aput-char|v6, v7, v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|nop||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|add-int/lit8|v4, v4, 1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|goto|-15|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remplirTableauMajeur
  
**Signature :** `()V`  
**Nombre de registre :** 8  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remplirTableauMajeur()
{
    com.example.testeadd2.Personne[] v1 = this.myArrayHomme;
    int v2 = v1.length;
    int v3 = 0;
    while (v3 < v2) {
        v1[v3].is_majeur(18)[0] = v1[v3].is_majeur(18);
        v3++;
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>const/4")
	8("n°4<br/>const/4") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget-object")
	10("n°5<br/>if-ge") -.-> 44("n°14<br/>return-void")
	14("n°6<br/>aget-object") --> 18("n°7<br/>iget-object")
	18("n°7<br/>iget-object") --> 22("n°8<br/>const/16")
	22("n°8<br/>const/16") --> 26("n°9<br/>invoke-virtual")
	26("n°9<br/>invoke-virtual") --> 32("n°10<br/>move-result")
	32("n°10<br/>move-result") --> 34("n°11<br/>aput")
	34("n°11<br/>aput") --> 38("n°12<br/>add-int/lit8")
	38("n°12<br/>add-int/lit8") --> 42("n°13<br/>goto")
	42("n°13<br/>goto") --> 10("n°5<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v2, v1|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|const/4|v3, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v3, v2, +11|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|aget-object|v4, v1, v3|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|int|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|18|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|[Z|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**8**|22|const/16|v6, 18|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|26|invoke-virtual|v4, v6, Lcom/example/testeadd2/Personne;->is_majeur(I)Z|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>[Z</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|32|move-result|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|boolean|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|34|aput|v6, v6, v0|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|boolean|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v6 n'est pas de type "int ou float"<br/>Le registre v6 n'est pas un tableau.<br/>le registre v6 est de type "None" au lieux de "boolean".</span>|
|**12**|38|add-int/lit8|v3, v3, 1|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**13**|42|goto|-10|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[Z</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**14**|44|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v2, v1|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const/4|v3, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v3, v2, +11|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget-object|v4, v1, v3|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**8**|const/16|v6, 18|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|invoke-virtual|v4, v6, Lcom/example/testeadd2/Personne;->is_majeur(I)Z|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|move-result|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|aput|v6, v6, v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|add-int/lit8|v3, v3, 1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|goto|-10|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## remplirTableauPoids
  
**Signature :** `()V`  
**Nombre de registre :** 8  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void remplirTableauPoids()
{
    int v0 = 0;
    com.example.testeadd2.Personne[] v1 = this.myArrayHomme;
    int v2 = v1.length;
    int v3 = 0;
    while (v3 < v2) {
        this.myArrayPoids[v0] = v1[v3].getPoids();
        v0++;
        v3++;
    }
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>iget-object")
	2("n°2<br/>iget-object") --> 6("n°3<br/>array-length")
	6("n°3<br/>array-length") --> 8("n°4<br/>const/4")
	8("n°4<br/>const/4") --> 10("n°5<br/>if-ge")
	10("n°5<br/>if-ge") --> 14("n°6<br/>aget-object")
	10("n°5<br/>if-ge") -.-> 46("n°15<br/>return-void")
	14("n°6<br/>aget-object") --> 18("n°7<br/>iget-object")
	18("n°7<br/>iget-object") --> 22("n°8<br/>invoke-virtual")
	22("n°8<br/>invoke-virtual") --> 28("n°9<br/>move-result")
	28("n°9<br/>move-result") --> 30("n°10<br/>aput")
	30("n°10<br/>aput") --> 34("n°11<br/>nop")
	34("n°11<br/>nop") --> 36("n°12<br/>add-int/lit8")
	36("n°12<br/>add-int/lit8") --> 40("n°13<br/>add-int/lit8")
	40("n°13<br/>add-int/lit8") --> 44("n°14<br/>goto")
	44("n°14<br/>goto") --> 10("n°5<br/>if-ge")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**2**|2|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**3**|6|array-length|v2, v1|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**4**|8|const/4|v3, 0|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**5**|10|if-ge|v3, v2, +12|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**6**|14|aget-object|v4, v1, v3|<span style='color:grey'>int</span>|[Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|int|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**7**|18|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|[F|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Tableau;|<span style='color:#f14848'></span>|
|**8**|22|invoke-virtual|v4, Lcom/example/testeadd2/Personne;->getPoids()F|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>[F</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**9**|28|move-result|v6|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|float|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**10**|30|aput|v6, v5, v0|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|[F|float|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**11**|34|nop||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**12**|36|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**13**|40|add-int/lit8|v3, v3, 1|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**14**|44|goto|-11|<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>[F</span>|<span style='color:grey'>float</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
|**15**|46|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|v7|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iget-object|v1, v7, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**3**|array-length|v2, v1|<span style='color:grey'>False</span>|True|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|const/4|v3, 0|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|if-ge|v3, v2, +12|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|False|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|aget-object|v4, v1, v3|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|False|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|iget-object|v5, v7, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**8**|invoke-virtual|v4, Lcom/example/testeadd2/Personne;->getPoids()F|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result|v6|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|aput|v6, v5, v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|nop||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|add-int/lit8|v3, v3, 1|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|goto|-11|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setMyArrayAge
  
**Signature :** `([I)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setMyArrayAge(int[] p1)
{
    this.myArrayAge = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayAge [I|Lcom/example/testeadd2/Tableau;|[I|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[I</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayAge [I|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setMyArrayHomme
  
**Signature :** `([Lcom/example/testeadd2/Personne;)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setMyArrayHomme(com.example.testeadd2.Personne[] p1)
{
    this.myArrayHomme = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|Lcom/example/testeadd2/Tableau;|[Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayHomme [Lcom/example/testeadd2/Personne;|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setMyArrayInitialNom
  
**Signature :** `([C)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setMyArrayInitialNom(char[] p1)
{
    this.myArrayInitialNom = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|Lcom/example/testeadd2/Tableau;|[C|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[C</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayInitialNom [C|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setMyArrayMajeur
  
**Signature :** `([Z)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setMyArrayMajeur(boolean[] p1)
{
    this.myArrayMajeur = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|Lcom/example/testeadd2/Tableau;|[Z|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v1 n'est pas de type "int ou float"<br/>Le champs n'est pas de type "int ou float"</span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[Z</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayMajeur [Z|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setMyArrayPoids
  
**Signature :** `([F)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setMyArrayPoids(float[] p1)
{
    this.myArrayPoids = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput-object") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|Lcom/example/testeadd2/Tableau;|[F|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Tableau;</span>|<span style='color:grey'>[F</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput-object|v1, v0, Lcom/example/testeadd2/Tableau;->myArrayPoids [F|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
