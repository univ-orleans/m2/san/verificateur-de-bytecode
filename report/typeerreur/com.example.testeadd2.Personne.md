
com.example.testeadd2.Personne
==============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<init>](#init)
	* [Femme](#femme)
	* [homme](#homme)
	* [arrondirAge](#arrondirage)
	* [arrondirPoids](#arrondirpoids)
	* [codeAsciiInitialNom](#codeasciiinitialnom)
	* [getAge](#getage)
	* [getNom](#getnom)
	* [getPoids](#getpoids)
	* [initialNomCodeAscii](#initialnomcodeascii)
	* [is_majeur](#is_majeur)
	* [setPoids](#setpoids)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**Femme**](#femme)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**homme**](#homme)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**arrondirAge**](#arrondirage)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**arrondirPoids**](#arrondirpoids)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**codeAsciiInitialNom**](#codeasciiinitialnom)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAge**](#getage)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getNom**](#getnom)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getPoids**](#getpoids)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**initialNomCodeAscii**](#initialnomcodeascii)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**is_majeur**](#ismajeur)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setPoids**](#setpoids)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<init>
  
**Signature :** `(Ljava/lang/Integer; Ljava/lang/String; F)V`  
**Nombre de registre :** 4  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Personne(Integer p1, String p2, float p3)
{
    this.age = p1;
    this.nom = p2;
    this.poids = p3;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>iput-object")
	6("n°2<br/>iput-object") --> 10("n°3<br/>iput-object")
	10("n°3<br/>iput-object") --> 14("n°4<br/>iput")
	14("n°4<br/>iput") --> 18("n°5<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/testeadd2/Personne;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
|**2**|6|iput-object|v1, v0, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|Lcom/example/testeadd2/Personne;|Ljava/lang/Integer;|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
|**3**|10|iput-object|v2, v0, Lcom/example/testeadd2/Personne;->nom Ljava/lang/String;|Lcom/example/testeadd2/Personne;|<span style='color:grey'>Ljava/lang/Integer;</span>|Ljava/lang/String;|<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
|**4**|14|iput|v3, v0, Lcom/example/testeadd2/Personne;->poids F|Lcom/example/testeadd2/Personne;|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|float|<span style='color:#f14848'></span>|
|**5**|18|return-void||<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>Ljava/lang/String;</span>|<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|iput-object|v1, v0, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|iput-object|v2, v0, Lcom/example/testeadd2/Personne;->nom Ljava/lang/String;|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|iput|v3, v0, Lcom/example/testeadd2/Personne;->poids F|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## Femme
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static String Femme()
{
    return "F";
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'F'|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'F'|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:#f14848'></span>|
  

## homme
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static String homme()
{
    return "H";
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const-string") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const-string|v0, 'H'|Ljava/lang/String;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|const-string|v0, 'H'|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:#f14848'></span>|
  

## arrondirAge
  
**Signature :** `()Z`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean arrondirAge()
{
    return ((float) this.getAge().intValue());
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>invoke-virtual")
	8("n°3<br/>invoke-virtual") --> 14("n°4<br/>move-result")
	14("n°4<br/>move-result") --> 16("n°5<br/>int-to-float")
	16("n°5<br/>int-to-float") --> 18("n°6<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**3**|8|invoke-virtual|v0, Ljava/lang/Integer;->intValue()I|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**4**|14|move-result|v0|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**5**|16|int-to-float|v0, v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**6**|18|return|v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v0 est de type "float" au lieux de "boolean".</span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v0, Ljava/lang/Integer;->intValue()I|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|int-to-float|v0, v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## arrondirPoids
  
**Signature :** `()I`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int arrondirPoids()
{
    return ((char) this.getPoids());
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result")
	6("n°2<br/>move-result") --> 8("n°3<br/>int-to-char")
	8("n°3<br/>int-to-char") --> 10("n°4<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v1, Lcom/example/testeadd2/Personne;->getPoids()F|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|6|move-result|v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**3**|8|int-to-char|v0, v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v0 est de type "float" au lieu de int.</span>|
|**4**|10|return|v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v0 est de type "float" au lieux de "int".</span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v1, Lcom/example/testeadd2/Personne;->getPoids()F|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|int-to-char|v0, v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## codeAsciiInitialNom
  
**Signature :** `()I`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int codeAsciiInitialNom()
{
    return this.getNom().charAt(0);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-virtual") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>const/4")
	8("n°3<br/>const/4") --> 10("n°4<br/>invoke-virtual")
	10("n°4<br/>invoke-virtual") --> 16("n°5<br/>move-result")
	16("n°5<br/>move-result") --> 18("n°6<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**3**|8|const/4|v1, 0|<span style='color:grey'>Ljava/lang/String;</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-virtual|v0, v1, Ljava/lang/String;->charAt(I)C|Ljava/lang/String;|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result|v0|char|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
|**6**|18|return|v0|char|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|const/4|v1, 0|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-virtual|v0, v1, Ljava/lang/String;->charAt(I)C|True|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|return|v0|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getAge
  
**Signature :** `()Ljava/lang/Integer;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Integer getAge()
{
    return this.age;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|Ljava/lang/Integer;|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getNom
  
**Signature :** `()Ljava/lang/String;`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public String getNom()
{
    return this.nom;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->nom Ljava/lang/String;|Ljava/lang/String;|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/String;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->nom Ljava/lang/String;|True|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getPoids
  
**Signature :** `()F`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public float getPoids()
{
    return this.poids;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget|v0, v1, Lcom/example/testeadd2/Personne;->poids F|float|Lcom/example/testeadd2/Personne;|<span style='color:#f14848'></span>|
|**2**|4|return|v0|float|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget|v0, v1, Lcom/example/testeadd2/Personne;->poids F|False|True|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## initialNomCodeAscii
  
**Signature :** `(I)C`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public char initialNomCodeAscii(int p2)
{
    return ((char) p2);
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>int-to-char") --> 2("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|int-to-char|v0, v2|char|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:#f14848'></span>|
|**2**|2|return|v0|char|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|int-to-char|v0, v2|False|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## is_majeur
  
**Signature :** `(I)Z`  
**Nombre de registre :** 3  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public boolean is_majeur(int p2)
{
    int v0_2;
    if (this.age.intValue() < p2) {
        v0_2 = 0;
    } else {
        v0_2 = 1;
    }
    return v0_2;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iget-object") --> 4("n°2<br/>invoke-virtual")
	4("n°2<br/>invoke-virtual") --> 10("n°3<br/>move-result")
	10("n°3<br/>move-result") --> 12("n°4<br/>if-lt")
	12("n°4<br/>if-lt") --> 16("n°5<br/>const/4")
	12("n°4<br/>if-lt") -.-> 20("n°7<br/>const/4")
	16("n°5<br/>const/4") --> 18("n°6<br/>goto")
	18("n°6<br/>goto") --> 22("n°8<br/>return")
	20("n°7<br/>const/4") --> 22("n°8<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|Ljava/lang/Integer;|Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-virtual|v0, Ljava/lang/Integer;->intValue()I|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|10|move-result|v0|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|if-lt|v0, v2, +4|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:#f14848'></span>|
|**5**|16|const/4|v0, 1|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**6**|18|goto|+2|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**7**|20|const/4|v0, 0|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**8**|22|return|v0|int|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iget-object|v0, v1, Lcom/example/testeadd2/Personne;->age Ljava/lang/Integer;|True|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v0, Ljava/lang/Integer;->intValue()I|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|move-result|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|if-lt|v0, v2, +4|True|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**5**|const/4|v0, 1|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|goto|+2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|const/4|v0, 0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|return|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setPoids
  
**Signature :** `(F)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void setPoids(float p1)
{
    this.poids = p1;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>iput") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|iput|v1, v0, Lcom/example/testeadd2/Personne;->poids F|Lcom/example/testeadd2/Personne;|float|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|iput|v1, v0, Lcom/example/testeadd2/Personne;->poids F|True|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
