
android.os.PowerManager$WakeLock
================================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [acquire](#acquire)
	* [release](#release)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**acquire**](#acquire)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
|[**release**](#release)|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## acquire

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.WAKE_LOCK|normal|prevent phone from sleeping|Allows the app to prevent the phone from going to sleep.|
  

## release

### Permissions demandées
  

|#|nom|niveau de protection|résumé|description|
| :--- | :--- | :--- | :--- | :--- |
|1|android.permission.WAKE_LOCK|normal|prevent phone from sleeping|Allows the app to prevent the phone from going to sleep.|
  
