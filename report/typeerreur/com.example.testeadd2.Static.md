
com.example.testeadd2.Static
============================

Table des matières
==================

* [Résultat des analyses](#rsultat-des-analyses)
* [Méthodes](#mthodes)
	* [\<clinit>](#clinit)
	* [\<init>](#init)
	* [getAgeCentenaire](#getagecentenaire)
	* [getAgeStandard](#getagestandard)
	* [getInitialNom](#getinitialnom)
	* [getPoidsStandard](#getpoidsstandard)
	* [isCinquantePourcentMajour](#iscinquantepourcentmajour)
	* [setAgeCentenaire](#setagecentenaire)
	* [setAgeMoyen](#setagemoyen)
	* [setCinquantePourcentMajour](#setcinquantepourcentmajour)
	* [setInitialNom](#setinitialnom)
	* [setPoidsMoyen](#setpoidsmoyen)
	* [cinquantePourcentMajour](#cinquantepourcentmajour)
	* [nbPersonneAgeStandard](#nbpersonneagestandard)
	* [nbPersonneCentenaire](#nbpersonnecentenaire)
	* [nbPersonnePoidsStandard](#nbpersonnepoidsstandard)
	* [nomCommanceParN](#nomcommanceparn)

# Résultat des analyses
  

|méthode|registre|objet|permission|
| :---: | :---: | :---: | :---: |
|[**\<clinit>**](#clinit)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**\<init>**](#init)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAgeCentenaire**](#getagecentenaire)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getAgeStandard**](#getagestandard)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getInitialNom**](#getinitialnom)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**getPoidsStandard**](#getpoidsstandard)|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**isCinquantePourcentMajour**](#iscinquantepourcentmajour)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setAgeCentenaire**](#setagecentenaire)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setAgeMoyen**](#setagemoyen)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setCinquantePourcentMajour**](#setcinquantepourcentmajour)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setInitialNom**](#setinitialnom)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**setPoidsMoyen**](#setpoidsmoyen)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**cinquantePourcentMajour**](#cinquantepourcentmajour)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nbPersonneAgeStandard**](#nbpersonneagestandard)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nbPersonneCentenaire**](#nbpersonnecentenaire)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nbPersonnePoidsStandard**](#nbpersonnepoidsstandard)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
|[**nomCommanceParN**](#nomcommanceparn)|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

# Méthodes

## \<clinit>
  
**Signature :** `()V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
static Static()
{
    com.example.testeadd2.Static.poidsStandard = 1116471296;
    com.example.testeadd2.Static.ageStandard = 1116471296;
    com.example.testeadd2.Static.ageCentenaire = new Integer(95);
    com.example.testeadd2.Static.initialNom = 78;
    com.example.testeadd2.Static.cinquantePourcentMajour = 0;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/high16") --> 4("n°2<br/>sput-boolean")
	4("n°2<br/>sput-boolean") --> 8("n°3<br/>const/high16")
	8("n°3<br/>const/high16") --> 12("n°4<br/>sput")
	12("n°4<br/>sput") --> 16("n°5<br/>new-instance")
	16("n°5<br/>new-instance") --> 20("n°6<br/>const/16")
	20("n°6<br/>const/16") --> 24("n°7<br/>invoke-direct")
	24("n°7<br/>invoke-direct") --> 30("n°8<br/>sput-object")
	30("n°8<br/>sput-object") --> 34("n°9<br/>const/16")
	34("n°9<br/>const/16") --> 38("n°10<br/>sput-char")
	38("n°10<br/>sput-char") --> 42("n°11<br/>const/4")
	42("n°11<br/>const/4") --> 44("n°12<br/>sput-boolean")
	44("n°12<br/>sput-boolean") --> 48("n°13<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/high16|v0, 17036 # [70.0]|float|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**2**|4|sput-boolean|v0, Lcom/example/testeadd2/Static;->poidsStandard F|float|<span style='color:grey'>None</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le registre v0 n'est pas de type "boolean"<br/>Le champs n'est pas de type "boolean"</span>|
|**3**|8|const/high16|v0, 17036 # [70.0]|float|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**4**|12|sput|v0, Lcom/example/testeadd2/Static;->ageStandard I|float|<span style='color:grey'>None</span>|<span style='color:#f14848'>**RegisterTypeError :**<br/>le registre v0 est de type "None" au lieux de "float".</span>|
|**5**|16|new-instance|v0, Ljava/lang/Integer;|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:#f14848'></span>|
|**6**|20|const/16|v1, 95|<span style='color:grey'>Ljava/lang/Integer;</span>|int|<span style='color:#f14848'></span>|
|**7**|24|invoke-direct|v0, v1, Ljava/lang/Integer;-><init>(I)V|Ljava/lang/Integer;|int|<span style='color:#f14848'></span>|
|**8**|30|sput-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**9**|34|const/16|v0, 78|int|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**10**|38|sput-char|v0, Lcom/example/testeadd2/Static;->initialNom C|int|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**11**|42|const/4|v0, 0|int|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**12**|44|sput-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|int|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**13**|48|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/high16|v0, 17036 # [70.0]|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**2**|sput-boolean|v0, Lcom/example/testeadd2/Static;->poidsStandard F|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**3**|const/high16|v0, 17036 # [70.0]|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**4**|sput|v0, Lcom/example/testeadd2/Static;->ageStandard I|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**5**|new-instance|v0, Ljava/lang/Integer;|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**6**|const/16|v1, 95|<span style='color:grey'>True</span>|False|<span style='color:#f14848'></span>|
|**7**|invoke-direct|v0, v1, Ljava/lang/Integer;-><init>(I)V|True|False|<span style='color:#f14848'></span>|
|**8**|sput-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**9**|const/16|v0, 78|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**10**|sput-char|v0, Lcom/example/testeadd2/Static;->initialNom C|True|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**11**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**12**|sput-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|False|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
|**13**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:#f14848'></span>|
  

## \<init>
  
**Signature :** `()V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public Static()
{
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-direct") --> 6("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-direct|v0, Ljava/lang/Object;-><init>()V|Lcom/example/testeadd2/Static;|<span style='color:#f14848'></span>|
|**2**|6|return-void||<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-direct|v0, Ljava/lang/Object;-><init>()V|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## getAgeCentenaire
  
**Signature :** `()Ljava/lang/Integer;`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static Integer getAgeCentenaire()
{
    return com.example.testeadd2.Static.ageCentenaire;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget-object") --> 4("n°2<br/>return-object")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|Ljava/lang/Integer;|<span style='color:#f14848'></span>|
|**2**|4|return-object|v0|Ljava/lang/Integer;|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sget-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|True|<span style='color:#f14848'></span>|
|**2**|return-object|v0|True|<span style='color:#f14848'></span>|
  

## getAgeStandard
  
**Signature :** `()I`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static int getAgeStandard()
{
    return com.example.testeadd2.Static.ageStandard;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget|v0, Lcom/example/testeadd2/Static;->ageStandard I|int|<span style='color:#f14848'></span>|
|**2**|4|return|v0|int|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sget|v0, Lcom/example/testeadd2/Static;->ageStandard I|False|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:#f14848'></span>|
  

## getInitialNom
  
**Signature :** `()C`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static char getInitialNom()
{
    return com.example.testeadd2.Static.initialNom;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget-char") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget-char|v0, Lcom/example/testeadd2/Static;->initialNom C|char|<span style='color:#f14848'></span>|
|**2**|4|return|v0|char|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sget-char|v0, Lcom/example/testeadd2/Static;->initialNom C|False|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:#f14848'></span>|
  

## getPoidsStandard
  
**Signature :** `()F`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-unvalidated.svg' alt='invalide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static float getPoidsStandard()
{
    return com.example.testeadd2.Static.poidsStandard;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget-boolean") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget-boolean|v0, Lcom/example/testeadd2/Static;->poidsStandard F|float|<span style='color:#f14848'>**RegisterTypeError :**<br/>Le champs n'est pas de type "boolean"</span>|
|**2**|4|return|v0|float|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sget-boolean|v0, Lcom/example/testeadd2/Static;->poidsStandard F|False|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:#f14848'></span>|
  

## isCinquantePourcentMajour
  
**Signature :** `()Z`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static boolean isCinquantePourcentMajour()
{
    return com.example.testeadd2.Static.cinquantePourcentMajour;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sget-boolean") --> 4("n°2<br/>return")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sget-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|boolean|<span style='color:#f14848'></span>|
|**2**|4|return|v0|boolean|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sget-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|False|<span style='color:#f14848'></span>|
|**2**|return|v0|False|<span style='color:#f14848'></span>|
  

## setAgeCentenaire
  
**Signature :** `(I)V`  
**Nombre de registre :** 2  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static void setAgeCentenaire(int p1)
{
    com.example.testeadd2.Static.ageCentenaire = Integer.valueOf(p1);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>invoke-static") --> 6("n°2<br/>move-result-object")
	6("n°2<br/>move-result-object") --> 8("n°3<br/>sput-object")
	8("n°3<br/>sput-object") --> 12("n°4<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|invoke-static|v1, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>None</span>|int|<span style='color:#f14848'></span>|
|**2**|6|move-result-object|v0|Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**3**|8|sput-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
|**4**|12|return-void||<span style='color:grey'>Ljava/lang/Integer;</span>|<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|invoke-static|v1, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;|<span style='color:grey'>False</span>|True|<span style='color:#f14848'></span>|
|**2**|move-result-object|v0|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|sput-object|v0, Lcom/example/testeadd2/Static;->ageCentenaire Ljava/lang/Integer;|True|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|return-void||<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setAgeMoyen
  
**Signature :** `(I)V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static void setAgeMoyen(int p0)
{
    com.example.testeadd2.Static.ageStandard = p0;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sput") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sput|v0, Lcom/example/testeadd2/Static;->ageStandard I|int|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>int</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sput|v0, Lcom/example/testeadd2/Static;->ageStandard I|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setCinquantePourcentMajour
  
**Signature :** `(Z)V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static void setCinquantePourcentMajour(boolean p0)
{
    com.example.testeadd2.Static.cinquantePourcentMajour = p0;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sput-boolean") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sput-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|boolean|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>boolean</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sput-boolean|v0, Lcom/example/testeadd2/Static;->cinquantePourcentMajour Z|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setInitialNom
  
**Signature :** `(C)V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static void setInitialNom(char p0)
{
    com.example.testeadd2.Static.initialNom = p0;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sput-char") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sput-char|v0, Lcom/example/testeadd2/Static;->initialNom C|char|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>char</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sput-char|v0, Lcom/example/testeadd2/Static;->initialNom C|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## setPoidsMoyen
  
**Signature :** `(F)V`  
**Nombre de registre :** 1  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public static void setPoidsMoyen(float p0)
{
    com.example.testeadd2.Static.poidsStandard = p0;
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>sput") --> 4("n°2<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|sput|v0, Lcom/example/testeadd2/Static;->poidsStandard F|float|<span style='color:#f14848'></span>|
|**2**|4|return-void||<span style='color:grey'>float</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|erreurs|
| :--- | :--- | :--- | :--- | :--- |
|**1**|sput|v0, Lcom/example/testeadd2/Static;->poidsStandard F|True|<span style='color:#f14848'></span>|
|**2**|return-void||<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## cinquantePourcentMajour
  
**Signature :** `(Ljava/util/ArrayList;)V`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public void cinquantePourcentMajour(java.util.ArrayList p6)
{
    int v0 = 0;
    int v1_1 = p6.iterator();
    while (v1_1.hasNext()) {
        if (((com.example.testeadd2.Personne) v1_1.next()).getAge().intValue() >= 18) {
            v0++;
        }
    }
    int v1_0;
    if (v0 != (p6.size() / 2)) {
        v1_0 = 0;
    } else {
        v1_0 = 1;
    }
    com.example.testeadd2.Static.setCinquantePourcentMajour(v1_0);
    return;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>invoke-virtual")
	2("n°2<br/>invoke-virtual") --> 8("n°3<br/>move-result-object")
	8("n°3<br/>move-result-object") --> 10("n°4<br/>invoke-interface")
	10("n°4<br/>invoke-interface") --> 16("n°5<br/>move-result")
	16("n°5<br/>move-result") --> 18("n°6<br/>if-eqz")
	18("n°6<br/>if-eqz") --> 22("n°7<br/>invoke-interface")
	18("n°6<br/>if-eqz") -.-> 64("n°18<br/>invoke-virtual")
	22("n°7<br/>invoke-interface") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>check-cast")
	30("n°9<br/>check-cast") --> 34("n°10<br/>invoke-virtual")
	34("n°10<br/>invoke-virtual") --> 40("n°11<br/>move-result-object")
	40("n°11<br/>move-result-object") --> 42("n°12<br/>invoke-virtual")
	42("n°12<br/>invoke-virtual") --> 48("n°13<br/>move-result")
	48("n°13<br/>move-result") --> 50("n°14<br/>const/16")
	50("n°14<br/>const/16") --> 54("n°15<br/>if-lt")
	54("n°15<br/>if-lt") --> 58("n°16<br/>add-int/lit8")
	54("n°15<br/>if-lt") -.-> 62("n°17<br/>goto")
	58("n°16<br/>add-int/lit8") --> 62("n°17<br/>goto")
	62("n°17<br/>goto") --> 10("n°4<br/>invoke-interface")
	64("n°18<br/>invoke-virtual") --> 70("n°19<br/>move-result")
	70("n°19<br/>move-result") --> 72("n°20<br/>div-int/lit8")
	72("n°20<br/>div-int/lit8") --> 76("n°21<br/>if-ne")
	76("n°21<br/>if-ne") --> 80("n°22<br/>const/4")
	76("n°21<br/>if-ne") -.-> 84("n°24<br/>const/4")
	80("n°22<br/>const/4") --> 82("n°23<br/>goto")
	82("n°23<br/>goto") --> 86("n°25<br/>invoke-static")
	84("n°24<br/>const/4") --> 86("n°25<br/>invoke-static")
	86("n°25<br/>invoke-static") --> 92("n°26<br/>return-void")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**2**|2|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|Ljava/util/ArrayList;|<span style='color:#f14848'></span>|
|**3**|8|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**6**|18|if-eqz|v2, +17|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**9**|30|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**10**|34|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**11**|40|move-result-object|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**12**|42|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**13**|48|move-result|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**14**|50|const/16|v4, 18|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**15**|54|if-lt|v3, v4, +4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**16**|58|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**17**|62|goto|-1a|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**18**|64|invoke-virtual|v6, Ljava/util/ArrayList;->size()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|Ljava/util/ArrayList;|<span style='color:#f14848'></span>|
|**19**|70|move-result|v1|<span style='color:grey'>int</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**20**|72|div-int/lit8|v1, v1, 2|<span style='color:grey'>int</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**21**|76|if-ne|v0, v1, +4|int|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**22**|80|const/4|v1, 1|<span style='color:grey'>int</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**23**|82|goto|+2|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**24**|84|const/4|v1, 0|<span style='color:grey'>int</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**25**|86|invoke-static|v1, Lcom/example/testeadd2/Static;->setCinquantePourcentMajour(Z)V|<span style='color:grey'>int</span>|int|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**26**|92|return-void||<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-eqz|v2, +17|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|const/16|v4, 18|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|if-lt|v3, v4, +4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|goto|-1a|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|invoke-virtual|v6, Ljava/util/ArrayList;->size()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**19**|move-result|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|div-int/lit8|v1, v1, 2|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|if-ne|v0, v1, +4|False|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|const/4|v1, 1|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|goto|+2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**24**|const/4|v1, 0|<span style='color:grey'>False</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**25**|invoke-static|v1, Lcom/example/testeadd2/Static;->setCinquantePourcentMajour(Z)V|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**26**|return-void||<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## nbPersonneAgeStandard
  
**Signature :** `(Ljava/util/ArrayList;)I`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int nbPersonneAgeStandard(java.util.ArrayList p6)
{
    int v0 = 0;
    java.util.Iterator v1 = p6.iterator();
    while (v1.hasNext()) {
        if (Math.abs((((com.example.testeadd2.Personne) v1.next()).getAge().intValue() - com.example.testeadd2.Static.getAgeStandard())) < 5) {
            v0++;
        }
    }
    return v0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>invoke-virtual")
	2("n°2<br/>invoke-virtual") --> 8("n°3<br/>move-result-object")
	8("n°3<br/>move-result-object") --> 10("n°4<br/>invoke-interface")
	10("n°4<br/>invoke-interface") --> 16("n°5<br/>move-result")
	16("n°5<br/>move-result") --> 18("n°6<br/>if-eqz")
	18("n°6<br/>if-eqz") --> 22("n°7<br/>invoke-interface")
	18("n°6<br/>if-eqz") -.-> 80("n°23<br/>return")
	22("n°7<br/>invoke-interface") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>check-cast")
	30("n°9<br/>check-cast") --> 34("n°10<br/>invoke-virtual")
	34("n°10<br/>invoke-virtual") --> 40("n°11<br/>move-result-object")
	40("n°11<br/>move-result-object") --> 42("n°12<br/>invoke-virtual")
	42("n°12<br/>invoke-virtual") --> 48("n°13<br/>move-result")
	48("n°13<br/>move-result") --> 50("n°14<br/>invoke-static")
	50("n°14<br/>invoke-static") --> 56("n°15<br/>move-result")
	56("n°15<br/>move-result") --> 58("n°16<br/>sub-int/2addr")
	58("n°16<br/>sub-int/2addr") --> 60("n°17<br/>invoke-static")
	60("n°17<br/>invoke-static") --> 66("n°18<br/>move-result")
	66("n°18<br/>move-result") --> 68("n°19<br/>const/4")
	68("n°19<br/>const/4") --> 70("n°20<br/>if-ge")
	70("n°20<br/>if-ge") --> 74("n°21<br/>add-int/lit8")
	70("n°20<br/>if-ge") -.-> 78("n°22<br/>goto")
	74("n°21<br/>add-int/lit8") --> 78("n°22<br/>goto")
	78("n°22<br/>goto") --> 10("n°4<br/>invoke-interface")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**2**|2|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|Ljava/util/ArrayList;|<span style='color:#f14848'></span>|
|**3**|8|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**6**|18|if-eqz|v2, +1f|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**9**|30|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**10**|34|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**11**|40|move-result-object|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**12**|42|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**13**|48|move-result|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**14**|50|invoke-static|Lcom/example/testeadd2/Static;->getAgeStandard()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**15**|56|move-result|v4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**16**|58|sub-int/2addr|v3, v4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**17**|60|invoke-static|v3, Ljava/lang/Math;->abs(I)I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**18**|66|move-result|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**19**|68|const/4|v4, 5|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**20**|70|if-ge|v3, v4, +4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**21**|74|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**22**|78|goto|-22|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**23**|80|return|v0|int|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-eqz|v2, +1f|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-static|Lcom/example/testeadd2/Static;->getAgeStandard()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result|v4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|sub-int/2addr|v3, v4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|invoke-static|v3, Ljava/lang/Math;->abs(I)I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|move-result|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|const/4|v4, 5|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|if-ge|v3, v4, +4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**22**|goto|-22|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**23**|return|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## nbPersonneCentenaire
  
**Signature :** `(Ljava/util/ArrayList;)I`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int nbPersonneCentenaire(java.util.ArrayList p6)
{
    int v0 = 0;
    java.util.Iterator v1 = p6.iterator();
    while (v1.hasNext()) {
        if (((com.example.testeadd2.Personne) v1.next()).getAge().intValue() >= com.example.testeadd2.Static.getAgeCentenaire().intValue()) {
            v0++;
        }
    }
    return v0;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>const/4") --> 2("n°2<br/>invoke-virtual")
	2("n°2<br/>invoke-virtual") --> 8("n°3<br/>move-result-object")
	8("n°3<br/>move-result-object") --> 10("n°4<br/>invoke-interface")
	10("n°4<br/>invoke-interface") --> 16("n°5<br/>move-result")
	16("n°5<br/>move-result") --> 18("n°6<br/>if-eqz")
	18("n°6<br/>if-eqz") --> 22("n°7<br/>invoke-interface")
	18("n°6<br/>if-eqz") -.-> 76("n°21<br/>return")
	22("n°7<br/>invoke-interface") --> 28("n°8<br/>move-result-object")
	28("n°8<br/>move-result-object") --> 30("n°9<br/>check-cast")
	30("n°9<br/>check-cast") --> 34("n°10<br/>invoke-virtual")
	34("n°10<br/>invoke-virtual") --> 40("n°11<br/>move-result-object")
	40("n°11<br/>move-result-object") --> 42("n°12<br/>invoke-virtual")
	42("n°12<br/>invoke-virtual") --> 48("n°13<br/>move-result")
	48("n°13<br/>move-result") --> 50("n°14<br/>invoke-static")
	50("n°14<br/>invoke-static") --> 56("n°15<br/>move-result-object")
	56("n°15<br/>move-result-object") --> 58("n°16<br/>invoke-virtual")
	58("n°16<br/>invoke-virtual") --> 64("n°17<br/>move-result")
	64("n°17<br/>move-result") --> 66("n°18<br/>if-lt")
	66("n°18<br/>if-lt") --> 70("n°19<br/>add-int/lit8")
	66("n°18<br/>if-lt") -.-> 74("n°20<br/>goto")
	70("n°19<br/>add-int/lit8") --> 74("n°20<br/>goto")
	74("n°20<br/>goto") --> 10("n°4<br/>invoke-interface")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|const/4|v0, 0|int|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**2**|2|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|Ljava/util/ArrayList;|<span style='color:#f14848'></span>|
|**3**|8|move-result-object|v1|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**4**|10|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**5**|16|move-result|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**6**|18|if-eqz|v2, +1d|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**7**|22|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>int</span>|Ljava/util/Iterator;|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**8**|28|move-result-object|v2|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**9**|30|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**10**|34|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**11**|40|move-result-object|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**12**|42|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/Integer;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**13**|48|move-result|v3|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**14**|50|invoke-static|Lcom/example/testeadd2/Static;->getAgeCentenaire()Ljava/lang/Integer;|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**15**|56|move-result-object|v4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**16**|58|invoke-virtual|v4, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|Ljava/lang/Integer;|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**17**|64|move-result|v4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**18**|66|if-lt|v3, v4, +4|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|int|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**19**|70|add-int/lit8|v0, v0, 1|int|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**20**|74|goto|-20|<span style='color:grey'>int</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>int</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**21**|76|return|v0|int|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|const/4|v0, 0|False|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**3**|move-result-object|v1|<span style='color:grey'>False</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**4**|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|move-result|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|if-eqz|v2, +1d|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>False</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|move-result-object|v2|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getAge()Ljava/lang/Integer;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|move-result-object|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|invoke-virtual|v3, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|move-result|v3|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-static|Lcom/example/testeadd2/Static;->getAgeCentenaire()Ljava/lang/Integer;|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result-object|v4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-virtual|v4, Ljava/lang/Integer;->intValue()I|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result|v4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|if-lt|v3, v4, +4|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|add-int/lit8|v0, v0, 1|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|goto|-20|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|return|v0|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  

## nbPersonnePoidsStandard
  
**Signature :** `(Ljava/util/ArrayList;)I`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public int nbPersonnePoidsStandard(java.util.ArrayList p6)
{
    int v0 = 0;
    java.util.Iterator v1 = p6.iterator();
    while (v1.hasNext()) {
        if (Math.abs((((com.example.testeadd2.Personne) v1.next()).getPoids() - com.example.testeadd2.Static.getPoidsStandard())) < 1084227584) {
            v0++;
        }
    }
    return v0;
}
```
## nomCommanceParN
  
**Signature :** `(Ljava/util/ArrayList;)Ljava/util/ArrayList;`  
**Nombre de registre :** 7  

|registre|objet|permission|
| :---: | :---: | :---: |
|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-validated.svg' alt='valide' style='vertical-align:text-bottom;'/>|<img src='../../res/svg/icon-unknow.svg' alt='pas réalisé' style='vertical-align:text-bottom;'/>|
  

### Source


```java
public java.util.ArrayList nomCommanceParN(java.util.ArrayList p6)
{
    java.util.ArrayList v0_1 = new java.util.ArrayList();
    java.util.Iterator v1 = p6.iterator();
    while (v1.hasNext()) {
        com.example.testeadd2.Personne v2_0 = ((com.example.testeadd2.Personne) v1.next());
        if (v2_0.getNom().charAt(0) == com.example.testeadd2.Static.getInitialNom()) {
            v0_1.add(v2_0);
        }
    }
    return v0_1;
}
```
### Graphe d'instruction


```mermaid
flowchart TB
	classDef node stroke-width:0;
	classDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;
	classDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;

	0("n°1<br/>new-instance") --> 4("n°2<br/>invoke-direct")
	4("n°2<br/>invoke-direct") --> 10("n°3<br/>invoke-virtual")
	10("n°3<br/>invoke-virtual") --> 16("n°4<br/>move-result-object")
	16("n°4<br/>move-result-object") --> 18("n°5<br/>invoke-interface")
	18("n°5<br/>invoke-interface") --> 24("n°6<br/>move-result")
	24("n°6<br/>move-result") --> 26("n°7<br/>if-eqz")
	26("n°7<br/>if-eqz") --> 30("n°8<br/>invoke-interface")
	26("n°7<br/>if-eqz") -.-> 80("n°21<br/>return-object")
	30("n°8<br/>invoke-interface") --> 36("n°9<br/>move-result-object")
	36("n°9<br/>move-result-object") --> 38("n°10<br/>check-cast")
	38("n°10<br/>check-cast") --> 42("n°11<br/>invoke-virtual")
	42("n°11<br/>invoke-virtual") --> 48("n°12<br/>move-result-object")
	48("n°12<br/>move-result-object") --> 50("n°13<br/>const/4")
	50("n°13<br/>const/4") --> 52("n°14<br/>invoke-virtual")
	52("n°14<br/>invoke-virtual") --> 58("n°15<br/>move-result")
	58("n°15<br/>move-result") --> 60("n°16<br/>invoke-static")
	60("n°16<br/>invoke-static") --> 66("n°17<br/>move-result")
	66("n°17<br/>move-result") --> 68("n°18<br/>if-ne")
	68("n°18<br/>if-ne") --> 72("n°19<br/>invoke-virtual")
	68("n°18<br/>if-ne") -.-> 78("n°20<br/>goto")
	72("n°19<br/>invoke-virtual") --> 78("n°20<br/>goto")
	78("n°20<br/>goto") --> 18("n°5<br/>invoke-interface")

```
### Registres d'instructions
  

|#|offset|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|0|new-instance|v0, Ljava/util/ArrayList;|Ljava/util/ArrayList;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**2**|4|invoke-direct|v0, Ljava/util/ArrayList;-><init>()V|Ljava/util/ArrayList;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**3**|10|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|Ljava/util/ArrayList;|<span style='color:#f14848'></span>|
|**4**|16|move-result-object|v1|<span style='color:grey'>Ljava/util/ArrayList;</span>|Ljava/util/Iterator;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**5**|18|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>Ljava/util/ArrayList;</span>|Ljava/util/Iterator;|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>char</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**6**|24|move-result|v2|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**7**|26|if-eqz|v2, +1b|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|boolean|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**8**|30|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>Ljava/util/ArrayList;</span>|Ljava/util/Iterator;|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**9**|36|move-result-object|v2|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Ljava/lang/Object;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**10**|38|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**11**|42|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**12**|48|move-result-object|v3|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/String;|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**13**|50|const/4|v4, 0|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>Ljava/lang/String;</span>|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**14**|52|invoke-virtual|v3, v4, Ljava/lang/String;->charAt(I)C|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|Ljava/lang/String;|int|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**15**|58|move-result|v3|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|char|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**16**|60|invoke-static|Lcom/example/testeadd2/Static;->getInitialNom()C|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>int</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**17**|66|move-result|v4|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|char|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**18**|68|if-ne|v3, v4, +5|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|char|char|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**19**|72|invoke-virtual|v0, v2, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z|Ljava/util/ArrayList;|<span style='color:grey'>Ljava/util/Iterator;</span>|Lcom/example/testeadd2/Personne;|<span style='color:grey'>char</span>|<span style='color:grey'>char</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**20**|78|goto|-1e|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>Lcom/example/testeadd2/Personne;</span>|<span style='color:grey'>char</span>|<span style='color:grey'>char</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
|**21**|80|return-object|v0|Ljava/util/ArrayList;|<span style='color:grey'>Ljava/util/Iterator;</span>|<span style='color:grey'>boolean</span>|<span style='color:grey'>None</span>|<span style='color:grey'>None</span>|<span style='color:grey'>Lcom/example/testeadd2/Static;</span>|<span style='color:grey'>Ljava/util/ArrayList;</span>|<span style='color:#f14848'></span>|
  

### Instanciation des objets
  

|#|instr|repr|v0|v1|v2|v3|v4|v5|v6|erreurs|
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
|**1**|new-instance|v0, Ljava/util/ArrayList;|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**2**|invoke-direct|v0, Ljava/util/ArrayList;-><init>()V|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**3**|invoke-virtual|v6, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|True|<span style='color:#f14848'></span>|
|**4**|move-result-object|v1|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**5**|invoke-interface|v1, Ljava/util/Iterator;->hasNext()Z|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**6**|move-result|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**7**|if-eqz|v2, +1b|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**8**|invoke-interface|v1, Ljava/util/Iterator;->next()Ljava/lang/Object;|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**9**|move-result-object|v2|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**10**|check-cast|v2, Lcom/example/testeadd2/Personne;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**11**|invoke-virtual|v2, Lcom/example/testeadd2/Personne;->getNom()Ljava/lang/String;|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**12**|move-result-object|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**13**|const/4|v4, 0|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**14**|invoke-virtual|v3, v4, Ljava/lang/String;->charAt(I)C|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|False|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**15**|move-result|v3|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**16**|invoke-static|Lcom/example/testeadd2/Static;->getInitialNom()C|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**17**|move-result|v4|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**18**|if-ne|v3, v4, +5|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|True|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**19**|invoke-virtual|v0, v2, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z|True|<span style='color:grey'>True</span>|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**20**|goto|-1e|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
|**21**|return-object|v0|True|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:grey'>False</span>|<span style='color:grey'>False</span>|<span style='color:grey'>True</span>|<span style='color:grey'>True</span>|<span style='color:#f14848'></span>|
  
