# IMPORTS #####################################################################
from typing import List, Dict
from androguard.core.androconf import load_api_specific_resource_module
from androguard.core.bytecodes.dvm import EncodedMethod

from src.core.data import Data
from src.core.method import Method
from src.core.permission import Permission
from src.util.logging import log


# CLASS #######################################################################
class PermissionsAnalyser:

	def __init__(self, data: Data) -> None:
		u"""
		Constructeur
		"""
		self._data: Data = data

		api_level: str = data.apk.get_target_sdk_version()
		if int(api_level) > 25:
			log.warning('API level {} trop élevé pour analyser les permissions dynamiques, retour à l\'API level 25 '
						'par défaut pour cette partie de l\'analyse.'.format(api_level))
			api_level = '25'
		# MAPPING ENTRE METHODE ET PERMISSIONS - clé : 'nom_classe-nom_methode-description_methode' ; valeur : liste des permissions
		self._permissions_map: Dict[str, List[str]] = load_api_specific_resource_module('api_permission_mappings', api_level)
		# INFO SUR LES PERMISSIONS - clé : nom permission ; valeur : dict[type d'info] = valeur
		self._permissions_info: Dict[str, Dict[str, str]] = load_api_specific_resource_module('aosp_permissions', api_level)


	def analyseRuntimePermissions(self, method: Method) -> None:
		u"""
		Récupère les permissions dynamiques pour chaque méthode.

		:param method: La méthode à analyser
		:type method: Method
		"""
		encoded_method: EncodedMethod = method.method
		permission_api_name = '-'.join(
			(encoded_method.get_class_name(), encoded_method.get_name(), str(encoded_method.get_descriptor())))
		if permission_api_name in self._permissions_map:
			permission_names: List[str] = self._permissions_map[permission_api_name]
			for perm_name in permission_names:
				permission = Permission(perm_name, self._permissions_info[perm_name]['protectionLevel'], self._permissions_info[perm_name]['label'], self._permissions_info[perm_name]['description'], False)
				method.permissions.append(permission)


	def analyseStaticPermissions(self) -> None:
		u"""
		Enregistrement des permissions statiques (Makefile)
		"""
		used_permissions: Dict[str, List[str]] = self._data.apk.get_details_permissions()
		for name, content in used_permissions.items():
			permission: Permission = Permission(name=name, protection_level=content[0], summary=content[1], description=content[2], is_declared=False)
			log.info(f"Permission statique : (utilisée) {permission.name}")
			self._data.static_permissions.get("used").append(permission)
		declared_permissions: Dict[str, Dict[str, str]] = self._data.apk.get_declared_permissions_details()
		for name, content in declared_permissions.items():
			permission: Permission = Permission(name=name, protection_level=content.get("protectionLevel"), summary=content.get("label"), description=content.get("description"), is_declared=True)
			log.info(f"Permission statique : (déclarée) {permission.name}")
			self._data.static_permissions.get("declared").append(permission)
