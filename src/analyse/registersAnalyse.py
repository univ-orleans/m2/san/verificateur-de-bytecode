# IMPORTS #####################################################################
from typing import List, Type, Tuple, Optional
from androguard.core.bytecodes import dvm
from androguard.misc import Analysis

from src.core.method import Method
from src.core.instruction import Instruction
from src.core.registers import Registers
from src.util.exception import RegisterTypeError, RegisterNumberError, RegisterIndexError
from src.util.typeManager import TypeManager
from src.util.tools import classPathToClassName
from src.util.logging import log


# CLASS #######################################################################
class RegistersAnalyser:

	def __init__(self, analysis: Analysis, classes: List[dvm.ClassDefItem]) -> None:
		u"""
		Constructeur

		:param analysis: Le support d'analyse Androguard
		:type analysis: Analysis
		"""
		self._type_manager: TypeManager = TypeManager(analysis)
		self._type_manager.addClass(classes)


	@property
	def type_manager(self) -> TypeManager:
		return self._type_manager


	def analyse(self, method: Method) -> None:
		u"""
		Analyser une méthode

		Analyse chaque instruction et ses registres pour déterminer si la méthode
		passée en paramètre est valide.

		:param method: La méthode à analyser
		:type method: Method
		"""
		# récupérer la liste des instructions à évaluer
		positions_to_evaluate: List[int] = list(method.instructions)
		# effectuer l'évaluation tant qu'il reste des instructions à évaluer
		while len(positions_to_evaluate) != 0:
			current_position: int = positions_to_evaluate.pop(0)
			current_instruction: Instruction = method.instructions.get(current_position)
			current_instruction.is_valid = True
			if len(current_instruction.predecessors) == 0 and current_position != 0:
				instruction: Instruction = method.instructions.get(current_position-1)
				current_instruction.registers = instruction.registers
			current_instruction.errors[RegisterTypeError.__name__] = []
			current_instruction.errors[RegisterIndexError.__name__] = []
			current_instruction.errors[RegisterNumberError.__name__] = []
			try:
				current_instruction.registers = self.evaluate(method, current_instruction)
			except (RegisterIndexError, RegisterTypeError) as registers_type_error:
				log.error(registers_type_error.message)
				# comme les registres ne sont pas bons, récupérer les registres de l'instruction précédente
				current_instruction.registers = method.instructions.get(current_position-1 if current_position > 0 else 0).registers
			finally:
				for successor_instruction in current_instruction.successors:
					try:
						estimated_registers: Registers = current_instruction.registers.max(successor_instruction.registers)
					except RegisterNumberError as registers_number_error:
						log.error(registers_number_error.message)
						# ajouter les erreurs registre à l'instruction
						current_instruction.addError(RegisterNumberError.__name__, registers_number_error.message)
					else:
						if estimated_registers != successor_instruction.registers:
							successor_instruction.registers = estimated_registers
							if positions_to_evaluate.__contains__(successor_instruction.position):
								positions_to_evaluate.remove(successor_instruction.position)
								positions_to_evaluate.insert(0, successor_instruction.position)
				# mettre à jour l'état de validité de la méthode en fonction des instructions
				method.is_valid = current_instruction.is_valid
				if not current_instruction.is_valid:
					for key in current_instruction.errors:
						if current_instruction.errors[key]:
							log.error("\n".join(current_instruction.errors[key]))


	def evaluate(self, method: Method, instruction: Instruction) -> Registers:
		u"""
		Évaluation de l'état des registres pour une instruction

		:param method: Une méthode
		:type method: Method
		:param instruction: Une instruction de la méthode
		:type instruction: Instruction

		:return: L'état des registres après exécution de l'instruction
		:rtype: Registers
		"""
		# le résultat de l'évaluation des registres de l'instruction
		estimated_registers: Registers = instruction.registers.copy()
		# le nom de l'instruction
		name: str = instruction.name
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers

		if name not in {"move-result", "move-result-object", "move-result-wide"} and method.pile[0]:
			method.pile = (False, None)

		if name == "const/4":
			if 0 <= instr.A < len(registers):
				estimated_registers.setState(instr.A, ("int", instr.B))
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.A))

		elif name == "const":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, "int")
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "const/16":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setState(instr.AA, ("int", instr.BBBB))
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "const/high16":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, "float")
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "const-wide/32":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, 'long')
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "const-string":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, "Ljava/lang/String;")
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "const-class":
			if 0 <= instr.AA < len(registers):
				id, new_type = self.checkConstClass(instruction)
				estimated_registers.setType(id, new_type)
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name in {"iput-object", "iput", "iput-boolean"}:
			self.checkIput(instruction)

		elif name in {"iget-object", "iget", "iget-boolean"}:
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				id, new_type = self.checkIget(instruction)
				estimated_registers.setType(id, new_type)
			else:
				if instr.A >= len(registers) or instr.A < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B >= len(registers) or instr.B < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		elif name in {"if-gt", "if-ge", "if-eq", "if-ne", "if-lt", "if-le"}:
			self.checkIfTest(instruction)

		elif name in {"if-eqz", "if-nez", "if-ltz", "if-gez", "if-gtz", "if-lez"}:
			self.checkIfTestz(instruction)

		elif name in {"mul-int", "add-int", "div-int", "sub-int", "rem-int", "and-int", "or-int", "shl-int", "xor-int", "shr-int", "ushr-int", "add-float", "sub-float", "mul-float", "div-float", "rem-float",
		              "and-float", "or-float", "shl-float", "xor-float", "shr-float", "ushr-float"}:
			if 0 <= instr.AA < len(registers) and 0 <= instr.BB < len(registers) and 0 <= instr.CC < len(registers):
				id, new_type = self.checkBinop(instruction)
				if id is not None and new_type is not None:
					estimated_registers.setType(id, new_type)
			else:
				if instr.AA >= len(registers) or instr.AA < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.AA))
				if instr.BB >= len(registers) or instr.BB < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.BB))
				if instr.CC >= len(registers) or instr.CC < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.CC))


		elif name in {'add-int/lit8', 'xor-int/lit8', "mul-int/lit8", "rsub-int/lit8", "div-int/lit8", "rem-int/lit8", "and-int/lit8", "or-int/lit8", "xor-int/lit8", "shl-int/lit8", "shr-int/lit8", "ushr-int/lit8"}:
			if 0 <= instr.AA < len(registers) and 0 <= instr.BB < len(registers):
				id, new_type = self.checkBinopLit8(instruction)
				if id is not None and new_type is not None:
					estimated_registers.setType(id, new_type)
			else:
				if instr.AA >= len(registers) or instr.AA < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.AA))
				if instr.BB >= len(registers) or instr.BB < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.BB))

		elif name in {"add-int/2addr", "sub-int/2addr", "mul-int/2addr", "div-int/2addr", "rem-int/2addr", "and-int/2addr", "or-int/2addr", "xor-int/2addr", "shl-int/2addr", "shr-int/2addr", "ushr-int/2addr",
		              "add-float/2addr", "sub-float/2addr", "mul-float/2addr", "div-float/2addr", "rem-float/2addr"}:
			self.checkBinop2AddrMessage(instruction)

		elif name == "return":
			if 0 <= instr.AA < len(registers):
				type_return: str = method.method.get_information().get("return")
				self.checkReturn(instruction, type_return)
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "return-object":
			if 0 <= instr.AA < len(registers):
				type_return: str = classPathToClassName(method.method.get_information().get("return"))
				self.checkReturnObject(instruction, type_return)
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == "fill-array-data":
			self.checkFillArrayData(instruction)

		elif name == "instance-of":
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				id, new_type = self.checkInstanceOf(instruction)
				if id is not None and new_type is not None:
					estimated_registers.setType(id, new_type)
			else:
				if instr.A < 0 or instr.A >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B < 0 or instr.B >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		elif name == "check-cast":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, instr.cm.get_type(instr.BBBB))
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name == 'new-instance':
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, instr.cm.get_type(instr.BBBB))
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name in {'invoke-direct', 'invoke-virtual', 'invoke-static', 'invoke-interface', 'invoke-super'}:
			if 0 <= instr.C < len(registers) and 0 <= instr.D < len(registers) and 0 <= instr.E < len(registers) and 0 <= instr.F < len(registers) and 0 <= instr.G < len(registers):
				type_return = self.checkInvokeKind(instruction)
				if type_return != 'V':
					method.pile = (True, type_return)
			else:
				if instr.C < 0 or instr.C >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.C))
				if instr.D < 0 or instr.D >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.D))
				if instr.E < 0 or instr.E >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.E))
				if instr.F < 0 or instr.F >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.F))
				if instr.G < 0 or instr.G >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.G))

		elif name in {"move-result", "move-result-object", "move-result-wide"}:
			if 0 <= instr.AA < len(registers):
				id, new_type = self.checkMoveResult(instruction, method.pile)
				estimated_registers.setType(id, new_type)
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		elif name in {"move-object", "move", "move-wide"}:
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				id, new_type = self.checkMoveObject(instruction)
				estimated_registers.setType(id, new_type)
			else:
				if instr.A < 0 or instr.A >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B < 0 or instr.B >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		elif name in {"move-object/from16", "move-wide/from16", "move-object/from16"}:
			if 0 <= instr.AA < len(registers) and 0 <= instr.BBBB < len(registers):
				id, new_type = self.checkMoveObjectFrom16t(instruction)
				estimated_registers.setType(id, new_type)
			else:
				if instr.AA < 0 or instr.AA >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.AA))
				if instr.BBBB < 0 or instr.BBBB >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.BBBB))

		elif name in {"move/16", "move-wide/16", "move-object/16"}:
			if 0 <= instr.AAAA < len(registers) and 0 <= instr.BBBB < len(registers):
				id, new_type = self.checkMove16(instruction)
				estimated_registers.setType(id, new_type)
			else:
				if instr.AAAA < 0 or instr.AAAA >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.AAAA))
				if instr.BBBB < 0 or instr.BBBB >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.BBBB))


		elif name == "move-exception":
			if 0 <= instr.AA < len(registers):
				estimated_registers.setType(instr.AA, "Ljava/lang/Exception;")
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		# arrayop instructions
		elif name == "new-array":
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				id, new_type = self.checkNewArray(instruction)
				estimated_registers.setType(id, new_type)
			else:
				if instr.A < 0 or instr.A >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B < 0 or instr.B >= len(registers):
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		elif name in {"aput-object", "aput", "aput-boolean", "aput-char"}:
			self.checkAput(instruction)

		elif name in {"aget-object", "aget", "aget-boolean", "aget-char"}:
			if 0 <= instr.AA < len(registers) and 0 <= instr.BB < len(registers) and 0 <= instr.CC < len(registers):
				id, new_type = self.checkAget(instruction)
				if id is not None and new_type is not None:
					estimated_registers.setType(id, new_type)
			else:
				if instr.AA >= len(registers) or instr.AA < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.AA))
				if instr.BB >= len(registers) or instr.BB < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.BB))
				if instr.CC >= len(registers) or instr.CC < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.CC))

		elif name == "array-length":
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				if registers.getType(instr.B) is not None and registers.getType(instr.B).startswith("["):
					estimated_registers.setType(instr.A, "int")
				else:
					instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieu de \"[*\".")
			else:
				if instr.A >= len(registers) or instr.A < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B >= len(registers) or instr.B < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		# sstaticop instructions
		elif name in {"sput-object", "sput", "sput-boolean", "sput-char"}:
			self.checkSput(instruction)

		elif name in {"sget-object", "sget", "sget-boolean", "sget-char"}:
			if 0 <= instr.AA < len(registers):
				id, new_type = self.checkSget(instruction)
				estimated_registers.setType(id, new_type)
			else:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))

		# opérations unaires
		elif name in {"int-to-float", "float-to-int", "int-to-char", "neg-int", "neg-float"}:
			if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
				id, new_type = self.checkUnop(instruction)
				if id is not None and new_type is not None:
					estimated_registers.setType(id, new_type)
			else:
				if instr.A >= len(registers) or instr.A < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.A))
				if instr.B >= len(registers) or instr.B < 0:
					instruction.addError(RegisterIndexError.__name__, str(instr.B))

		# ne réalise pas de modification sur les registres
		elif name in {"goto", "return-void", "nop", "monitor-enter", "monitor-exit", 'throw', "fill-array-data-payload"}:
			pass

		# si l'instruction arrive ici, c'est qu'elle n'est pas géré par ce programme
		else:
			instruction.addError(RegisterTypeError.__name__, f"L'instruction \"{name}\" non prise en charge")

		# lever une exception si une erreur a été détecté sur l'instruction en cours d'évaluation
		if instruction.is_valid is bool and not instruction.is_valid:
			if instruction.errors[RegisterIndexError.__name__]:
				raise RegisterIndexError(instruction.errors[RegisterIndexError.__name__], len(estimated_registers))
			if instruction.errors[RegisterTypeError.__name__]:
				raise RegisterTypeError(instruction.errors[RegisterTypeError.__name__])
		# retourner le nouvel état des registres
		return estimated_registers


	def _checkTypeOfInstr(self, _type: str, instr_type: str) -> bool:
		u"""
		Vérification le type d'une instruction

		:param _type: Le type du registre ou field
		:type _type: str
		:param instr_type: Le type de l'instruction
		:type instr_type: str

		:return: vérification des 2 types
		:rtype: bool
		"""
		is_ok: bool = True
		if _type in dvm.TYPE_DESCRIPTOR:
			_type = dvm.TYPE_DESCRIPTOR.get(_type)
		if (instr_type == "object" and _type in dvm.TYPE_DESCRIPTOR and _type != "I") or \
			(instr_type == "boolean" and _type not in {"boolean", "int"}) or \
			(instr_type == "char" and _type not in {"char", "int"}) or \
			(instr_type == "" and _type not in {"int", "float"}) or \
			(instr_type == "short" and _type != "short"):
			is_ok = False
		return is_ok

	def _errorMessageFromTypeOfInstr(self, _type: str, instr_type: str, something: str) -> str:
		u"""
        Le message d'erreur pour le type d'une instruction

        :param _type: Le type du registre ou field
        :type _type: str
        :param instr_type: Le type de l'instruction
        :type instr_type: str
        :param something: L'object concerné
        :type something: int

        :return: Les messages d'erreur
        """
		message: str = f"{something} n'est pas de type"
		end_of_message: str = ""
		if _type in dvm.TYPE_DESCRIPTOR:
			_type = dvm.TYPE_DESCRIPTOR.get(_type)
		if instr_type == "object" and _type in dvm.TYPE_DESCRIPTOR and _type != "int":
			end_of_message = "object"
		elif instr_type == "boolean" and _type not in {"boolean", "int"}:
			end_of_message = "boolean"
		elif instr_type == "char" and _type not in {"char", "int"}:
			end_of_message = "char"
		elif instr_type == "" and _type not in {"int", "float"}:
			end_of_message = "int ou float"
		elif instr_type == "short" and _type != "short":
			end_of_message = "short"
		if len(end_of_message) == 0:
			message = ""
		else:
			message = f"{message} \"{end_of_message}\""
		return message


	def checkIget(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `iget`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		name: str = instruction.name
		instr_type: str = ""
		if "-" in name:
			instr_type = name[name.find("-") + 1:]
		field = instr.cm.get_field(instr.CCCC)
		_type: str = field[1]
		if _type in dvm.TYPE_DESCRIPTOR:
			_type = dvm.TYPE_DESCRIPTOR.get(_type)
		if not self._type_manager.checkClasseType(field[0], registers.getType(instr.B), registers.getValue(instr.B)):
			instruction.addError(RegisterTypeError.__name__, f"La classe d'appel est incorrect: \"{field[0]}\" au lieu de \"{registers.getType(instr.B)}\"")
		instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(_type, instr_type, "Le champs"))
		return instr.A, _type


	def checkIput(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `iput`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
			name: str = instruction.name
			instr_type: str = ""
			if "-" in name:
				instr_type = name[name.find("-") + 1:]
			field = instr.cm.get_field(instr.CCCC)
			_type: str = field[1]
			if _type in dvm.TYPE_DESCRIPTOR:
				_type = dvm.TYPE_DESCRIPTOR.get(_type)
			if registers.getType(instr.A) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.A} est vide.")
			else:
				instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(registers.getType(instr.A), instr_type, f"Le registre v{instr.A}"))
			if not self._type_manager.checkClasseType(field[0], registers.getType(instr.B), registers.getValue(instr.B)):
				instruction.addError(RegisterTypeError.__name__, f"la classe d'appel est incorrect: \"{field[0]}\" au lieu \"{registers.getType(instr.B)}\"")
			instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(_type, instr_type, "Le champs"))
			instruction.addErrors(RegisterTypeError.__name__, self._type_manager.checkType(instr.A, _type, registers.getType(instr.A), registers.getValue(instr.A)))
		else:
			if instr.A >= len(registers) or instr.A < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.A))
			if instr.B >= len(registers) or instr.B < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.B))


	def checkAget(self, instruction: Instruction) -> Tuple[int, Optional[str]]:
		u"""
		Vérification de l'analyse des types pour les instructions `aget`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		name: str = instruction.name
		instr_type: str = ""
		if "-" in name:
			instr_type = name[name.find("-") + 1:]
		BB_type: Optional[str] = registers.getType(instr.BB)
		tab_type = None
		if BB_type is not None and BB_type.startswith("[") and self._checkTypeOfInstr(BB_type[1:], instr_type) and registers.getType(instr.CC) == "int":
			tab_type = BB_type[1:]
			if tab_type in dvm.TYPE_DESCRIPTOR:
				tab_type = dvm.TYPE_DESCRIPTOR.get(tab_type)
			if BB_type is not None and not (BB_type.startswith("[") and self._checkTypeOfInstr(BB_type[1:], instr_type)):
				if not BB_type.startswith("["):
					instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} n'est pas un tableau.")
				else:
					instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(tab_type, instr_type, f"Le registre v{instr.BB}"))
		if BB_type is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} est vide.")
		if registers.getType(instr.CC) != "int":
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.CC} est de type \"{registers.getType(instr.CC)}\" au lieu de \"int\".")
		return instr.AA, tab_type


	def checkAput(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `aput`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.AA < len(registers) and 0 <= instr.BB < len(registers) and 0 <= instr.CC < len(registers):
			name: str = instruction.name
			instr_type: str = ""
			if "-" in name:
				instr_type = name[name.find("-") + 1:]
			BB_type: Optional[str] = registers.getType(instr.BB)
			tab_type = None
			if BB_type is not None and BB_type.startswith("["):
				tab_type = BB_type[1:]
				if tab_type in dvm.TYPE_DESCRIPTOR:
					tab_type = dvm.TYPE_DESCRIPTOR.get(tab_type)
			if registers.getType(instr.AA) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est vide.")
			else:
				instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(registers.getType(instr.AA), instr_type, f"Le registre v{instr.AA}"))
			if registers.getType(instr.BB) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} est vide.")
			if registers.getType(instr.CC) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.CC} est vide.")
			if not (BB_type is not None and BB_type.startswith("[") and self._checkTypeOfInstr(BB_type[1:], instr_type)):
				if BB_type is not None and not BB_type.startswith("["):
					instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} n'est pas un tableau.")
				else:
					instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(tab_type, instr_type, f"Le tableau n'est pas du bon type, le registre v{instr.BB}"))
			if registers.getType(instr.CC) != "int":
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.CC} est de type \"{registers.getType(instr.CC)}\" au lieu de \"int\".")
			instruction.addErrors(RegisterTypeError.__name__, self._type_manager.checkType(instr.AA, tab_type, registers.getType(instr.AA), registers.getValue(instr.AA)))
		else:
			if instr.AA >= len(registers) or instr.AA < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.AA))
			if instr.BB >= len(registers) or instr.BB < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.BB))
			if instr.CC >= len(registers) or instr.CC < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.CC))


	def checkSget(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `sget`

		:param instruction: l'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		name: str = instruction.name
		instr_type: str = ""
		if "-" in name:
			instr_type = name[name.find("-") + 1:]
		field = instr.cm.get_field(instr.BBBB)
		_type: str = field[1]
		if _type in dvm.TYPE_DESCRIPTOR:
			_type = dvm.TYPE_DESCRIPTOR.get(_type)
		instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(_type, instr_type, "Le champs"))
		return instr.AA, _type


	def checkSput(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `sput`

		:param instruction: l'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.AA < len(registers):
			name: str = instruction.name
			instr_type: str = ""
			if "-" in name:
				instr_type = name[name.find("-") + 1:]
			field = instr.cm.get_field(instr.BBBB)
			_type: str = field[1]
			if _type in dvm.TYPE_DESCRIPTOR:
				_type = dvm.TYPE_DESCRIPTOR.get(_type)
			if registers.getType(instr.AA) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est vide.")
			else:
				instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(registers.getType(instr.AA), instr_type, f"Le registre v{instr.AA}"))
			instruction.addError(RegisterTypeError.__name__, self._errorMessageFromTypeOfInstr(_type, instr_type, "Le champs"))
			instruction.addErrors(RegisterTypeError.__name__, self._type_manager.checkType(instr.AA, _type, registers.getType(instr.AA), registers.getValue(instr.AA)))
		else:
			instruction.addError(RegisterIndexError.__name__, str(instr.AA))


	def checkConstClass(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour l'instruction `const-class`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""

		instr: Type[dvm.Instruction] = instruction.instr
		_type: str = instr.cm.get_type(instr.BBBB)
		if _type in dvm.TYPE_DESCRIPTOR:
			_type = dvm.TYPE_DESCRIPTOR.get(_type)
			_type: str = f"Ljava/lang/{_type.capitalize()};"
			if _type == "int":
				_type = "Ljava/lang/Integer;"
			elif _type == "char":
				_type = "Ljava/lang/Character;"
		return instr.AA, _type


	def checkIfTest(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `if-test`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
			if registers.getType(instr.B) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est vide.")
			if registers.getType(instr.A) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.A} est vide.")
			if registers.getType(instr.B) != registers.getType(instr.A):  # int < double
				instruction.addError(RegisterTypeError.__name__, f"Les registres v{instr.B} et v{instr.A} n'ont pas le même type. Ils ont respectivement les types \"{registers.getType(instr.B)}\" et \"{registers.getType(instr.A)}\".")
		else:
			if instr.A >= len(registers) or instr.A < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.A))
			if instr.B >= len(registers) or instr.B < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.B))


	def checkIfTestz(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `if-testz`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.AA < len(registers):
			_type: str = self._type_manager.getType(registers.getState(instr.AA), "boolean")
			if _type not in {"boolean", "int"} and _type in dvm.TYPE_DESCRIPTOR:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} n'est pas un boolean mais un \"{_type}\".")
		else:
			instruction.addError(RegisterIndexError.__name__, str(instr.AA))


	def checkBinop(self, instruction: Instruction) -> Tuple[Optional[int], Optional[str]]:
		u"""
		Vérification de l'analyse des types pour les instructions `binop`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[Optional[int], Optional[str]]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		name: str = instruction.name
		_type = name[name.find("-") + 1:]
		result: Tuple[Optional[int], Optional[str]] = (None, None)
		if registers.getType(instr.BB) == _type and registers.getType(instr.BB) == registers.getType(instr.CC):
			result = (instr.AA, _type)
		# messages d'erreur
		if registers.getType(instr.CC) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est vide.")
		if registers.getType(instr.BB) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} est vide.")
		if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.BB),registers.getValue(instr.BB)):
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} est de type \"{registers.getType(instr.BB)}\" au lieux de \"{_type}\".")
		if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.CC),registers.getValue(instr.CC)):
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.CC} est de type \"{registers.getType(instr.CC)}\" au lieux de \"{_type}\".")
		if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.BB),registers.getValue(instr.BB)) or not self._type_manager.checkPrimitifType(_type, registers.getType(instr.CC),registers.getValue(instr.CC)):
			instruction.addError(RegisterTypeError.__name__, f"Les registres v{instr.BB} et v{instr.CC} n'ont pas le même type. Ils ont respectivement les types \"{registers.getType(instr.BB)}\" et \"{registers.getType(instr.CC)}\".")
		return result



	def checkBinopLit8(self, instruction: Instruction) -> Tuple[Optional[int], Optional[str]]:
		u"""
		Vérification de l'analyse des types pour les instructions `binop/lit8`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[Optional[int], Optional[str]]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		result: Tuple[Optional[int], Optional[str]] = (None, None)
		if type(instr.CC) == int and registers.getType(instr.BB) == "int":
			result = (instr.AA, "int")
		elif type(instr.CC) == int and registers.getType(instr.BB) == "boolean":
			result = (instr.AA, "boolean")
		# messages d'erreur
		if registers.getType(instr.BB) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} est vide.")
		if not type(instr.CC) == int:
			instruction.addError(RegisterTypeError.__name__, f"La constante \"{instr.CC}\" n'est de type \"int\"")
		if registers.getType(instr.BB) not in {"int", "boolean"}:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BB} devrai être de type \"{registers.getType(instr.BB)}\" au lieux de \"int\".")
		return result



	def checkBinop2AddrMessage(self, instruction: Instruction) -> None:
		u"""
		Les messages d'erreur de l'analyse des types  pour les instructions `binop/2addr`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.A < len(registers) and 0 <= instr.B < len(registers):
			name: str = instruction.name
			_type: str = name[name.find("-") + 1: name.find("/")]
			if registers.getType(instr.A) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.A} est vide.")
			if registers.getType(instr.B) is None:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est vide.")
			if not (registers.getType(instr.A) == _type and registers.getType(instr.B) == registers.getType(instr.A)):
				if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.B),registers.getValue(instr.B)):
					instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieux de \"{_type}\".")
				if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.A), registers.getValue(instr.A)):
					instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.A} est de type \"{registers.getType(instr.A)}\" au lieux de \"{_type}\".")
				if not self._type_manager.checkPrimitifType(_type, registers.getType(instr.B),registers.getValue(instr.B)) or not self._type_manager.checkPrimitifType(_type, registers.getType(instr.A), registers.getValue(instr.A)):
					instruction.addError(RegisterTypeError.__name__, f"Les registres v{instr.B} et v{instr.A} n'ont pas le même type. Ils ont respectivement les types \"{registers.getType(instr.B)}\" et \"{registers.getType(instr.A)}\".")
		else:
			if instr.A >= len(registers) or instr.A < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.A))
			if instr.B >= len(registers) or instr.B < 0:
				instruction.addError(RegisterIndexError.__name__, str(instr.B))


	def checkReturn(self, instruction: Instruction, type_return: str) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `return`

		:param instruction: L'instruction
		:type instruction: Instruction
		:param type_return: Le type de retour
		:type type_return: str
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if not self._type_manager.checkPrimitifType(type_return, registers.getType(instr.AA), registers.getValue(instr.AA)):
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est de type \"{registers.getType(instr.AA)}\" au lieux de \"{type_return}\".")


	def checkReturnObject(self, instruction: Instruction, type_return: str) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `return-object`

		:param instruction: L'instruction
		:type instruction: Instruction
		:param type_return: Le type de retour
		:type type_return: str
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if registers.getType(instr.AA) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est vide.")
		if not self._type_manager.checkClasseType(type_return, registers.getType(instr.AA), registers.getValue(instr.AA)):
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} est de type \"{registers.getType(instr.AA)}\" au lieux de \"{type_return}\".")


	def checkInvokeKind(self, instruction: Instruction) -> str:
		u"""
		Vérification de l'analyse des types pour les instructions `invoke-kind`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le type de retour
		:rtype: str
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		name: str = instruction.name
		method = instr.cm.get_method(instr.BBBB)
		param: List[str] = list(filter(None, method[2][0][1:-1].split(" ")))
		is_erreur:bool= False
		if name != 'invoke-static':
			list_indice: List[int] = [instr.D, instr.E, instr.F, instr.G]
			if not self._type_manager.checkClasseType(method[0], registers.getType(instr.C), registers.getValue(instr.C)):
				instruction.addError(RegisterTypeError.__name__, f"La classe d'appel est incorrect: \"{method[0]}\" au lieu de \"{registers.getType(instr.C)}\"")
			if len(param) + 1 != len(instr.get_operands()) - 1:
				is_erreur = True
		else:
			list_indice: List[int] = [instr.C, instr.D, instr.E, instr.F, instr.G]
			if len(param) != len(instr.get_operands()) - 1:
				is_erreur = True
		if not is_erreur:
			for i in range(len(param)):  # Vérifier le type des params
				instruction.addErrors(RegisterTypeError.__name__, self._type_manager.checkType(i+1, param[i], registers.getType(list_indice[i]), registers.getValue(list_indice[i])))
		type_return: str = method[2][1]
		if type_return in dvm.TYPE_DESCRIPTOR:
			type_return = dvm.TYPE_DESCRIPTOR[type_return]
		return type_return


	def checkMoveResult(self, instruction: Instruction, pile: Tuple[bool, Optional[str]]) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `move-result`

		:param instruction: L'instruction
		:type instruction: Instruction
		:param pile: La pile
		:type pile: Tuple[bool, Optional[str]]

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		if not pile[0]:
			instruction.addError(RegisterTypeError.__name__, f"Il n'y a pas de valeur sur la pile")
		return instruction.instr.AA, pile[1]


	def checkMoveObject(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `move-object`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if registers.getType(instr.B) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} n'a pas de valeur")
		return instr.A, registers.getType(instr.B)


	def checkMoveObjectFrom16t(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `move-object/from16t`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if registers.getType(instr.BBBB) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BBBB} n'a pas de valeur")
		return instr.AA, registers.getType(instr.BBBB)


	def checkMove16(self, instruction: Instruction) -> Tuple[int, str]:
		u"""
		Vérification de l'analyse des types pour les instructions `move/16`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[int, str]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if registers.getType(instr.BBBB) is None:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.BBBB} n'a pas de valeur")
		return instr.AAAA, registers.getType(instr.BBBB)


	def checkFillArrayData(self, instruction: Instruction) -> None:
		u"""
		Vérification de l'analyse des types pour les instructions `fill-array-data`

		:param instruction: L'instruction
		:type instruction: Instruction
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		if 0 <= instr.AA < len(registers):
			_type = registers.getType(instr.AA)
			if '[' not in _type:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} n'a pas un tableau mais \"{_type}\"")
			elif '[' in _type and _type[1] not in dvm.TYPE_DESCRIPTOR:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.AA} n'a pas un tableau de type primitif mais \"{_type}\"")
		else:
			instruction.addError(RegisterIndexError.__name__, str(instr.AA))


	def checkInstanceOf(self, instruction: Instruction) -> Tuple[Optional[int], Optional[str]]:
		u"""
			Vérification de l'analyse des types pour les instructions `instance-of`

			:param instruction: L'instruction
			:type instruction: Instruction

			:return: Le numéro du registre et son nouveau type
			:rtype: Tuple[Optional[int], Optional[str]]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		result: Tuple[Optional[int], Optional[str]] = (None, None)
		if self._type_manager.checkClasseType(instr.cm.get_type(instr.CCCC), registers.getType(instr.B), registers.getValue(instr.B)):
			result = (instr.A, registers.getType(instr.B))
		# messages d'erreur
		if not self._type_manager.checkClasseType(instr.cm.get_type(instr.CCCC), registers.getType(instr.B), registers.getValue(instr.B)):
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieux de \"{instr.cm.get_type(instr.CCCC)}\".")
		return result


	def checkNewArray(self, instruction: Instruction) -> Tuple[Optional[int], Optional[str]]:
		u"""
		Vérification de l'analyse des types pour l'instruction `new-array`

		:param instruction: L'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[Optional[int], Optional[str]]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		result: Tuple[Optional[int], Optional[str]] = (None, None)
		array_type: str = instr.cm.get_type(instr.CCCC)
		if registers.getType(instr.B) == "int":
			if array_type is not None and array_type.startswith("["):
				result = (instr.A, array_type)
			else:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.CCCC} est de type \"{array_type}\" au lieu d'un type \"array\".")
		else:
			instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieu de \"int\".")
		return result


	def checkUnop(self, instruction: Instruction) -> Tuple[Optional[int], Optional[str]]:
		u"""
		Vérification de l'analyse des types pour les instructions `unop`

		:param instruction: l'instruction
		:type instruction: Instruction

		:return: Le numéro du registre et son nouveau type
		:rtype: Tuple[Optional[int], Optional[str]]
		"""
		instr: Type[dvm.Instruction] = instruction.instr
		registers: Registers = instruction.registers
		name: str = instruction.name
		result: Tuple[Optional[int], Optional[str]] = (None, None)
		if "to" in name:
			type_1 = name[:name.find("-to-")]
			type_2 = name[name.find("-to-") + 4:]
			if registers.getType(instr.B) == type_1:
				result = (instr.A, type_2)
			else:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieu de {type_1}.")
		else:
			type_1 = name[name.find("-") + 1:]
			if registers.getType(instr.B) == type_1:
				result = (instr.A, type_1)
			else:
				instruction.addError(RegisterTypeError.__name__, f"Le registre v{instr.B} est de type \"{registers.getType(instr.B)}\" au lieu de \"int\".")
		return result
