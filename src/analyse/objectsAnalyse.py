# IMPORTS #####################################################################
from typing import List, Type

from androguard.core.bytecodes import dvm

from src.core.instruction import Instruction
from src.core.method import Method
from src.core.objects import Objects
from src.util.exception import ObjectInstantiationError
from src.util.logging import log


# CLASS #######################################################################
class ObjectsAnalyser:

	def analyse(self, method: Method) -> None:
		u"""
		Analyse des objets

		Effectue l'analyse de l'instanciation des objets utilisés par la méthode passée en paramètre.

		:param method: Une méthode à analyser
		:type method: EncodedMethod
		"""
		# Récupérer les successeurs et la liste des instructions de la méthode courante
		positions_to_evaluate: List[int] = list(method.instructions)
		while len(positions_to_evaluate) != 0:
			current_position: int = positions_to_evaluate.pop(0)  # récupérer le décalage en tête de liste
			current_instruction: Instruction = method.instructions.get(current_position)
			current_instruction.is_valid = True
			# mettre par défaut les valeurs de l'objet lorsqu'il n'y a pas de prédécesseurs
			if len(current_instruction.predecessors) == 0 and current_position != 0:
				instruction: Instruction = method.instructions.get(current_position - 1)
				current_instruction.objects = instruction.objects
			current_instruction.errors[ObjectInstantiationError.__name__] = []
			try:
				current_instruction.objects = self.evaluate(current_instruction)
			except ObjectInstantiationError as instantiation_error:
				current_instruction.addError(ObjectInstantiationError.__name__, instantiation_error.message)
				current_instruction.is_valid = False
			finally:
				for successor_instruction in current_instruction.successors:
					successor_instruction.objects = current_instruction.objects
					if positions_to_evaluate.__contains__(successor_instruction.position):
						positions_to_evaluate.remove(successor_instruction.position)
						positions_to_evaluate.insert(0, successor_instruction.position)
				# mettre à jour l'état de validité de la méthode en fonction des instructions
				method.is_valid = current_instruction.is_valid
				if not current_instruction.is_valid:
					log.error("\n".join(current_instruction.errors[ObjectInstantiationError.__name__]))


	def evaluate(self, instruction: Instruction) -> Objects:
		"""
		Evaluation de l'instantiation des objets

		:param instruction: instruction à évaluer
		:return:
		"""
		objects = instruction.objects.copy()
		name = instruction.name
		instr: Type[dvm.Instruction] = instruction.instr
		# instanciation des objets de registre A
		if name in {"new-array"}:
			objects.setInst(instr.A, True)
		# instanciation des objets de registre AA
		elif name in {"new-instance", "sget-object", "sput-object", "const-string", "move-result",
		              "move-result-object"}:
			objects.setInst(instr.AA, True)
		# instanciation des objets et vérification d'instanciation

		elif name in {"move-object", "move-object/from16"}:
			if (name == "move-object" and objects.get(instr.B)) or (name == "move-object/from16" and objects.get(instr.BBBB)):
				if name == "move-object":
					objects.setInst(instr.A, True)
				elif name == "move-object/from16":
					objects.setInst(instr.AA, True)

			else:
				raise ObjectInstantiationError([
					"Le registre v{} n'est pas instancialisé à l'instruction {} en position {}.".format(
						instr.B, instruction.name, instruction.position)])
		# vérification d'instanciation avec registre B
		elif name in {"iget", "iget-object", "iget-boolean", "iget-char", "iput", "iput-object", "iput-boolean",
		              "iput-char"}:
			if not objects.get(instr.B):
				raise ObjectInstantiationError([
					"Le registre v{} n'est pas instancialisé à l'instruction {} en position {}.".format(
						instr.B, instruction.name, instruction.position)])
			# cas spécifique avec les objets
			elif name in {"iget-object"}:
				objects.setInst(instr.A, True)
		# vérification d'instanciation avec registre BB
		elif name in {"aget", "aget-object", "aget-boolean", "aget-char", "aput", "aput-object", "aput-boolean",
		              "aput-char"}:
			if not objects.get(instr.BB):
				raise ObjectInstantiationError([
					"Le registre v{} n'est pas instancialisé à l'instruction {} en position {}.".format(
						instr.BB, instruction.name, instruction.position)])
			elif name in {"aget-object"}:
				objects.setInst(instr.AA, True)
		# instructions avec registre C
		elif name in {"invoke-virtual", "invoke-direct", "invoke-super", "invoke-interface"}:
			if not objects.get(instr.C):
				raise ObjectInstantiationError([
					"Le registre v{} n'est pas instancialisé à l'instruction {} en position {}.".format(
						instr.C, instruction.name, instruction.position)])
		# instruction const null
		elif name == "const/4" and instr.B == 0:
			objects.setInst(instr.A, False)
		# instruction exception
		elif name == "move-exception":
			objects.setInst(instr.AA, True)
		return objects
