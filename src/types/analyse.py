# IMPORTS #####################################################################
from typing import Dict, List, Final


# CLASS #######################################################################
class AnalyseTypes:

	# déclaration des types d'analyse possible
	_types: Dict[str, str] = {
		"register": "registre",
		"object": "objet",
		"permission": "permission"
	}

	# La liste de toutes les analyses
	ALL: Final[List[str]] = list(_types.values())

	# Chaque analyse
	REGISTER: Final[str] = _types["register"]
	OBJECT: Final[str] = _types["object"]
	PERMISSION: Final[str] = _types["permission"]