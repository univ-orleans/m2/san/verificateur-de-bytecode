# IMPORTS #####################################################################
from __future__ import annotations
from typing import Dict, Optional, Tuple

from src.util.exception import RegisterNumberError, TypeComparaisonError


# CLASS #######################################################################
class Registers(Dict[int, Tuple[Optional[str], Optional[int]]]):

	def __init__(self, registers_number: int) -> None:
		u"""
		Constructeur

		:param registers_number: Le nombre de registres
		:type registers_number: int
		"""
		super().__init__()
		# initialiser le tableau des registres avec des `None` de la forme état = (type, valeur)
		for i in range(registers_number):
			self.__setitem__(i, (None, None))


	def getValue(self, id: int) -> Optional[int]:
		u"""
		La valeur du registre

		:param id: Le numéro du registre
		:type id: int

		:return: La valeur stockée dans le registre
		:rtype: Optional[int]
		"""
		return self.get(id)[1]


	def getType(self, id: int) -> Optional[str]:
		u"""
		Le type du registre

		:param id: Le numéro du registre
		:type id: int

		:return: Le type de la valeur du registre
		:rtype: Optional[str]
		"""
		return self.get(id)[0]


	def getState(self, id: int) -> Tuple[Optional[str], Optional[int]]:
		u"""
		L'état du registre

		:param id: Le numéro du registre
		:type id: int

		:return: L'état du registre (type, valeur)
		:rtype: Tuple[Optional[str], Optional[int]]
		"""
		return self.get(id)


	def setValue(self, id: int, new_value: int) -> None:
		u"""
		Affecter une nouvelle valeur à un registre

		:param id: Le numéro du registre
		:type id: int
		:param new_value: La nouvelle valeur du registre
		:type new_value: int
		"""
		self.__setitem__(id, (self.getType(id), new_value))


	def setType(self, id: int, new_type: Optional[str]) -> None:
		u"""
		Affecter un nouveau type à un registre

		:param id: Le numéro du registre
		:type id: int
		:param new_type: Le nouveau type du registre
		:type new_type: Optional[str]
		"""
		self.__setitem__(id, (new_type, self.getValue(id)))


	def setState(self, id: int, new_state: Tuple[Optional[str], Optional[int]]) -> None:
		u"""
		Affecter un nouvel état à un registre

		:param id: Le numéro du registre
		:type id: int
		:param new_state: Le nouvel état du registre
		:type new_state: Tuple[Optional[str], Optional[int]]
		"""
		self.__setitem__(id, new_state)


	def copy(self) -> Registers:
		u"""
		Copie le tableaux de registre

		:return: Une copie des registres
		:rtype Registers
		"""
		new_registers: Registers = Registers(len(self))
		new_registers.update(self)
		return new_registers


	def max(self, successor: Registers) -> Registers:
		u"""
		Maximum entre les registres

		Calcule l'état maximal des registres entre les registres courants et les registres à l'étape suivante
		(les prochains registres).

		:param successor: Les registres à la prochaine étape
		:type successor: Registers

		:return: Le maximum entre les registres courants et ceux de la prochaine étape
		:rtype: Registers

		:raise RegisterNumberError: Erreur de typage, les deux tableaux de registres n'ont pas le même nombre de registres
		"""
		# créer le registre de retour de la même taille que le registre courant
		result: Registers = Registers(len(self))
		# vérifier que les deux registres qui sont comparés ont la même taille
		if len(self) == len(successor):
			for id in self.keys():
				try:
					result.setState(id, self._maxType(self.getState(id), successor.getState(id)))
				except TypeComparaisonError as type_comp_error:
					# TODO : À VÉRIFIER
					result.setType(id, self.getType(id))
		# propager une erreur si les tableaux de registres n'ont pas la même taille
		else:
			raise RegisterNumberError(len(self), len(successor))
		# retourner le résultat si aucune erreur n'est survenue
		return result


	@staticmethod
	def _maxType(state_1: Tuple[Optional[str], Optional[int]], state_2: Tuple[Optional[str], Optional[int]]) -> Tuple[Optional[str], Optional[int]]:
		u"""
		Maximum entre les types

		Détermine le type le plus grand entre `type_1` et `type_2` appartenant respectivement aux `state_1` et `state_2`.

		:param state_1: Un status de registre
		:type state_1: Tuple[Optional[str], Optional[int]]
		:param state_2: Un autre status de registre
		:type state_2: Tuple[Optional[str], Optional[int]]

		:return: Le status avec le type le plus grand
		:rtype: Tuple[Optional[str], Optional[int]]

		:raise TypeComparaisonError: Les deux types ont des valeurs différentes autres que `None` (ex: `int` et `string`)
		"""
		# initialiser une valeur par défaut
		result: Tuple[Optional[str], Optional[int]] = (None, None)
		# si le `type_1` est inférieur au `type_2`
		if (state_1[0] is None) and (state_2[0] is not None):
			result = state_2
		# si le `type_1` est supérieur au `type_2`
		elif ((state_1[0] is not None) and (state_2[0] is None)) or (state_1[0] == state_2[0]):
			result = state_1
		# si le `type_1` est différent du `type_2` mais ils ne sont pas `None`
		else:
			raise TypeComparaisonError(state_1[0], state_2[0])
		return result