# IMPORTS #####################################################################
from __future__ import annotations
from typing import Type, List, Optional, Dict
from androguard.core.bytecodes import dvm

from src.core.objects import Objects
from src.core.registers import Registers


# CLASS #######################################################################
class Instruction:

	def __init__(self, position: int, offset: int, instruction: Type[dvm.Instruction], registers_number: int) -> None:
		u"""
		Constructeur

		:param position: La position de cette instruction parmi les instructions de la méthode
		:type position: int
		:param offset: Le décalage depuis le début de la méthode pour accéder à cette instruction
		:type offset: int
		:param instruction: L'instruction Androguard
		:type instruction: Type[dvm.Instruction]
		:param registers_number: Le nombre de registres
		:type registers_number: int
		"""
		# informations sur l'instruction
		self._position: int = position
		self._offset: int = offset
		self._instruction: Type[dvm.Instruction] = instruction
		self._registers: Registers = Registers(registers_number)
		self._objects: Objects = Objects(registers_number)
		self._is_valid: Optional[bool] = None
		self._errors: Dict[str, List[str]] = {}
		# informations sur les successeurs de cette instruction
		self._successors_position: List[int] = []
		self._successors: List[Instruction] = []
		# informations sur les prédécesseurs de cette instruction
		self._predecessors_position: List[int] = []
		self._predecessors: List[Instruction] = []


	@property
	def position(self) -> int:
		return self._position

	@property
	def offset(self) -> int:
		return self._offset

	@property
	def instr(self) -> Type[dvm.Instruction]:
		return self._instruction

	@property
	def registers(self) -> Registers:
		return self._registers

	@property
	def objects(self) -> Objects:
		return self._objects

	@property
	def is_valid(self) -> Optional[bool]:
		return self._is_valid

	@property
	def errors(self) -> Dict[str, List[str]]:
		return self._errors

	@property
	def successors_position(self) -> List[int]:
		return self._successors_position

	@property
	def successors(self) -> List[Instruction]:
		return self._successors

	@property
	def predecessors_position(self) -> List[int]:
		return self._predecessors_position

	@property
	def predecessors(self) -> List[Instruction]:
		return self._predecessors

	@property
	def name(self) -> str:
		return self._instruction.get_name()

	@is_valid.setter
	def is_valid(self, new_is_valid: bool) -> None:
		if isinstance(new_is_valid, bool):
			self._is_valid = new_is_valid
		else:
			raise TypeError("La propriété \"is_valid\" doit être de type \"bool\".")

	@registers.setter
	def registers(self, new_registers: Registers) -> None:
		if isinstance(new_registers, Registers):
			self._registers = new_registers
		else:
			raise TypeError("La propriété \"registers\" doit être de type \"Registers\".")

	@objects.setter
	def objects(self, new_objects: Objects) -> None:
		if isinstance(new_objects, Objects):
			self._objects = new_objects
		else:
			raise TypeError("La propriété \"objects\" doit être de type \"Objects\".")



	def addError(self, type_error: str, message: str) -> None:
		u"""
		Ajouter un message d'erreur à l'instruction

		:param type_error: Le type d'erreur
		:type type_error: str
		:param message: Le message d'erreur
		:type message: str
		"""
		if len(message):
			self._errors[type_error].append(message)
			self._is_valid = False


	def addErrors(self, type_error: str, messages: List[str]) -> None:
		u"""
		Ajouter plusieurs messages d'erreur à l'instruction

		:param type_error: Le type d'erreur
		:type type_error: str
		:param messages: Les messages d'erreur
		:type messages: List[str]
		"""
		messages = list(filter(None, messages))
		if len(messages):
			self._errors[type_error].extend(messages)
			self._is_valid = False


	def addSuccessor(self, instruction: Instruction) -> None:
		u"""
		Ajouter un successeur à cette instruction

		:param instruction: Le successeur
		:type instruction: Instruction
		"""
		self._successors.append(instruction)
		self._successors_position.append(instruction.position)


	def addPredecessor(self, instruction: Instruction) -> None:
		u"""
		Ajouter un prédécesseur à cette instruction

		:param instruction: Le prédécesseur
		:type instruction: Instruction
		"""
		self._predecessors.append(instruction)
		self._predecessors_position.append(instruction.position)
