# IMPORTS #####################################################################
from typing import Union, List, Optional

from src.analyse.objectsAnalyse import ObjectsAnalyser
from src.analyse.permissionsAnalyse import PermissionsAnalyser
from src.core.data import Data
from src.util.tools import classPathToClassName
from src.util.filter import Filter
from src.util.logging import log
from src.types.analyse import AnalyseTypes
from src.analyse.registersAnalyse import RegistersAnalyser
from src.util.reporter import Report


# CLASS #######################################################################
class Engine:

	def __init__(self, apk_path: str, class_names: Union[None, str, List[str]] = None, method_names: Union[None, str, List[str]] = None, filter_classes: bool = False, report_directory: str = "../../report") -> None:
		u"""
		Constructeur

		:param apk_path: Le chemin d'accès à l'archive à analyser
		:type apk_path: str
		:param class_names: Le nom des classes à analyser
		:type class_names: Union[None, str, List[str]]
		:param method_names: Le nom des méthodes à analyser
		:type method_names: Union[None, str, List[str]]
		:param filter_classes: Indique s'il faut filtrer les classes pour éliminer les classes générées par Android lors de la compilation
		:type filter_classes: bool
		:param report_directory: Le répertoire racine de génération des rapports
		:type report_directory: str
		"""
		self._report: Report = Report(report_directory)
		self._data: Data = Data(apk_path)
		# Récupérer la ou les classes spécifiées ou toutes les classes (si non spécifiées)
		if class_names is not None:
			class_names: List[str] = [class_names] if type(class_names) is str else class_names
			class_names = list(map(classPathToClassName, class_names))
		black_list: Optional[List[str]] = None
		if filter_classes:
			black_list = Filter.CLASSE_NAMES_TO_EXCLUDE
		self._data.loadClasses(class_names, black_list)
		# Récupérer la ou les méthodes spécifiées ou toutes les méthodes (si non spécifiées)
		if method_names is not None:
			method_names: List[str] = [method_names] if type(method_names) is str else method_names
		self._data.loadMethods(method_names)

		# analyseurs
		self._objects_analyser: ObjectsAnalyser = ObjectsAnalyser()
		self._registers_analyser: RegistersAnalyser = RegistersAnalyser(self._data.analysis, list(map(lambda x: x.class_def, filter(lambda x: not x.is_external, self._data.classes))))
		self._permissions_analyser: PermissionsAnalyser = PermissionsAnalyser(self._data)


	def analyse(self, analyse_types: Union[str, List[str]]) -> None:
		u"""
		Analyse

		:param analyse_types: Une liste de type d'analyse à exécuter
		:type analyse_types: Union[str, List[str]]
		"""
		analyse_types: List[str] = [analyse_types] if type(analyse_types) is str else analyse_types

		# si il n'y as pas de classe à analyser
		if len(self._data.classes) == 0:
			log.warning("Aucune classe à analyser.")
			exit()

		# si des classes sont en attente d'être analysées
		else:
			for analyse_type in analyse_types:
				if analyse_type not in AnalyseTypes.ALL:
					if analyse_types[-1] == analyse_type:
						log.error(f"Le type d'analyse \"{analyse_type}\" n'est pas pris en charge.")
						exit()
					else:
						log.warning(f"Le type d'analyse \"{analyse_type}\" n'est pas pris en charge.")
				else:
					self._data.analyse_types.append(analyse_type)
					log.info(f"Analyse: {analyse_type}", color=log.COLOR_YELLOW)
					log.debug(f"Début de l'analyse \"{analyse_type}\".")

					# analyse des registres (utilisation des classes externes uniquement)
					if analyse_type == AnalyseTypes.PERMISSION:
						# analyse statique
						self._permissions_analyser.analyseStaticPermissions()
						# analyse dynamique
						for _class in filter(lambda x: x.is_external, self._data.classes):
							_class.is_evaluate = True
							_class.analyse_types.append(analyse_type)
							found_permissions: bool = False
							for method in _class.methods:
								method.init(analyse_type)
								method.is_evaluate = True
								self._permissions_analyser.analyseRuntimePermissions(method)
								if len(method.permissions):
									found_permissions = True
							if found_permissions:
								log.info(_class.name, color=log.COLOR_BLUE)
								for method in _class.methods:
									if len(method.permissions):
										log.info(method.name, color=log.COLOR_PURPLE)
										for permission in method.permissions:
											log.info(f"Permission dynamique : (utilisée) {permission.name}")

					# autres analyses (utilisation des classes définies uniquement)
					else:
						# analyse dynamique des méthodes de classe
						for _class in filter(lambda x: not x.is_external, self._data.classes):
							_class.analyse_types.append(analyse_type)
							log.info(_class.name, color=log.COLOR_BLUE)
							for method in _class.methods:
								method.init(analyse_type)
								# vérifier que la méthode respecte les éléments du filtre d'instruction
								is_ok: bool = True
								i: int = 0
								while is_ok and i < len(method.instructions):
									is_ok = Filter.verify(method.instructions.get(i).name, Filter.METHOD_INSTRUCTIONS_TO_EXCLUDE)
									i += 1
								if is_ok:
									method.is_evaluate = True
									log.debug(f"Début de l'analyse de la méthode \"{method.name}\".")
									# faire l'analyse si la méthode passe le contrôle
									if analyse_type == AnalyseTypes.REGISTER:
										self._registers_analyser.analyse(method)
									elif analyse_type == AnalyseTypes.OBJECT:
										self._objects_analyser.analyse(method)
									log.info(f"{'✔' if method.is_valid_detail.get(analyse_type) else '✘'} {method.name}", color=log.COLOR_PURPLE)
									log.debug(f"Fin de l'analyse de la méthode \"{method.name}\".")
								else:
									log.warning(f"La méthode \"{method.name}\" ne peut pas être analysé car elle possède des instructions non prises en charge.")
							_class.is_evaluate = max([False, False] + list(map(lambda x: max([False, False] + list(x.is_evaluate.values())), _class.methods)))
					log.debug(f"Fin de l'analyse \"{analyse_type}\".")
			# génération des rapports
			self._report.generate(self._data)


if __name__ == '__main__':
	engine: Engine = Engine("../../apk/eva.apk")
	engine.analyse([AnalyseTypes.REGISTER, AnalyseTypes.OBJECT, AnalyseTypes.PERMISSION])