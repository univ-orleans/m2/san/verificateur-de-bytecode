# IMPORTS #####################################################################
from typing import Dict, List, Type, Optional, Tuple, Union
from re import sub
from androguard.core.bytecodes import dvm
from androguard.core.analysis.analysis import ExternalMethod

from src.core.instruction import Instruction
from src.core.permission import Permission
from src.types.analyse import AnalyseTypes
from src.util.tools import classPathToClassName


# CLASS #######################################################################

class Method:

	def __init__(self, method: Union[dvm.EncodedMethod, ExternalMethod]) -> None:
		u"""
		Constructeur

		:param method: La méthode Androguard
		:type method: Union[dvm.EncodedMethod, ExternalMethod]
		"""
		self._method: Union[dvm.EncodedMethod, ExternalMethod] = method
		self._analyse_types: List[str] = []
		self._instructions: Dict[int, Instruction] = dict()
		self._permissions: List[Permission] = []
		self._is_external: bool = isinstance(method, ExternalMethod)
		self._is_valid: Dict[str, Optional[bool]] = dict([(_type, None) for _type in AnalyseTypes.ALL])
		self._is_init: Dict[str, bool] = dict([(_type, False) for _type in AnalyseTypes.ALL])
		self._registers_number: int = 0 if self._is_external else method.code.get_registers_size()
		self._is_evaluate: Dict[str, bool] = dict([(_type, False) for _type in AnalyseTypes.ALL])
		self._pile: Tuple[bool, Optional[int]] = (False, None)


	@property
	def method(self) -> Union[dvm.EncodedMethod, ExternalMethod]:
		return self._method

	@property
	def name(self) -> str:
		return self._method.name

	@property
	def code_source(self) -> str:
		return "\n".join(filter(None, map(lambda x: sub(r"^\s{4}", "", x), self._method.get_source().split("\n"))))

	@property
	def signature(self) -> str:
		return self._method.proto

	@property
	def analyse_types(self) -> List[str]:
		return self._analyse_types

	@property
	def instructions(self) -> Dict[int, Instruction]:
		return self._instructions

	@property
	def permissions(self) -> List[Permission]:
		return self._permissions

	@property
	def is_external(self) -> bool:
		return self._is_external

	@property
	def is_valid(self) -> bool:
		return all(filter(lambda x: isinstance(x, bool), self._is_valid.values())).__eq__(True)

	@property
	def is_valid_detail(self) -> Dict[str, Optional[bool]]:
		return self._is_valid

	@property
	def is_evaluate(self) -> Dict[str, Optional[bool]]:
		return self._is_evaluate

	@property
	def registers_number(self) -> int:
		return self._registers_number

	@property
	def pile(self) -> Tuple[bool, Optional[int]]:
		return self._pile

	@is_valid.setter
	def is_valid(self, new_is_valid: bool) -> None:
		if isinstance(new_is_valid, bool):
			current_analysis: str = self._analyse_types[-1]
			self._is_valid.__setitem__(current_analysis, min(self._is_valid.get(current_analysis), new_is_valid))
		else:
			raise TypeError("La propriété \"is_valid\" doit être de type \"bool\".")

	@is_evaluate.setter
	def is_evaluate(self, new_is_evaluate: bool) -> None:
		if isinstance(new_is_evaluate, bool):
			current_analysis: str = self._analyse_types[-1]
			self._is_evaluate.__setitem__(current_analysis, new_is_evaluate)
		else:
			raise TypeError("La propriété \"is_evaluate\" doit être de type \"bool\".")

	@pile.setter
	def pile(self, new_pile: Tuple[bool, Optional[int]]) -> None:
		if isinstance(new_pile, tuple):
			self._pile = new_pile
		else:
			raise TypeError("La propriété \"pile\" doit être de type \"Tuple[bool, Optional[int]]\".")


	def init(self, analyse_type: str):
		u"""
		Prépare les structures de données pour chaque analyse qui sera utilisé

		:param analyse_type: Le type d'analyse à réaliser
		:type analyse_type: str
		"""
		if not self._is_init.get(analyse_type):
			# instancier la liste des instructions si une analyse des registres ou objets est demandé
			if analyse_type in {AnalyseTypes.REGISTER, AnalyseTypes.OBJECT}:
				if len(self._instructions) == 0:
					# récupérer toutes les instructions de la méthode
					offset: int = 0
					instructions: List[Type[dvm.Instruction]] = list(self._method.get_instructions())
					for position in range(len(instructions)):
						instruction: Instruction = Instruction(position, offset, instructions[position], self._registers_number)
						self._instructions.__setitem__(position, instruction)
						offset += instruction.instr.get_length()
					# définir les successeurs et prédécesseurs de chaque instruction
					for pos, instruction in self._instructions.items():
						successors_position: List[int] = list(filter(lambda x: x != -1, map(self._method.code.get_bc().off_to_pos, self._successorsOffset(instruction.offset, instruction.instr))))
						for successor_position in successors_position:
							if successor_position in self._instructions:
								instruction.addSuccessor(self._instructions.get(successor_position))
								self._instructions.get(successor_position).addPredecessor(instruction)
				# définir les registres de la première instruction
				nb_decalage: int = self.numberParameterUnlivable()
				method_info = self._method.get_information()
				for (param_num, param_type) in method_info.get("params", []):
					decalage = param_num - nb_decalage
					if decalage >= 0:
						if analyse_type == AnalyseTypes.REGISTER:
							self._instructions[0].registers.setType(decalage, classPathToClassName(param_type))
						elif analyse_type == AnalyseTypes.OBJECT:
							self._instructions[0].objects.setInst(decalage, True)
				# définir le type correspondant à la classe (si ce n'est pas une méthode statique)
				if "static" not in self._method.access_flags_string:
					static_decalage = method_info.get("registers", (0, 0))[1] - nb_decalage
					if static_decalage >= 0:
						if analyse_type == AnalyseTypes.REGISTER:
							self._instructions[0].registers.setType(static_decalage, self._method.get_class_name())
						elif analyse_type == AnalyseTypes.OBJECT:
							self._instructions[0].objects.setInst(static_decalage, True)

			self._is_valid.__setitem__(analyse_type, True)
			self._is_init.__setitem__(analyse_type, True)
			self._analyse_types.append(analyse_type)

	@staticmethod
	def _successorsOffset(offset: int, instr: Type[dvm.Instruction]) -> List[int]:
		u"""
		Calcul des décalages

		Calcule le décalage avec les successeurs de l'instruction courante.

		:param offset: Décalage entre l'instruction précédente et l'instruction courante
		:param instr: Instruction courante
		:type offset: int
		:type instr: Instruction

		:return: La liste des décalages avec les prochaines instructions
		:rtype: List[int]
		"""
		offset_list: List[int] = [offset + instr.get_length()]
		instr_name: str = instr.get_name()
		# Instructions avec branchement "##t"
		if instr_name in {"if-eq", "if-ne", "if-lt", "if-ge", "if-gt", "if-le"}:
			offset_list = [offset + instr.get_length(), offset + instr.__getattribute__("CCCC") * 2]
		elif instr_name in {"if-eqz", "if-nez", "if-ltz", "if-gez", "if-gtz", "if-lez"}:
			offset_list = [offset + instr.get_length(), offset + instr.__getattribute__("BBBB") * 2]
		elif instr_name == "fill-array-data":
			offset_list = [offset + instr.get_length(), offset + instr.__getattribute__("BBBBBBBB") * 2]
		elif instr_name == "goto":
			offset_list = [offset + instr.__getattribute__("AA") * 2]
		elif instr_name == "goto/16":
			offset_list = [offset + instr.__getattribute__("AAAA") * 2]
		elif instr_name == "goto/32":
			offset_list = [offset + instr.__getattribute__("AAAAAAAA") * 2]
		elif instr_name in {"return", "return-void", "return-wide", "return-object", "throw", "fill-array-data-payload"}:
			offset_list = []
		return offset_list

	def numberParameterUnlivable(self) -> int:
		u"""
		Calculer le nombre paramètre invisible

		:return: le nombre parametre invisible
		:rtype: int
		"""
		number_arameter_unlivable = 0
		for instr in self._method.get_instructions():
			if instr.get_name() in {'invoke-direct', 'invoke-virtual', 'invoke-static', 'invoke-interface',
			                        'invoke-super'}:
				method = instr.cm.get_method(instr.BBBB)
				param: List[str] = list(filter(None, method[2][0][1:-1].split(" ")))
				if instr.get_name() != 'invoke-static':
					if len(param) + 1 != len(instr.get_operands()) - 1:  # Vérifier le nombre de paramètres
						for i in [instr.C, instr.D, instr.E, instr.F, instr.G]:
							if i > self._method.get_locals():
								number_arameter_unlivable = abs((len(param) + 1) - (len(instr.get_operands()) - 1))
				else:
					if len(param) != len(instr.get_operands()) - 1:  # Vérifier le nombre paramètres
						for i in [instr.C, instr.D, instr.E, instr.F, instr.G]:
							if i > self._method.get_locals():
								number_arameter_unlivable = abs((len(param)) - (len(instr.get_operands()) - 1))
		return number_arameter_unlivable
