# IMPORTS #####################################################################
import re


# CLASS #######################################################################
class Permission:

	def __init__(self, name: str, protection_level: str, summary: str, description: str, is_declared: bool) -> None:
		u"""
		Constructeur

		:param name: Un nom
		:type name: str
		:param protection_level: Le niveau de protection
		:type protection_level: str
		:param summary: Un court résumé
		:type summary: str
		:param description: La description complète
		:type description: str
		:param is_declared: Indique si c'est une permission déclarée ou utilisé
		"""
		self._name: str = name
		self._protection_level: str = protection_level.replace("|", ", ")
		self._summary: str = re.sub(r"\s+", " ", summary)
		self._description: str = re.sub(r"\s+", " ", description)
		# self._description: str = description
		self._is_declared: bool = is_declared


	@property
	def name(self) -> str:
		return self._name

	@property
	def protection_level(self) -> str:
		return self._protection_level

	@property
	def summary(self) -> str:
		return self._summary

	@property
	def description(self) -> str:
		return self._description

	@property
	def is_declared(self) -> bool:
		return self._is_declared