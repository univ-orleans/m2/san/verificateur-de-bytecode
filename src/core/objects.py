# IMPORTS #####################################################################
from __future__ import annotations
from typing import Dict


# CLASS #######################################################################
class Objects(Dict[int, bool]):

	def __init__(self, registers_number: int) -> None:
		u"""
		Constructeur : créer un dictionnaire avec registers_number clés avec une valeur False par défaut
		(registre non instancié)

		:param registers_number: Le nombre de registres
		:type registers_number: int
		"""
		super().__init__()
		# initialiser le tableau des registres avec des `None` de la forme état = (type, valeur)
		for i in range(registers_number):
			self.__setitem__(i, False)

	def setInst(self, id: int, value: bool) -> None:
		u"""
		Affecter un nouveau boolean à un registre

		:param id: Le numéro du registre
		:type id: int
		:param value: La nouvelle valeur du registre
		:type value: bool
		"""
		self.__setitem__(id, value)

	def copy(self) -> Objects:
		u"""
		Copie le tableaux de registre

		:return: Une copie des registres
		:rtype Objects
		"""
		new_objects: Objects = Objects(len(self))
		new_objects.update(self)
		return new_objects
