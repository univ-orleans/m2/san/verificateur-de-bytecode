# IMPORTS #####################################################################
from typing import List, Union
from androguard.core.bytecodes import dvm
from androguard.core.analysis.analysis import ExternalClass

from src.core.method import Method


# CLASS #######################################################################
class Class:

	def __init__(self, _class: Union[dvm.ClassDefItem, ExternalClass]) -> None:
		u"""
		Constructeur

		:param _class: La classe Androguard
		:type _class: Union[dvm.ClassDefItem, ExternalClass]
		"""
		self._class: Union[dvm.ClassDefItem, ExternalClass] = _class
		self._methods: List[Method] = []
		self._is_external: bool = isinstance(_class, ExternalClass)
		self._is_evaluate: bool = False
		self._analyse_types: List[str] = []


	@property
	def class_def(self) -> Union[dvm.ClassDefItem, ExternalClass]:
		return self._class

	@property
	def name(self) -> str:
		return self._class.get_name()

	@property
	def methods(self) -> List[Method]:
		return self._methods

	@property
	def is_external(self) -> bool:
		return self._is_external

	@property
	def is_evaluate(self) -> bool:
		return self._is_evaluate

	@property
	def analyse_types(self) -> List[str]:
		return self._analyse_types

	@is_evaluate.setter
	def is_evaluate(self, new_is_evaluate: bool) -> None:
		if isinstance(new_is_evaluate, bool):
			self._is_evaluate = new_is_evaluate
		else:
			raise TypeError("La propriété \"is_evaluate\" doit être de type \"bool\".")


	def addMethod(self, method: dvm.EncodedMethod) -> None:
		u"""
		Ajouter une méthode à analyser

		:param method: La méthode Androguard
		:type method: dvm.EncodedMethod
		"""
		self._methods.append(Method(method))