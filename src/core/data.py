# IMPORTS #####################################################################
from typing import Optional, List, Union, Dict
from os.path import isfile, exists
from re import fullmatch
from androguard.misc import AnalyzeAPK, Analysis
from androguard.core.bytecodes.apk import APK
from androguard.core.bytecodes import dvm
from androguard.core.analysis.analysis import ClassAnalysis, ExternalMethod

from src.core._class import Class
from src.core.permission import Permission
from src.util.filter import Filter
from src.util.logging import log


# CLASS #######################################################################
class Data:

	def __init__(self, apk_path: str) -> None:
		u"""
		Constructeur

		:param apk_path: Le chemin d'accès à l'archive à analyser
		:type apk_path: str
		"""
		self._apk: APK  # apk
		self._dalvik: List[dvm.DalvikVMFormat]  # contenu des fichiers DEX
		self._analysis: Analysis  # support d'analyse
		self._classes: List[Class] = []
		self._static_permissions: Dict[str, List[Permission]] = {"used": [], "declared": []}
		self._analyse_types: List[str] = []

		# vérifier que l'archive existe
		if exists(apk_path):
			# vérifier que c'est bien un fichier
			if isfile(apk_path):
				log.debug("Démarrage du chargement des données de l'archive.")
				# récupération des données de l'archive
				self._apk, self._dalvik, self._analysis = AnalyzeAPK(apk_path)
				log.debug("Fin du chargement des données de l'archive.")
				log.info(f"L'archive \"{self._apk.get_app_name()}\" est chargé.")
			else:
				log.error(f"Le chemin \"{apk_path}\" ne pointe pas sur un fichier.")
				exit()
		else:
			log.error(f"Le chemin \"{apk_path}\" n'existe pas ou n'est pas atteignable.")
			exit()


	@property
	def apk(self) -> APK:
		return self._apk

	@property
	def dalvik(self) -> List[dvm.DalvikVMFormat]:
		return self._dalvik

	@property
	def analysis(self) -> Analysis:
		return self._analysis

	@property
	def classes(self) -> List[Class]:
		return self._classes

	@property
	def static_permissions(self) -> Dict[str, List[Permission]]:
		return self._static_permissions

	@property
	def analyse_types(self) -> List[str]:
		return self._analyse_types


	def loadClasses(self, class_names: Optional[List[str]] = None, black_list: Optional[List[str]] = None) -> None:
		u"""
		Charger les classes à partir des classes Androguard

		:param class_names: La liste des classes sélectionnées à analyser (défaut: None, pour prendre toutes les classes)
		:type class_names: Optional[List[str]]
		:param black_list: La liste des d'éléments indésirable utilisé pour filtrer (défaut: None pour ne pas appliquer de filtrage)
		:type black_list: Optional[List[str]]
		"""
		package: str = self._apk.package.replace(".", "/")
		# Si aucune classe n'est définit, alors il faut toutes les prendre.
		if class_names is None:
			for class_name, class_analysis in self._analysis.classes.items():
				if (len(class_analysis.get_methods())) and (package in class_name or class_analysis.external):
					if black_list is None:
						self._classes.append(Class(class_analysis.orig_class))
						log.debug(f"Ajout de la classe \"{class_name}\" dans la liste des classes à analyser.")
					elif Filter.verify(class_name, black_list):
						self._classes.append(Class(class_analysis.orig_class))
						log.debug(f"Ajout de la classe \"{class_name}\" dans la liste des classes à analyser.")
					else:
						log.debug(f"La classe \"{class_name}\" ne sera pas analysé à cause du filtrage.")
		# Sinon, récupérer les classes spécifiées.
		else:
			for class_name in class_names:
				classes_analysis: List[ClassAnalysis] = list(filter(lambda x: fullmatch(fr"^{class_name[0:-1]}(\$\d+)*;$", x.name), self._analysis.get_classes()))
				for class_analysis in classes_analysis:
					if (class_analysis is not None) and (len(class_analysis.get_methods())):
						if black_list is None:
							self._classes.append(Class(class_analysis.orig_class))
							log.debug(f"Ajout de la classe \"{class_analysis.name}\" dans la liste des classes à analyser.")
						elif Filter.verify(class_analysis.name, black_list):
							self._classes.append(Class(class_analysis.orig_class))
							log.debug(f"Ajout de la classe \"{class_analysis.name}\" dans la liste des classes à analyser.")
						else:
							log.warning(f"La classe \"{class_analysis.name}\" ne sera pas analysé à cause du filtrage.")
					else:
						log.warning(f"La classe \"{class_name}\" n'a pas été trouvé.")
				if len(classes_analysis) == 0:
					log.warning(f"La classe \"{class_name}\" n'a pas été trouvé.")


	def loadMethods(self, method_names: Optional[List[str]] = None) -> None:
		u"""
		Charger les méthodes à partir des méthodes Androguard

		:param method_names: La liste des méthodes sélectionnées à analyser (défaut: None, pour prendre toutes les méthodes)
		:type method_names: Optional[List[str]]
		"""
		# Si aucune méthode spécifier, alors prendre toutes les méthodes
		if method_names is None:
			for _class in self._classes:
				methods: List[Union[dvm.EncodedMethod, ExternalMethod]] = list(_class.class_def.get_methods())
				for method in methods:
					if Filter.verify(method.get_access_flags_string(), Filter.METHOD_TAGS_TO_EXCLUDE):
						_class.addMethod(method)
						log.debug(f"Ajout de la méthode \"{method.name}\" dans la liste des méthodes à analyser.")
					else:
						log.warning(f"La méthode \"{method.name}\" ne peut pas être analysé car le prototype \"{method.access_flags_string}\" n'est pas prise en charge.")
		# Sinon, récupérer les méthodes spécifiées
		else:
			for _class in self._classes:
				methods: List[Union[dvm.EncodedMethod, ExternalMethod]] = list(_class.class_def.get_methods())
				for method in methods:
					if method.name in method_names:
						if Filter.verify(method.get_access_flags_string(), Filter.METHOD_TAGS_TO_EXCLUDE):
							_class.addMethod(method)
							log.debug(f"Ajout de la méthode \"{method.name}\" dans la liste des méthodes à analyser.")
						else:
							log.warning(f"La méthode \"{method.name}\" ne peut pas être analysé car le prototype \"{method.access_flags_string}\" n'est pas prise en charge.")
					else:
						log.warning(f"La méthode \"{method.name}\" n'a pas été trouvé.")
