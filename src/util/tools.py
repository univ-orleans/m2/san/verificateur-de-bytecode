# IMPORTS #####################################################################
from androguard.core.bytecodes.dvm import TYPE_DESCRIPTOR


# METHODS #####################################################################
def classPathToClassName(class_path: str) -> str:
	u"""
	Transformer un chemin de classe en nom de classe

	Change le chemin d'accès d'une classe (ex: fr.orleans.univ.myapplication.MainActivity) en un nom de classe tel
	qu'utilisé par Androguard (ex: Lfr/orleans/univ/myapplication/MainActivity;) pour l'indexation des classes.

	:param class_path: Un chemin d'accès à une classe
	:type class_path: str

	:return: Le nom de la classe (définit par Androguard)
	:rtype: str
	"""
	class_name: str = class_path
	# Si ce n'est pas un type primitif, faire les modifications
	if class_path not in TYPE_DESCRIPTOR.values():
		if "[]" in class_path and class_path[:-2] in TYPE_DESCRIPTOR.values():
			for key, value in TYPE_DESCRIPTOR.items():
				if value == class_path[:-2]:
					return "[" + key
		elif "[]" in class_path:
			classe = classPathToClassName(class_path[:-2])
			return ("[" * class_path.count("[]")) + classe
		class_name = class_name.replace(".", "/")
		if not class_name.startswith("L"):
			class_name = f"L{class_name}"
		if not class_name.endswith(";"):
			class_name = f"{class_name};"
	return class_name


def classNameToClassPath(class_name: str) -> str:
	u"""
	Transformer un nom de classe en chemin de classe

	Change le nom d'une classe (ex: Lfr/orleans/univ/myapplication/MainActivity;) en un chemin de classe
	Android (ex: fr.orleans.univ.myapplication.MainActivity).

	:param class_name: Un nom de classe Androguard
	:type class_name: str

	:return: Un chemin d'accès à la classe
	:rtype: str
	"""
	class_path: str = class_name
	# Si ce n'est pas un type primitif, faire les modifications
	if class_name not in TYPE_DESCRIPTOR.values():
		class_path = class_path.replace("/", ".")[1:-1]
	return class_path