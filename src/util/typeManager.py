# IMPORTS #####################################################################
from typing import Set, List, Tuple, Optional, Dict
from androguard.core.bytecodes import dvm
from androguard.misc import Analysis
from androguard.core.analysis.analysis import ClassAnalysis
from py4j.java_gateway import JavaGateway, GatewayParameters, launch_gateway, JavaObject

from src.util.tools import classPathToClassName, classNameToClassPath


# CLASS #######################################################################
class TypeManager:

	def __init__(self, analysis: Analysis) -> None:
		u"""
		Constructeur

		:param analysis: Le support d'analyse Androguard
		:type analysis: Analysis
		"""
		self._analysis: Analysis = analysis
		self._gateway: JavaGateway = JavaGateway(gateway_parameters=GatewayParameters(port=launch_gateway()))
		self._classes: Dict[str, Set[str]] = dict()


	def addClass(self, classes: List[dvm.ClassDefItem]) -> None:
		u"""
		Ajoute une classe comportant un dollar "$" (ex: com/univ/eva/Environment$1)

		:param classes: La liste des classes à analyser
		:type classes: List[dvm.ClassDefItem]
		"""
		for _class in classes:
			if "$" in _class.name and _class.get_superclassname() is not None:
				result: Set[str] = {_class.name}
				types_to_evaluate: Set[str] = {_class.get_superclassname()}
				types_to_evaluate.update(_class.get_interfaces())
				for current_type in types_to_evaluate:
					result.update(self.getInheritedTypes(current_type))
				self._classes.__setitem__(_class.name, result)



	def _getInheritedExternalTypes(self, _type: str) -> Set[str]:
		u"""
		Récupère l'ensemble des classes parent catégorisées comme externe (Java et AndroidX)

		:param _type: Le type d'une classe
		:type _type: str

		:return: L'ensemble des types des classes parentes (pour les classes Java)
		:rtype: Set[str]

		:Exemple:
		>>> self._getInheritedExternalTypes("java.util.ArrayList")
		{"Ljava/util/ArrayList;", "Ljava/lang/Object;", "Ljava/util/RandomAccess;", "Ljava/lang/Cloneable;",
		"Ljava/io/Serializable;", "Ljava/util/Collection;", "Ljava/util/List;", "Ljava/util/AbstractList;",
		"Ljava/util/AbstractCollection;"}
		"""
		# changer le chemin d'accès en nom de classe Androguard
		_type = classNameToClassPath(_type)
		result: Set[str] = {_type}
		types_to_evaluate: Set[str] = {_type}
		while len(types_to_evaluate):
			# récupérer le type à évaluer
			current_type: str = types_to_evaluate.pop()
			# récupérer la classe Java correspondant à ce type
			current_class: JavaObject = self._gateway.jvm.java.lang.Class.forName(current_type)
			if current_class.getSuperclass() is not None:
				# ajouter la classe parente dans le résultat et dans la liste des types à évaluer
				types_to_evaluate.add(current_class.getSuperclass().getName())
				result.add(current_class.getSuperclass().getName())
				# ajouter les interfaces dans le résultat et dans la liste des types à évaluer
				for interface in current_class.getInterfaces():
					types_to_evaluate.add(interface.getName())
					result.add(interface.getName())
			else:
				result.add("java.lang.Object")
		return set(map(classPathToClassName, result))


	def getInheritedTypes(self, _type: str) -> Set[str]:
		u"""
		Récupère l'ensemble des classes parent

		:param _type: Le nom d'une classe Androguard
		:type _type: str

		:return: L'ensemble des types des classes parentes
		:rtype: Set[str]

		:Exemple:
		>>> self.getInheritedTypes("java.util.ArrayList")
		{"Ljava/util/ArrayList;", "Ljava/lang/Object;", "Ljava/util/RandomAccess;", "Ljava/lang/Cloneable;",
		"Ljava/io/Serializable;", "Ljava/util/Collection;", "Ljava/util/List;", "Ljava/util/AbstractList;",
		"Ljava/util/AbstractCollection;"}
		"""
		is_tableau = (False, 0)
		if _type is not None and _type[0] == "[" and _type[1:] not in dvm.TYPE_DESCRIPTOR:
			is_tableau = (True, _type.find("L"))
			_type = _type[is_tableau[1]:]

		result: Set[str] = {_type}
		# si `_type` est null ou un tableau, renvoyer `_type`, sinon faire le traitement dans le block conditionel
		if _type in self._classes:
			result = self._classes.get(_type)
		elif (_type is not None) and (_type[0] != "["):
			types_to_evaluate: Set[str] = {_type}
			while len(types_to_evaluate):
				# récupérer le type à évaluer
				current_type: str = types_to_evaluate.pop()
				# si le type en cours d'évaluation est un type primitif, l'ajouter dans la liste des types
				# à évaluer comme classe Java.
				if current_type in dvm.TYPE_DESCRIPTOR.values():
					value: str = f"Ljava/lang/{current_type.capitalize()};"
					if current_type == "int":
						value = "Ljava/lang/Integer;"
					elif current_type == "char":
						value = "Ljava/lang/Character;"
					types_to_evaluate.add(value)
					result.add(value)
				# récupérer toutes les classes qui correspondent avec le type courant
				classes: List[ClassAnalysis] = list(self._analysis.find_classes(current_type))
				is_found = False
				for _class in classes:
					is_found = True
					# vérifier si la classe est définie dans le DEX (pas le cas pour les classes Java et AndroidX)
					if not _class.is_external():
						# ajouter la classe parente dans le résultat et dans la liste des types à évaluer
						types_to_evaluate.add(_class.get_vm_class().get_superclassname())
						result.add(_class.get_vm_class().get_superclassname())
						# ajouter les interfaces dans le résultat et dans la liste des types à évaluer
						for interface in _class.get_vm_class().get_interfaces():
							types_to_evaluate.add(interface)
							result.add(interface)
					else:
						if "java" in _class.get_vm_class().get_name():
							result.update(self._getInheritedExternalTypes(_class.get_vm_class().get_name()))
						else:
							result.add("Landroid/*")
				if len(classes) == 0:
					if "java" in current_type:
						result.update(self._getInheritedExternalTypes(current_type))
		if is_tableau[0]:
			_result = set()
			for s in result:
				_result.add((is_tableau[1] * "[") + s)
			return _result
		return result


	@staticmethod
	def getType(register_state: Tuple[Optional[str], Optional[int]], instr_type: str) -> str:
		u"""
		Retourne le bon type

		Dans le cas des entiers `int` par exemple, il est difficile de savoir si c'est un entier ou
		un boolean. Cette méthode retourne le bon type de registre en fonction du type de l'instruction
		en cours d'évaluation.

		:param register_state: L'état d'un registre (type, valeur)
		:type register_state: Tuple[Optional[str], Optional[int]]
		:param instr_type: Le type de l'instruction
		:type instr_type: str

		:return: Le bon type
		:type: str

		:Exemple:
		>>> TypeManager.getType("int", 0, "boolean")
		"boolean"
		>>> TypeManager.getType("int", 9, "boolean")
		"int"
		"""
		result: str = register_state[0]
		if register_state[0] == "int" and instr_type == "boolean" and register_state[1] in {0, 1}:
			result = "boolean"
		if register_state[0] == "int" and instr_type == "char" and register_state[1] in range(1, 256):
			result = "char"
		return result


	def checkPrimitifType(self, type_1: str, type_2: str, valeur_2: Optional[int]) -> bool:
		u"""
        Vérifie l'égalite de 2 types primitifs

        :param type_1: Le type de l'instruction
        :type type_1: str
        :param type_2: Le type du registre
        :type type_2: str
        :param valeur_2: La valeur du registre
        :type valeur_2: Optional[int]

        :return: vérification si 2 types sont égaux
        :rtype: bool

        :Exemple:
        >>> self.checkPrimitifType("I", "int", 0)
        True
        >>> self.checkPrimitifType("I", "char", 0)
        False
        """
		types: Set[str] = self.getInheritedTypes(type_2)
		if type_1 in dvm.TYPE_DESCRIPTOR:
			type_1 = dvm.TYPE_DESCRIPTOR.get(type_1)
		if type_1 in types:
			return True
		elif type_2 == "int" and type_1 == "boolean" and valeur_2 in {0, 1}:
			return True
		elif type_2 == "int" and type_1 == "char" and valeur_2 in range(1, 256):
			return True
		elif type_2 == "int" and type_1 == "float":
			return True
		elif type_1 == "int" and type_2 == "char":
			return True
		return False

	def checkClasseType(self, type_1: str, type_2: str, valeur_2: Optional[int]) -> bool:
		u"""
        Pour les objets :
        Vérifie si `type_1` est inclus dans le `type_2`. Si `type_2` est égale à ('int', 0) et
        `type_1` est égale à "Ljava/lang/Object;" alors `type_2` correspond à null (en Java)

        :param type_1: Le type de l'instruction
        :type type_1: str
        :param type_2: Le type du registre
        :type type_2: str
        :param valeur_2: La valeur du registre
        :type valeur_2: Optional[int]

        :return: vérification si 2 types sont égaux
        :rtype: bool

        :Exemple:
        >>> self.checkClasseType("Ljava/lang/Object;", "Ljava/lang/Integer;", 0) # avec un type complexe
        True # vrai car "Ljava/lang/Object;" est dans {"Ljava/lang/Object;", "Ljava/lang/Integer;"}
        """
		types: Set[str] = self.getInheritedTypes(type_2)
		if type_1 in types:
			return True
		elif type_2 == "int" and valeur_2 == 0:
			return True
		elif type_1 == "Ljava/lang/Class;":
			return True
		elif "Landroid/*" in types:
			return True
		return False


	def getMessage(self, type_1: str, type_2: str, valeur_2: int, id_register: Optional[int]) -> List[str]:
		u"""
        Donne les messages d'erreur pour le type 'classe'

        :param type_1: Le type de l'instruction
        :type type_1: str
        :param type_2: Le type du registre
        :type type_2: str
        :param valeur_2: La valeur du registre
        :type valeur_2: Optional[int]
        :param id_register: Le numéro du registre
        :type id_register: Optional[int]

        :return: les messages d'erreur
        :rtype: List[str]
        """
		types: Set[str] = self.getInheritedTypes(type_2)
		messages: List[str] = []
		if type_1 in types:
			return messages
		elif type_2 == "int" and valeur_2 == 0:
			return messages
		elif type_1 == "Ljava/lang/Class;":
			return messages
		elif "Landroid/*" in types:
			return messages
		messages.append(f"le registre v{id_register} est de type \"{type_1}\" au lieux de \"{type_2}\".")
		return messages


	def checkType(self, i: int, type_1: str, type_2: str, valeur_2: Optional[int]) -> List[str]:
		u"""
        Pour les objets:
        Vérifie si `type_1` est inclus dans le `type_2` sinon lève une exception. Si `type_2` est égale à ('int', 0) et
        `type_1` est égale à "Ljava/lang/Object;" alors `type_2` correspond à null (en Java)

        Pour les types prédéfinis:
        Transforme de type et vérifie si `type_1` est égale à `type_2`. Si `type_1` est un boolean alors il faut que
        `type_2` soit égale à ('int', 0) ou ('int', 1).

        EXEMPLE
        >>> self.checkType("I", "int", 0) # avec un type primitif
        []
        >>> self.checkType("Ljava/lang/Object;", "Ljava/lang/Integer;", 0) # avec un type complexe
        [] # vrai car "Ljava/lang/Object;" est dans {"Ljava/lang/Object;", "Ljava/lang/Integer;"}

        :param type_1: Le type de l'instruction
        :type type_1: str
        :param type_2: Le type du registre
        :type type_2: str
        :param valeur_2: La valeur du registre
        :type valeur_2: Optional[int]

        :return: La liste des messages d'erreur
        :rtype: List[str]
        """
		messages: List[str] = []
		if type_1 in dvm.TYPE_DESCRIPTOR:
			type_1 = dvm.TYPE_DESCRIPTOR.get(type_1)
		if type_1 in dvm.TYPE_DESCRIPTOR.values():  # type prédéfini
			if not self.checkPrimitifType(type_1, type_2, valeur_2):
				messages.append(f"le registre v{i} est de type \"{dvm.TYPE_DESCRIPTOR.get(type_1)}\" au lieux de \"{type_2}\".")
		elif not self.checkClasseType(type_1, type_2, valeur_2):
			messages.extend(self.getMessage(type_1, type_2, valeur_2, i))
		return messages