# IMPORTS #####################################################################
from __future__ import annotations
from typing import Dict, Final
from datetime import datetime
from sty import fg, Style, RgbFg


# CLASS #######################################################################
class Log:

	# les couleurs
	fg.orange = Style(RgbFg(255, 150, 50))
	COLOR_BLUE: Style = fg.blue
	COLOR_DEFAULT: Style = fg.rs
	COLOR_ORANGE: Style = fg.orange
	COLOR_RED: Style = fg.red
	COLOR_GREEN: Style = fg.green
	COLOR_YELLOW: Style = fg.yellow
	COLOR_PURPLE: Style = fg.magenta

	# la définition des niveaux
	DEBUG: Final[int] = 0
	INFO: Final[int] = 1
	WARNING: Final[int] = 2
	ERROR: Final[int] = 3

	# les niveaux pour l'affichage
	_levels_to_display: Dict[int, str] = {
		DEBUG: f"{COLOR_BLUE}DEBUG  {COLOR_DEFAULT}",
		INFO: f"{COLOR_DEFAULT}INFO   {COLOR_DEFAULT}",
		WARNING: f"{COLOR_ORANGE}WARNING{COLOR_DEFAULT}",
		ERROR: f"{COLOR_RED}ERROR  {COLOR_DEFAULT}",
	}


	def __init__(self, level: int = INFO):
		# le niveau minimum pour l'affichage
		self.level: int = level


	def _print(self, id_level: int, message: str, offset: int, color) -> None:
		if id_level >= self.level:
			offset_str: str = "" if offset == 0 else "    " * (offset-1) + "    "
			message = message.replace("\n", f"\n{' ' * 29 + ' ' * len(offset_str)}")
			print(f"{datetime.now().__format__('%Y-%m-%d %H:%M:%S')} [{Log._levels_to_display.get(id_level)}] {offset_str}{color}{message}{Log.COLOR_DEFAULT}")


	def debug(self, message: str, offset: int = 0, color: Style = COLOR_DEFAULT) -> None:
		self._print(0, message, offset, color)


	def info(self, message: str, offset: int = 0, color: Style = COLOR_DEFAULT) -> None:
		self._print(1, message, offset, color)


	def warning(self, message: str, offset: int = 0, color: Style = COLOR_ORANGE) -> None:
		self._print(2, message, offset, color)


	def error(self, message: str, offset: int = 0, color: Style = COLOR_RED) -> None:
		self._print(3, message, offset, color)




log: Log = Log()