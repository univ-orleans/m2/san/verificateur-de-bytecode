# IMPORTS #####################################################################
from typing import Final, List
from re import fullmatch


# CLASS #######################################################################
class Filter:

	CLASSE_NAMES_TO_EXCLUDE: Final[List[str]] = [
		r"^L.*/BuildConfig(\$\d+)*;$",
		r"^L.*/R(\$.*)?;$",
		r"^L.*/ui/dashboard/DashboardFragment(\$\d+)*;$",
		r"^L.*/ui/dashboard/DashboardViewModel(\$\d+)*;$",
		r"^L.*/ui/home/HomeFragment(\$\d+)*;$",
		r"^L.*/ui/home/HomeViewModel(\$\d+)*;$",
		r"^L.*/ui/notifications/NotificationsFragment(\$\d+)*;$",
		r"^L.*/ui/notifications/NotificationsViewModel(\$\d+)*;$",
		r"^L.*_Impl(\$\d+)*;$",
	]

	METHOD_TAGS_TO_EXCLUDE: Final[List[str]] = [
		r"^.*abstract.*$",
		r"^.*native.*$"
	]

	METHOD_INSTRUCTIONS_TO_EXCLUDE: Final[List[str]] = [
		# r"^nop$",
		# r"^move$",
		# r"^move/from16$",
		# r"^move/16$",
		# r"^move-wide$",
		# r"^move-wide/from16$",
		# r"^move-wide/16$",
		# r"^move-object$",
		# r"^move-object/from16$",
		# r"^move-object/16$",
		# r"^move-result$",
		# r"^move-result-wide$",
		# r"^move-result-object$",
		# r"^move-exception$",
		# r"^return-void$",
		# r"^return$",
		# r"^return-wide$",
		# r"^return-object$",
		# r"^const/4$",
		# r"^const/16$",
		# r"^const$",
		# r"^const/high16$",
		r"^const-wide/16$",
		# r"^const-wide/32$",
		r"^const-wide$",
		r"^const-wide/high16$",
		# r"^const-string$",
		r"^const-string/jumbo$",
		# r"^const-class$",
		# r"^monitor-enter$",
		# r"^monitor-exit$",
		# r"^check-cast$",
		# r"^instance-of$",
		# r"^array-length$",
		# r"^new-instance$",
		# r"^new-array$",
		r"^filled-new-array$",
		r"^filled-new-array/range$",
		# r"^filled-array-data$",
		# r"^throw$",
		# r"^goto$",
		r"^goto/16$",
		r"^goto/32$",
		r"^packed-switch$",
		r"^sparse-switch$",
		r"^cmp(l-float|g-float|l-double|g-double|-long)$",
		# r"^if-(eq|ne|lt|ge|gt|le)$",
		# r"^if-(eq|ne|lt|ge|gt|le)z$",
		# r"^(aget|aput)(|-(object|boolean|char))$",
		r"^(aget|aput)-(wide|byte|short)$",
		# r"^(iget|iput)(|-(object|boolean|char))$",
		r"^(iget|iput)-(wide|byte|short)$",
		# r"^(sget|sput)(|-(object|boolean|char))$",
		r"^(sget|sput)-(wide|byte|short)$",
		# r"^invoke-(virtual|super|direct|static|interface)$",
		r"^invoke-(virtual|super|direct|static|interface)/range$",
		# r"^(neg-(long|double|int|float)|not-(int|long)|int-to-(float|double|byte|short|char)|long-to-(int|float|double)|float-to-(int|long|double)|double-to-(int|long|float))$",
		r"^(neg-(long|double)|not-long|int-to-(double|byte|short|long)|long-to-double|float-to-(long|double)|double-to-long)$",
		# r"^((and|or|xor|shl|shr|ushr)-int|(add|sub|mul|div|rem)-(int|float))$",
		r"^((and|or|xor|shl|shr|ushr)-long|(add|sub|mul|div|rem)-(long|double))$",
		# r"^((and|or|xor|shl|shr|ushr)-int|(add|sub|mul|div|rem)-(int|float))/2addr$",
		r"^((and|or|xor|shl|shr|ushr)-long|(add|sub|mul|div|rem)-(long|double))/2addr$",
		# r"^((add|mul|div|rem|and|or|xor)-int/lit16|rsub-int)$",
		# r"^(add|rsub|mul|div|rem|and|or|xor|shl|shr|ushr)-int/lit8$",
		r"^invoke-polymorphic$",
		r"^invoke-polymorphic/range$",
		r"^invoke-custom$",
		r"^invoke-custom/range$",
		r"^const-method-handle$",
		r"^const-method-type$",
		r"^packed-switch-payload$",
		r"^sparse-switch-payload$",
		# r"^fill-array-data-payload$",
	]

	@staticmethod
	def verify(elem: str, black_list: List[str]) -> bool:
		u"""
		Vérifier que le nom ne soit pas dans une liste de nom à ne pas utiliser

		:param elem: L'élément à vérifier
		:type elem: str
		:param black_list: La liste d'élément à exclure de l'analyse
		:type black_list: List[str]

		:return: Si le nom est valide ou non
		:rtype: bool
		"""
		not_found: bool = True
		i: int = 0
		while not_found and i < len(black_list):
			if fullmatch(black_list[i], elem) is not None:
				not_found = False
			i += 1
		return not_found
