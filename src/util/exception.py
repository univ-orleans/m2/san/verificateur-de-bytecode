# IMPORTS #####################################################################
from typing import Optional, List


# CLASS #######################################################################
class TypeComparaisonError(Exception):

	def __init__(self, type_1: Optional[str], type_2: Optional[str]) -> None:
		u"""
		Constructeur

		:param type_1: Un type
		:param type_2: Un autre type
		:type type_1: Optional[str]
		:type type_2: Optional[str]
		"""
		self.message: str = f"Erreur de type: impossible de comparer les types \"{type_1}\" et \"{type_2}\"."


class RegisterNumberError(Exception):

	def __init__(self, len_registers_1: int, len_registers_2: int) -> None:
		u"""
		Constructeur

		:param len_registers_1: La taille du premier tableau de registre
		:type len_registers_1: int
		:param len_registers_2: La taille du second tableau de registre
		:type len_registers_2: int
		"""
		self.message: str = f"Erreur de registre: les tableaux de registre n'ont pas la même taille ({len_registers_1} et {len_registers_2})."


class RegisterTypeError(Exception):

	def __init__(self, error_messages: List[str]) -> None:
		u"""
		Constructeur

		:param error_messages: La liste des messages d'erreur
		:type error_messages: List[str]
		"""
		self.message: str = f"Erreur de registre:\n  - " + "\n  - ".join(error_messages)


class ObjectInstantiationError(Exception):

	def __init__(self, error_messages: List[str]) -> None:
		u"""
		Constructeur

		:param error_messages: La liste des messages d'erreur
		:type error_messages: List[str]
		"""
		self.message: str = f"Erreur d'instantiation: " + " ".join(error_messages)


class RegisterIndexError(Exception):

	def __init__(self, register_indexes: List[str], registers_size: int) -> None:
		u"""
		Constructeur

		:param register_indexes: Un numéro de registre
		:param registers_size: Un tableau de registre
		:type register_indexes: int
		:type registers_size: Registers
		"""
		self.message = "Erreur de registre:"
		for index in register_indexes:
			self.message += f"\n- Le numéro de registre v{index} est invalide : il doit être compris entre 0 et {registers_size-1}."