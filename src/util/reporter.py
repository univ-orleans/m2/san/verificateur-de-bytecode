# IMPORTS #####################################################################
from typing import List, Optional, Set, Tuple
from os import path, makedirs
from shutil import rmtree
from mdutils.mdutils import MdUtils
import re

from src.core.data import Data
from src.core.method import Method
from src.core.permission import Permission
from src.util.tools import classNameToClassPath
from src.types.analyse import AnalyseTypes
from src.util.exception import ObjectInstantiationError
from src.util.logging import log


# CLASS #######################################################################
class Report:

	def __init__(self, root_dir: str) -> None:
		u"""
		Constructeur

		:param root_dir: Le répertoire de travail
		"""
		self._root_dir: str = root_dir
		self._working_dir: str = root_dir


	def _getMarkdownFile(self, file_name: str, title: Optional[str] = None) -> MdUtils:
		u"""
		Génère un fichier Markdown

		:param file_name: Le nom du fichier
		:type file_name: str
		:param title: Le titre du fichier
		:type title: str

		:return: Un fichier Markdown
		:rtype: MdUtils
		"""
		return MdUtils(
			file_name=path.join(self._working_dir, file_name),
			title=file_name if title is None else title
		)


	def _getClassAnalysisTable(self, methods: List[Method]) -> Tuple[List[str], int, int]:
		u"""
		Génère un tableau des résultats de validation d'analyse

		:param methods: La liste des méthodes de la classe
		:type methods: List[Method]

		:return: Un tableau en markdown avec sa longueur et sa hauteur
		:rtype: Tuple[List[str], int, int]
		"""
		table: List[str] = ["méthode"] + list(methods[0].is_valid_detail.keys())
		columns: int = len(table)
		rows: int = len(methods)+1  # +1 pour l'entête du tableau
		for method in methods:
			name: str = re.sub("<", r"\<", method.name)
			link: str = re.sub(" ", "-", re.sub(r"[^A-Za-z0-9 ]+", "", name.lower()))
			table.append(f"[**{name}**](#{link})")
			table.extend(map(self._getIcon, method.is_valid_detail.values()))
		return table, columns, rows


	def _getIcon(self, valid: Optional[bool]) -> str:
		u"""
		Icon de d'état d'analyse

		:param valid: Le résultat de l'analyse sur une méthode ou une classe
		:type valid: Optional[bool]

		:return: L'icone associé à cet état
		:rtype: str
		"""
		icon: str = "icon-unknow.svg"
		alt: str = "pas réalisé"
		if valid is True:
			icon = "icon-validated.svg"
			alt = "valide"
		elif valid is False:
			icon = "icon-unvalidated.svg"
			alt = "invalide"
		return f"<img src='../../res/svg/{icon}' alt='{alt}' style='vertical-align:text-bottom;'/>"


	def _getInstrctionsGraph(self, method: Method) -> str:
		u"""
		Génère le graphe de connexion entre les instructions

		:param method: Une méthode
		:type method: Method

		:return: mermaid (un graphe de type flowchart)
		:rtype: str
		"""
		graph: str = "flowchart TB\n" + \
			"\tclassDef node stroke-width:0;\n" + \
			"\tclassDef nodeError fill:#f14848,color:#ffffff,stroke-width:0;\n" + \
			"\tclassDef nodeNotEvaluate fill:#ff9632,color:#000000,stroke-width:0;\n\n"
		for current_instruction in method.instructions.values():
			current_node: str = f"{current_instruction.offset}(\"n°{current_instruction.position+1}<br/>{current_instruction.name}\")"
			if not current_instruction.is_valid:
				current_node += ":::nodeError" if len(current_instruction.errors) else ":::nodeNotEvaluate"
			for successor_instruction in current_instruction.successors:
				successor_node: str = f"{successor_instruction.offset}(\"n°{successor_instruction.position+1}<br/>{successor_instruction.name}\")"
				link: str = f"-{'' if successor_instruction == current_instruction.successors[0] else '.'}->"
				graph += f"\t{current_node} {link} {successor_node}\n"
		return graph


	def _getRegistersTable(self, method: Method) -> Tuple[List[str], int, int]:
		u"""
		Génère le tableau d'évaluation des registres de chaque instruction

		:param method: Une méthode
		:type method: Method

		:return: Un tableau en markdown avec sa longueur et sa hauteur
		:rtype: Tuple[List[str], int, int]
		"""
		table: List[str] = ["#", "offset", "instr", "repr"] + [f"v{i}" for i in range(method.registers_number)] + ["erreurs"]
		columns: int = len(table)
		rows: int = len(method.instructions)+1  # +1 pour l'entête du tableau
		for instruction in method.instructions.values():
			# informations sur l'instruction
			instr_repr: str = instruction.instr.get_output().replace("|", r"\|")
			table.extend([f"**{instruction.position+1}**", f"{instruction.offset}", instruction.name, instr_repr])
			# récupérer les numéros de registres qui sont utilisés par l'instruction
			register_pos_used: Set[int] = set(map(lambda x: int(x[1:]), re.findall(r"v\d", instr_repr)))
			for register_pos in instruction.registers:
				if register_pos in register_pos_used:
					table.append(instruction.registers.getType(register_pos))
				else:
					table.append(f"<span style='color:grey'>{instruction.registers.getType(register_pos)}</span>")
			# erreurs de l'instruction
			error_messages: List[str] = []
			for error_key, error_values in instruction.errors.items():
				if error_key != ObjectInstantiationError.__name__ and len(error_values):
					error_messages.append(f"**{error_key} :**<br/>" + "<br/>".join(map(lambda x: re.sub(r"\n", r"<br/>", x), error_values)))
			table.append(f"<span style='color:#f14848'>{'<br/><br/>'.join(error_messages)}</span>")
		return table, columns, rows


	def _getObjectTable(self, method: Method) -> Tuple[List[str], int, int]:
		u"""
		Génère le tableau d'évaluation des objets à chaque instruction

		:param method: Une méthode
		:type method: Method

		:return: Un tableau en markdown avec sa longueur et sa hauteur
		:rtype: Tuple[List[str], int, int]
		"""
		table: List[str] = ["#", "instr", "repr"] + [f"v{i}" for i in range(method.registers_number)] + ["erreurs"]
		columns: int = len(table)
		rows: int = len(method.instructions) + 1  # +1 pour l'entête du tableau
		for instruction in method.instructions.values():
			# informations sur l'instruction
			instr_repr: str = instruction.instr.get_output().replace("|", r"\|")
			table.extend([f"**{instruction.position + 1}**", instruction.name, instr_repr])
			# récupérer les numéros de registres qui sont utilisés par l'instruction
			register_pos_used: Set[int] = set(map(lambda x: int(x[1:]), re.findall(r"v\d", instr_repr)))
			for register_pos in instruction.objects:
				if register_pos in register_pos_used:
					table.append(instruction.objects.get(register_pos))
				else:
					table.append(f"<span style='color:grey'>{instruction.objects.get(register_pos)}</span>")
			# erreurs de l'instruction
			error_messages: List[str] = []
			for error_key, error_values in instruction.errors.items():
				if error_key == ObjectInstantiationError.__name__ and len(error_values):
					error_messages.append(f"**{error_key} :**<br/>" + "<br/>".join(map(lambda x: re.sub(r"\n", r"<br/>", x), error_values)))
			table.append(f"<span style='color:#f14848'>{'<br/><br/>'.join(error_messages)}</span>")
		return table, columns, rows


	def _getPermissionsTable(self, permissions: List[Permission]) -> Tuple[List[str], int, int]:
		u"""
		Génère le tableau de permissions demandées dynamiquement

		:param permissions: Une liste de permission non vide
		:type permissions: List[Permission]

		:return: Un tableau en markdown avec sa longueur et sa hauteur
		:rtype: Tuple[List[str], int, int]
		"""
		table: List[str] = ["#", "nom", "niveau de protection", "résumé", "description"]
		columns: int = len(table)
		rows: int = len(permissions) + 1  # +1 pour l'entête du tableau
		cpt: int = 1
		for permission in permissions:
			table.extend([f"{cpt}", permission.name, permission.protection_level, re.sub("\n", "<br/>", permission.summary), re.sub("\n", "<br/>", permission.description)])
			cpt += 1
		return table, columns, rows


	def generate(self, data: Data) -> None:
		u"""
		Générateur de rapport

		:param data: La structure de données principale du projet
		:type data: Data
		"""
		# dossier de génération des rapports
		# le contenu est supprimé pour être remplacé par les nouveaux rapports
		self._working_dir = path.join(self._root_dir, path.basename(data.apk.filename).replace(".apk", "").lower())
		if path.exists(self._working_dir):
			rmtree(self._working_dir)
		makedirs(self._working_dir)

		# affichage de l'analyse statique
		if AnalyseTypes.PERMISSION in data.analyse_types:
			md_file: MdUtils = self._getMarkdownFile("#MAKEFILE", "Makefile")
			md_file.new_header(level=1, title="Permissions utilisées")
			used_permissions: List[Permission] = data.static_permissions.get("used")
			if len(used_permissions):
				table, table_width, table_height = self._getPermissionsTable(used_permissions)
				md_file.new_line()
				md_file.new_table(text=table, columns=table_width, rows=table_height, text_align="left")
				md_file.new_line()
			else:
				md_file.new_paragraph("Aucune permission n'est utilisée.")
			md_file.new_header(level=1, title="Permissions déclarées")
			declared_permissions: List[Permission] = data.static_permissions.get("declared")
			if len(declared_permissions):
				table, table_width, table_height = self._getPermissionsTable(used_permissions)
				md_file.new_line()
				md_file.new_table(text=table, columns=table_width, rows=table_height, text_align="left")
				md_file.new_line()
			else:
				md_file.new_paragraph("Aucune permission n'est déclarée.")
			# créer une table des matières
			md_file.new_table_of_contents(table_title="Table des matières", depth=1)
			# sauvegarder le fichier markdown dans le répertoire qui lui est associé
			md_file.create_md_file()


		# affichage de l'analyse dynamique
		for _class in data.classes:
			# générer un fichier markdown pour chaque classe
			file_name: str = classNameToClassPath(_class.name)
			md_file: MdUtils = self._getMarkdownFile(file_name)

			# supprimer les classes non évaluées ou sans méthode
			if (not _class.is_evaluate) or \
				(len(_class.methods) == 0) or \
				(_class.is_external and sum(map(lambda x: len(x.permissions), _class.methods)) == 0):
				continue

			# afficher le tableau de résultat d'analyse de chaque méthode
			md_file.new_header(level=1, title="Résultat des analyses")
			table, table_width, table_height = self._getClassAnalysisTable(list(filter(lambda x: (not _class.is_external) or (_class.is_external and len(x.permissions)), _class.methods)))
			md_file.new_line()
			md_file.new_table(text=table, columns=table_width, rows=table_height)
			md_file.new_line()

			md_file.new_header(level=1, title="Méthodes")
			for method in _class.methods:
				# permissions dynamiques (affichées en tableau)
				if _class.is_external and len(method.permissions) > 0:
					md_file.new_header(level=2, title=re.sub("<", r"\<", method.name))
					md_file.new_header(level=3, title="Permissions demandées")
					table, table_width, table_height = self._getPermissionsTable(method.permissions)
					md_file.new_line()
					md_file.new_table(text=table, columns=table_width, rows=table_height, text_align="left")
					md_file.new_line()

				elif not _class.is_external:
					# entête de section avec les informations sur la méthode
					md_file.new_header(level=2, title=re.sub("<", r"\<", method.name))
					md_file.new_line(text=f"**Signature :** `{method.signature}`")
					md_file.new_line(text=f"**Nombre de registre :** {method.registers_number}")
					# tableau de résultat d'analyse de la méthode
					table: List[str] = list(method.is_valid_detail.keys())
					table_width: int = len(table)
					table_height: int = 2
					table.extend(map(self._getIcon, method.is_valid_detail.values()))
					md_file.new_line()
					md_file.new_table(text=table, columns=table_width, rows=table_height)
					md_file.new_line()
					# code source
					md_file.new_header(level=3, title="Source")
					md_file.insert_code(code=method.code_source, language="java")

					if method.is_evaluate.get(AnalyseTypes.REGISTER):
						# graphe d'instruction
						md_file.new_header(level=3, title="Graphe d'instruction")
						md_file.insert_code(code=self._getInstrctionsGraph(method), language="mermaid")
						# tableaux des registres d'instruction
						md_file.new_header(level=3, title="Registres d'instructions")
						table, table_width, table_height = self._getRegistersTable(method)
						md_file.new_line()
						md_file.new_table(text=table, columns=table_width, rows=table_height, text_align="left")
						md_file.new_line()

					if method.is_evaluate.get(AnalyseTypes.OBJECT):
						# tableaux des objets
						md_file.new_header(level=3, title="Instanciation des objets")
						table, table_width, table_height = self._getObjectTable(method)
						md_file.new_line()
						md_file.new_table(text=table, columns=table_width, rows=table_height, text_align="left")
						md_file.new_line()


			# créer une table des matières
			md_file.new_table_of_contents(table_title="Table des matières", depth=2)
			# sauvegarder le fichier markdown dans le répertoire qui lui est associé
			md_file.create_md_file()